import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisableKeyboard } from './core/common/disablekeyboard.directive';
import { OrderrByPipe } from './core/common/pipes';

@NgModule({
  imports: [],
  declarations: [DisableKeyboard,OrderrByPipe],
  exports:[DisableKeyboard,OrderrByPipe],
  providers: []
})

export class ShareModule {}
