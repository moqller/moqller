import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialDetailComponent } from './materialdetail.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const MaterialDetailRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: MaterialDetailComponent },
  { path: ':Id', component: MaterialDetailComponent }
]

@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(MaterialDetailRoutes)
  ],
  declarations: [MaterialDetailComponent],
  providers:[DataService,NotificationService]
})
export class MaterialDetailModule { }
