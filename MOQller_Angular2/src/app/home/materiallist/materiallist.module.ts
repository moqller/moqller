import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialListComponent } from './materiallist.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const materialListRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index/:keyword', component: MaterialListComponent },
  { path: 'index/:keyword/:searchfield', component: MaterialListComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(materialListRoutes)
  ],
  declarations: [MaterialListComponent],
  providers:[DataService,NotificationService]
})
export class MaterialListModule { }
