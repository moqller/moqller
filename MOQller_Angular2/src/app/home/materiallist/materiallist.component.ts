import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { SystemConstants } from '../../core/common/system.constants';
import { UtilityService } from '../../core/services/utility.service';
import { CartService } from '../../core/services/cart.service';
@Component({
  selector: 'app-materiallist',
  templateUrl: './materiallist.component.html',
  styleUrls: ['./materiallist.component.css']
})
export class MaterialListComponent implements OnInit, OnDestroy {
  public isLoading: boolean = true;
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  @ViewChild('modalDetail') public modalDetail : ModalDirective;
  public pageIndex: number = 1;
  public pageSize: number = 20;
  public pageDisplay: number = 10;
  public totalRow: number;
  public hiddenpaging = false;
  public filter: string = '';
  public materiallists: any[];
  public materialGroup: any[];
  public entity: any;
  public carts:any;
  public addcart: number[] =[];
  private user: LoggedInUser;
  private subscription: Subscription;
  private searchField: number = 1;
  private userlogin: LoggedInUser;
  private userId: string;
  public items: any[];
  public item: any;
  public RequestQty: number;
  public selectIndex: number;
  public expands: any[];
  public selectIndexExpand: number;

  constructor(private _dataService: DataService, private _notificationService: NotificationService, private activatedRoute: ActivatedRoute, private _http: Http,private _utilityService: UtilityService, private changeDetectRef:ChangeDetectorRef, private _cartservice: CartService,) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.filter = params['keyword'];
      this.searchField = params['searchfield'];
    })
    if(this.searchField != null)
    {
      this.loadDataAdvance();
    }
    else  {
      this.loadData();
    }
    
  }

  loadData() {
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.userlogin == null) 
    {
      this._http.get(SystemConstants.BASE_API + '/api/materiallist/getlistpaging?filter=' + this.filter)
      .subscribe((response: any) => {
        this.isLoading = false;
        this.materiallists = response.json();
        this.materiallists.map(m=>{
          m.RequestQty = parseInt("0", 10);
          m.IsMultiSeller = parseInt("0", 10);
          m.IsDrop =  true;
        });
        
        this.materialGroup = this.fillterMOQ(this.materiallists);
        this.totalRow = this.materialGroup.length;
        if (this.totalRow <= this.pageSize) this.hiddenpaging = true;
      }, error => this._dataService.handleError(error));
    }
    else
    {  
      this._dataService.get('/api/materiallist/getlistpaging?filter=' + this.filter)
        .subscribe((response: any) => {
          this.isLoading = false;
          this.materiallists = response;
          this.materiallists.map(m=>{
            m.RequestQty = parseInt("0", 10);
            m.IsMultiSeller = parseInt("0", 10);
            m.IsDrop =  true;
          });
          
          this.materialGroup = this.fillterMOQ(this.materiallists);
          this.totalRow = this.materialGroup.length;
          if (this.totalRow <= this.pageSize) this.hiddenpaging = true;
        }, error => this._dataService.handleError(error));
    }
  }

  loadDataAdvance() 
  {
    this._dataService.get('/api/materiallist/getlistpagingadvance?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&searchfield=' + this.searchField + '&filter=' + this.filter)
      .subscribe((response: any) => {
        this.materiallists = response.map(m=>{
          m.RequestQty = parseInt("0", 10);
          m.IsMultiSeller = parseInt("0", 10);
          m.IsDrop = true;
        });  
        this.materialGroup = this.fillterMOQ(this.materiallists);      
        this.totalRow = this.materialGroup.length;
        if (this.totalRow <= this.pageSize) this.hiddenpaging = true;
      }, error => this._dataService.handleError(error));
  }

  isMultiSeller(i:any)
  {
    return i == 0 ? false : true;
  }
  

  pageChanged(event: any): void
  {
    this.pageIndex = event.page;
    
  }
 
  saveChange(valid: boolean) 
  {
    if (valid)
    {
      this.materiallists = this.materiallists.map( m=> {
        m.RequestQty = m.Material.PartNumber.trim() == this.entity.Material.PartNumber.trim() ? this.RequestQty : m.RequestQty;
        return m;
      });
      this.modalAddEdit.hide();
     
    }
  }

  deleteItem(id: any) 
  {
    this._notificationService.printConfirmationDialog(MessageContstants.CONFIRM_DELETE_MSG, () => this.deleteItemConfirm(id));
  }

  deleteItemConfirm(id: any) 
  {
    this._dataService.delete('/api/materiallist/delete', 'id', id).subscribe((response: Response) => {
      this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
      this.loadData();
    });
  }

  showDetail(entity: any) 
  {
    
    if (this.entity != undefined && this.entity.Id !== entity.Id)
      this.materiallists.filter(m => m.MaterialListId == this.entity.MaterialListId)
        .map(o => {
          o.IsDrop = true;
        })
    if (entity.IsDrop) 
    {
      this.expands = this.materiallists.filter(m => m.Material.PartNumber.trim() === entity.Material.PartNumber.trim() && m.Id !== entity.Id);

    }

    this.entity = entity;
    return !entity.IsDrop;   
  }

  deleteRowExpand(index: number) 
  {
    this.expands.splice(index, 1)
  }

  ChangeIndex(i:number)
  {
    this.selectIndex = i;
  }

  CheckNumber(event)
  {
    
    return /^\d+$/.test(event) || event == ""  ? event : null;
  }

  isSelectRow(index: number) 
  {
    if (this.selectIndex == undefined) {
      return false;
    }
    else {
      return this.selectIndex == index ? true : false;
    };
  }

  isSelectExpand(index: number) 
  {
    if (this.selectIndexExpand == undefined) {
      return false;
    }
    else {
      return this.selectIndexExpand == index ? true : false;
    };
  }
  
  isOldDate(duedate:Date)
  {
    return new Date(duedate).getTime() < new Date().getTime() ? true : false;
  }

  formatDate(duedate:Date) {
    
    let format = 'DD-MM-YYYY';
    let d= new Date(duedate);
    return d.toLocaleDateString('en-US');
  }

  ChangeRequestQty(event, entity){

    if(entity.IsDrop)
    {
      this.materiallists = this.materiallists.map(m=>{
        m.RequestQty = m.Material.PartNumber.trim() == entity.Material.PartNumber.trim() ? event.target.value : m.RequestQty;
        return m;
      });
    }
    else
    {
      this.materialGroup = this.materialGroup.map(m=>{
        m.RequestQty = m.Material.PartNumber.trim() == entity.Material.PartNumber.trim() ? event.target.value : m.RequestQty;
        return m;
      })
    }

  }

  AddToCart(entity:any){
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.userlogin == null) 
    {
      this._utilityService.navigateToLogin()
      return;
    }
    var buyerId = this.userlogin.id;
    var qty = entity.RequestQty >= entity.AvailableQty ? entity.AvailableQty : entity.RequestQty;
    this.carts = { MaterialListId: entity.Id, Quantity: qty, UnitPrice: entity.Price, ExtendedPrice: entity.Price*entity.AvailableQty, BuyerId: buyerId };
    var updateRequest = entity.RequestQty - qty;
    this._dataService.post('/api/cart/add', JSON.stringify(this.carts))
        .subscribe((response: any) => {
          this._notificationService.printSuccessMessage("Add to cart successful");
          var push = this.addcart.push(entity.Id);
          if(entity.IsDrop)
          {
            this.materiallists = this.materiallists.map(m=>{
              m.RequestQty = m.Material.PartNumber.trim() == entity.Material.PartNumber.trim() ? updateRequest : m.RequestQty;
              m.AvailableQty = m.Id == entity.Id ? m.AvailableQty - qty : m.AvailableQty;
              return m;
            });
          }
          else
          {
            this.materialGroup = this.materialGroup.map(m=>{
              m.RequestQty = m.Material.PartNumber.trim() == entity.Material.PartNumber.trim() ? updateRequest : m.RequestQty;
              m.AvailableQty = m.Id == entity.Id ? m.AvailableQty - qty : m.AvailableQty;
              return m;
            })
          }
          this._cartservice.editCount(response);
        }, error => {
          if(error.status === 409)
          {
            this._notificationService.printErrorMessage(JSON.parse(error._body));
          }
          else
          {
            this._dataService.handleError(error);
          }
        });
     
    }

  addCartEvent(indexRow: number) 
  {
        if (this.addcart == undefined)
          return false;
        else {
          return this.addcart.includes(indexRow) ? true : false;
        }
  }
       
  fillterMOQ(arr: any[]) 
  {
    var arrTemp: any[] = [];
    return arr.reduce((first, curr) => {
      var found = first.find(m => m.Material.PartNumber.trim() === curr.Material.PartNumber.trim());
      if (found != undefined) 
      {

        if (found.MOQ > curr.MOQ) 
        {
          arrTemp.push(curr);
          first = first.map(obj => arrTemp.find(o => o.Material.PartNumber.trim() === obj.Material.PartNumber.trim()) || obj);
        }

        first.filter(m => m.Material.PartNumber.trim() === curr.Material.PartNumber.trim())
          .map(o => {
            o.IsMultiSeller = 1
          });
      }
      else 
      {
        first.push(curr);
      }
      return first;
    }, []);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  hideModal() {
    this.selectIndex = undefined;
    this.items = undefined;
    this.modalDetail.hide();
  }
  IsOdd(i)
  {
    return i%2 ? true: false;
  }
}
