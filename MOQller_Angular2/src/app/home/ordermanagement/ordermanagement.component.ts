import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import { AuthenService } from '../../core/services/authen.service';
import { LoggedInUser } from '../../core/domain/loggedin.user';

import { UrlConstants } from '../../core/common/url.constants';

import { Router } from '@angular/router';
import { SystemConstants } from '../../core/common/system.constants';

@Component({
  selector: 'app-ordermanagement',
  templateUrl: './ordermanagement.component.html',
  styleUrls: ['./ordermanagement.component.css']
})

export class OrderManagementComponent implements OnInit {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  public isLoading: boolean = true;
  public pageIndex: number = 1;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow1: number;
  public pageIndexOrderDetail: number =1;
  public totalRow: number;
  public filter: string = '';
  public orders: any[];
  public entity: any[];
  private userlogin: LoggedInUser;
  public entityDetails: any;
  public billing: any = {};
  public delivery: any = {};
  
  constructor(private _dataService: DataService, 
    private _router: Router,
    public _authenService: AuthenService,
    private _notificationService: NotificationService) { }

  ngOnInit() {
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
     // Kiểm tra nếu không login đuổi
    if (this.userlogin == null) {
      this._router.navigate([UrlConstants.HOME]);
    }
    else
    {
      this.loadData();
    }
  }

  loadData() {
    this._dataService.get('/api/order/getbyorderpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize)
      .subscribe((response: any) => {
        this.orders = response.Items;
        this.totalRow = response.TotalRows;
        this.isLoading = false;
      },error => this._dataService.handleError(error));
  }

  loadOrder(id: any) {
    this._dataService.get('/api/order/getorderone?id='+ id)
      .subscribe((response: any) => {
        this.entity = response.Items;
       });
  }

  loadOrderDetail(id: any) {
    this._dataService.get('/api/order/getorderdetail?id=' + id)
      .subscribe((response: any) => {
        this.entityDetails = response;
        this.totalRow1 = this.entityDetails.length;
      },error=>{
        this._dataService.handleError(error)
      });
  }

  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadData();
  }
  showAddModal() {
    this.entity = [];
    this.modalAddEdit.show();
  }
  showEditModal(id: any) {
    this.loadOrder(id);
    this.loadOrderDetail(id);
    this.modalAddEdit.show();
  }
  
  saveChange(valid: boolean) {
    if (valid) {
      if (this.entity[0].Id == undefined) {
        this._dataService.post('/api/order/add', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.CREATED_OK_MSG);
          }, error => {
            if (error.status==409){
              this._notificationService.printErrorMessage("Code exists");  
            }
            else {
              this._dataService.handleError(error)  
            }
            
          });
      }
      else {
        this._dataService.put('/api/order/update', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
          }, error => this._dataService.handleError(error));
      }
    }
  }

  pageChangedOderdetail(event:any){
    this.pageIndexOrderDetail = event.page;
  }

  CancelItem(id: any, id_order: any) {
    this._notificationService.printConfirmationDialog("Are you sure? </br></br> Warning: Transaction cannot be undue", () => this.ItemCancel(id, id_order));
  }

  ItemCancel(id: any, id_order: any)
  {
    this._dataService.put('/api/order/CancelOrderDetail?Id='+ id)
    .subscribe((response: any) => {
      this.loadOrderDetail(id_order);
      this.modalAddEdit.show();
      this._notificationService.printSuccessMessage('Cancel success');
    }, error => {
      if (error.status==409){
        this._notificationService.printErrorMessage("Cannot cancel because material is delivery");  
      }
      if (error.status == 400)
      {
        this._notificationService.printErrorMessage(JSON.parse(error._body));
      }
      else {
        this._dataService.handleError(error);
      }
    });
  }

  // deleteItemConfirm(id: any) {
  //   this._dataService.delete('/api/order/delete', 'id', id).subscribe((response: Response) => {
  //     this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
  //     this.loadData();
  //   });
  // }

}
