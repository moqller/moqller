import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { UploadService } from '../../core/services/upload.service';
import { AuthenService } from '../../core/services/authen.service';
import { UtilityService } from '../../core/services/utility.service';

import { MessageContstants } from '../../core/common/message.constants';
import { SystemConstants } from '../../core/common/system.constants';

import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { fail } from 'assert';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Router } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})



export class RegisterComponent implements OnInit {
  public entity: any = {};
  public loading = false;
  public _countries: any[];
  public _states: any[];
  public toggleBool: boolean = false;
  private userlogin: LoggedInUser;
  private user: any;
  public showConfirm:boolean=false;
  public countrydefault: number;
 // @ViewChild('country1') default ;
  public defaultCountry: any={};
  constructor(private _dataService: DataService,
    private _notificationService: NotificationService,
    private _utilityService: UtilityService,
    private _router: Router,
    public _authenService: AuthenService, private _http: Http) {

  }
  ngOnInit() {
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.userlogin != null)
    {
      this._router.navigate([UrlConstants.HOME]);
    }
    // this.getListForCountryDropdown();
    // console.log(this.countrydefault);
    // if(this.countrydefault)
    // {
    // this.getListForStateDropdown(this.countrydefault);
    // };
    this._http.get(SystemConstants.BASE_API + '/api/register/getallcountry')
      .toPromise().then((response: Response) => {
        this.entity.CountryId = response.json().find(m => m.Code === 'USA').Id;
        this.countrydefault = this.entity.CountryId;
        this._countries = response.json();
   
      }, error => this._dataService.handleError(error)
    )
    .then(()=>{
      this.getListForStateDropdown(this.countrydefault);
    });
  }

  saveChange(form: NgForm) {
    if (form.valid) {

      if (!this.toggleBool) {
        this._notificationService.printErrorMessage("When you Register, you agree to our User Agreement and acknowledge reading our User Privacy Note.");
        return;
      }
      if (this.checkEmail(this.entity.Email)) {
        this._notificationService.printErrorMessage("Email must be a business email");
        return;
      }
      this.saveData(form);

    }
  }
  public getListForCountryDropdown() {
    this._http.get(SystemConstants.BASE_API + '/api/register/getallcountry')
      .subscribe((response: Response) => {
        this.entity.CountryId = response.json().find(m => m.Code === 'USA').Id;
        this.countrydefault = this.entity.CountryId;
        console.log(this.countrydefault);
        this._countries = response.json();
   
      }, error => this._dataService.handleError(error));
      return;
  }
  public getListForStateDropdown(countryId: any) {
    if (!countryId) {
      this._states = [];
      return;
    }

    this._http.get(SystemConstants.BASE_API + '/api/register/getallstatebycountry?countryId=' + countryId)
      .subscribe((response: Response) => {
        this._states = response.json();
      }, error => this._dataService.handleError(error));
  }

  onCountryChange(newValue) {

    this.getListForStateDropdown(newValue);

  }

  private saveData(form: NgForm) {
    this.loading = true;
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({ headers: headers });
    this._http.post(SystemConstants.BASE_API +'/api/register/add', JSON.stringify(this.entity),options)
      .subscribe((response: Response) => {
        
        form.resetForm();
        this.user = response.json();
        //send mail
        this._http.get(SystemConstants.BASE_API + '/api/register/sendmail?toEmail=' + this.user.Email + '&userName=' + this.user.FirstName + '&id=' + this.user.Id)
          .subscribe((response: Response) => {
            this.showConfirm=true;
            this._notificationService.printSuccessMessage(MessageContstants.REGISTERED_OK_MSG);
          }, error => {
            this._dataService.handleError(error);
            this.loading = false;
          });
        //this._notificationService.printSuccessMessage(MessageContstants.REGISTERED_OK_MSG);
        //this._utilityService.navigateToLogin();
      }, error => {
        if (error.status == 417) {
          this._notificationService.printErrorMessage("Email already exists");
          this.loading = false;
        }
        else if (error.status == 406) {
          this._notificationService.printErrorMessage("User already exists");
          this.loading = false;
        }
        else if (error.status == 404) {
          this._notificationService.printErrorMessage("Please enter Confirm Password to match with Password");
          this.loading = false;
        }
      });
  }
  public checkEmail(email:string)
  {
    if(email !== undefined)
    {
      if(email.includes('@'))
      {
        var splitEmail: any[]=[];
        var checkEmail: any[] = [];
        splitEmail = email.split('@');
        checkEmail = splitEmail[1].split('.');
        if(checkEmail.includes('yahoo', 0) || checkEmail.includes('gmail', 0) || checkEmail.includes('hotmail'))
        {
          return true;
        }
      }
      
      }
      return false;
  }
  
  public changeEvent(event) {
    if (event.target.checked) {
      this.toggleBool = false;
    }
    else {
      this.toggleBool = true;

    }

  }
}
