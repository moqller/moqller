import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegisterComponent} from './register.component';
import { FormsModule } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { DataService } from 'app/core/services/data.service';
export const routes: Routes = [
  //localhost:4200/home/register
  { path: '', component: RegisterComponent }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [AuthenService, NotificationService,DataService],
  declarations: [RegisterComponent]
})
export class RegisterModule { }