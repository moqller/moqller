import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UploadService } from '../../core/services/upload.service';
import { error } from 'util';
import { NotificationService } from '../../core/services/notification.service';
import { SystemConstants } from '../../core/common/system.constants';
import { DataService } from '../../core/services/data.service';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { LoggedInUser } from 'app/core/domain/loggedin.user';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscriber ,  Subscription } from 'rxjs';
import { mainRoutes } from 'app/main/main.routes';
import { UtilityService } from 'app/core/services/utility.service';
import { ShareService } from 'app/core/services/share.service';
@Component({
  selector: 'app-uploadbom',
  templateUrl: './uploadbom.component.html',
  styleUrls: ['./uploadbom.component.css']
})
export class UploadBomComponent implements OnInit {  
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  @ViewChild('modalNameBom') public modalNameBom: ModalDirective;
  public showLoading: boolean;
  public showMapColumn: boolean;
  public  showResult: boolean;
  public  dataError : any;
  public  dataSuccess: any;
  public fileNameBack: string = "";

  public entitys: any[];
  public suggests: any[];
  public entityList:  any[];
   
  public showList: boolean = false;
  public pageIndex: number = 1;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRowLogIn: number;
  public totalRowLogOut: number;
  public hiddenpaging: boolean;
  public index: number;
  public partNumberField: string = "";
  public qtyField: string = "";

  public _partNumbers: any[];
  public _sheets: any[];
  
  public requestQuantity: number = 0;
  public requestQuantityWithMOQ: number = 0;
  public selectBomId: number = 0;
  public showConfirm: boolean = false;
  private user: LoggedInUser;
  private userId: string;
  public fileName: string;

  public iPartNumber: string;
  public iRequestQty: number;
  public iMatchedType: number;
  public _index: number;
  public id: number;
  public checkSeller: boolean;
  public subscription: Subscription;
  public formData: FormData;
  constructor(private _uploadService: UploadService, 
    private _http: Http, 
    private _notificationService: NotificationService, 
    private _dataService: DataService, 
    private _router:Router, 
    private changeDetectRef: ChangeDetectorRef, 
    private _utilityService: UtilityService, 
    private _shareService: ShareService, 
    public _activatedRoute: ActivatedRoute) { }
          
  ngOnInit() 
  {
    this.showLoading = true;
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    this.subscription = this._activatedRoute.params.subscribe(m=>{
      this.fileName = m['file'];
    })
    this.fileName = this.fileName.substring(0, this.fileName.lastIndexOf('.'));
    this._shareService.getContentFile().subscribe(data=>{
      this.formData = data;
    });
    if(this.formData)
    {
      this.LoadData(this.formData);
    }
    else
    {
      this._router.navigate(['/home/index']);
    }
    
    if (this.user != null )
    {
      this.showConfirm = true;     
    }   
    
  }
  
  hideColumn() {
    this.showMapColumn = false;
    this.partNumberField = "";
  }

  LoadData(formData) 
  {
   
      let headers = new Headers();
      let options = new RequestOptions({ headers: headers });      
      this._http.post(SystemConstants.BASE_API + '/api/upload/checkvaliduploadfile?partNumberField=' + this.partNumberField.replace("#", "[[]]") + '&qtyField=' + this.qtyField.replace("#", "[[]]") + '&fileName=' + this.fileNameBack , formData, options)
        .subscribe((data: any) => {
          
          if (data.json() == "") 
          {            
            this.showLoading = false;            
            this._notificationService.printConfirmationDialog("Can not find any data to upload", () => this.GoToHome());
            return;           
          }
         
          if (data.json()[0].Name) 
          {      
            this.fileNameBack = data.json()[0].FileName;     
            this.showLoading = false;
            this.showMapColumn = true;
            this._notificationService.printErrorMessage(data.json()[0].Error);
            this.dataError = data.json();           
          }                  
          else 
          {
            this.showResult = true;
            this.showLoading = false;
            this.showMapColumn = false;
            this.ProcessData(data.json());
          }
        })
    }    

  ProcessData(data: any)
  {
    this.entityList = this.fillterMOQ(data);
    this.entitys = data.filter(m => {
      return m.MatchedType === 1;
    });
    this.suggests = data.filter(m => {
      return m.MatchedType === 0;
    });
    
    this.requestQuantity = this.entitys.reduce((x, y) => {
      return x += y.RequestQty * y.Price
    }, 0)

    this.requestQuantityWithMOQ = this.entitys.reduce((a, b) => {
      return a += Math.max(b.RequestQty, b.MOQ) * b.Price

    }, 0)

    this.totalRowLogOut = this.entitys.length;
    this.totalRowLogIn = this.entityList.length;
    if (this.totalRowLogOut <= this.pageSize) this.hiddenpaging = true;
    this.showList = true;
    
  }

  GoToHome()
  {
    this._router.navigate(['/home/index']);
  }

  pageChanged(event: any): void 
  {
    this.pageIndex = event.page;
    
  }

  isSelectRow(index: number) 
  {
    if (this.index == undefined) {
      return false;
    }
    else {
      return this.index == index ? true : false;
    };
  }

  ChangeIndex(i: number) 
  {
    this.index = i;
  }
  
  CheckNumber(event) 
  {

    return /^\d+$/.test(event) || event == "" ? event : null;
  }

  saveBOM()
  {
    var bom : any={};
    bom.Name = this.fileName;
    bom.UserId = this.user.id;
    
    bom.BomItem = this.entityList.map(m=>{
     
      return {
        PartNumber: m.PartNumber.trim(),
        Quantity: (!isNaN(parseInt(m.RequestQty.toString())) && m.RequestQty.toString().match(/^\d+$/)) ?  m.RequestQty : "0"
      }
    });
    //loại bỏ row trùng
    bom.BomItem = bom.BomItem.filter((BomItem, index, self) =>
    index === self.findIndex((t) => (
    t.PartNumber.trim() === BomItem.PartNumber.trim()
    )))
    
    this._dataService.post('/api/BOM/addbom', JSON.stringify(bom)).subscribe((data: any)=>{
      this._notificationService.printSuccessMessage('Save BOM successfully'); 
      this.goToBomManagement();
     }, error => {
       if(error.status === 409)
        { 
          this.modalNameBom.show();
          this._notificationService.printErrorMessage("Duplicate file name, please change!");
        }
        else if(error.status === 201){
          this._notificationService.printErrorMessage("listbom null");        
        }
        else if(error.status === 403){
          this._notificationService.printErrorMessage(JSON.parse(error._body).Message);
        }
        else if(error.status === 400){
          this._notificationService.printErrorMessage("RequestQty must be number");
        }
     });
    
  }
  
  goToBomManagement()
  {
    this._router.navigate(['/home/bommanagement/index',this.selectBomId]);
  }

  AddToCart()
  {
    this._utilityService.navigateToLogin();
  }

  saveChange() 
  {
    //console.log(this._index);
    var iDuplicate = this.entityList.find((m, index) => m.PartNumber.trim() === this.iPartNumber.trim() && index !== this._index);
    if (iDuplicate) 
    {
      this._notificationService.printErrorMessage("PartNumber exists");
    }
    else 
    {

      //console.log(this.id);
      this._http.get(SystemConstants.BASE_API + '/api/BOM/addrowbom?Id=' + 0 + '&partnumber=' + this.iPartNumber + '&requestQty=' + this.iRequestQty).subscribe((response: any) => {
        var list =[] ;
        list = response.json();
        list = this.fillterMOQ(list);
        if(this.iMatchedType === 1)
        {
          if(list.find(o=>o.MatchedType === 1))
          {
            this.entitys = this.entitys.map((value, index)=> index === this._index ? list.find(o=>o.MatchedType === 1): value );
          }
          else
          {
            this.entitys.splice(this._index, 1);
            this.suggests = this.suggests.concat(list);
          }
        }
        else
        {
          if(list.find(o=>o.MatchedType === 1))
          {
            this.entitys = this.entitys.concat(list);
            this.suggests.splice(this._index, 1);
          }
          else
          {
            this.suggests = this.suggests.map((value, index)=> index === this._index ? list.find(o=>o.MatchedType === 0): value );
          }
        }
       
        this.requestQuantity = this.entitys.reduce((x, y) => {
          return x += y.RequestQty * y.Price
        }, 0);
        //console.log(this.requestQuantity);
        this.requestQuantityWithMOQ = this.entitys.reduce((a, b) => {
          return a += Math.max(b.RequestQty, b.MOQ) * b.Price
        }, 0);
        this.modalAddEdit.hide();
        this.id = 0;
        this.iPartNumber = "";
        this.iRequestQty = 0;
        this._index = undefined;
        this.entityList = this.entitys.concat(this.suggests);
       
      });
    }
  }
  checkPartNumber(partnumber: string, list: any[]){
    if ((list.filter(m => {
      return m.PartNumber.trim() === partnumber.trim();
    })).length > 0)
      return 1;
    return 0;
  }

  fillterMOQ(arr:any[])
  {
    var arrTemp: any[]=[];
       return arr.reduce((first, curr)=>{
          var found = first.find(m => m.PartNumber.trim() === curr.PartNumber.trim());
          if(found != undefined)
          {
              if(found.MOQ > curr.MOQ)
              {
                arrTemp.push(curr);
               first = first.map(obj => arrTemp.find(o=> o.PartNumber.trim() === obj.PartNumber.trim()) || obj);
            
              }
          }
          else
          {
              first.push(curr);
          }
          return first;
      }, []);
  }
 
}
