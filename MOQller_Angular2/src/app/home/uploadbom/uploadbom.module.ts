import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadBomComponent } from './uploadbom.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UploadService } from '../../core/services/upload.service';
const uploadBomRoutes: Routes = [
  
  // { path: '', redirectTo: 'index', pathMatch: 'full' },
 
  { path: 'index/:file', component: UploadBomComponent}
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(uploadBomRoutes)
  ],
  declarations: [UploadBomComponent],
  providers:[DataService,NotificationService,UploadService]
})
export class UploadBomModule { }
