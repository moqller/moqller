import { Component, OnInit, ViewChild } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { NotificationService } from 'app/core/services/notification.service';
import { Router } from '@angular/router';
import { ShareService } from '../../core/services/share.service';

@Component({
  selector: 'app-buyer',
  templateUrl: './buyer.component.html',
  styleUrls: ['./buyer.component.css']
})
export class BuyerComponent implements OnInit {
  @ViewChild('excelFile') file;
  public fileContent: any;
  public redirectToUploadBom : boolean = false;
  public partNumberField : string = "";
  public qtyField : string = "";
  //biến truyền sang uploadbom
  public showLoading: boolean =  false;
  public showMapColumn: boolean = false;
  public showResult: boolean = false;
  public dataError : any;
  public dataSuccess: any;
  public fileName : string = "";
  public keyWord: string = '';

  constructor(private _http: Http, private _notificationService: NotificationService, private _router: Router, private _shareService: ShareService) { }

  ngOnInit() {
  }

  GoToUploadBom()
  {
    this.fileContent = this.file.nativeElement;
    if(this.fileContent.files.length > 0)
    {
      
      this.showLoading = true;
      this.fileName = this.fileContent.files[0].name;
      let formData: FormData = new FormData();
      formData.append('files', this.fileContent.files[0], this.fileContent.files[0].name);
      console.log(formData);
      this._shareService.setContenFile(formData);
      this._router.navigate(['/home/uploadbom/index',this.fileName]);
      
      }
      else {
        this._notificationService.printErrorMessage("please choose file upload")
      }
  }

}
