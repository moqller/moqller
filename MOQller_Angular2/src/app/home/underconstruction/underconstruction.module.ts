import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnderconstructionComponent } from './underconstruction.component';
import { Routes, RouterModule } from '@angular/router';

import { UtilityService } from '../../core/services/utility.service';
import { AuthenService } from '../../core/services/authen.service';
import { SignalrService } from '../../core/services/signalr.service';
import { NotificationService } from '../../core/services/notification.service';
import { AuthGuard } from '../../core/guards/auth.guard';

import { FormsModule } from '@angular/forms';


const UnderconstructionRoutes: Routes = [
  //localhost:4200
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: UnderconstructionComponent }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(UnderconstructionRoutes)
  ],
  declarations: [UnderconstructionComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService]
})
export class UnderconstructionModule { }
