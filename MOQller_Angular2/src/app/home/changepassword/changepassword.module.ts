import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './changepassword.component';
import { FormsModule } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { DataService } from './../../core/services/data.service';

export const routes: Routes = [
  { path: '', component: ChangePasswordComponent }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [ DataService, AuthenService, NotificationService],
  declarations: [ ChangePasswordComponent]
})
export class ChangePasswordModule { }
