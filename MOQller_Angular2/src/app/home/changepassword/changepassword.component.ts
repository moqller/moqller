import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { MessageContstants } from '../../core/common/message.constants';
import { UrlConstants } from '../../core/common/url.constants';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { DataService } from '../../core/services/data.service';
import { UtilityService } from '../../core/services/utility.service';
import { Http, Response } from '@angular/http';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { SystemConstants } from '../../core/common/system.constants';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})

export class ChangePasswordComponent implements OnInit {
  public entity: any = {};
  public user: LoggedInUser;
  public toggleBool: boolean = false;
  public loading = false;
  constructor(private _dataService: DataService,
    private _notificationService: NotificationService,
    private _utilityService: UtilityService,
    private _router: Router,
    public _authenService: AuthenService, private _http: Http) {
  }
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    // kiểm tra nếu ko có user ra home
    if (this.user == null)
    {
      this._router.navigate([UrlConstants.HOME]);
    }
  }
  saveChange(form: NgForm)  {
    if (form.valid) {
      this.loading = true;
      this.entity.UserName = this.user.username;
      this._dataService.put('/api/appUser/ChangePassword/', JSON.stringify(this.entity)).subscribe((response: Response) => {
        this._authenService.logout();
        this._router.navigate([UrlConstants.LOGIN]);
        this._notificationService.printSuccessMessage("Your password has been changed successfully");
      }, error => {
        if (error.status==404){
          this._notificationService.printErrorMessage("The old password has invalid");  
          this.loading = false;
        }
        else {
          this._dataService.handleError(error);
          this.loading = false;
        }
      });
    }
  }
}
