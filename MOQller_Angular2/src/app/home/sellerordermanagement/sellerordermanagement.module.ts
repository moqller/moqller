import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SellerOrderManagementComponent } from './sellerordermanagement.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const SellerOrderManagementRoutes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: SellerOrderManagementComponent },
  { path: ':deliverystatus', component: SellerOrderManagementComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(SellerOrderManagementRoutes)
  ],
  declarations: [SellerOrderManagementComponent],
  providers:[DataService,NotificationService]
})
export class SellerOrderManagementModule { }
