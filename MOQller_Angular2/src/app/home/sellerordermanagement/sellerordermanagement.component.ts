import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import { AuthenService } from '../../core/services/authen.service';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { UrlConstants } from '../../core/common/url.constants';
import { Router } from '@angular/router';
import { SystemConstants } from '../../core/common/system.constants';
import { ShareService } from '../../core/services/share.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sellerordermanagement',
  templateUrl: './sellerordermanagement.component.html',
  styleUrls: ['./sellerordermanagement.component.css']
})

export class SellerOrderManagementComponent implements OnInit {
  
  public pageIndex: number = 1;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow: number;
  private userlogin: LoggedInUser;
  public entityDetails: any[];
  public checkSeller: boolean;
  public subscription: Subscription;
  public filter: string = "";
  public listdelivery: any[];
  public isLoading: boolean = true;
  public titleByDelivery : string ="";
  
  constructor(private _dataService: DataService, 
    private _router: Router,
    public _authenService: AuthenService,
    private _notificationService: NotificationService,
    private _shareService: ShareService,
    private _activeRoute: ActivatedRoute) {
      this._shareService.getSeller().subscribe( m => {
        this.checkSeller = m});
     }
    

  ngOnInit() {
    this.subscription = this._activeRoute.params.subscribe(m=>{
      this.filter = m["deliverystatus"];
    });

    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
     // Kiểm tra nếu không login đuổi
    if (this.userlogin == null || !this.checkSeller) {
      this._router.navigate([UrlConstants.HOME]);
    }
    else
    {
      this.filter =  this.filter == undefined ? '' : this.filter;
      this.loadData();
    }
  }

  loadData() {
    this.isLoading = true;
    if (this.filter == 'DEF')
    {
      this.titleByDelivery = ' - Ready to ship';
    }
    else if (this.filter == 'OTW')
    {
      this.titleByDelivery = ' - In Transit';
    }

    this._dataService.get('/api/DeliveryStatus/getall')
     .toPromise().then((response: any) => {
       this.listdelivery = response;
     },error => this._dataService.handleError(error)).then(() => {
        this._dataService.get('/api/order/getbysellerorderpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
          .toPromise().then((response: any) => {
            this.entityDetails = this.rewwrite(response.Items);// Rewrite lại để định dạng
            this.totalRow = response.TotalRows;
            this.isLoading = false;
          },error => this._dataService.handleError(error))
      });
  }

  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadData();
  }

  CancelItem(id: any, id_order: any) {
    this._notificationService.printConfirmationDialog("Are you sure? </br></br> Warning: Transaction cannot be undue", () => this.ItemCancel(id, id_order));
  }

  ItemCancel(id: any, id_order: any)
  {
    this._dataService.put('/api/order/CancelOrderDetail?Id='+ id)
    .subscribe((response: any) => {
      this.loadData();
      
      this._notificationService.printSuccessMessage('Cancel success');
    }, error => {
      if (error.status==409){
        this._notificationService.printErrorMessage("Cannot cancel because material is delivery");  
      }
      if (error.status == 400)
      {
        this._notificationService.printErrorMessage(JSON.parse(error._body));
      }
      else {
        this._dataService.handleError(error);
      }
    });
  }

  DeliveryStatusChange(DeliveryStautsId: any, item: any)
  {
    this._dataService.put('/api/order/UpdateDeliveredStatus?OrderDetailId='+ item.Id + '&DeliveryStatusId='+ DeliveryStautsId)
    .subscribe((response: any) => {
      this.loadData();
    });
  }

  rewwrite(arr){
    arr = arr.map(m => { //thêm thuộc tính iIndex ảo vào obj, 
      m.iIndex = 0;
      return m;
    });
    let arr1 = arr.reduce((first, curr)=>{ // trả về arr có iIndex của các row cùng order sẽ giống nhau
      var found = first.find(m => m.OrderId === curr.OrderId);
      if(found != undefined)
      {     
            curr.iIndex = found.iIndex;   
      }
      else
      {
        if(first.length !== 0)
          curr.iIndex = first[first.length - 1].iIndex + 1;
        else
        {
            curr.iIndex = 1;
        }
          
      }
      first.push(curr);
      return first;
    }, []);
    return arr1;
  }

  IsOdd(i){
   return i%2 ? true : false;
  }
}
