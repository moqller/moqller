import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { DataService } from '../../core/services/data.service';
import { Routes, RouterModule } from '@angular/router';

import { UtilityService } from '../../core/services/utility.service';
import { AuthenService } from '../../core/services/authen.service';
import { SignalrService } from '../../core/services/signalr.service';
import { NotificationService } from '../../core/services/notification.service';
import { AuthGuard } from '../../core/guards/auth.guard';

import { FormsModule } from '@angular/forms';
import { Daterangepicker } from 'ng2-daterangepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxBraintreeModule } from 'ngx-braintree';
import { HttpClientModule } from '@angular/common/http';

const CartRoutes: Routes = [
  //localhost:4200
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: CartComponent }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Daterangepicker,
    ModalModule.forRoot(),
    RouterModule.forChild(CartRoutes),
    NgxBraintreeModule,
    HttpClientModule,
    PaginationModule
  ],
  declarations: [CartComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService, DataService]
})
export class CartModule { }
