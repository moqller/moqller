import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { UtilityService } from '../../core/services/utility.service';

import { MessageContstants } from '../../core/common/message.constants';
import { SystemConstants } from '../../core/common/system.constants';

import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Router } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';
import { CookieService } from 'ngx-cookie';
import { CartService } from '../../core/services/cart.service';
import { Observable, of } from 'rxjs';
import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-Cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
    public shippingMethodId: any = undefined;

    public loading = false;
    public carts:any;
    public shippingMethods: any[] = [];
    public toggleBool: boolean = false;
    private userlogin: LoggedInUser;
    public baseFolder: string = SystemConstants.BASE_API;

    public TotalQuantity: number = 0;
    public SubTotal: number = 0;
    public TotalTax: number = 0;
    public OrderFee: number = 0;
    public ShippingFee: number = 0;
    public entity: any = {};
    public billing: any = {};
    public delivery: any = {};
    public ViewCart: boolean = true;
    public ViewBilling: boolean = true;
    public ViewConfirm: boolean = false;
    public IsShowDilivery: boolean = false;
    public _Adresss: any = [];
    public IsSaveBilling: boolean = false;
    public IsSaveDelivery: boolean = true;
    public TotalItems: number = 0;

    public Total: number = 0;
    public AdresssBillingId: number = 0;
    public AdresssDeliveryId:any;

    public _countries: any = [];
    public _states: any = [];
    public _DlStates: any = [];
    public shippingMethodsDescription: string ='';
    public bomId: string;
    public messagePay: string="Your order is sent to ";

    public ViewPayment: boolean = false;
    private httpheader : HttpHeaders;
    private CartForm: NgForm;

    constructor(private _dataService: DataService,
        private _notificationService: NotificationService,
        private _utilityService: UtilityService,
        private _router: Router,
        public _authenService: AuthenService,
        private _http: Http,
        private _httpclient: HttpClient,
        private _cartservice: CartService,
        private _cookieService: CookieService) {

    }

    ngOnInit() {
        this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));

        // Kiểm tra nếu không login đuổi
        if (this.userlogin == null) {
            this._router.navigate([UrlConstants.HOME]);
        }
        this.httpheader = new HttpHeaders({ 'Content-Type': 'application/json', "Authorization": "Bearer " + this.userlogin.access_token });

        this.LoadCart();
        this.LoadShippingMethod();
        this.LoadCountry();
        this.ViewCart = true;
        this.ViewBilling = false;
        this.ViewConfirm = false;
        this.LoadBillingAddress(0);
    }
  
    onBlurQuantity(NumberChage, CartId, oldvalue){
        if(NumberChage > 0)
        {
            if( NumberChage != oldvalue)
            {
                this._dataService.put('/api/cart/updateqty?CartId='+ CartId +'&Quantity=' + NumberChage)
                .subscribe((response: any) => {
                    this.LoadCart();
                    this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
                }, error => { 
                    if (error.status === 409)
                    {
                        this._notificationService.printErrorMessage(JSON.parse(error._body));
                        this.LoadCart();
                    } 
                    else
                    {
                        this._dataService.handleError(error);
                        this.LoadCart();
                    }
                });
            }
        }
        else{
            this._notificationService.printErrorMessage('The quantity must be a valid number greater than 0 ');
        }
    }

    onBillingAddressChange(e){
       if(e)
       {
            this.billing = this._Adresss.find(x => x.Id === e);
            this.IsSaveBilling = false;
       }
       else
       {
            //Add New
            this.billing = {};
            this._states =[];
            this.IsSaveBilling = true;
       }
    }

    onDeliveryAddressChange(e){
        if(e)
        {
            this.delivery = this._Adresss.find(x => x.Id === e);
            this.IsSaveDelivery = false;
        }
        else
        {
            //Add New
            this.delivery = {};
            this._DlStates = [];
            this.IsSaveDelivery = true;
        }
    }

    // showEditModal(id: any) {
    //     this.LoadCartDetail(id);
    //     this.modalAddEdit.show();
    // }

    LoadCartDetail(id: any) {
        this._dataService.get('/api/cart/detail/' + id)
            .subscribe((response: any) => {
                this.entity = response;
            });
    }

    LoadBillingAddress(id: any){
        this._dataService.get('/api/address/getbyUsrId/'+ this.userlogin.id)
        .subscribe((response: any) => {
            if (response.length > 0)
            {
                this.IsSaveBilling = false;
                this._Adresss = response;
                if (id == 0)
                {
                    this.billing = this._Adresss[0];
                    this.onCountryChange(this._Adresss[0].CountryId);
                    this.AdresssBillingId = this._Adresss[0].Id;
                }
                else{
                    this.billing = this._Adresss.find(x => x.Id === id);
                    this.onCountryChange(this.billing.CountryId);
                    this.AdresssBillingId = id;
                }
            }
            else
            {
                this.loadUserDetail(this.userlogin.id);
                this.IsSaveBilling = true;
            }
        }, error => this._dataService.handleError(error));
    }

    LoadDeliveryAddress(id: any){
        this._dataService.get('/api/address/getbyUsrId/'+ this.userlogin.id)
        .subscribe((response: any) => {
            if (response.length > 0)
            {
                this._Adresss = response;
                if (id == 0){
                    this.delivery = {};
                }
                else{
                    this.delivery = this._Adresss.find(x => x.Id === id);
                    this.onCountryChange(this.delivery.CountryId);
                    this.AdresssDeliveryId = id;
                }
            }
        }, error => this._dataService.handleError(error));
    }


    LoadShippingMethod() {
        this._dataService.get('/api/ShippingMethod/getall')
            .subscribe((response: any) => {
                this.shippingMethods = response;
            }, error => this._dataService.handleError(error));
    }

    LoadCart() {
        this._dataService.get('/api/cart/getlistpaging')
            .subscribe((response: any) => {
                this.carts = response.Items;

                this.TotalQuantity = this.carts.reduce((x, y) => {
                    return x += y.Quantity;
                }, 0);

                this.SubTotal = this.carts.reduce((x, y) => {
                    return x += y.ExtendedPrice;
                }, 0);

                this.OrderFee = 0.1 * this.SubTotal;
                this.Total = this.SubTotal + this.ShippingFee + this.OrderFee;
                
                this.TotalItems = response.CountByPartNumber;

            }, error => this._dataService.handleError(error));
    }

    loadUserDetail(id: any) {
        this._dataService.get('/api/appUser/detail/' + id)
            .subscribe((response: any) => {
               this.billing = response;
               this.billing.Id = 0;
               this.billing.Address1 = this.billing.Address;
            });
    }

    LoadCountry(){
        this._dataService.get('/api/country/getall')
        .subscribe((response: any) => {
            this._countries = response;
          }, error => this._dataService.handleError(error));
    }

    onCountryChange(countryId: any) {
        if (!countryId) {
          this._states = [];
          return;
        }

        this._dataService.get('/api/register/getallstatebycountry?countryId=' + countryId)
          .subscribe((response: Response) => {
            this._states = response;
          }, error => this._dataService.handleError(error));
    }

    onCountryDlChange(countryId: any){
        if (!countryId) {
            this._DlStates = [];
            return;
          }

          this._dataService.get('/api/register/getallstatebycountry?countryId=' + countryId)
            .subscribe((response: Response) => {
              this._DlStates = response;
            }, error => this._dataService.handleError(error));
    }


    AddBilling() {
        if (this.carts.length > 0)
        {
            this.ViewBilling = true;
            this.ViewCart = false;
        }
        else
        {
            this._notificationService.printErrorMessage('Cart is empty');
        }
    }


    ShowDelivery(e){
        this.IsShowDilivery = !(e.target.checked);
        this.LoadDeliveryAddress(0);
        
    }

    Pay(form: NgForm) {        
        // Kiểm tra form
        if (form.valid)
        {
            // Kiểm tra xem có sản phẩm ko
            if (this.carts.length != 0)
            {
                // Kiểm tra xem địa chỉ thanh toán và vận chuyển giống nhau ko
                if (this.IsShowDilivery)
                {
                    // Nếu ko trùng nhau thì kiểm tra Id billing
                    if (this.AdresssBillingId > 0)
                    {
                        // Lưu dữ liệu dù ko thay đổi đảm bảo thông tin là mới nhất
                        this._dataService.put('/api/address/update', JSON.stringify(this.billing))
                        .toPromise().then((response: any) => {
                            this.AdresssBillingId = response.Id;
                        }, error => {this._dataService.handleError(error);}).then(() => {
                            // Kiểm tra địa chỉ vận chuyển
                            if (this.AdresssDeliveryId > 0)
                            {
                                // Nếu 2 Id vận chuyển và thanh toán giống nhau bắt thêm mới
                                if (this.AdresssBillingId == this.AdresssDeliveryId)
                                {
                                    this._notificationService.printErrorMessage('Cannot change adress. Please add new address or chose other address');
                                }
                                else
                                {
                                    this.entity.BillingInfoId = this.AdresssBillingId;
                                    this.entity.DeliveryInfoId = this.AdresssDeliveryId;
                                    this.CheckQuantityInStock(form);

                                }
                            }
                            // Thêm mới địa chỉ 
                            else{
                                this.delivery.UserId = this.userlogin.id;
                                this._dataService.post('/api/address/add', JSON.stringify(this.delivery))
                                .toPromise().then((response: any) => {
                                    this.AdresssDeliveryId = response.Id;
                                }, error => { this._dataService.handleError(error); }).then(() => {
                                    this.entity.DeliveryInfoId = this.AdresssDeliveryId;
                                    this.entity.BillingInfoId = this.AdresssBillingId;
                                    this.CheckQuantityInStock(form);
                                });
                            }
                        });
                    }
                    // Nếu Id vận chuyển bằng 0 tức thêm mới
                    else{
                        this.billing.UserId = this.userlogin.id;
                        this._dataService.post('/api/address/add', JSON.stringify(this.billing))
                        .toPromise().then((response: any) => {
                            this.LoadBillingAddress(response.Id);
                        }, error => { this._dataService.handleError(error); }).then(() => {
                            if (this.AdresssDeliveryId > 0)
                            {
                                // Nếu 2 Id vận chuyển và thanh toán giống nhau bắt thêm mới
                                if (this.AdresssBillingId == this.AdresssDeliveryId)
                                {
                                    this._notificationService.printErrorMessage('Cannot change adress. Please add new address or chose other address');
                                }
                                else
                                {
                                    this.entity.BillingInfoId = this.AdresssBillingId;
                                    this.entity.DeliveryInfoId = this.AdresssDeliveryId;
                                    this.CheckQuantityInStock(form);
                                }
                            }
                            // Thêm mới địa chỉ 
                            else{
                                this.delivery.UserId = this.userlogin.id;
                                this._dataService.post('/api/address/add', JSON.stringify(this.delivery))
                                .toPromise().then((response: any) => {
                                    this.AdresssDeliveryId = response.Id;
                                }, error => { this._dataService.handleError(error); }).then(() => {
                                    this.entity.DeliveryInfoId = this.AdresssDeliveryId;
                                    this.entity.BillingInfoId = this.AdresssBillingId;
                                    this.CheckQuantityInStock(form);
                                });
                            }
                        });
                    }
                }
                // trùng địa chỉ thanh toán và vận chuyển
                else
                {
                    if (this.AdresssBillingId > 0)
                    {
                        // Lưu dữ liệu dù ko thay đổi đảm bảo thông tin là mới nhất
                        this._dataService.put('/api/address/update', JSON.stringify(this.billing))
                        .toPromise().then((response: any) => {
                            this.AdresssBillingId = response.Id;
                        }, error => {this._dataService.handleError(error);})
                        .then(() => {
                            this.entity.BillingInfoId = this.entity.DeliveryInfoId = this.AdresssBillingId;
                            this.CheckQuantityInStock(form);
                        });
                    }
                    else
                    {
                        this.billing.UserId = this.userlogin.id;
                        this._dataService.post('/api/address/add', JSON.stringify(this.billing))
                        .toPromise().then((response: any) => {
                            this.AdresssBillingId = response.Id;
                        }, error => { this._dataService.handleError(error);})
                        .then(()=>
                        {
                            this.entity.BillingInfoId = this.entity.DeliveryInfoId = this.AdresssBillingId;
                            this.CheckQuantityInStock(form);
                        });
                    }
                }
            }
            else
            {
                this._notificationService.printErrorMessage('Cart is empty');
            }
        }
        this._cartservice.CallCount();
    }

    // Gọi hàm check quantity trước khi pay
    private CheckQuantityInStock(form: NgForm)
    {
        this._dataService.put('/api/cart/CheckQtyByPay')
            .toPromise().then((response: Response) => {
                this.ViewBilling = false;
                this.ViewPayment = true;
                this.CartForm = form;
            }, error => { this._dataService.handleError(error);});
    }

    private saveOrder(form: NgForm)
    {
        if (form.valid)
        {
            // Kiểm tra xem có sản phẩm ko
            if (this.carts.length != 0)
            {
                debugger;
                this.entity.BuyerId = this.userlogin.id;
                this.entity.TotalQuantity = this.TotalQuantity;
                this.entity.SubTotal = this.SubTotal;
                this.entity.TotalSellerFee = 0;
                this.entity.TotalBuyerFee = 0;
                this.entity.TransactionAmount = this.OrderFee;
                this.entity.Total = this.Total;
                // inser OrderStatusId vao du leiu
                //this.entity.OrderStatusId = 1;
                this.entity.ShippingMethodId = this.shippingMethodId;
                
                this.entity.OrderDetails = this.carts;
                this._dataService.post('/api/Order/add', JSON.stringify(this.entity))
                .toPromise().then((response: any) => {
                    this.messagePay = "Your order is sent to " + response;
                    // this._notificationService.printSuccessMessage(MessageContstants.CREATED_OK_MSG);
                    this.ViewBilling = false;
                    this.ViewConfirm = true;
                    this._cartservice.CallCount();
                }, error => { 
                    if (error.status === 409)
                    {
                        this.ViewBilling = false;
                        this.ViewCart = true;
                        this._notificationService.printErrorMessage("Quantity large or AvailableQty is empty");
                    } 
                    if (error.status === 411)
                    {
                        this.ViewBilling = false;
                        this.ViewCart = true;
                        this._notificationService.printErrorMessage('Cart is empty');
                    }
                    if(error.status === 410)
                    {
                        this._notificationService.printErrorMessage(JSON.parse(error._body).Message);
                    }
                    else
                    {
                        this._dataService.handleError(error);
                    }
                });
            }
            else
            {
                this._notificationService.printErrorMessage('Cart is empty');
            }
        }
    }

    onShippingMethodChange(newValue) {
        this._dataService.get('/api/ShippingMethod/detail/' + newValue)
            .subscribe((response: any) => {
                this.ShippingFee = response.ShippingFee;
                this.shippingMethodsDescription = response.Description ;
                this.Total = this.SubTotal + this.ShippingFee + this.OrderFee
            });
    }

    BacktoCart(){
        this.ViewBilling = false;
        this.ViewCart = true;
    }

    deleteItem(id: any) {
        this._notificationService.printConfirmationDialog(MessageContstants.CONFIRM_DELETE_MSG, () => this.deleteItemConfirm(id));
    }

    deleteItemConfirm(id: any) {
        this._dataService.delete('/api/cart/delete', 'id', id).subscribe((response: Response) => {
            this._cartservice.CallCount();
            this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
            this.LoadCart();
        });
    }
   
    goBackToBom()
    {
        if(this._cookieService.get('bomId'))
        {
            this.bomId = this._cookieService.get('bomId')
            this._router.navigate(['/home/bommanagement/index',this.bomId])
        }
        else
        {
            this._router.navigate(['/home/bommanagement/index',0]);
        }
    }


    // -------------------- Payment from braintreegateway

    getClientTokenFC(): Observable<string> {
        return this._httpclient.get(SystemConstants.BASE_API + '/api/checkout/getclienttoken', { 
            'headers': this.httpheader
          }).pipe(
            map((response: any) => {
                return response.token;
            })
        );
    }

    createPurchaseFC(nonce: string, chargeAmount: number): Observable<any> {
        return this._httpclient
        .post(SystemConstants.BASE_API + '/api/checkout/createpurchase', { nonce: nonce, chargeAmount: this.Total }, { 'headers': this.httpheader })
        .pipe(map((response: any) => {
            if (!response.Errors)
            {
                console.log(response);
                this.saveOrder(this.CartForm);
                this._cartservice.CallCount();
                return response;
            }
        }));
    }

}