import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { MessageContstants } from '../../core/common/message.constants';
import { UrlConstants } from '../../core/common/url.constants';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { SystemConstants } from '../../core/common/system.constants';
import { CartService } from '../../core/services/cart.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private roles: any [];
  loading = false;
  model: any = {};
  returnUrl: string;
  constructor(private authenService: AuthenService,
    private notificationService: NotificationService,
    private _cartservice: CartService,
    private router: Router,
    private _location: Location) {}

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.authenService.login(this.model.username, this.model.password).subscribe(data => {
      this._cartservice.CallCount();
      this.roles = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER)).roles;
      if (this.roles.indexOf("DataEntry") != -1)
      {
        this.router.navigate([UrlConstants.DATAENTRY]);
      }
      else{
        this.router.navigate([UrlConstants.HOME]);
        this._cartservice.CallCount();
      }
    }, error => {
      this.notificationService.printErrorMessage(error.json().error_description);
      this.loading = false;
    });
  }
}
