import { Component, OnInit, ViewChild, Input, DoCheck, OnChanges, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { UploadService } from '../../core/services/upload.service';
import { AuthenService } from '../../core/services/authen.service';
import { UtilityService } from '../../core/services/utility.service';

import { MessageContstants } from '../../core/common/message.constants';
import { SystemConstants } from '../../core/common/system.constants';

import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { fail } from 'assert';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Router, Routes } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';
import { LoginComponent } from '../login/login.component';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { CartService } from '../../core/services/cart.service';
import { ShareService } from 'app/core/services/share.service';


@Component({
  selector: 'app-bommanagement',
  templateUrl: './BOMManagement.component.html',
  styleUrls: ['./BOMManagement.component.css']
})

export class BOMManagementComponent implements OnInit, OnDestroy {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  @ViewChild('modalNameBom') public modalNameBom: ModalDirective;
  @ViewChild('excelFile') file;
  public fileContent: any;
  public fileName : string = "";
  
  public id: number = 0;
  public requestQty: number = 0;
  public partNumber: string = "";
  public _index: number;
  public matchedType;
  public entityList: any[];
  public resultSearch: any[];
  public entitys: any[];
  public entitysTemp: any[];
  public carts: any;
  public entity: any;
  public entityExist: any[];
  public suggests: any[];

  public toggleBool: boolean = false;
  public bomList: any[];
  public bomName: string;
  public bomItem: any[];
  public selectBomId: number = 0;
  private userlogin: LoggedInUser;
  private userId: string;
  public isShown: boolean = false;
  public requestQuantity: number;
  public requestQuantityWithMOQ: number;
  public baseFolder: string = SystemConstants.BASE_API;
  //biến đổi màu row
  public items: any[];
  public item: any;
  public selectIndex: number;
  public selectIndexExpand: number;
  public addCart: number[] = [];
  //biến sắp xếp
  public column: string = "PartNumber";
  public isDesc: boolean = false;
  public direction: number = -1;

  //biến phân trang
  public pageIndex: number = 1;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow1: number;
  //biến router params
  public subscription: Subscription;
  //biến add all item
  public successCart: any[];
  public warningCart: any[];
  public errorCart: any[];
  public successQty: number = 0;
  public warningQty: number = 0;
  public errorQty: number = 0;
  public countClick: number = 0;
  public isLoading: boolean = false;

  public expands: any[];
  constructor(private _dataService: DataService,
    private _notificationService: NotificationService,
    private _utilityService: UtilityService,
    private _router: Router,
    private _cartservice: CartService,
    public _authenService: AuthenService, private _http: Http,
    public _changeDetectRef: ChangeDetectorRef,
    public _activatedRoute: ActivatedRoute,// lan sau cho nay khai bao cho chuan nhe ko sau nhieu ko biết thằng nào la thằng tiêm vào đâu
    private _cookieService: CookieService,
    private _shareService : ShareService
  ) {}

  ngOnInit() {
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.userlogin == null) {
      this._utilityService.navigateToLogin();
      return;
    };

    this.subscription = this._activatedRoute.params.subscribe(params => {
      this.selectBomId = Number(params['selectBomId']);
      this.countClick = 0;
      //console.log(this.selectBomId);
      if (this.selectBomId !== 0) {
        this.loadDataId(this.selectBomId);
      }
      else {
        this.loadData();
      }
    });

  }

  loadData() {
    this._dataService.get('/api/BOM/userid').subscribe((response: any) => {
      this.bomList = response;
    }, error => this._dataService.handleError(error));
    this.selectBomId = 0;
    this.entitys = [];
    //console.log(this.entitys);
    this.suggests = [];
  }

  loadDataId(bomId: number) {
    this.isLoading = true;
    this.addCart = [];
    this.errorCart = [];
    this.successCart = [];
    this.warningCart = [];
    this.isShown = false;
    //console.log(this.isLoading);
    this._dataService.get('/api/BOM/userid')
      .toPromise().then((response: any) => {
        this.bomList = response;
      }, error => this._dataService.handleError(error))
      .then(() => {
        this.bomName = this.bomList.find(m => m.Id === bomId).Name;
      })
      .then(() => {
        this.isLoading = false;
        this.getBOMItem(bomId);
      })

  }

  getBOMItem(bomId: number) 
  {
    // set cookie nếu muốn thêm thời gian hết hạn của cookie thì đọc thêm tẹo gủi link cho
    this._cookieService.put('bomId', bomId.toString());
    this._shareService.setBomId();
    // lay giá trị
    //console.log(this._cookieService.get('bomId'));
    this._dataService.get('/api/BOMItem/search?bomid=' + bomId).subscribe((response: any) => {
      this.entityList = response;
      this.entityList.map(m => {
        m.IsMultiSeller = parseInt("0", 10);
        m.IsDrop = true;
      });

      this.suggests = this.entityList.filter(m => {
        return m.MatchedType === 0;
      });
      this.entitysTemp = this.entityList.filter(m => {
        return m.MatchedType === 1;
      });

      this.entitys = this.fillterMOQ(this.entitysTemp);
      this.totalRow1 = this.entitys.length;

      this.requestQuantity = this.entitys.reduce((x, y) => {
        return x += y.RequestQty * y.Price
      }, 0);

      this.requestQuantityWithMOQ = this.entitys.reduce((a, b) => {
        return a += Math.max(b.RequestQty, b.MOQ) * b.Price
      }, 0)
    }
      , error => this._dataService.handleError(error)
    )

  }

  onSheetChange(event: any) 
  {
    this.selectBomId = event;
  }

  deleteBom(id: any) {
    if (document.body.className == "nav-sm") {
      $('BODY').toggleClass('nav-md nav-sm');
    }
    if(id !== 0)
    this._notificationService.printConfirmationDialog("Do you want to delete this BOM?", () => this.deleteItemConfirm(id));
  }

  deleteItemConfirm(id: any) 
  {
    this._dataService.delete('/api/BOM/delete', 'id', id).toPromise().then(() => {
      this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG)
    })
      .then(() => {
        this.loadData();
        this._router.navigate(['/home/bommanagement/index',0]);
      });

  }

  uploadNewBom() 
  {
    this.fileContent = this.file.nativeElement;
    if(this.fileContent.files.length > 0)
    {
      
      //this.showLoading = true;
      this.fileName = this.fileContent.files[0].name;
      let formData: FormData = new FormData();
      formData.append('files', this.fileContent.files[0], this.fileContent.files[0].name);
      console.log(formData);
      this._shareService.setContenFile(formData);
      this._router.navigate(['/home/uploadbom/index',this.fileName]);
      
      }
      else {
        this._notificationService.printErrorMessage("please choose file upload")
      }
  }

  deleteRowExpand(index: number) {
    this.expands.splice(index, 1)
  }
  deleteRow(id: any, entity: any) {
    if (document.body.className == "nav-sm") {
      $('BODY').toggleClass('nav-md nav-sm');
    }

    this._notificationService.printConfirmationDialog(MessageContstants.CONFIRM_DELETE_MSG, () => this.deleteRowConfirm(id, entity));
  }

  deleteRowConfirm(index: number, entity: any) {

    if (entity.MatchedType === 1) {

      this.entitys.splice(index, 1);
      this.totalRow1 = this.entitys.length;

    }
    else {

      this.suggests.splice(index, 1);
    };
    this.requestQuantity = this.entitys.reduce((x, y) => {
      return x += y.RequestQty * y.Price
    }, 0);

    this.requestQuantityWithMOQ = this.entitys.reduce((a, b) => {
      return a += Math.max(b.RequestQty, b.MOQ) * b.Price
    }, 0);
    this.entityList = this.entityList.filter(m => m.PartNumber !== entity.PartNumber);
    //console.log(this.entityList);

  }

  showConfirmChangeName(bomId: any) 
  {
    this._notificationService.printConfirmationDialogFull("Do you want to change BOM name??", () => this.showChangeName(), () => this.saveBOM(bomId));
  }

  saveBOM(bomId: any) 
  {
    this.entityList = this.entitys.concat(this.suggests);
    this._dataService.post('/api/BOM/updatebom/' + bomId, JSON.stringify(this.entityList)).toPromise().then((data: any) => {
      this._notificationService.printSuccessMessage('Update BOM successfully');

    }).then(() => {

      this._router.navigate(['/home/bommanagement/index',0]);
    });

  }

  showChangeName() 
  {
    this.modalNameBom.show();
  }

  saveBOMChangeName(valid: Boolean) 
  {
    if (valid) 
    {
      this.entityList = this.entitys.concat(this.suggests);
      var bom : any={};
      bom.Id = this.selectBomId;
      bom.Name = this.bomName;
      bom.UserId = this.userlogin.id;   
      bom.BomItem = this.entityList.map(m=>{
      
        return {
          PartNumber: m.PartNumber.trim(),
          Quantity: m.RequestQty
        }
      });
    
      this._dataService.post('/api/BOM/updatebom', JSON.stringify(bom)).toPromise().then((data: any) => {
        this._notificationService.printSuccessMessage('Update BOM successfully');
 
      }, error => {

        if (error.status === 409) {
          this.modalNameBom.show();
          this._notificationService.printErrorMessage("BOM name exist.Please change name!");
        }

      }).then(() => {
        this.modalNameBom.hide();
        this._router.navigate(['/home/bommanagement/index',0]);
      });
    }

  }

  showAddModal() 
  {
    if (document.body.className == "nav-sm") {
      $('BODY').toggleClass('nav-md nav-sm');
    }

    this._index = undefined;
    this.id = 0;
    this.partNumber = "";
    this.requestQty = 0;
    this.modalAddEdit.show();
  }

  saveChange(valid: Boolean) 
  {

    if (valid) {

      var entityExist = this.entityList.filter(m => {
        return m.PartNumber.trim() === this.partNumber.trim();
      });
      if (entityExist.length > 0) {
        this._notificationService.printErrorMessage("PartNumber exists")
      }
      else {

        this._dataService.get('/api/BOM/addrowbom?id=' + this.id + '&partnumber=' + this.partNumber + '&requestQty=' + this.requestQty + '&buyerId=' + this.userlogin.id).subscribe((response: any) => {
          var list = [];
          list = response;
          list.map(m => {
            m.IsMultiSeller = parseInt("0", 10);
            m.IsDrop = true;
          });
          this.entityList.concat(list);
          list = this.fillterMOQ(list);
          this.entitys = this.entitys.concat(list.filter(m => {
            return m.MatchedType === 1
          }));
          this.totalRow1 = this.entitys.length;
          this.suggests = this.suggests.concat(list.filter(m => {
            return m.MatchedType === 0
          }));
          this.modalAddEdit.hide();
          this.requestQuantity = this.entitys.reduce((x, y) => {
            return x += y.RequestQty * y.Price
          }, 0);

          this.requestQuantityWithMOQ = this.entitys.reduce((a, b) => {
            return a += Math.max(b.RequestQty, b.MOQ) * b.Price
          }, 0);


        })
      }
    }
  }

  exportFile() 
  {
    var exportList = this.entitys.concat(this.suggests).concat(this.expands);   
    exportList = exportList.filter(m =>{
      return m != undefined;
    });
    //console.log(exportList);
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    headers.delete("Authorization");
    headers.append("Authorization", "Bearer " + this.userlogin.access_token);
    let options = new RequestOptions({ headers: headers });
    this._http.post(SystemConstants.BASE_API + '/api/BOM/exportExcel', JSON.stringify(exportList), options)
      .subscribe((response: any) => {
        window.location.href = this.baseFolder + JSON.parse(response._body).Message;
      }, error => this._dataService.handleError(error));
    }
  

  showDetail(entity: any) 
  {

    if (this.entity != undefined && this.entity.MaterialListId !== entity.MaterialListId)
      this.entitys.filter(m => m.MaterialListId == this.entity.MaterialListId)
        .map(o => {
          o.IsDrop = true;
        })
    if (entity.IsDrop) {
      this.expands = this.entityList.filter(m => m.PartNumber.trim() === entity.PartNumber.trim() && m.MaterialListId !== entity.MaterialListId && m.MatchedType === 1);

    }

    this.entity = entity;
    return !entity.IsDrop;

  }

  ChangeIndex(i: number) 
  {
    this.selectIndex = i;
  }

  CheckNumber(event) 
  {

    return /^\d+$/.test(event) || event == "" ? event : null;
  }

  isSelectRow(index: number)
  {
    if (this.selectIndex == undefined) {
      return false;
    }
    else {
      return this.selectIndex == index ? true : false;
    };
  }

  ChangeIndexExpand(i: number) 
  {
    this.selectIndexExpand = i;
  }

  isSelectExpand(index: number) 
  {
    if (this.selectIndexExpand == undefined) {
      return false;
    }
    else {
      return this.selectIndexExpand == index ? true : false;
    };
  }

  isMultiSeller(i: any) 
  {
    return i == 0 ? false : true;
  }

 
  sort(property: string) 
  {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  pageChanged(event: any) 
  {
    this.pageIndex = event.page;
  }

  addCartEvent(indexRow: number) 
  {
    if (this.addCart == undefined)
      return false;
    else {
      return this.addCart.includes(indexRow) ? true : false;
    }
  }

  addToCart(id: number, partnumber: string, materialistId: number, requestQty: number, availableQty: number, MOQ: number, price: number, extendedPrice: number) 
  {
    var buyerId = this.userlogin.id;
    var qty = requestQty >= availableQty ? availableQty : requestQty;
    this.carts = { MaterialListId: materialistId, Quantity: qty, UnitPrice: price, ExtendedPrice: extendedPrice, BuyerId: buyerId, BomItemId: id };

    var updateRequest = requestQty - qty;
    this._dataService.post('/api/cart/AddCartAndUpdateBomitem', JSON.stringify(this.carts))
      .subscribe((response: number) => {
        this._notificationService.printSuccessMessage("Add to cart successful");
        var push = this.addCart.push(materialistId);
        this.entitys = this.entitys.map(m => {
          m.RequestQty = m.PartNumber.trim() === partnumber.trim() ? m.RequestQty - qty : m.RequestQty;
          m.AvailableQty = m.MaterialListId == materialistId ? m.AvailableQty - qty : m.AvailableQty;
          m.InCart = m.PartNumber.trim() === partnumber.trim() ? m.InCart + qty : m.InCart;
          return m;
        });
        this._cartservice.editCount(response); // goi ham sua gia tri item cart
      }, error => {
        if (error.status === 409) {
          this._notificationService.printErrorMessage(JSON.parse(error._body));
        }
        else {
          this._dataService.handleError(error);
        }
      });
  }


  addAllToCart() 
  {
    this.countClick += 1;
    var listCart = [];

    var tempWarning = []; var tempWarning2 = []; var tempError = []; var tempSuccess = [];// các mảng lấy cac phân đoạn(phân đoạn warning có 2 temp vì dài quá nên tách =))

    if (this.countClick === 1)
      listCart = this.entitys.map(m => {
        return {
          MaterialListId: m.MaterialListId,
          Quantity: m.RequestQty,
          UnitPrice: m.Price,
          ExtendedPrice: m.ExtendedPrice,
          BuyerId: this.userlogin.id,
          BomItemId: m.Id
        }
      });
    else {
      tempWarning2 = this.entitys.filter(m => m.AvailableQty === 0 && m.RequestQty > 0);// bỏ temp warning khi nhấp lần 2
      listCart = this.entitys.filter(m => tempWarning2.map(o => o.MaterialListId).indexOf(m.MaterialListId) === -1)
        .map(t => {
          return {
            MaterialListId: t.MaterialListId,
            Quantity: t.RequestQty,
            UnitPrice: t.Price,
            ExtendedPrice: t.ExtendedPrice,
            BuyerId: this.userlogin.id,
            BomItemId: t.Id
          }
        });
      //console.log(listCart);
    };
    this._dataService.post('/api/cart/addlist', JSON.stringify(listCart)).toPromise().then((response: any) => {

      tempError = this.entitys.filter(m => tempWarning2.map(o => o.MaterialListId).indexOf(m.MaterialListId) === -1)
        .filter(m => response.map(o => o.MaterialListId).indexOf(m.MaterialListId) === -1) //tempError là temp k có trên mảng trả về
        .map(t => {
          t.AvailableQty = t.RequestQty > t.MOQ ? 0 : t.AvailableQty
          return t;
        });
      this.errorCart = tempError.map(o => {
        return o.MaterialListId;
      });
      this.errorQty = tempError.length;

      tempWarning = response.filter(m => m.Quantity > 0) // tempwwarning là kết quả trả về  > 0 đây chính là sluong còn thiếu
        .map(o => {
          return {
            MaterialListId: o.MaterialListId,
            Quantity: o.Quantity
          }
        })

      tempWarning2 = tempWarning2.concat(this.entitys.filter(m => tempWarning.map(o => o.MaterialListId).indexOf(m.MaterialListId) !== -1)
        .map(t => {
          t.InCart = t.InCart + tempWarning.find(u => u.MaterialListId === t.MaterialListId).Quantity;
          t.RequestQty = t.RequestQty - tempWarning.find(obj => obj.MaterialListId === t.MaterialListId).Quantity;
          t.AvailableQty = 0;
          return t;
        }));
      this.warningCart = tempWarning2.map(m => m.MaterialListId);
      this.warningQty = this.warningCart.length;
      //console.log(tempWarning2);
      this.successCart = response.filter(m => m.Quantity <= 0) //tempSuccess là kết quả trả về >=0 đây chính là số lượng còn, k trừ trên frontend vì muốn  search lại số lượng
        .map(o => {
          return {
            MaterialListId: o.MaterialListId,
            Quantity: o.Quantity
          }

        });

      this.addCart = this.addCart.concat(this.successCart.map(obj => obj.MaterialListId));
      tempSuccess = this.entitys.filter(m => this.successCart.map(o => o.MaterialListId).indexOf(m.MaterialListId) !== -1)
        .map(t => {
          t.InCart = t.InCart + t.RequestQty;

          t.AvailableQty = - this.successCart.find(obj => obj.MaterialListId === t.MaterialListId).Quantity;
          t.RequestQty = 0;
          return t;
        });
      this.successQty = tempSuccess.length;
      //console.log(tempSuccess);
      this.entitys = tempSuccess.concat(tempWarning2).concat(tempError);
      this._notificationService.printSuccessMessage("Add to cart successfully");
      this.isShown = true;
      this._cartservice.editCount(response);
    }, error => {

      this._dataService.handleError(error);


    }).then(() => {
      this._dataService.get('/api/cart/countitem')
        .subscribe((response: any) => {
          this._cartservice.editCount(response.Count);
        }, error => console.error(error));
    })
  }

  addErrorCart(materiallistId: number) 
  {
    if (this.errorCart == undefined)
      return false;
    else {
      return this.errorCart.includes(materiallistId) ? true : false;
    }
  }

  addWarningCart(materiallistId: number) 
  {
    if (this.warningCart == undefined)
      return false;
    else {
      return this.warningCart.includes(materiallistId) ? true : false;
    }
  }

  fillterMOQ(arr: any[]) 
  {
    var arrTemp: any[] = [];
    return arr.reduce((first, curr) => {
      var found = first.find(m => m.PartNumber.trim() === curr.PartNumber.trim());
      if (found != undefined) {

        if (found.MOQ > curr.MOQ) {
          arrTemp.push(curr);
          first = first.map(obj => arrTemp.find(o => o.PartNumber.trim() === obj.PartNumber.trim()) || obj);
        }

        first.filter(m => m.PartNumber.trim() === curr.PartNumber.trim())
          .map(o => {
            o.IsMultiSeller = 1
          });
      }
      else {
        first.push(curr);
      }
      return first;
    }, []);
  }

  checkPartNumber(partnumber: string, list: any[]) 
  {
    if ((list.filter(m => {
      return m.PartNumber.trim() === partnumber.trim();
    })).length > 0)
      return 1;
    return 0;
  }

  IsOdd(i)
  {
    return i%2 ? true: false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

