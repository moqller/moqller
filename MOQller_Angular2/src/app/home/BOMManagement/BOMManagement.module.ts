import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BOMManagementComponent } from './BOMManagement.component';
import { FormsModule } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';
import { AuthenService } from '../../core/services/authen.service';
import { DataService } from 'app/core/services/data.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { OrderrByPipe } from '../../core/common/pipes';
import { ShareModule } from '../../share.module';
export const routes: Routes = [
  //{ path: '', redirectTo: 'index/:selectBomId', pathMatch: 'full' },
  { path: 'index/:selectBomId', component: BOMManagementComponent },
  //{ path: 'index', component: BOMManagementComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(routes),
    ShareModule
  ],
  providers: [AuthenService, NotificationService, DataService],
  declarations: [BOMManagementComponent]
})
export class BOMManagementModule { }