import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { Routes, RouterModule } from '@angular/router';

import { UtilityService } from '../core/services/utility.service';
import { AuthenService } from '../core/services/authen.service';
import { SignalrService } from '../core/services/signalr.service';
import { NotificationService } from '../core/services/notification.service';
import { AuthGuard } from '../core/guards/auth.guard';
import { FormsModule } from '@angular/forms';
import { ShareService } from '../core/services/share.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UploadService } from '../core/services/upload.service';

const homeRoutes: Routes = [
  //localhost:4200
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: HomeComponent },
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'buyer', loadChildren: './buyer/buyer.module#BuyerModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterModule' },
  { path: 'materiallist', loadChildren: './materiallist/materiallist.module#MaterialListModule' },
  { path: 'materialdetail', loadChildren: './materialdetail/materialdetail.module#MaterialDetailModule' },
  { path: 'advancesearch', loadChildren: './advancesearch/advancesearch.module#AdvanceSearchModule' },
  { path: 'uploadbom', loadChildren: './uploadbom/uploadbom.module#UploadBomModule' },
  { path: 'changepassword', loadChildren: './changepassword/changepassword.module#ChangePasswordModule' },
  { path: 'verify', loadChildren: './verify/verify.module#VerifyModule' },
  { path: 'underconstruction', loadChildren: './underconstruction/underconstruction.module#UnderconstructionModule' },
  { path: 'bommanagement', loadChildren: './BOMManagement/BOMManagement.module#BOMManagementModule'},
  { path: 'transactionhistory', loadChildren: './TransactionHistory/TransactionHistory.module#TransactionHistoryModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartModule' },
  { path: 'ordermanagement', loadChildren: './ordermanagement/ordermanagement.module#OrderManagementModule' },
  { path: 'seller', loadChildren: './seller/seller.module#SellerModule' },
  { path: 'sellerordermanagement', loadChildren: './sellerordermanagement/sellerordermanagement.module#SellerOrderManagementModule' },
  { path: 'howitwork', loadChildren: './howitwork/howitwork.module#howitworkModule' },
  { path: 'forgotpwd', loadChildren: './forgotpwd/forgotpwd.module#forgotpwdModule' },
  { path: 'resetpwd', loadChildren: './resetpwd/resetpwd.module#resetpwdModule' },
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    ModalModule.forRoot(),
    RouterModule.forChild(homeRoutes),
  ],
  declarations: [HomeComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService,UploadService]
})
export class HomeModule { }
