import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-advancesearch',
  templateUrl: './advancesearch.component.html',
  styleUrls: ['./advancesearch.component.css']
})
export class AdvanceSearchComponent implements OnInit {
  public keyWord: string = '';
  public partNumber: boolean = true;
  public description: boolean = false;
  public distributer: boolean = false;
  public searchField: number = 1;
  constructor() { }

  ngOnInit() {

  }
  
  public changeEventPartNumber(event) {
    if (event.target.checked) {
      this.searchField = 1;
    }
  }
  public changeEventDescription(event) {
    if (event.target.checked) {
      this.searchField = 2;
    }
  }
  public changeEventDistributer(event) {
    if (event.target.checked) {
      this.searchField = 3;
    }
  }
}
