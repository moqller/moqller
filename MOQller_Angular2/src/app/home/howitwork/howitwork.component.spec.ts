import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { howitworkComponent } from './howitwork.component';

describe('BuyerComponent', () => {
  let component: howitworkComponent;
  let fixture: ComponentFixture<howitworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ howitworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(howitworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
