import { NgModule } from '@angular/core';
import { howitworkComponent } from './howitwork.component';
import { Routes, RouterModule } from '@angular/router';


const howitworkRoutes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: howitworkComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(howitworkRoutes)
  ],
  declarations: [howitworkComponent],
  providers: []
})
export class howitworkModule { }
