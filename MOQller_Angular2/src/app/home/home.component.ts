import { Component, OnInit, ViewChild } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { SystemConstants } from '../core/common/system.constants';
import { NotificationService } from 'app/core/services/notification.service';
import { Router } from '@angular/router';
import { ShareService } from '../core/services/share.service';
import { LoggedInUser } from '../core/domain/loggedin.user';
import { UrlConstants } from '../core/common/url.constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private userlogin: LoggedInUser;

  constructor(private _http: Http, private _notificationService: NotificationService, private _router: Router, private _shareService: ShareService) { }

  ngOnInit() 
  {
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
  }
  
  GoToSeller(){
    this._shareService.setSeller(true);
    if (this.userlogin == null)
    {
      this._router.navigate([UrlConstants.LOGIN]);
    }
    else
    {
      this._router.navigate(['home/seller/index']);
    }
  }

  GoToBuyer(){
    this._shareService.setSeller(false);
    this._router.navigate(['home/buyer/index']);
  }
}
