import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionHistoryComponent } from './TransactionHistory.component';
import { DataService } from '../../core/services/data.service';
import { Routes, RouterModule } from '@angular/router';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import { UtilityService } from '../../core/services/utility.service';
import { AuthenService } from '../../core/services/authen.service';
import { SignalrService } from '../../core/services/signalr.service';
import { NotificationService } from '../../core/services/notification.service';
import { AuthGuard } from '../../core/guards/auth.guard';

import { FormsModule } from '@angular/forms';
import { Daterangepicker } from 'ng2-daterangepicker';
import { ShareModule } from '../../share.module';


const TransactionHistoryRoutes: Routes = [
  //localhost:4200
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: TransactionHistoryComponent },
  { path: ':orderstatus', component: TransactionHistoryComponent }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    Daterangepicker,
    RouterModule.forChild(TransactionHistoryRoutes),
    ShareModule
  ],
  declarations: [TransactionHistoryComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService, DataService]
})
export class TransactionHistoryModule { }
