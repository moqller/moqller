import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { UploadService } from '../../core/services/upload.service';
import { AuthenService } from '../../core/services/authen.service';
import { UtilityService } from '../../core/services/utility.service';

import { SystemConstants } from '../../core/common/system.constants';

import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Router } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';
import { ShareService } from 'app/core/services/share.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-TransactionHistory',
  templateUrl: './TransactionHistory.component.html',
  styleUrls: ['./TransactionHistory.component.css']
})
export class TransactionHistoryComponent implements OnInit {
  @ViewChild('StartDate') StartDate;
  @ViewChild('EndDate') EndDate;
  public pageIndexBuying: number = 1;
  public pageIndexSelling: number = 1;
  public pageSize: number = 10;
  public totalRowBuying: number;
  public totalRowSelling: number;
  public hiddenpagingBuying = false;
  public hiddenpagingSelling = false;
  public filterPaymentStatusId: number = 0;
  public filterOrderStatusId: number = 0;
  public filterDeliveryStatusId: number = 0;
  public BuyingTransactions: any[];
  public SellingTransactions: any[];
  public loading = false;
  public PaymentStatuses: any[];
  public DeliveryStatuses: any[];
  public OrderStatuses: any[];
  public toggleBool: boolean = false;
  private userlogin: LoggedInUser;
  public showConfirm:boolean=false;
  public baseFolder: string = SystemConstants.BASE_API;
  public checkSeller: boolean;
  public filter: string = "";
  public subscription : Subscription;
  public isLoading: boolean = true;
  public titleByDelivery: string = "";

  public StartdateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false,
    maxDate: moment()
  };

  public EnddateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false,
    maxDate: moment(),
    minDate: ''
  };

  //biến sắp xếp
  public column: string = "OrderDate";
  public isDesc: boolean = false;
  public direction: number = -1

  constructor(private _dataService: DataService,
    private _router: Router,
    public _authenService: AuthenService, 
    private _shareService: ShareService,
    private _activatedRoute: ActivatedRoute) {
      this._shareService.getSeller().subscribe( m => {
        this.checkSeller = m});
       }

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(params => {
      this.filter = params['orderstatus'] == undefined ? '' : params['orderstatus'];  
    });
    
    this.userlogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));

    if (this.userlogin == null)
    {
      this._router.navigate([UrlConstants.HOME]);
    }
    else
    {
      if(this.filter !== '' && this.checkSeller){
        this.StartDate.nativeElement.value = '01-01-' + new Date().getFullYear().toString();
        this.EndDate.nativeElement.value = moment(new Date()).format('MM-DD-YYYY');
      }
      // this.LoadPayments();
      // this.LoadDeliverys();
      // this.LoadOrderStatus();
      if (this.checkSeller == false)
      {
        this.LoadBuying();
      }
      else this.LoadSelling();
    }
  }

  public search() {
    this.isLoading = true;
    if (this.checkSeller == false)
    {
      this.LoadBuying();
    }
    else this.LoadSelling();
  }

  public LoadBuying(){
    this._dataService.get('/api/transactionhistory/getBuying?StartDate=' + this.StartDate.nativeElement.value + '&EndDate=' + this.EndDate.nativeElement.value +'&PaymentId=' + this.filterPaymentStatusId + '&deliveryId=' + this.filterDeliveryStatusId + '&OrderStatusId=' + this.filterOrderStatusId
    +'&pageIndex=' + this.pageIndexBuying + '&pageSize=' + this.pageSize)
    .toPromise().then((response: any) => {
      this.isLoading = false;
      this.BuyingTransactions = response.Items;
      this.totalRowBuying = response.TotalRows;
      if(this.totalRowBuying <= this.pageSize) this.hiddenpagingBuying = true;
    }, error => this._dataService.handleError(error));
  }

  public LoadSelling(){
    if (this.filter == 'FIN')
    {
      this.titleByDelivery = ' - Delivered';
    }
    else if (this.filter == 'CAN')
    {
      this.titleByDelivery = ' - Cancelled';
    }

    this._dataService.get('/api/transactionhistory/getselling?StartDate=' + this.StartDate.nativeElement.value + '&EndDate=' + this.EndDate.nativeElement.value +'&PaymentId=' + this.filterPaymentStatusId + '&deliveryId=' + this.filterDeliveryStatusId + '&OrderStatusId=' + this.filterOrderStatusId
      +'&pageIndex=' + this.pageIndexSelling + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
      .toPromise().then((response: any) => {
        console.log(response.Items);
        this.isLoading = false;
        this.SellingTransactions = response.Items;
        this.totalRowSelling = response.TotalRows;
        if(this.totalRowSelling <= this.pageSize) this.hiddenpagingSelling = true;
      }, error => this._dataService.handleError(error));
  }


  // public LoadPayments() {
  //   this._dataService.get('/api/transactionhistory/getallpayment')
  //     .subscribe((response: any) => {
  //       this.PaymentStatuses = response;
  //     }, error => this._dataService.handleError(error));
  // }

  // public LoadDeliverys() {
  //   this._dataService.get('/api/transactionhistory/getalldelivery')
  //     .subscribe((response: any) => {
  //       this.DeliveryStatuses = response;
  //     }, error => this._dataService.handleError(error));
  // }

  // public LoadOrderStatus(){
  //   this._dataService.get('/api/transactionhistory/getallorderstatus')
  //     .subscribe((response: any) => {
  //       this.OrderStatuses = response;
  //     }, error => this._dataService.handleError(error));
  // }

  public changeStartDate(value: any) {
    this.EnddateOptions.minDate = this.StartDate.nativeElement.value = moment(new Date(value.end._d)).format('MM-DD-YYYY');
    this.EndDate.nativeElement.disabled = false;
  }
  public changeEndDate(value: any) {
    this.StartdateOptions.maxDate  = this.EndDate.nativeElement.value = moment(new Date(value.end._d)).format('MM-DD-YYYY');
  }

  public Export()
  {
    this._dataService.get('/api/transactionhistory/exportExcel?username='+ this.userlogin.username 
    + '&StartDate=' + this.StartDate.nativeElement.value + '&EndDate=' + this.EndDate.nativeElement.value +'&PaymentId=' + this.filterPaymentStatusId + '&deliveryId=' + this.filterDeliveryStatusId + '&OrderStatusId=' + this.filterOrderStatusId + '&isSeller=' + this.checkSeller)
    .subscribe((response: any) => {
      window.location.href = (this.baseFolder + response.Message);
    }, error => this._dataService.handleError(error));
  }

  // public cDeliveryStatus(event)
  // {
  //   this.filterDeliveryStatusId = event.target.id;
  //   this.search();
  // }

  // public cOrderStatuses(event)
  // {
  //   this.filterOrderStatusId = event.target.id;
  //   this.search();
  // }

  public pageChangedBuying(event: any): void {
    this.pageIndexBuying = event.page;
    this.LoadBuying();
  }

  public pageChangedSelling(event: any): void {
    this.pageIndexSelling = event.page;
    this.LoadSelling();
  }
  
  sort(property: string) 
  {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
  ngOnDestroy(): any {
     this.subscription.unsubscribe();
  }
}



