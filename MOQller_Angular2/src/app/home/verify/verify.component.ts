import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import { LoggedInUser } from '../../core/domain/loggedin.user';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { SystemConstants } from '../../core/common/system.constants';
import { UtilityService } from '../../core/services/utility.service';
@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit, OnDestroy {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  public id: string;
  private subscription: Subscription;

  constructor(private _dataService: DataService, private _notificationService: NotificationService, private activatedRoute: ActivatedRoute, private _http: Http,private _utilityService: UtilityService) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    })

    this.loadData();
  }

  loadData() {
    this._http.get(SystemConstants.BASE_API + '/api/register/verify?id=' + this.id )
      .subscribe((response: Response) => {
        this._notificationService.printSuccessMessage("You are active, please login");
        this._utilityService.navigateToLogin();
      }, error => this._dataService.handleError(error));
  }
  

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
