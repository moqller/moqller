import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyComponent } from './verify.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const verifyRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index/:id', component: VerifyComponent },
  
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(verifyRoutes)
  ],
  declarations: [VerifyComponent],
  providers:[DataService,NotificationService]
})
export class VerifyModule { }
