import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UploadService } from '../../core/services/upload.service';
import { error } from 'util';
import { NotificationService } from '../../core/services/notification.service';
import { SystemConstants } from '../../core/common/system.constants';
import { DataService } from '../../core/services/data.service';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { LoggedInUser } from 'app/core/domain/loggedin.user';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscriber } from 'rxjs';
import { mainRoutes } from 'app/main/main.routes';
import { UtilityService } from 'app/core/services/utility.service';
@Component({
  selector: 'app-uploadmaterial',
  templateUrl: './uploadmaterial.component.html',
  styleUrls: ['./uploadmaterial.component.css']
})
export class UploadMaterialComponent implements OnInit, OnDestroy {
  @ViewChild('excelFile') file;
  @ViewChild('price') Price;
  @ViewChild('content') Content;
  
  public entitys: any[];
  public entityOlds: any[];
  public partNumber: number = 1;
  public qty: number = 2;
  public startRow: number = 2;
  public showList: boolean = false;
  public showColumn: boolean = false;
  public pageIndex: number = 1;
  public hiddenpaging = false;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow1: number;
  public loading = false;
  public fi: any;

  public partNumberField: string = "";
  public qtyField: string = "";
  public moqField: string = "";
  public priceField: string = "";
  public dateField: string = "";
  public statusField: string = "";
  public manufacturerField: string = "";
  public descriptionField: string = "";
  public techLinkField: string= ""
  public selectedsheet: string = "";
  public _partNumbers: any[];
  public _sheets: any[];
  public showStatus: boolean = false;
  public showUpload: boolean = true;
  public hideUpload: boolean = false;
  
  public ShippingStatus: any;
  public showConfirm: boolean = false;
  private user: LoggedInUser;
  private userId: string;
  public empty: string = "";
  public dateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false
  };
  public i: number;
  constructor(private _uploadService: UploadService, private _http: Http, private _notificationService: NotificationService, private _dataService: DataService, private _router:Router,  private _utilityService: UtilityService) 
  {
    document.body.style.background = "#572A61";
   }

  ngOnInit() {
      this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
      if (this.user != null)
    {
      this.showConfirm = true;
    }
    

  }
  checkValidFile(form: NgForm) {
    if (form.valid) {
      //this.loading = true;
      this.fi = this.file.nativeElement;
      this.showStatus = true;
      this.showUpload = false;
      this.hideUpload = true;
      this.loadData();

    }
  }
  pageChanged1(event: any): void {
    this.pageIndex = event.page;
    //this.loadData();
  }
  
  hideColumn() {
    this.showColumn = false;
    this.partNumberField = "";
  }
  loadData() {
    if (this.fi.files.length > 0) {
      if (this.partNumberField == undefined) {
        this.loading = false;
        this.showUpload = true;
        this.hideUpload = false;
        this.showStatus = false;
        this._notificationService.printErrorMessage("Can’t find any column to map. Please map columns yourself.");
        return;
      }
      let formData: FormData = new FormData();
      formData.append('files', this.fi.files[0], this.fi.files[0].name);
      let headers = new Headers();
      //headers.append("Content-Type", "application/x-www-form-urlencoded");     
      let options = new RequestOptions({ headers: headers });     
      this._http.post(SystemConstants.BASE_API + '/api/upload/materialfile?partNumberField=' + this.partNumberField.replace("#", "[[]]") + '&qtyField=' + this.qtyField.replace("#", "[[]]") + '&moqField=' + this.moqField.replace("#", "[[]]") + '&dateField=' + this.dateField.replace("#", "[[]]") +  '&priceField=' + this.priceField.replace("#", "[[]]") + '&statusField=' + this.statusField.replace("#", "[[]]")
      + '&manufacturerField=' + this.manufacturerField.replace("#", "[[]]") + '&descriptionField=' + this.descriptionField.replace("#", "[[]]") + '&techLinkField=' + this.techLinkField.replace("#", "[[]]")+ '&page=' + this.pageIndex + '&pageSize=' + this.pageSize, formData, options)
        .subscribe((data: any) => {
          
          if (data.json() == "") 
          {
            
            this.loading = false;
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("Can not find any data to upload");
            return;
          }
          if (data.json()[0].Name) 
          {
            
            this.loading = false;
            this.showUpload = true;
            this.hideUpload = false;
            this._notificationService.printErrorMessage(data.json()[0].Error);
            this._partNumbers = data.json();
            //console.log(this._partNumbers);
            this.showColumn = true;
            this.showStatus = false;
          }
          else {
            this._notificationService.printSuccessMessage("Congratulations! your upload is completed");
            this.LoadShippingStatus();
            this.entitys = data.json();           
            console.log(this.entitys);            
            this.entityOlds = this.entitys;
            this._sheets = data.json().map(m => {
              return m.Sheet;
            });
            this._sheets = this._sheets.reduce((x, y) => x.includes(y) ? x : [...x, y], []);
            this.totalRow1 = this.entitys.length;
            if (this.totalRow1 <= this.pageSize) this.hiddenpaging = true;
            this.showList = true;
            this.showStatus = false;
            this.showUpload = false;
          }
        }, error => {
          
          this.loading = false;
          if (error.status == 400) {
            
            this._notificationService.printErrorMessage("File format is not correct. Please select another file in csv or xls or xlsx");
            this.showStatus = false;
            this.showUpload = true;
            this.hideUpload = false;
          }
          else if (error.status == 502) {
            
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("File size cannot be 5MB, please select a smaller file");
          }
          else {
            
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("File format is not correct. Please select another file in csv or xls or xlsx");
          }

        });
    }
    else {
      this.loading = false;
      this.showUpload = true;
      this.hideUpload = false;
      this.showStatus = false;
      this._notificationService.printErrorMessage("Please select file to upload");
    }

  }
  pageChanged(event: any): void {
    this.pageIndex = event.page;
    //this.loadData();
  }

  onSheetChange(newValue) {

    if (newValue != undefined) 
    {
      this.entitys = this.entityOlds;
      this.entitys = this.entitys.filter(m => {
        return m.Sheet === newValue
      })
    
    }
    else {
      this.entitys = this.entityOlds;
    }
    this.totalRow1 = this.entitys.length;
    if (this.totalRow1 <= this.pageSize) this.hiddenpaging = true;
  }

  LoadShippingStatus(){
    this._dataService.get('/api/shippingstatus/getlistpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + "")
                    .subscribe(m=>{
                      this.ShippingStatus = m.Items;
                    });
  }

  Upload()
  {
    let tempFilter = this.entitys.filter(m=>{
      return this.Check(m);
    });
    console.log(tempFilter);
    this._dataService.post('/api/materiallist/add', JSON.stringify(tempFilter)).subscribe(()=>{
      this._notificationService.printSuccessMessage("Add Material successfully");
      this.entitys = this.entitys.filter(m =>{
        return !this.Check(m) });
    },error =>this._dataService.handleError(error)
    );
    
  }

  Check(entity)
  {
    return entity.PartNumber.trim() == "" || entity.AvailableQty == 0  || entity.ShippingStatusId == 0 || (entity.ShippingStatusId != 'STOCK' && entity.DueDate == "") || entity.Price == "" ? false : true;
  }

  Rewrite(event){
    return (moment(new Date(event.end._d)).format('MM-DD-YYYY'));
  }

  CheckDate(value)
  {    
    if(value == "")
    return this.empty;
  
   return !isNaN(Date.parse(value)) ? (moment(new Date(value)).format('MM-DD-YYYY')) : this.empty;
     
  }

  SelectChange(event, entity){
    entity.ShippingStatusId = event;
    entity.DueDate = event == 'STOCK' ? "" : entity.DueDate;
  }
  
  CheckNumber(value)
  {
    return isNaN(value) || value == ""  ? false : true;
  }

  RewriteNumber(value)
  {
    let num = value.replace(/,/g, "");
    //console.log(num);
    //console.log(isNaN(num));
    return isNaN(num) ? '' : num; 
  }

  ChangePrice(value, i)
  {
    this.i = i;
    console.log(value);
    
  }

  InputDueDate(event){
   
   //console.log(event);
   let v = moment(event.target.value, 'MM-DD-YYYY', true).isValid() ? moment(new Date(event.target.value), 'MM-DD-YYYY') : '';
   let u =this.CheckDate(v);
   //console.log(v);
   //console.log(u);
   return v;
  }
  unhidden()
  {
    this.i = undefined;
  }

  isSelectRow(index: number) {
    if (this.i == undefined) {
      return false;
    }
    else {
      return this.i == index ? true : false;
    };
  }

  RemoveMaterial(i){
    this.entitys.splice(i, 1);
  }

  ngOnDestroy()
  {
    document.body.style.background = "#4D6E9B";
  }
}