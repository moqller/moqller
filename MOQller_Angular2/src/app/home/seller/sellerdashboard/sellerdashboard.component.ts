import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../core/services/data.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ShareService } from '../../../core/services/share.service';
import { AuthenService } from '../../../core/services/authen.service';
import { Router } from '@angular/router';
import { UrlConstants } from '../../../core/common/url.constants';

@Component({
  selector: 'app-sellerdashboard',
  templateUrl: './sellerdashboard.component.html',
  styleUrls: ['./sellerdashboard.component.css']
})
export class SellerDashboardComponent implements OnInit {

  public checkSeller: boolean;

  public orderStatistics : any;
  public orderMonthStatic: any;
  public currentInventory: any;
  public statusInventory: any;
  public dataArr: any[] = [];
  public emptyData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  
  constructor(private _dataService: DataService, 
    private _notification: NotificationService, 
    private _share: ShareService, 
    private _user: AuthenService,
    private _router: Router) { 
      this._share.getSeller().subscribe(m=>{
        this.checkSeller =m
      })
    }

  ngOnInit() 
  {
    if(!this.checkSeller || !this._user.isUserAuthenticated())
    {
      return this._router.navigate([UrlConstants.HOME]);
    }
    this.LoadData();
  }

  LoadData()
  {
    
    this.LoadOrderStatistics();
    this.LoadOrderMonthStatistics();
    this.LoadCurrentInventory();
    
  }

  LoadOrderStatistics()
  {
    this._dataService.get('/api/statistic/getorderstatistic').subscribe(m => {
      this.orderStatistics = m;
    }, error=> this._dataService.handleError(error))
  }

  LoadOrderMonthStatistics()
  {
    this._dataService.get('/api/statistic/getordermonthstatistic').subscribe( m=> {
      let temp = m;
     let tempDelivered = m.find(m => m.Label ==='Delivered') ;
     let tempCancelled = m.find(m => m.Label ==='Cancelled') ;
     if(tempDelivered == null)
     {
       this.dataArr.push({data: this.emptyData, label:'Delivered'});
     }
     else
     {
        var result = Object.keys(tempDelivered).map(function(key) {
          return tempDelivered[key];
        });
        this.dataArr.push({data: result, label: 'Delivered'});
     }
    })
  }

  LoadCurrentInventory(){
    this._dataService.get('/api/materiallist/getcurrentinventory').subscribe(m=>{
      this.currentInventory = { 
                                Less30Day: m.Less30Day, 
                                From30To60Day : m.From30To60Day,
                                From60To120Day : m.From60To120Day,
                                To120Day : m.To120Day
                              };
      this.statusInventory = {
                                Confirm : m.Confirm,
                                UnConfirm : m.UnConfirm
                              };

      }, error => this._dataService.handleError(error));
    
  }

  GoToAddMaterial(){
    this._router.navigate(['home/seller/index']);
  }

  GoToOrder(){
    this._router.navigate(['home/sellerordermanagement/index']);
  }
  GoToInventory(){
    this._router.navigate(['home/seller/inventorymanagement']);
  }
  GoToTransaction(){
    this._router.navigate(['home/transactionhistory/index']);
  }

}
