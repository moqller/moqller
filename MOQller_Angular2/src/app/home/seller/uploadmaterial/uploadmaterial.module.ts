import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadMaterialComponent } from './uploadmaterial.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../../core/services/data.service';
import { NotificationService } from '../../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UploadService } from '../../../core/services/upload.service';
import { Daterangepicker } from 'ng2-daterangepicker';
import { FilterByPipe } from '../../seller/uploadmaterial/FiltePipes';
import { ChartsModule } from 'ng2-charts';
//import { DisableKeyboard } from '../../../core/common/disablekeyboard.directive';
import { ShareModule } from '../../../share.module';
const uploadMaterialRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
 
  { path: 'index', component: UploadMaterialComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ChartsModule, // Chart module
    Daterangepicker,
    ModalModule.forRoot(),
    RouterModule.forChild(uploadMaterialRoutes),
    ShareModule
    
  ],
  declarations: [UploadMaterialComponent, FilterByPipe],
  providers:[DataService,NotificationService,UploadService]
})
export class UploadMaterialModule { }
