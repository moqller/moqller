import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadMaterialComponent } from './uploadmaterial.component';

describe('UploadmaterialComponent', () => {
  let component: UploadMaterialComponent;
  let fixture: ComponentFixture<UploadMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
