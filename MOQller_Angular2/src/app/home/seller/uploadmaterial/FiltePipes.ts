import { Pipe, PipeTransform } from '@angular/core';
@Pipe({  name: 'filter' })
export class FilterByPipe implements PipeTransform {

    transform(arr: any, fun: Function,filterCase:string, id: number): any {
        if(!arr || !filterCase) 
        {
            return false;
          }
        else
        {
            if(filterCase === "DQTY")
            return arr.InStock !== null && arr.InStock !== arr.AvailableQty ? false : true;
            if(filterCase === "DALL")
            return arr.InStock !== null && arr.InStock === arr.AvailableQty ? false : true;
            if(filterCase === "NEW")
            return arr.InStock == null && fun(arr, id) ? false : true;
            if(filterCase === "ERROR")
            return !fun(arr, id) && arr.InStock == null ? false : true;
            if(filterCase === "OTHER")
            return true;
        }
    };
}