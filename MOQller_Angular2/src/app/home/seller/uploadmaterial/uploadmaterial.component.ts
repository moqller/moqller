import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UploadService } from '../../../core/services/upload.service';
import { error } from 'util';
import { NotificationService } from '../../../core/services/notification.service';
import { SystemConstants } from '../../../core/common/system.constants';
import { DataService } from '../../../core/services/data.service';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { LoggedInUser } from 'app/core/domain/loggedin.user';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { mainRoutes } from 'app/main/main.routes';
import { UtilityService } from 'app/core/services/utility.service';
import { ShareService } from 'app/core/services/share.service';
import { UrlConstants } from '../../../core/common/url.constants';
import { AuthenService } from 'app/core/services/authen.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { MessageContstants } from '../../../core/common/message.constants';
import { NullTemplateVisitor } from '@angular/compiler';
@Component({
  selector: 'app-uploadmaterial',
  templateUrl: './uploadmaterial.component.html',
  styleUrls: ['./uploadmaterial.component.css']
})
export class UploadMaterialComponent implements OnInit, OnDestroy {
  
  @ViewChild('price') Price;
  @ViewChild('content') Content;
  
  public entitys: any[];
  public showList: boolean = false;
  public showColumn: boolean = false;
  public pageIndex: number = 1;
  public hiddenpaging = false;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow1: number;
  public loading = false;

  public hideme:any = {};
  public partNumberField: string = "";
  public qtyField: string = "";
  public moqField: string = "";
  public priceField: string = "";
  public dateField: string = "";
  public statusField: string = "";
  public manufacturerField: string = "";
  public descriptionField: string = "";
  public techLinkField: string= "";
  public dateCodeField: string ="";
  public conditionField: string= "";
  public selectedsheet: string = "";
  public _partNumbers: any[];
  public formData: FormData;
  public showStatus: boolean = false;
  public showUpload: boolean = true;
  public hideUpload: boolean = false;
  public checkSeller: boolean;
  public ShippingStatus: any;
  public showConfirm: boolean = false;
  private user: LoggedInUser;
  private userId: string;
  public empty: string = "";  
  public ConditionEnum = [{"Id":1,"Name":"New"},{"Id":2,"Name":"Used"},{"Id":3,"Name":"Refurbished"},{"Id":4,"Name":"New other"}];

  //Option Chart PartNumber
  public ChartLabels:string[] = [];
  public ChartColors:any[] = [{ backgroundColor: ["#008000", "#0000ff", "#ffff00", "#e2871f", "#ffc0cb"] }];
  public ChartData:number[] = [];
  public ChartType:string = "doughnut";
  public Chartoptions = {
    animation: false,
    responsive: true,
    legend: false
  };

  //Option Chart Day
  public ChartLabelsDay:string[] = [];
  public ChartColorsDay:any[] = [{ backgroundColor: ["#008000", "#0000ff", "#ff0000", "#e2e21f"] }];
  public ChartDataDay:number[] = [];
  public ChartTypeDay:string ="doughnut";
  public ChartoptionsDay = {
    animation: false,
    responsive: true,
    legend: false
  };

  public dateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false
  };
  public index: number;
  //variable ShippingStatusId
  public idStock : number;
  public idUnconfrim : number;

  //biến đếm số lượng
  public duplicateDiffQty: number;
  public duplicateAll: number;
  public newPartNumber: number;
  public missInfor: number;
  public otherErrors: number;
  // variable pipes
  public CaseFilter: string;
  //variable currentInventory;
  public currentInventory: any;

  constructor(private _uploadService: UploadService, private _http: Http, private _notificationService: NotificationService, private _dataService: DataService, private _router:Router,  private _utilityService: UtilityService, private _shareService: ShareService, private _authenService: AuthenService) 
  {
    this._shareService.getContentFile().subscribe(o=>{
      this.formData = o;
    })
    
    this._shareService.getSeller().subscribe( m => {
      this.checkSeller = m});
   }

  ngOnInit() {
    this.showStatus = true;
      this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
      if ((this.user == null) || this.checkSeller === false || this.checkSeller == undefined)
      {
        this._router.navigate([UrlConstants.HOME]);
      }
     else{
       this.showConfirm = true
     }
    this.LoadCurrentInventory();
    this.LoadShippingStatus();
    if(this.formData)
    {
      this.loadData()     
    }
    else
    {
      this._router.navigate(['/home/seller/addmaterialtext/index']);
    }

  }
  
  pageChanged1(event: any): void 
  {
    this.pageIndex = event.page;
    
  }
  
  hideColumn() {
    this.showColumn = false;
    this.partNumberField = "";
  }
  loadData() 
  {
      
      let headers = new Headers();
      headers.delete("Authorization");
      headers.append("Authorization", "Bearer " + this._authenService.getLoggedInUser().access_token);   
      let options = new RequestOptions({ headers: headers });     
      this._http.post(SystemConstants.BASE_API + '/api/upload/materialfile?partNumberField=' + this.partNumberField.replace("#", "[[]]") + '&qtyField=' + this.qtyField.replace("#", "[[]]") + '&moqField=' + this.moqField.replace("#", "[[]]") + '&dateField=' + this.dateField.replace("#", "[[]]") +  '&priceField=' + this.priceField.replace("#", "[[]]") + '&statusField=' + this.statusField.replace("#", "[[]]")
      + '&manufacturerField=' + this.manufacturerField.replace("#", "[[]]") + '&descriptionField=' + this.descriptionField.replace("#", "[[]]") + '&techLinkField=' + this.techLinkField.replace("#", "[[]]")+ '&ConditionField=' + this.conditionField.replace("#", "[[]]")+ '&DateCodeField=' + this.dateCodeField.replace("#", "[[]]")+ '&page=' + this.pageIndex + '&pageSize=' + this.pageSize , this.formData, options)
        .subscribe((data: any) => {
          
          if (data.json() == "") 
          {
            
            this.loading = false;
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("Can not find any data to upload");
            return;
          }
          if (data.json()[0].Name) 
          {
            
            this.loading = false;
            this.showUpload = true;
            this.hideUpload = false;
            this._notificationService.printErrorMessage(data.json()[0].Error);
            this._partNumbers = data.json();
            //console.log(this._partNumbers);
            this.showColumn = true;
            this.showStatus = false;
          }
          else {
            this._notificationService.printSuccessMessage("Congratulations! your upload is completed");
            this.entitys = data.json();                           
            this.totalRow1 = this.entitys.length;
            if (this.totalRow1 <= this.pageSize) this.hiddenpaging = true;
            this.showList = true;
            this.showStatus = false;
            this.showUpload = false;
            this.showColumn = false;
          }
        }, error => {
          
          this.loading = false;
          if (error.status == 400) {
            
            this._notificationService.printErrorMessage("File format is not correct. Please select another file in csv or xls or xlsx");
            this.showStatus = false;
            this.showUpload = true;
            this.hideUpload = false;
          }
          else if (error.status == 502) {
            
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("File size cannot be 5MB, please select a smaller file");
          }
          else {
            
            this.showUpload = true;
            this.hideUpload = false;
            this.showStatus = false;
            this._notificationService.printErrorMessage("File format is not correct. Please select another file in csv or xls or xlsx");
          }

        });

  }
  pageChanged(event: any): void {
    this.pageIndex = event.page;
    //this.loadData();
  }


  LoadShippingStatus(){
    this._dataService.get('/api/shippingstatus/getall').toPromise().then((m: Response) =>{
      this.ShippingStatus = m;
      
    }).then(()=>{
      let StockElement = this.ShippingStatus.find(m => m.Code === 'STOCK');
      let Unconfirm = this.ShippingStatus.find(m => m.Code === 'UNC');
      if(StockElement == undefined || Unconfirm == undefined)
      {
        this._notificationService.printErrorMessage("StockStatus doesn't exist in ShippingStatus. Please, feedback to admin");
        
      }
        else
      {
        this.idStock = StockElement.Id;
        this.idUnconfrim = Unconfirm.Id;
      }
    });
    
  }

  LoadCurrentInventory(){
    this._dataService.get('/api/materiallist/getcurrentinventory').subscribe(m=>{
      this.currentInventory = m;
      this.ChartLabelsDay[0]= '0 - 30 Days';
      this.ChartLabelsDay[1] = '31 - 60 Days';
      this.ChartLabelsDay[2] = '61 - 120 Days';
      this.ChartLabelsDay[3] = '> 120 Days';
      
      this.ChartDataDay[0] = m.Less30Day;
      this.ChartDataDay[1] = m.From30To60Day;
      this.ChartDataDay[2] = m.From60To120Day;
      this.ChartDataDay[3] = m.To120Day;
      
    }, error => this._dataService.handleError(error));
  }

  UpLoadConfirm()
  {
    if(this.entitys.length === 0)
    {
      return this._notificationService.printErrorMessage("List of valid material is empty");
    }
    if(this.entitys.filter(m =>{
      return m.InStock !== 0 && m.InStock != null
    }).length > 0)
    { return this._notificationService.printConfirmationDialog(MessageContstants.SAVEMATERIAL, () => this.Upload());}
    this.Upload();
  }

  Upload()
  {
    let tempFilter = this.entitys.filter(m=>{
      return this.Check(m, this.idStock);
    });
    tempFilter =  tempFilter.map(m =>{
      m.SellerId = this.user.id;
      return m
    });
    //console.log(tempFilter);
    this._dataService.post('/api/materiallist/add', JSON.stringify(tempFilter)).toPromise().then(()=>{
      
      this.entitys = this.entitys.filter(m =>{
        return !this.Check(m,this.idStock) });
        this._dataService.get('/api/materiallist/sendmail?toEmail=' + this.user.email + '&userName=' + this.user.firtName).subscribe(()=>{
          this._notificationService.printSuccessMessage("Materials are uploaded successfully into inventory and Email is sent to you");
        });

    },error =>this._dataService.handleError(error)
    ).then(()=>{
      this.totalRow1 = this.entitys.length;
      if (this.totalRow1 <= this.pageSize) this.hiddenpaging = true;
      this.ChangeStatisticVar(this.entitys);
      this.LoadCurrentInventory();
    });
    
  }

  Check(entity, id)// id là idStock, phải để như vậy để pipes có thể dùng lại đc hàm check.
  {
    return entity.PartNumber.trim() == "" || entity.AvailableQty == 0 || entity.AvailableQty == null || entity.ShippingStatusId == 0 || entity.Condition == 0 || entity.DueDate == null || entity.DueDate == ''  || entity.Price == null ? false : true;
  }

  RewriteDate(event, entity)
  {
    entity.DueDate = (moment(new Date(event.end._d)).format('MM-DD-YYYY'));
    entity.ShippingStatusId = new Date(entity.DueDate).getTime() < new Date().getTime() ? this.idStock : entity.ShippingStatusId;
    this.RegetInStock(entity);
  }

  CheckDate(value)
  {    
    if (value == null)
    {
      return null
    }
    else
    {
      return moment(value, 'MM-DD-YYYY', true).isValid() ? (moment(new Date(value)).format('MM-DD-YYYY')) : 'Now';
    }
  }

  SelectChange(event, entity){
    if(event === 0) 
    {
      entity.ShippingStatusId = 0
    }
    else
    {
    this._dataService.get('/api/shippingstatus/detail/' +  event).toPromise().then(m=>{
      entity.ShippingStatusId = m.Id;
      if( m.Code == 'STOCK')
      {
        entity.DueDate = 'Now';
      }
      else
      {
        entity.DueDate = entity.DueDate == 'Now'? moment(new Date().setDate(new Date().getDate() + 1)).format('MM-DD-YYYY') : entity.DueDate;
      }
    }).then(()=>{
      this.RegetInStock(entity);
    });
  }
  }

  SelectChangeConditon(event, entity)
  {
    entity.Condition = event;
    this.RegetInStock(entity);
  }
  
  CheckNumber(event)
  {
    
    return /^\d+$/.test(event) || event == ""  ? event : null;
  }

  RewriteNumber(value)
  {
    let num = value.replace(/,/g, "");
    return isNaN(num) ? '' : num; 
  }

  ChangePrice(value, i)
  {
    this.index = i;   
    
  }

  InputDueDate(event, entity)
  {
 
   if (entity.DueDate ==  null)
   {
     entity.ShippingStatusId = this.idUnconfrim;
     entity.InStock =  null;
   }
   else
   {
     entity.ShippingStatusId = new Date(entity.DueDate).getTime() < new Date().getTime() ? this.idStock : entity.ShippingStatusId;
     this._dataService.post('/api/materiallist/getinstock', JSON.stringify(entity)).toPromise().then(m=>{
      entity.InStock = m.InStock;
    }).then(()=>{
      this.ChangeStatisticVar(this.entitys);
    })
   }
  }


  isSelectRow(index: number) 
  {
    if (this.index == undefined) {
      return false;
    }
    else {
      return this.index == index ? true : false;
    };
  }

  RegetInStock(entity:any)
  {
    
    if(!this.Check(entity, this.idStock))
    {
      entity.InStock = null;
    }
    else
    {
      this._dataService.post('/api/materiallist/getinstock', JSON.stringify(entity)).toPromise().then(m=>{
        entity.InStock = m.InStock;
      }).then(()=>{
        this.ChangeStatisticVar(this.entitys);
      })
    } 
  }

  ChangeStatisticVar(array:any[])
  {
    this.duplicateDiffQty = array.filter(x=>{
      return x.InStock !== null && x.InStock !== x.AvailableQty;
    }).length;
    this.duplicateAll = array.filter(x=>{
      return x.InStock !== null && x.InStock === x.AvailableQty
    }).length;
    this.newPartNumber = array.filter(x=>{
      return x.InStock == null && this.Check(x, this.idStock) ;
    }).length;
    this.missInfor = array.filter(x=>{
      return !this.Check(x, this.idStock) && x.InStock == null ;
    }).length;
    this.otherErrors = array.length - (this.duplicateDiffQty +  this.duplicateAll + this.newPartNumber + this.missInfor);

    //chart

    this.ChartLabels[0] = 'Duplicate with diff quantity';
    this.ChartLabels[1] = 'Part number with missing infor';
    this.ChartLabels[2] = 'New part number';
    this.ChartLabels[3] = 'Duplicate partnumber';
    this.ChartLabels[4] = 'Other errors';
    
    this.ChartData[0] = (this.duplicateDiffQty);
    this.ChartData[1] = (this.missInfor);
    this.ChartData[2] = (this.newPartNumber);
    this.ChartData[3] = (this.duplicateAll);
    this.ChartData[4] = (this.otherErrors);
   
  }

  RemoveMaterialConfirm(i)
  {
    this._notificationService.printConfirmationDialog("Are you sure to remove this material from the list?",()=> this.RemoveMaterial(i));
    
  }

  RemoveMaterial(i)
  {
    this.entitys.splice(i, 1);
    this.totalRow1 = this.entitys.length;
    if (this.totalRow1 <= this.pageSize) this.hiddenpaging = true;
    this.ChangeStatisticVar(this.entitys);
  }

  Filter(value){
    
    this.CaseFilter = value;
  
  }

  ngOnDestroy()
  {
   
  }

  public chartClicked(e:any):void {
    console.log(e);
  }
}