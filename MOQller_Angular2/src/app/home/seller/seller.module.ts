import { NgModule } from '@angular/core';
import { SellerComponent } from './seller.component';
import { DataService } from '../../core/services/data.service';
import { Routes, RouterModule } from '@angular/router';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import { UtilityService } from '../../core/services/utility.service';
import { AuthenService } from '../../core/services/authen.service';
import { SignalrService } from '../../core/services/signalr.service';
import { NotificationService } from '../../core/services/notification.service';
import { AuthGuard } from '../../core/guards/auth.guard';
import { FormsModule } from '@angular/forms';
import { ShareService } from '../../core/services/share.service';
import { ChartsModule } from 'ng2-charts';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { calendarsellerComponent } from './calendarseller/calendarseller.component';
import { calendarsellerModule }  from './calendarseller/calendarseller.module';

const SellerRoutes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: SellerComponent },
  { path: 'uploadmaterial', loadChildren: "./uploadmaterial/uploadmaterial.module#UploadMaterialModule" },
  { path: 'addmaterialtext', loadChildren: "./addmaterialtext/addmaterialtext.module#AddMaterialTextModule" },
  { path: 'inventorymanagement', loadChildren: "./inventorymanagement/inventorymanagement.module#InventoryManagementModule" }
]
@NgModule({
  imports: [
    calendarsellerModule,
    CommonModule,
    FormsModule,
    PaginationModule,
    ChartsModule,
    RouterModule.forChild(SellerRoutes),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [SellerComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService, DataService  ]
})
export class SellerModule { }
