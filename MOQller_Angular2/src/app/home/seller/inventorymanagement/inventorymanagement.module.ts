import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../../core/services/data.service';
import { NotificationService } from '../../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { InventoryManagementComponent } from './inventorymanagement.component';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DisableKeyboard } from '../../../core/common/disablekeyboard.directive';
import { ShareModule } from '../../../share.module';
//import { OrderrByPipe } from '../../../core/common/pipes';

const inventoryManagementRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
 
  { path: 'index', component: InventoryManagementComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    Daterangepicker,
    ModalModule.forRoot(),
    RouterModule.forChild(inventoryManagementRoutes),
    ShareModule
    
  ],
  declarations: [InventoryManagementComponent],
  providers:[DataService,NotificationService]
})
export class InventoryManagementModule { }
