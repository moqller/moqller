import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from '../../../core/services/notification.service';
import { DataService } from '../../../core/services/data.service';
import { Router } from '@angular/router';
import { UtilityService } from '../../../core/services/utility.service';
import { ShareService } from '../../../core/services/share.service';
import { AuthenService } from '../../../core/services/authen.service';
import { LoggedInUser } from 'app/core/domain/loggedin.user';
import { SystemConstants } from '../../../core/common/system.constants';
import { UrlConstants } from '../../../core/common/url.constants';
import { MessageContstants } from 'app/core/common/message.constants';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { stringify } from 'querystring';


@Component({
  selector: 'app-inventorymanagement',
  templateUrl: './inventorymanagement.component.html',
  styleUrls: ['./inventorymanagement.component.css', ]
})
export class InventoryManagementComponent implements OnInit {
 
  @ViewChild("modalEditMaterial") public modalEditMaterial: ModalDirective;
  public isLoading: boolean = true;
  public checkSeller: boolean;
  public user: LoggedInUser;
  public materiallists: any;
  public entity: any;
  public pageIndex: number = 1;
  public pageSize: number = 10;
  public pageDisplay: number = 10;
  public totalRow: number;
  public shippingStatus: any;
  public idStock : number;
  public idUnconfrim : number;
  public cancelled: number;
  public ConditionEnum = [{"Id":1,"Name":"New"},{"Id":2,"Name":"Used"},{"Id":3,"Name":"Refurbished"},{"Id":4,"Name":"New other"}];
  public dateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false
  };
  //biến sắp xếp
  public column: string = "Material.PartNumber";
  public isDesc: boolean = false;
  public direction: number = -1;

  constructor( private _notificationService: NotificationService, 
    private _dataService: DataService, 
    private _router:Router,  
    private _utilityService: UtilityService, 
    private _shareService: ShareService, 
    private _authenService: AuthenService) 
  {  
    this._shareService.getSeller().subscribe( m => {
      this.checkSeller = m});
  }

  ngOnInit() 
  {
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
      if ((this.user == null) || this.checkSeller === false || this.checkSeller == undefined)
      {
       return this._router.navigate([UrlConstants.HOME]);
      }
      this.LoadShippingStatus();
      this.LoadData();
  }

  LoadData()
  {
    this.isLoading = true;
    this._dataService.get('/api/materiallist/getlistseller?page=' + this.pageIndex + '&pageSize=' + this.pageSize).subscribe(m=>{
      this.materiallists = m.Items;
      this.isLoading = false;
      this.totalRow = m.TotalRows;
    }, error => this._dataService.handleError(error));
  }

  CheckDueDate(duedate:any, status:number)
  {
    return status === this.idStock ? 'Now' : (moment(new Date(duedate)).format('MM-DD-YYYY'));
  }

  RemoveMaterialConfirm(id: any){
    this._notificationService.printConfirmationDialog("Are you sure to remove this material from your inventory?",()=> this.RemoveMaterial(id))
  }

  public pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.LoadData();
  }

  RemoveMaterial(id: any){
    this._dataService.delete('/api/materiallist/delete', 'id', id).subscribe(m=>{
      this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
      this.LoadData();
    }, error => this._dataService.handleError(error))
  }

  LoadMaterialList(id){
    this._dataService.get('/api/materiallist/detail/' + id).subscribe(m=>{
      this.entity = m;
    }, error => this._dataService.handleError(error))
  }

  ShowEditModal(id: number)
  {
    this.modalEditMaterial.show();
    this.LoadShippingStatus();
    this.LoadMaterialList(id);
  }

  SaveChange(valid: any, status: number, id: number){
    var iEntity: any;
    if(valid)
    {
      this.entity.MOQ = this.entity.MOQ == ""  ? 0 : this.entity.MOQ;
      iEntity = this.entity;
      this.Update(iEntity);
      this.LoadData();
    }
    else
    {
      if(status === this.cancelled)
      {
        this._dataService.get('/api/materiallist/detail/' + id).toPromise().then(m=>{
        iEntity = m;
        }, error => this._dataService.handleError(error)).then(()=>{
        iEntity.ShippingStatusId = this.cancelled;}
        ).then(()=>{
          this.Update(iEntity);
          this.LoadData();
        })
      }
    }
  }
   
  Update(entity){
    entity.DueDate = entity.DueDate == 'Now'? moment(new Date().setDate(new Date().getDate() + 1)).format('MM-DD-YYYY') : entity.DueDate;
    this._dataService.put('/api/materiallist/update', JSON.stringify(entity)).subscribe(()=>{
      this._notificationService.printSuccessMessage("Material is updated");
      this.modalEditMaterial.hide();
      this.LoadData();
    }, error => this._dataService.handleError(error))
  }
  
  LoadShippingStatus(){
    this._dataService.get('/api/shippingstatus/getall').toPromise().then((m: Response) =>{
      this.shippingStatus = m;
    }).then(()=>{
      let StockElement = this.shippingStatus.find(m => m.Code === 'STOCK');
      let Unconfirm = this.shippingStatus.find(m => m.Code === 'UNC');
      let can = this.shippingStatus.find(m=> m.Code === 'CAN')
      if(StockElement == undefined || Unconfirm == undefined || can == undefined)
      {
        this._notificationService.printErrorMessage("StockStatus doesn't exist in ShippingStatus. Please, feedback to admin");
      }
        else
      {
        this.idStock = StockElement.Id;
        this.idUnconfrim = Unconfirm.Id;
        this.cancelled = can.Id
      }
    });
  }

  RewriteDate(event, entity){
    entity.DueDate = (moment(new Date(event.end._d)).format('MM-DD-YYYY'));
    entity.ShippingStatusId = new Date(entity.DueDate).getTime() < new Date().getTime() ? this.idStock : entity.ShippingStatusId;
  }

  CheckDate(value)
  {    
    return moment(value, 'MM-DD-YYYY', true).isValid() ? (moment(new Date(value)).format('MM-DD-YYYY')) : null;
  }

  SelectChange(event, entity){
    this._dataService.get('/api/shippingstatus/detail/' +  event).toPromise().then(m=>{
      entity.ShippingStatusId = m.Id;
      entity.DueDate = m.Code == 'STOCK' ? moment(new Date().setDate(new Date().getDate() -1)).format('MM-DD-YYYY'): entity.DueDate;
    })
  }

  sort(property: string) 
  {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
