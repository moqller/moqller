import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryManagementComponent } from './inventorymanagement.component';

describe('InventorymanagementComponent', () => {
  let component: InventoryManagementComponent;
  let fixture: ComponentFixture<InventorymanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventorymanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
