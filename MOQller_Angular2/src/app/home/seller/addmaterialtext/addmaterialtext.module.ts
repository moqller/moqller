import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadMaterialComponent } from '../uploadmaterial/uploadmaterial.component';
import { DataService } from '../../../core/services/data.service';
import { Routes, RouterModule } from '@angular/router';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import { UtilityService } from '../../../core/services/utility.service';
import { AuthenService } from '../../../core/services/authen.service';
import { SignalrService } from '../../../core/services/signalr.service';
import { NotificationService } from '../../../core/services/notification.service';
import { AuthGuard } from '../../../core/guards/auth.guard';
import { FormsModule } from '@angular/forms';
import { ShareService } from '../../../core/services/share.service';
import { InventoryManagementComponent } from '../inventorymanagement/inventorymanagement.component';
import { AddMaterialTextComponent } from '../addmaterialtext/addmaterialtext.component';



const SellerRoutes: Routes = [
  //localhost:4200
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: AddMaterialTextComponent },
  
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    RouterModule.forChild(SellerRoutes)
  ],
  declarations: [AddMaterialTextComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService, DataService]
})
export class AddMaterialTextModule { }
