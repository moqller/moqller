import { Component, OnInit, ViewChild, ViewChildren, OnDestroy, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../../../core/services/data.service';
import { NotificationService } from '../../../core/services/notification.service';
import { UploadService } from '../../../core/services/upload.service';
import { AuthenService } from '../../../core/services/authen.service';
import { UtilityService } from '../../../core/services/utility.service';
import { MessageContstants } from '../../../core/common/message.constants';
import { SystemConstants } from '../../../core/common/system.constants';
import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { fail } from 'assert';
import { LoggedInUser } from '../../../core/domain/loggedin.user';
import { Router } from '@angular/router';
import { UrlConstants } from '../../../core/common/url.constants';
import { ShareService } from '../../../core/services/share.service';


@Component({
  selector: 'app-AddMaterialText',
  templateUrl: './addmaterialtext.component.html',
  styleUrls: ['./addmaterialtext.component.css']
})
export class AddMaterialTextComponent implements OnInit, OnDestroy{
  @ViewChild("dataText") dataT;
  @ViewChild('excelFile') file;
  public data: string = "";
  public entity: any;
  public userLogin : LoggedInUser;
  public checkSeller: boolean;
  public fileContent: any;
  public redirectToUploadBom : boolean = false;
  public partNumberField : string = "";
  public qtyField : string = "";
  //biến truyền sang uploadbom
  public showLoading: boolean =  false;
  public showMapColumn: boolean = false;
  public showResult: boolean = false;
  constructor(private _dataService: DataService,
    private _notificationService: NotificationService,
    private _utilityService: UtilityService,
    private _router: Router,
    public _authenService: AuthenService, 
    private _http: Http,
    private _shareService: ShareService) {
      
     
      this._shareService.getSeller().subscribe( m => {
        this.checkSeller = m});
    
  }

  ngOnInit() 
  {
    
    this.userLogin = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    
    if ((this.userLogin == null) || this.checkSeller === false || this.checkSeller == undefined)
    {
      this._router.navigate([UrlConstants.HOME]);
    };
    this.LoadData();    
  }

 EmptyData(){
      return this.data.trim() === "" ? true : false;
  }

  LoadData(){
    this.data = "";
    
  }

  AddData(){
    
    this.entity = {SellerId: this.userLogin.id, Data: this.data};
    this._dataService.post('/api/materialTempData/add', JSON.stringify(this.entity)).subscribe((response: any) => {
        
        this._notificationService.printSuccessSellerMessage("Information uploaded pending for review");
      }, error=>{
          this._dataService.handleError(error);
      });
      this.LoadData();
  }

  GoToUploadBom()
  {
    this.fileContent = this.file.nativeElement;
    if(this.fileContent.files.length > 0)
    {     
      this.showLoading = true;
      let formData: FormData = new FormData();
      formData.append('files', this.fileContent.files[0], this.fileContent.files[0].name);
      console.log(formData);
      this._shareService.setContenFile(formData);
      this._router.navigate(['/home/seller/uploadmaterial/index']);
      
      }
      else 
      {
        this._notificationService.printConfirmationDialog("Please select file to upload", ()=>{
          
        })
      }
  }
  GoToUploadMaterial()
  {
    this._router.navigate(['/home/seller/uploadmaterial/index']);
  }
  
  ngOnDestroy(): any {
    
  }
  
}



