import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMaterialTextComponent } from './addmaterialtext.component';

describe('AddmaterialtextComponent', () => {
  let component: AddMaterialTextComponent;
  let fixture: ComponentFixture<AddMaterialTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMaterialTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMaterialTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
