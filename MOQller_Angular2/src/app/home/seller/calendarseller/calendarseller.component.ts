import { Component, ChangeDetectionStrategy, OnInit,  Input, Output, EventEmitter} from '@angular/core';
import { map } from 'rxjs/operators';
import { CalendarEvent, CalendarMonthViewDay, CalendarDateFormatter, DateFormatterParams } from 'angular-calendar';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { DataService } from '../../../core/services/data.service';

export class CustomDateFormatter extends CalendarDateFormatter {
  // Format date view calendar here
  public monthViewColumnHeader({ date, locale }: DateFormatterParams): string {
    return new DatePipe(locale).transform(date, 'EEE', locale);
  }
}

@Component({
  selector: 'mwl-calendar-seller',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendarseller.component.html',
  styleUrls: ['./calendarseller.component.css', ],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})

export class calendarsellerComponent implements OnInit {
  @Input()
  view: string = 'month'; // set view dạng tháng

  @Input()
  viewDate: Date  = new Date(); // lấy ngày hiện tại hiển thị

  @Input()
  locale: string = 'en';

  public isLoading$:  Observable<Array<CalendarEvent>>;

  @Output()
  viewDateChange: EventEmitter<string> = new EventEmitter();

  public data_calendar: Array<CalendarEvent>;ng

  constructor(private _dataService: DataService, ) {}

  ngOnInit(): void {
    this.LoadData(new Date());
  }

  fetchEvents(dateview: Date): void {
    this.LoadData(dateview)   
  }

  LoadData(dateview: Date)
  {
    var month_current = dateview.getMonth() + 1;
    this.isLoading$ = this._dataService.get('/api/statistic/getMaterialAlertDay?year=' + dateview.getFullYear() + '&month=' + month_current); // gọi lại 1 lần để lấy đồng bộ

    this.isLoading$.subscribe((result: any) =>{ // gọi lại lần nữa để đổ dữ liệu
      this.data_calendar = [];
      if(result.length > 0)
      {
        result.forEach(item =>
        {
          if(item.AlertStatus == 2)
          {
            this.data_calendar.push(
              {
                title: 'red',
                color: colors.red,
                start: new Date(item.DayNo),
                allDay: true
              }
            )
          }

          if(item.AlertStatus == 1)
          {
            this.data_calendar.push(
              {
                title: 'orange',
                color: colors.orange,
                start: new Date(item.DayNo),
                allDay: true
              } 
            )
          }

          if(item.AlertStatus == 0)
          {
            this.data_calendar.push(
              {
                title: '',
                color: colors.white,
                start: new Date(item.DayNo),
                allDay: true
              }
            )
          }
        })
      }
    }, error => this._dataService.handleError(error));
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach(day => {
      if(day.events.length > 0)
      {
        day.backgroundColor = day.events[0].color.primary;
      }
    });
  }

  dayClicked(e)
  {
    console.log(e);
  }

  ngOnDestroy() {
  }
}

export const colors: any = {
  red: {
    primary: '#ec4848',
    secondary: '#FAE3E3'
  },
  orange: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  white: {
    primary: '#fff',
    secondary: '#FDF1BA'
  }
};

