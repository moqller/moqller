import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { forgotpwdComponent } from './forgotpwd.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const forgotpwdRoutes: Routes = [
  { path: '', component: forgotpwdComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(forgotpwdRoutes)
  ],
  declarations: [forgotpwdComponent],
  providers:[DataService,NotificationService]
})
export class forgotpwdModule { }
