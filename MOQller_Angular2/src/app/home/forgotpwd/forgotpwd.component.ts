import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { SystemConstants } from '../../core/common/system.constants';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';

@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.css']
})
export class forgotpwdComponent implements OnInit {
  
  public model: any = {};

  constructor(private _notificationService: NotificationService, private _http: Http,private _router: Router) { }

  ngOnInit() {
  }

  forgot(form: NgForm) {
    if (form.valid) {
      this._http.get(SystemConstants.BASE_API + '/api/appUser/RequestResetPasswordToken/' + this.model.UserName + '/' + this.model.Email)
      .subscribe((response: any) => {
        this._router.navigate([UrlConstants.HOME]);
        this._notificationService.printSuccessMessage(JSON.parse(response._body).Message);
      }, error => this._notificationService.printErrorMessage(JSON.parse(error._body).Message));
      return;
    }
  }
}
