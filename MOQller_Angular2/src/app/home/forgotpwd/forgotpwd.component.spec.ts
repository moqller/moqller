import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { forgotpwdComponent } from './forgotpwd.component';

describe('forgotpwdComponent', () => {
  let component: forgotpwdComponent;
  let fixture: ComponentFixture<forgotpwdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ forgotpwdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(forgotpwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
