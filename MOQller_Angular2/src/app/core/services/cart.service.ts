import { Injectable, Inject } from "@angular/core";
import { DataService } from './data.service';
import { SystemConstants } from '../../core/common/system.constants';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CartService {
  private CountItems = new BehaviorSubject<number>(0);
  cast = this.CountItems.asObservable();

  constructor(private _dataservice: DataService) {
    var user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (user)
    {
      if (user.access_token)
      {
        this.CallCount();
      }
    }
  }

  editCount(newCount: number){
    this.CountItems.next(newCount);
  }

  CallCount()
  {
    this._dataservice.get('/api/cart/countitem')
    .subscribe((response: any) => {
      this.editCount(response.Count);
    }, error => console.error(error));
  }
}
