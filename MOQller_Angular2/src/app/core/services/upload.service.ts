
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { UrlConstants } from '../../core/common/url.constants';
import { UtilityService } from './utility.service';
import { NotificationService } from '../../core/services/notification.service';
@Injectable()
export class UploadService {
  public responseData: any;
  
  constructor(private dataService: DataService, private utilityService: UtilityService,private _notificationService: NotificationService) { }

  postWithFile(url: string, postData: any, files: File[]) {
    let formData: FormData = new FormData();
    formData.append('files', files[0], files[0].name);

    if (postData !== "" && postData !== undefined && postData !== null) {
      for (var property in postData) {
        if (postData.hasOwnProperty(property)) {
          formData.append(property, postData[property]);
        }
      }
    }
    var returnReponse = new Promise((resolve, reject) => {
      this.dataService.postFile(url, formData).subscribe(
        res => {
          this.responseData = res;
          resolve(this.responseData);
        },
        error => {
          if(error.status ==400)
          {
            this._notificationService.printErrorMessage("File format is not correct. Please select another file in csv or xls or xlsx");
          }
          else if(error.status ==502)
          {
            this._notificationService.printErrorMessage("File size cannot be 5MB, please select a smaller file");
          }
          
        }
      );
    });
    return returnReponse;
  }
}
