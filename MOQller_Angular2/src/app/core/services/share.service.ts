import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Subject ,  Observable } from 'rxjs';
import { LoggedInUser } from 'app/core/domain/loggedin.user';
import { SystemConstants } from '../common/system.constants';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class ShareService {

  public nodeSeller = new BehaviorSubject<boolean>(false);
  public nodeFile = new BehaviorSubject<FormData>(null);
  public nodeBomId = new BehaviorSubject<number>(0);
  
  public user: LoggedInUser
  // node$ = this.node.asObservable();

  constructor(public _cookieService: CookieService ) { 
    if (this._cookieService.get("BomId"))
    {
    let t = new BehaviorSubject<number>(Number(this._cookieService.get("BomId")));
    this.nodeBomId = t;
    }
  }

  setSeller(message: boolean) 
  {
    this.nodeSeller.next(message) ;
    
  }
  
  getSeller(){
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
      if (this.user == null) 
      {
         this.setSeller(false)
         return this.nodeSeller.asObservable() ;
      }
      else
      {
        return this.nodeSeller.asObservable();
      }
  }

  setContenFile(formData)
  {
    this.nodeFile.next(formData);
  }

  getContentFile(){
    return this.nodeFile.asObservable();
  }

  setBomId()
  {
    this.nodeBomId.next(Number(this._cookieService.get("bomId")));
  }

  getBomId(){
    return this.nodeBomId.asObservable();
  }
}
