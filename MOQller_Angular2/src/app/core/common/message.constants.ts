export class MessageContstants {
    public static SYSTEM_ERROR_MSG = "Connection to server is wrong";
    public static CONFIRM_DELETE_MSG = "Are you sure to delete this record?";
    public static LOGIN_AGAIN_MSG = "You are expire, please relogin."
    public static CREATED_OK_MSG = "Add new successfull";
    public static REGISTERED_OK_MSG = "Register successfull";
    public static UPDATED_OK_MSG = "Update Successfull";
    public static DELETED_OK_MSG = "Delete Successfull";
    public static FORBIDDEN = "You was blocked to access";
    public static LOGIN = "Invalid user or password";
    public static SENDMAIL_OK_MSG = "Send e-mail successfull";
    public static SAVEMATERIAL = "Would you like to save InStock material into database?"
}