export class ListOfWorkOrder {
    constructor(
        PartNumber: string,
        Manufacturer: string ,
        Description: string ,
        AvailableQty: string ,
        MOQ: string ,
        DueDate: string ,
        ShippingStatusId:number ,
        Price:string ,
        StatustoSearch: string ,
        UploadImage:string ,
        Techlink:string ,
        InStock: string ,
        SellerId: string ,
        WorkOrderId: number
    ) {
        this.PartNumber = PartNumber;
        this.Manufacturer = Manufacturer;
        this.Description = Description;
        this.AvailableQty = AvailableQty;
        this.MOQ = MOQ;
        this.DueDate = DueDate;
        this.ShippingStatusId = ShippingStatusId;
        this.Price = Price;
        this.StatustoSearch = StatustoSearch;
        this.UploadImage = UploadImage;
        this.Techlink = Techlink;
        this.InStock = InStock;
        this.SellerId = SellerId;
        this.WorkOrderId = WorkOrderId;
    }
    public PartNumber: string;
    public Manufacturer: string;
    public Description: string;
    public AvailableQty: string;
    public MOQ: string;
    public DueDate: string;
    public ShippingStatusId: number;
    public Price: string;
    public StatustoSearch: string;
    public UploadImage: string;
    public Techlink: string;
    public InStock: string;
    public SellerId: string;
    public WorkOrderId: number;
}