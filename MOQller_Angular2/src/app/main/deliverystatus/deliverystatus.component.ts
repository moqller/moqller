import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import {LoggedInUser} from '../../core/domain/loggedin.user';

@Component({
  selector: 'app-deliverystatus',
  templateUrl: './deliverystatus.component.html',
  styleUrls: ['./deliverystatus.component.css']
})
export class DeliveryStatusComponent implements OnInit {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  public pageIndex: number = 1;
  public pageSize: number = 20;
  public pageDisplay: number = 10;
  public hiddenpaging = false;
  public totalRow: number;
  public filter: string = '';
  public deliverystatuses: any[];
  public entity: any;
  private user: LoggedInUser;
  
  constructor(private _dataService: DataService, private _notificationService: NotificationService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this._dataService.get('/api/DeliveryStatus/getlistpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
      .subscribe((response: any) => {
        this.deliverystatuses = response.Items;
        this.totalRow = response.TotalRows;
        if(this.totalRow <= this.pageSize) this.hiddenpaging = true
      },error => this._dataService.handleError(error));
  }
  loaddeliverystatus(id: any) {
    this._dataService.get('/api/DeliveryStatus/detail/' + id)
      .subscribe((response: any) => {
        this.entity = response;
        console.log(this.entity);
      });
  }
  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadData();
  }
  showAddModal() {
    this.entity = {};
    this.modalAddEdit.show();
  }
  showEditModal(id: any) {
    this.loaddeliverystatus(id);
    this.modalAddEdit.show();
  }
  saveChange(valid: boolean) {
    if (valid) {
      if (this.entity.Id == undefined) {
        this._dataService.post('/api/DeliveryStatus/add', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.CREATED_OK_MSG);
          }, error => {
            if (error.status==409){
              this._notificationService.printErrorMessage("Name exists");  
            }
            else {
              this._dataService.handleError(error)  
            }
            
          });
      }
      else {
        this._dataService.put('/api/DeliveryStatus/update', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
          }, error => this._dataService.handleError(error));
      }
    }
  }
  deleteItem(id: any) {
    this._notificationService.printConfirmationDialog(MessageContstants.CONFIRM_DELETE_MSG, () => this.deleteItemConfirm(id));
  }
  deleteItemConfirm(id: any) {
    this._dataService.delete('/api/DeliveryStatus/delete', 'id', id).subscribe((response: Response) => {
      this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
      this.loadData();
    });
  }
}
