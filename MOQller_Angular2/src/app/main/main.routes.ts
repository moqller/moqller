import { Routes } from '@angular/router';
import { MainComponent } from './main.component';

export const mainRoutes: Routes = [
    {
        //localhost:4200/main
        path: '', component: MainComponent, children: [
            //localhost:4200/main
            { path: '', redirectTo: 'product', pathMatch: 'full' },
            //localhost:4200/main/home
            { path: 'function', loadChildren: './function/function.module#FunctionModule' },
            //localhost:4200/main/user
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'role', loadChildren: './role/role.module#RoleModule' },
            { path: 'product-category', loadChildren: './product-category/product-category.module#ProductCategoryModule' },
            { path: 'order', loadChildren: './order/order.module#OrderModule' },
            { path: 'announcement', loadChildren: './announcement/announcement.module#AnnouncementModule' },
            { path: 'report', loadChildren: './report/report.module#ReportModule' },
            { path: 'country', loadChildren: './country/country.module#CountryModule' },
            { path: 'shippingstatus', loadChildren: './shippingstatus/shippingstatus.module#ShippingStatusModule' },
            { path: 'state', loadChildren: './state/state.module#StateModule' },
            { path: 'material', loadChildren: './material/material.module#MaterialModule' },
            { path: 'deliverystatus', loadChildren: './deliverystatus/deliverystatus.module#DeliveryStatusModule' },
            { path: 'paymentstatus', loadChildren: './paymentstatus/paymentstatus.module#PaymentStatusModule' },
            { path: 'shippingmethod', loadChildren: './shippingmethod/shippingmethod.module#ShippingMethodModule' },
            { path: 'orderstatus', loadChildren: './orderstatus/orderstatus.module#OrderStatusModule' }
        ]
    }

]