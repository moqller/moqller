import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShippingStatusComponent } from './shippingstatus.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const shippingstatusRoutes: Routes = [
  //localhost:4200/main/user
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  //localhost:4200/main/home/index
  { path: 'index', component: ShippingStatusComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(shippingstatusRoutes)
  ],
  declarations: [ShippingStatusComponent],
  providers:[DataService,NotificationService]
})
export class ShippingStatusModule { }
