import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { MessageContstants } from '../../core/common/message.constants';
import {LoggedInUser} from '../../core/domain/loggedin.user';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  public pageIndex: number = 1;
  public pageSize: number = 20;
  public pageDisplay: number = 10;
  public totalRow: number;
  public hiddenpaging = false;
  public filter: string = '';
  public states: any[];
  public countries: any[];
  public entity: any;
  private user: LoggedInUser;
  public countryId: number;
  
  constructor(private _dataService: DataService, private _notificationService: NotificationService) { }

  ngOnInit() {
    this.loadData();
    this.loadCountries();
  }

  loadData() {
    
    this._dataService.get('/api/state/getlistpaging?countryId='+ this.countryId + '&page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
      .subscribe((response: any) => {
        this.states = response.Items;
        this.totalRow = response.TotalRows;
        if (this.totalRow <= this.pageSize) this.hiddenpaging = true;
      },error => this._dataService.handleError(error));
  }
  private loadCountries() {
    this._dataService.get('/api/country/getall').subscribe((response: any[]) => {
      this.countries = response;
    }, error => this._dataService.handleError(error));
  }
  loadstate(id: any) {
    this._dataService.get('/api/state/detail/' + id)
      .subscribe((response: any) => {
        this.entity = response;
        //console.log(this.entity);
      });
  }
  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadData();
  }
  showAddModal() {
    this.entity = {};
    this.modalAddEdit.show();
  }
  showEditModal(id: any) {
    this.loadstate(id);
    this.modalAddEdit.show();
  }
  saveChange(valid: boolean) {
    if (valid) {
      if (this.entity.Id == undefined) {
        this._dataService.post('/api/state/add', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.CREATED_OK_MSG);
          }, error => {
            if (error.status==409){
              this._notificationService.printErrorMessage("Code exists");  
            }
            else {
              this._dataService.handleError(error)  
            }
            
          });
      }
      else {
        this._dataService.put('/api/state/update', JSON.stringify(this.entity))
          .subscribe((response: any) => {
            this.loadData();
            this.modalAddEdit.hide();
            this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
          }, error => this._dataService.handleError(error));
      }
    }
  }
  deleteItem(id: any) {
    this._notificationService.printConfirmationDialog(MessageContstants.CONFIRM_DELETE_MSG, () => this.deleteItemConfirm(id));
  }
  deleteItemConfirm(id: any) {
    this._dataService.delete('/api/state/delete', 'id', id).subscribe((response: Response) => {
      this._notificationService.printSuccessMessage(MessageContstants.DELETED_OK_MSG);
      this.loadData();
    });
  }
}
