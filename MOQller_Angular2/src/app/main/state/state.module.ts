import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StateComponent } from './state.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

const stateRoutes: Routes = [
  
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  
  { path: 'index', component: StateComponent }
]
@NgModule({
  imports: [
    CommonModule,
    PaginationModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forChild(stateRoutes)
  ],
  declarations: [StateComponent],
  providers:[DataService,NotificationService]
})
export class StateModule { }
