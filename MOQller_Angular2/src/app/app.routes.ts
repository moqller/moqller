import { Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
export const appRoutes: Routes = [
    //localhost:4200
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    //localhost:4200/home
    { path: 'home', loadChildren: './home/home.module#HomeModule' },
    //localhost:4200/main
    { path: 'main', loadChildren: './main/main.module#MainModule',canActivate:[AuthGuard] },
    // localhost:4200/dataentry
    { path: 'dataentry', loadChildren: './dataentry/dataentry.module#DataEntryModule' }
]