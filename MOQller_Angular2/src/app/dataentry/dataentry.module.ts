import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { listofworkorderComponent } from './listofworkorder/listofworkorder.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../core/services/data.service';

import { UtilityService } from '../core/services/utility.service';
import { AuthenService } from '../core/services/authen.service';
import { SignalrService } from '../core/services/signalr.service';
import { NotificationService } from '../core/services/notification.service';
import { AuthGuard } from '../core/guards/auth.guard';
import { Daterangepicker } from 'ng2-daterangepicker';
import { InputTrimModule } from 'ng2-trim-directive';
import { FormsModule } from '@angular/forms';
//import { DisableKeyboard } from '../core/common/disablekeyboard.directive';
import { ShareModule } from '../share.module';

const dataentryRoutes: Routes = [
  //localhost:4200/dataentry
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: listofworkorderComponent },
  { path: 'reviewmateriallist', loadChildren: './reviewmateriallist/reviewmateriallist.module#ReviewMaterialListModule' }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Daterangepicker,
    ShareModule,
    InputTrimModule,
    RouterModule.forChild(dataentryRoutes)
  ],
  declarations: [listofworkorderComponent],
  providers: [UtilityService, AuthenService, SignalrService,NotificationService, DataService]
})
export class DataEntryModule { }
