import { Component, OnInit, ViewChild } from '@angular/core';

import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MessageContstants } from '../../core/common/message.constants';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reviewmateriallist',
  templateUrl: './reviewmateriallist.component.html',
  styleUrls: ['./reviewmateriallist.component.css']
})
export class reviewmateriallistComponent implements OnInit {
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  @ViewChild('StartDate') StartDate;
  @ViewChild('EndDate') EndDate;
  
  public keyWord: string = 'Review Material List';
  public pageIndex: number = 1;
  public pageSize: number = 20;
  public pageDisplay: number = 10;
  public hiddenpaging = false;
  public Description: boolean = false;
  public Manufacturer: boolean = false;
  public TeckLink: boolean = false;
  public ReviewMaterialList: any[];
  public Material: any;
  public totalRow: number = 0;
  
  public StartdateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false,
    maxDate: moment()
  };

  public EnddateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false,
    maxDate: moment(),
    minDate: ''
  };

  constructor( private _notificationService: NotificationService,
    private _dataService: DataService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  public changeStartDate(value: any) {
    this.EnddateOptions.minDate = this.StartDate.nativeElement.value = moment(new Date(value.end._d)).format('MM-DD-YYYY');
    this.EndDate.nativeElement.disabled = false;
  }
  public changeEndDate(value: any) {
    this.StartdateOptions.maxDate  = this.EndDate.nativeElement.value = moment(new Date(value.end._d)).format('MM-DD-YYYY');
  }

  loadData() {
    this._dataService.get('/api/materiallist/getreviewmaterial?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&StartDate=' + this.StartDate.nativeElement.value + '&EndDate=' + this.EndDate.nativeElement.value 
    + '&Description=' + this.Description + '&Manufacturer=' + this.Manufacturer + '&TeckLink=' + this.TeckLink)
      .subscribe((response: any) => {
        this.ReviewMaterialList = response.Items;
        this.totalRow = response.TotalRows;
        if (this.totalRow <= this.pageSize)
        {
          this.hiddenpaging = true;
        }
      },error => this._dataService.handleError(error));
  }

  loadDetailMaterial(id: any) {
    this._dataService.get('/api/material/detail/' + id)
      .subscribe((response: any) => {
        this.Material = response;
        this.Material.IsMaterialList = false;
      });
  }

  loadDetailMaterialList(id: any)
  {
    this._dataService.get('/api/materiallist/detail/' + id)
    .subscribe((response: any) => {
      this.Material = response;
      this.Material.IsMaterialList = true;
    });
  }

  showEditModal(MaterialId: any, MaterialListId: any) {
    if (MaterialId != 0)
    {
      this.loadDetailMaterial(MaterialId);
    }
    else
    {
      this.loadDetailMaterialList(MaterialListId);
    }
    this.modalAddEdit.show();
  }

  saveChange(form: NgForm) {
    if (form.valid && this.Material.Id != undefined) {
      if (this.Material.IsMaterialList)
      {
        this._dataService.put('/api/materiallist/UpdateByEntryData', JSON.stringify(this.Material))
        .subscribe((response: any) => {
          this.loadData();
          this.modalAddEdit.hide();
          this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
        }, error => this._dataService.handleError(error));
      }
      else
      {
        this._dataService.put('/api/material/update', JSON.stringify(this.Material))
        .subscribe((response: any) => {
          this.loadData();
          this.modalAddEdit.hide();
          this._notificationService.printSuccessMessage(MessageContstants.UPDATED_OK_MSG);
        }, error => this._dataService.handleError(error));
      }
    }
  }

  public pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadData();
  }
}
