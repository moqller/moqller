import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { reviewmateriallistComponent } from './reviewmateriallist.component';
import { Routes, RouterModule } from '@angular/router';
import { DataService } from '../../core/services/data.service';

import { UtilityService } from '../../core/services/utility.service';
import { AuthenService } from '../../core/services/authen.service';
import { NotificationService } from '../../core/services/notification.service';
import { AuthGuard } from '../../core/guards/auth.guard';
import { PaginationModule  } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
import { Daterangepicker } from 'ng2-daterangepicker';
import { ModalModule } from 'ngx-bootstrap/modal';

const reviewmateriallistRoutes: Routes = [
  //localhost:4200/dataentry
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: '', component: reviewmateriallistComponent }
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule,
    Daterangepicker,
    ModalModule.forRoot(),
    RouterModule.forChild(reviewmateriallistRoutes)
  ],
  declarations: [reviewmateriallistComponent],
  providers: [UtilityService, AuthenService, NotificationService, DataService]
})
export class ReviewMaterialListModule {}
