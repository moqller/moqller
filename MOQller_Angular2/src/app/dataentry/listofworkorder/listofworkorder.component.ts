import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../core/services/data.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NotificationService } from '../../core/services/notification.service';
import { UploadService } from '../../core/services/upload.service';
import { AuthenService } from '../../core/services/authen.service';
import { UtilityService } from '../../core/services/utility.service';

import { MessageContstants } from '../../core/common/message.constants';
import { SystemConstants } from '../../core/common/system.constants';
import { ListOfWorkOrder } from '../../core/domain/listofworkorder';
import { LoggedInUser } from '../../core/domain/loggedin.user';

@Component({
  selector: 'app-listofworkorder',
  templateUrl: './listofworkorder.component.html',
  styleUrls: ['./listofworkorder.component.css']
})
export class listofworkorderComponent implements OnInit {
  public keyWord: string = 'List Of Work Order';
  private user: LoggedInUser;
  public entitys: ListOfWorkOrder[];
  public entitys_old = [];
  public showWorkOrder: boolean = false;
  public workorders: any;
  public workorder: any;
  public partNumber: number = 1;
  public ShippingStatus: any[];
  public description: string = '';
  public idStock: number = 0;
  public ConditionEnum = [{"Id":1,"Name":"New"},{"Id":2,"Name":"Used"},{"Id":3,"Name":"Refurbished"},{"Id":4,"Name":"New other"}];
  public currentWorkOrder: number ;
  public base64textString: any = "data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QBaRXhpZgAATU0AKgAAAAgABQMBAAUAAAABAAAASgMDAAEAAAABAAAAAFEQAAEAAAABAQAAAFERAAQAAAABAAALE1ESAAQAAAABAAALEwAAAAAAAYagAACxj//bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIATsBpAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKivb6HTbV57iaK3hjGXkkcKqj3J4FJqOoQ6Vp891cP5dvbRtLI+CdqqMk4HPAHavlv4k/Ei++I+uvcXDslrGxFtbg/LCvbjux7nv7AAAA96ufj34RtLh421iMtGcEpBK6n6MqkH6g0z/hoPwh/0F/8AyVm/+Ir5mooA+mf+Gg/CH/QX/wDJWb/4ij/hoPwh/wBBf/yVm/8AiK+ZqKAPpn/hoPwh/wBBf/yVm/8AiKP+Gg/CH/QX/wDJWb/4ivmaigD6Z/4aD8If9Bf/AMlZv/iKP+Gg/CH/AEF//JWb/wCIr5mooA+mf+Gg/CH/AEF//JWb/wCIo/4aD8If9Bf/AMlZv/iK+ZqKAPpn/hoPwh/0F/8AyVm/+Io/4aD8If8AQX/8lZv/AIivmaigD6Z/4aD8If8AQX/8lZv/AIij/hoPwh/0F/8AyVm/+Ir5mooA+mf+Gg/CH/QX/wDJWb/4ij/hoPwh/wBBf/yVm/8AiK+ZqKAPpn/hoPwh/wBBf/yVm/8AiKP+Gg/CH/QX/wDJWb/4ivmaigD6Z/4aD8If9Bf/AMlZv/iKP+Gg/CH/AEF//JWb/wCIr5mooA+sfDHxH0Pxi23TdSt7iTn90SY5DjqdjANj3xituvjRHaNwykqynII6g19A/s8/FWbxnp82l6jKZtQsUDpKwO6eLIGWPdlJAJPJBHU5NAHpVFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAch8ebuSy+EusPC7RsyRxkjurSorD8VJH418xV9M/tB/8kh1f/tj/AOjo6+ZqAPZfgZ8FtE8UeDV1PVIZLyS6kZUTzWjWJVOP4SCSTk5J9OOpPaf8M+eEP+gR/wCTU3/xdQfs5f8AJKLH/rpL/wCjGruqAOM/4Z88If8AQI/8mpv/AIuj/hnzwh/0CP8Ayam/+Lrs6xtc+Ieh+GpXjvtVsbeaPhojKDIvGeVGW6e1AGL/AMM+eEP+gR/5NTf/ABdH/DPnhD/oEf8Ak1N/8XWjpfxZ8N6xJtg1qx3ZAAkk8osfbdjP4V0QORQBxn/DPnhD/oEf+TU3/wAXR/wz54Q/6BH/AJNTf/F12dFAHGf8M+eEP+gR/wCTU3/xdH/DPnhD/oEf+TU3/wAXXZ0UAcZ/wz54Q/6BH/k1N/8AF0f8M+eEP+gR/wCTU3/xddnRQBxn/DPnhD/oEf8Ak1N/8XR/wz54Q/6BH/k1N/8AF12dFAHGf8M+eEP+gR/5NTf/ABdH/DPnhD/oEf8Ak1N/8XXZ0UAcZ/wz54Q/6BH/AJNTf/F0f8M+eEP+gR/5NTf/ABddnRQBxn/DPnhD/oEf+TU3/wAXSN+z34QK/wDIJx7i6m4/8frtKKAPkz4heGY/BvjTUNNikeaK1lwjP94qQGGfcA4zxn0Fb37O13JbfFrTkjdlW4SaOQD+NfKZsH/gSqfwqp8c/wDkq+sf9dE/9FrU/wCz5/yV7SP+23/omSgD6ZooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDjP2g/+SQ6v/wBsf/R0dfM1fTP7Qf8AySHV/wDtj/6Ojr5moA+kv2cv+SUWP/XSX/0Y1d1XC/s5f8kosf8ArpL/AOjGrpPHutP4d8FarfRt5c1vayPE2M7X2nbx/vYoA8r+O3xznF9NouizzW32eQpd3KfK7sP4EPUAHIJ4JIwOM7vHaKKACu2+EHxgvPAGrQ29xNJNosh2ywtlvIBOd6ehBOSBwQTxnBHE0UAfZNvcR3lvHNDIksUqh0dG3K6nkEEdQfWn157+zX4obXfh6trKWaTS5TAGZixZD8y/lkqB2CivQqACiiigAooooAKKKo6v4msdCvbG3uriOGbUpTDArMAXbBP+A+rKO4oAvUUUUAFFFFABRRRQB8v/ABz/AOSr6x/10T/0WtT/ALPn/JXtI/7bf+iZKg+Of/JV9Y/66J/6LWp/2fP+SvaR/wBtv/RMlAH0zRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHGftB/8AJIdX/wC2P/o6Ovmavpn9oP8A5JDq/wD2x/8AR0dfM1AH0l+zl/ySix/66S/+jGrc+KWlf218Otat1V2ZrR3RUGWZlG5QB7kAVh/s5f8AJKLH/rpL/wCjGruqAPjOiu++MnwYuvAd9Pf2kfmaLNL8hQlmtc4wr56DJwDk5wMnJ54GgAoore+Hnw9vviNri2lopSFCDcXBXKW6nufUnBwO+OwBIAPYv2XPD0mm+CLi/k/5iU5MfPVEyuf++t/5V6ZVXQ9Gh8PaNa2NuGEFnEsSZ6kAYyfc9T71aoAKKKKACiiub+I3xQ034b6aZLp/Nu5ELW9qh+eY9Ov8K+pPocZPFAEfxN+Klj8M9NWSZftN5N/qbVX2s47knB2r746183eL/F99441yXUNQl8yaThVHCRL2RR2Uf4k5JJLfFfim88Z6/calfMrXFwckKNqoBwFUegHHr6knJrOoA9j+C/7QEn2mPSvEFxvR8Jb3kmAUOMBZD3B/vHnPUnOR7TXxnXs3wV+P6Qw2eia1tjWNRDBek4UAYCK4/Td9Mjq1AHs1FFFABRRRQB8v/HP/AJKvrH/XRP8A0WtT/s+f8le0j/tt/wCiZKg+Of8AyVfWP+uif+i1qf8AZ8/5K9pH/bb/ANEyUAfTNFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAcZ+0H/ySHV/+2P8A6Ojr5mr6Z/aD/wCSQ6v/ANsf/R0dfM1AH0l+zl/ySix/66S/+jGruq4X9nL/AJJRY/8AXSX/ANGNXdUANnhS5heORFkjkUqysMqwPBBHpXB+J/2cfDfiCRpIIptMmYlibZ/kJP8AsNkAey4rvqKAPNdB/Zf0DTZi95LeakMYCO/loP8AvnB/Wu+0TQbPw3p62thaw2lunRI12gnAGT6ngZJ5NXKM0AFFFFABRRXM/FP4k2/w08O/anVZrqcmO2g3Y3tjknvtHGSPUDuKAJviN8RrH4b6Gbq6PmTSZW3t1OHuG/oo4y3b3JAPzB4h8Q3ninV5r6+mae4nOWY9vQAdgOwp3iTxPfeLtVkvNQuJLid+AWPCDJO1R2UZPA45qhQAUUUUAFFFFAHtHwC+NcMVmuia1deW0f8Ax6XMzgJtA/1TE9MYOCTznHGAD7JXxnXsf7P/AMaJPtNv4f1WTej/ALuzuHYAoccRsT1B6L3zgc5GAD2miiigD5f+Of8AyVfWP+uif+i1qf8AZ8/5K9pH/bb/ANEyVB8c/wDkq+sf9dE/9FrU/wCz5/yV7SP+23/omSgD6ZooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDjP2g/+SQ6v/wBsf/R0dfM1fTP7Qf8AySHV/wDtj/6Ojr5moA+kv2cv+SUWP/XSX/0Y1d1XC/s5f8kosf8ArpL/AOjGruqACiisvxf4x0/wPo0l9qE3lxLwqjl5W7Ko7k/l3OBzQA3xn4zsfAmhyX9/JtjXhEH35m7Ko7k/p1PFfOfi/wCL+seKfF8OrLcSWbWbE2cUbfLbD/2Yn+IkfN0xjAFb4j/Ee++JOuG6uj5cEeVt7dTlIF/qx4y3f2AAHPUAfSnwn+Nll8RY0tZlFpq6R7niP+rmx1MZzn3weRnuATXcV8eaPrF14f1OG8s5nt7q3bfHInVT/UHoQeCCQeK900j9p3S5PBbXd5Gy6vCNhs4wcTv2KtyAp6nPK4I+bjcAdL8U/inZ/DXSNzbZ9QnB+z2+fvf7Teij9eg9vnDxf4vvvHGuS6hqEvmTScKo4SJeyKOyj/EnJJJPF/i++8ca5LqGoS+ZNJwqjhIl7Io7KP8AEnJJJzKACiiigAooooAKKKKACiiigD2v9nr4wX2s6iug6k0t47Kz29y7bpBgZKuTyR1weo6dMY9hr5t/Zy/5KvY/9c5f/RbV9JUAfL/xz/5KvrH/AF0T/wBFrU/7Pn/JXtI/7bf+iZKg+Of/ACVfWP8Aron/AKLWp/2fP+SvaR/22/8ARMlAH0zRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHGftB/8kh1f/tj/wCjo6+Zq+mf2g/+SQ6v/wBsf/R0dfM1AH0l+zl/ySix/wCukv8A6Mau6rhf2cv+SUWP/XSX/wBGNXWeJ/EVv4T0C61K63eRaIXYKMs3YAe5JA9OaAI/Fvi2x8E6HLqGoS+XDHwAOXlbsijux/xJwATXzH8RPiFe/EbX2vLpikK5W3gDZWBPQepPc9/pgA+IPxE1D4ja011eNthQkQW6n5IFPYepOBk9/pgDBoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAO6/Zy/5KvY/9c5f/AEW1fSVfNv7OX/JV7H/rnL/6LavpKgD5f+Of/JV9Y/66J/6LWp/2fP8Akr2kf9tv/RMlQfHP/kq+sf8AXRP/AEWtT/s+f8le0j/tt/6JkoA+maKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA4z9oP8A5JDq/wD2x/8AR0dfM1fTP7Qf/JIdX/7Y/wDo6OvmagD6S/Zy/wCSUWP/AF0l/wDRjVe+Of8AySjWP+uaf+jFqj+zl/ySix/66S/+jGq98c/+SUax/wBc0/8ARi0AfL9FFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB3X7OX/JV7H/AK5y/wDotq+kq+bf2cv+Sr2P/XOX/wBFtX0lQB8v/HP/AJKvrH/XRP8A0WtT/s+f8le0j/tt/wCiZKg+Of8AyVfWP+uif+i1qf8AZ8/5K9pH/bb/ANEyUAfTNFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAcZ+0H/ySHV/+2P8A6Ojr5mr6g+Oeny6n8KNZjhXcyxrKR/so6ux/BVJr5foA+kv2cv8AklFj/wBdJf8A0Y1dxJGsqFWVWU9QRkGvAfhR8f4/AHhkaZdafJdJHIzxSRyBSA3O0gj1yc574xxXTf8ADWWn/wDQJvP+/q0Aeqf2bb/8+8P/AHwKP7Nt/wDn3h/74FeV/wDDWWn/APQJvP8Av6tH/DWWn/8AQJvP+/q0Aeqf2bb/APPvD/3wKP7Nt/8An3h/74FeV/8ADWWn/wDQJvP+/q0f8NZaf/0Cbz/v6tAHqn9m2/8Az7w/98Cj+zbf/n3h/wC+BXlf/DWWn/8AQJvP+/q0f8NZaf8A9Am8/wC/q0Aeqf2bb/8APvD/AN8Cj+zbf/n3h/74FeV/8NZaf/0Cbz/v6tH/AA1lp/8A0Cbz/v6tAHqn9m2//PvD/wB8Cj+zbf8A594f++BXlf8Aw1lp/wD0Cbz/AL+rR/w1lp//AECbz/v6tAHqn9m2/wDz7w/98Cj+zbf/AJ94f++BXlf/AA1lp/8A0Cbz/v6tH/DWWn/9Am8/7+rQB6p/Ztv/AM+8P/fAo/s23/594f8AvgV5X/w1lp//AECbz/v6tH/DWWn/APQJvP8Av6tAHqn9m2//AD7w/wDfAo/s23/594f++BXlf/DWWn/9Am8/7+rR/wANZaf/ANAm8/7+rQB6tHZQwvuSGNW9QgBqSvJf+GstP/6BN5/39Whv2stPCnbpF4W7AyrQB5z8c/8Akq+sf9dE/wDRa1P+z5/yV7SP+23/AKJkrn/GXiaTxj4ovtTkjWFryQuEBzsHQDPfAAGcDNdP+zlp8t78V7GSNdy2ccssh/uqUKZ/76dR+NAH0lRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFADZoluImjkVZI5AVZWGQwPUEV83/ABh+Dd18P7+W7t087RppP3Tg5aDPIRu/HQHoeO5xX0lRQB8Z0V9aP8PPD8rszaHo7MxySbKMkn8qb/wrjw9/0AdF/wDAKL/4mgD5Nor6y/4Vx4e/6AOi/wDgFF/8TR/wrjw9/wBAHRf/AACi/wDiaAPk2ivrL/hXHh7/AKAOi/8AgFF/8TR/wrjw9/0AdF/8Aov/AImgD5Nor6y/4Vx4e/6AOi/+AUX/AMTR/wAK48Pf9AHRf/AKL/4mgD5Nor6y/wCFceHv+gDov/gFF/8AE0f8K48Pf9AHRf8AwCi/+JoA+TaK+sv+FceHv+gDov8A4BRf/E0f8K48Pf8AQB0X/wAAov8A4mgD5Nor6y/4Vx4e/wCgDov/AIBRf/E0f8K48Pf9AHRf/AKL/wCJoA+TaK+sv+FceHv+gDov/gFF/wDE0f8ACuPD3/QB0X/wCi/+JoA+TaK+sv8AhXHh7/oA6L/4BRf/ABNH/CuPD3/QB0X/AMAov/iaAPk2ivrL/hXHh7/oA6L/AOAUX/xNH/CuPD3/AEAdF/8AAKL/AOJoA+VdK0u41vUYbS1hae4uHCRovVif89T0r6O+Cnwp/wCFb6JJJdeXJql5gzMvIiUdEB/UkdT64BrqtJ8P2GgI62NjZ2SyEFxBCsYYj12gZq5QAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9k=";
  
  public dateOptions: any = {
    locale: { format: 'MM-DD-YYYY' },
    alwaysShowCalendars: false,
    singleDatePicker: true,
    autoUpdateInput: false
  };

  constructor(
    private _notificationService: NotificationService,
    private _dataService: DataService,
    private _router: Router,
    private _utilityService: UtilityService) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.user == null)
    {
      this._router.navigate(['/home/login'])
    }

    this.loadworkorder();
    this.LoadShippingStatus();
  }

  SendMail(form: NgForm)
  {
    this._dataService.get('/api/materialTempData/sendmail?toEmail=' + this.workorder.SellerEmail + '&userName=' + this.workorder.SellerName + '&Content=' + this.description)
      .subscribe((response: Response) => {
        this._notificationService.printSuccessMessage("Email is sent successfully!");
      }, error => {
        this._dataService.handleError(error);
      });
  }

  loadworkorder()
  {
    this._dataService.get('/api/materialTempData/getwork')
    .subscribe((response: any) => {
        this.workorders = response;
    });
  }

  Save(form: NgForm) {
    if(!form.valid)
    {
      this._notificationService.printErrorMessage("Some columns are missing. Please fill up before you save");
    }
    else
    {
      if (typeof this.entitys !== 'undefined'  && this.entitys.length > 0)
      {
        this._notificationService.printConfirmationDialog("You can’t edit material information after saving. Please review carefully!", () => this.SaveData(this.entitys));
      }
    }
  }

  SaveData(entitys, showmes: boolean = true)
  {
    this._dataService.post('/api/materiallist/createbydataentry', JSON.stringify(entitys))
    .subscribe((response: any) =>{
      if (showmes == true)
      {
        
        this._notificationService.printSuccessMessage("Add Material successfully");
        this.entitys = [];
        this.LoadMaterial(this.workorder.Id);
      }
    },error =>this._dataService.handleError(error)
    );
  }
  

  AddNewRecord(form: NgForm){
    if (typeof this.entitys !== 'undefined')
    {
      if(!form.valid)
      {
        this._notificationService.printErrorMessage("Some columns are missing. Please fill up before you add new");
      }
      else
      {
        if (this.CheckInStockOrStatusDate(this.entitys[this.entitys.length-1]))
        {
          this.RegetInStock(this.entitys[this.entitys.length-1]);
          this.entitys.push(new ListOfWorkOrder('','','','','','',0,'','','','','',this.workorder.SellerName,this.workorder.Id));
        }
        else
        {
          this._notificationService.printErrorMessage("DueDate or Shipping Status is required");
        }
      }
    }
    else
    {
      this.entitys = [new ListOfWorkOrder('','','','','','',0,'','','','','',this.workorder.SellerName,this.workorder.Id)];
    }
  }

  RemoveItem(index){
    this.entitys.splice(index, 1);
  }

  changeDueDate(value: any, i) {
    this.entitys[i].DueDate = moment(new Date(value.end._d)).format('MM-DD-YYYY');
  }

  LoadInfomation(Id: number)
  {
    if ( typeof this.entitys !== 'undefined' && this.entitys.length > 0)
    {
      this._notificationService.printConfirmationDialog("You have already filled out some information and they have not been saved. You agree to remove such information!!!", () => this.LoadInformationSure(Id));
    }
    else
    {
      this.currentWorkOrder = Id;
      this.LoadInformationSure(Id);
    }
  }

  LoadInformationSure(Id: number)
  {
    this._dataService.get('/api/materialTempData/detail/' + Id)
      .subscribe((response: any) => {
        this.showWorkOrder = true;
        this.workorder = response;
        this.LoadMaterial(this.workorder.Id);
      });
  }

  LoadShippingStatus(){
    this._dataService.get('/api/shippingstatus/getall')
    .toPromise().then((response: any) =>{
      this.ShippingStatus = response;
    }).then(()=>{
      let StockElement = this.ShippingStatus.find(m => m.Code === 'STOCK');
      if(StockElement == undefined)
      {
        this._notificationService.printErrorMessage("Status doesn't exist in Status list. Please, feedback to admin");
      }
        else
      {
          this.idStock = StockElement.Id;
      }
    });
  }

  LoadMaterial(Id: number)
  {
    this._dataService.get('/api/materiallist/getbyworkorder/'+ Id)
    .subscribe((response: any) => {
      this.entitys_old = response;
      this.entitys = undefined;
    });
  }

  RegetInStock(item:any)
  {
    this._dataService.post('/api/materiallist/getinstockbyorderwork', JSON.stringify(item)).subscribe(m => {
      item.InStock = m;
    })
  }

  CheckInStockOrStatusDate(item: any)
  {
    return ((item.ShippingStatusId !== this.idStock && item.DueDate == "") || item.ShippingStatusId === 0) ? false : true;
  }

  complete(form: NgForm)
  {
    if(!form.valid)
    {
      this._notificationService.printErrorMessage("Some columns are missing. Please fill up before you complete work order");
    }
    else
    {
      this._notificationService.printConfirmationDialog("You can’t edit material information after compeleting. Please review carefully!", () => this.CallComplete());
    }
  }

  CallComplete()
  {
    if ( typeof this.entitys !== 'undefined' && this.entitys.length > 0)
    {
      this.SaveData(this.entitys, false);
    }
    
    this._dataService.post('/api/materialTempData/completeorderwork/'+ this.workorder.Id)
    .subscribe((response: any) => {
      this.loadworkorder();
      this.workorder = [];
      this.entitys = undefined;
      this.showWorkOrder = false;
    });
  }

  public inputFileClick(event: any, item:any) {
    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      let file: File = fileList[0];
      item.FileName = file.name;
      let reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        item.ImagePartNumber = btoa(e.target.result);
      }
      reader.readAsBinaryString(file);
    }
  }

  public browse(Idindex: number) {
    let el = document.getElementById(Idindex.toString());
    if (el) {
      el.click();
    }
  }

  public getImage(FileName: string, base64Content: string): string | null {
    if (FileName) {
      if (base64Content) {
        let fn: string = FileName.toLowerCase();
        if (fn.endsWith(".png")) return "data:image/png;base64," + base64Content;
        else if (fn.endsWith(".jpg")) return "data:image/jpeg;base64," + base64Content;
        else if (fn.endsWith(".jpeg")) return "data:image/jpeg;base64," + base64Content;
        else if (fn.endsWith(".gif")) return "data:image/gif;base64," + base64Content;
        else if (fn.endsWith(".bmp")) return "data:image/bmp;base64," + base64Content;
        else if (fn.endsWith(".webp")) return "data:image/webp;base64," + base64Content;
      }
    }
    return null;
  }

}

