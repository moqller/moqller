import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { DataService } from './../../core/services/data.service';
import { UtilityService } from '../../core/services/utility.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.css']
})
export class SidebarMenuComponent implements OnInit {
  public functions: any[];
  public a = true;
  constructor(private dataService: DataService, private utilityService: UtilityService, private renderer: Renderer, private elementRef: ElementRef) { }

  ngOnInit() {
    this.dataService.get('/api/function/getlisthierarchy').subscribe((response: any[]) => {
      this.functions = response.sort((n1, n2) => {
        if (n1.DisplayOrder > n2.DisplayOrder)
          return 1;
        else if (n1.DisplayOrder < n2.DisplayOrder)
          return -1;
        return 0;
      });
    }, error => {

      //this.dataService.handleError(error)
    });



  }
  onMenuClick() {
    //alert(document.body.className);
    if(document.body.className== "nav-sm")
    {
      if(document.getElementById('treeFunction') != null)
      {
        $('BODY').toggleClass('nav-md nav-sm');
      }
    }
    
  }
  

}
