import { Component, AfterViewChecked, ElementRef, OnInit, DoCheck, OnDestroy, Renderer } from '@angular/core';
import { LoggedInUser } from './core/domain/loggedin.user';
import { CartService } from './core/services/cart.service';
import { SystemConstants } from './core/common/system.constants';
import { UtilityService } from './core/services/utility.service';
import { UrlConstants } from './core/common/url.constants';
import { SlimLoadingBarService, SlimLoadingBarEvent } from 'ng2-slim-loading-bar';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import * as $ from 'jquery';
//import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
//import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ShareService } from './core/services/share.service';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewChecked, OnInit, DoCheck, OnDestroy {
  private sub: any;
  private user: LoggedInUser;
  public viewLogin = true;
  public viewManagement = false;
  public viewSeller: boolean;
  public viewDataEntry: boolean = false;
  public runone = false;
  public lastTime: any;
  public selectBomId: string = "0";
  public empty: string = "";
  public isScrollDown: any;
  public CountItemCart: number;
  public date = new Date();

  constructor(private elementRef: ElementRef, private router: Router, private utilityService: UtilityService, private slimLoadingBarService: SlimLoadingBarService, private renderer: Renderer, private _http: Http,
    public _cartservice: CartService,
    public _shareService: ShareService, 
    public _cookieService: CookieService
    ) {
    // Listen the navigation events to start or complete the slim bar loading
    this.sub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.slimLoadingBarService.start();
      } else if (event instanceof NavigationEnd ||
        event instanceof NavigationCancel ||
        event instanceof NavigationError) {
        this.slimLoadingBarService.complete();
      }
    }, (error: any) => {
      this.slimLoadingBarService.complete();
    });
    this._shareService.getSeller().subscribe(m=> this.viewSeller = m);
    this._shareService.getBomId().subscribe(m=> this.selectBomId = m.toString());
  }

  startLoading() {
    this.slimLoadingBarService.start(() => {
    });
  }

  stopLoading() {
    this.slimLoadingBarService.stop();
  }

  completeLoading() {

    this.slimLoadingBarService.complete();
  }


  onSellerClick()
  {
      this.viewSeller = !this.viewSeller;
      this._shareService.setSeller(this.viewSeller);
    }

  onBuyerClick()
  {
      this.viewSeller = !this.viewSeller;
      this._shareService.setSeller(this.viewSeller);
    }

  ngOnInit() {
    this._cartservice.cast.subscribe(
      CountItems => this.CountItemCart = CountItems);
      
  }

  ngDoCheck() {
    this.user = JSON.parse(localStorage.getItem(SystemConstants.CURRENT_USER));
    if (this.user && this.user.access_token) {
      this.viewLogin = false;
      if (this.lastTime == undefined) {
        this.lastTime = new Date();
      }
      let currentDate: any = new Date();

      let diffInMs: number = Date.parse(currentDate) - Date.parse(this.lastTime);
      if (diffInMs >= 1800000) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        header.delete("Authorization");
        header.append("Authorization", "Bearer " + this.user.access_token);
        let options = new RequestOptions({ headers: header });

        this._http.get(SystemConstants.BASE_API + '/api/country/checkauthen', options)
          .subscribe((response: any) => {
            this.viewLogin = false;
          }, error => {
            this.viewLogin = true;
            localStorage.removeItem(SystemConstants.CURRENT_USER);
            this.utilityService.navigate(UrlConstants.LOGIN);
            this.viewSeller = false;
            $('BODY').removeClass('seller');
          });
      }
      this.lastTime= new Date();
    }
    else {
      this.viewLogin = true;
      this.viewSeller = false;
      $('BODY').removeClass('seller');
    }
    //An hien nut management
    if (this.user) {
      var permission: any[] = JSON.parse(this.user.permissions);

      var roles: any[] = JSON.parse(this.user.roles);
      var hasPermission: number = permission.findIndex(x => x.CanRead == true);

      if (hasPermission != -1 || roles.findIndex(x => x == "DataEntry") != -1) {
        this.viewDataEntry = true;
      }

      if (hasPermission != -1 || roles.findIndex(x => x == "Admin") != -1) {
        this.viewManagement = true;
      }
      else
        this.viewManagement = false;
    }
    else {
      this.viewManagement = false;
      this.viewDataEntry = false;
    }

  }


  logout() {
    localStorage.removeItem(SystemConstants.CURRENT_USER);

    this.utilityService.navigate(UrlConstants.LOGIN);
    this.viewSeller = false;
    $('BODY').removeClass('seller');
  }
  
  ngAfterViewChecked() {

    var existsScript = document.getElementById("customJS");
    if (existsScript != null) {
      this.elementRef.nativeElement.removeChild(existsScript);
    }
    else {
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.src = "./assets/js/custom.js";
      s.id = "customJS";
      this.elementRef.nativeElement.appendChild(s);
    }
    if (document.body.className == "nav-sm") {
      if(document.getElementById('treeFunction') == null)
      {
        $('BODY').toggleClass('nav-md nav-sm');
      }

    }
  }
  ngOnDestroy(): any {
    this.sub.unsubscribe();
    this._shareService.setSeller(this.viewSeller);
  }
}
