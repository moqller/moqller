import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';
import { AuthGuard } from './core/guards/auth.guard';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { UtilityService } from './core/services/utility.service';
import { AuthenService } from './core/services/authen.service';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { CookieModule } from 'ngx-cookie';
import { ShareService } from 'app/core/services/share.service';
import { DataService } from './core/services/data.service';
import { CartService } from './core/services/cart.service';
import { NotificationService } from './core/services/notification.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CookieModule.forRoot(),
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    SlimLoadingBarModule.forRoot(),
    PaginationModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [AuthGuard,UtilityService,AuthenService, ShareService, DataService, NotificationService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
