webpackJsonp(["material.module"],{

/***/ "../../../../../src/app/main/material/material.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/material/material.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Materials List</h3>\n  </div>\n\n  <div class=\"title_right\">\n    <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n      <div class=\"input-group\">\n        <input type=\"text\" class=\"form-control\" name=\"filter\" (keyup.enter)=\"loadData()\" [(ngModel)]=\"filter\" placeholder=\"Search...\">\n        <span class=\"input-group-btn\">\n          <button class=\"btn btn-default\" (click)=\"loadData()\" type=\"button\">Go</button>\n        </span>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"clearfix\"></div>\n<div class=\"row\">\n  <div class=\"col-md-12 col-sm-12 col-xs-12\">\n    <div class=\"x_panel\">\n      <div class=\"x_title\">\n        <ul class=\"nav navbar-right panel_toolbox\">\n          <li>\n            <button class=\"btn btn-success\" (click)=\"showAddModal()\">Add new</button>\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"x_content\">\n\n        <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th>Part Number</th>\n              <th>Description</th>\n              <th>Manufacturer</th>\n              <th>IntNumber</th>\n              <th>Description Detail</th>\n\n              <th></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let material of materials\">\n              <td>{{material.PartNumber}}</td>\n              <td>{{material.Description}}</td>\n              <td>{{material.Manufacturer}}</td>\n              <td>{{material.IntNumber}}</td>\n              <td>{{material.DescriptionDetail}}</td>\n              <td>\n                <button class=\"btn btn-primary\" (click)=\"showEditModal(material.Id)\">\n                  <i class=\"fa fa-pencil-square-o\"></i>\n                </button>\n                <button class=\"btn btn-danger\" (click)=\"deleteItem(material.Id)\">\n                  <i class=\"fa fa-trash\"></i>\n                </button>\n\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"col-md-12\">\n          <pagination [boundaryLinks]=\"true\" [itemsPerPage]=\"pageSize\" (pageChanged)=\"pageChanged($event)\" [totalItems]=\"totalRow\"\n            [(ngModel)]=\"pageIndex\" class=\"pagination-sm\" previousText=\"&lsaquo;\" nextText=\"&rsaquo;\" firstText=\"&laquo;\" lastText=\"&raquo;\"></pagination>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Modal add and edit-->\n<div bsModal #modalAddEdit=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Add/edit</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"modalAddEdit.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChange(addEditForm.valid)\"\n          *ngIf=\"entity\">\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">PartNumber</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"hidden\" [(ngModel)]=\"entity.Id\" name=\"id\" />\n              <input type=\"text\" #partNumber=\"ngModel\" [(ngModel)]=\"entity.PartNumber\" required minlength=\"3\" name=\"partNumber\" class=\"form-control\">\n              <small [hidden]=\"partNumber.valid || (partNumber.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input at lease 3 characters\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Description </label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" class=\"form-control\" #description=\"ngModel\" required [(ngModel)]=\"entity.Description\" name=\"description\"\n              />\n              <small [hidden]=\"description.valid || (description.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input Description\n              </small>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Manufacturer </label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" class=\"form-control\" #manufacturer=\"ngModel\" [(ngModel)]=\"entity.Manufacturer\" name=\"manufacturer\" />\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">IntNumber </label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" class=\"form-control\" #intNumber=\"ngModel\" [(ngModel)]=\"entity.IntNumber\" name=\"intNumber\" />\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n              <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Description Detail </label>\n              <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                <textarea class=\"form-control\" #descriptionDetail=\"ngModel\" [(ngModel)]=\"entity.DescriptionDetail\" name=\"descriptionDetail\" rows=\"5\" ></textarea> \n              </div>\n            </div>\n\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addEditForm.form.valid\">Update</button>\n              <button type=\"button\" (click)=\"modalAddEdit.hide()\" class=\"btn btn-primary\">Cancel</button>\n\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/material/material.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MaterialComponent = (function () {
    function MaterialComponent(_dataService, _notificationService) {
        this._dataService = _dataService;
        this._notificationService = _notificationService;
        this.pageIndex = 1;
        this.pageSize = 20;
        this.pageDisplay = 10;
        this.filter = '';
    }
    MaterialComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    MaterialComponent.prototype.loadData = function () {
        var _this = this;
        this._dataService.get('/api/material/getlistpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
            .subscribe(function (response) {
            _this.materials = response.Items;
            _this.totalRow = response.TotalRows;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    MaterialComponent.prototype.loadmaterial = function (id) {
        var _this = this;
        this._dataService.get('/api/material/detail/' + id)
            .subscribe(function (response) {
            _this.entity = response;
            //console.log(this.entity);
        });
    };
    MaterialComponent.prototype.pageChanged = function (event) {
        this.pageIndex = event.page;
        this.loadData();
    };
    MaterialComponent.prototype.showAddModal = function () {
        this.entity = {};
        this.modalAddEdit.show();
    };
    MaterialComponent.prototype.showEditModal = function (id) {
        this.loadmaterial(id);
        this.modalAddEdit.show();
    };
    MaterialComponent.prototype.saveChange = function (valid) {
        var _this = this;
        if (valid) {
            if (this.entity.Id == undefined) {
                this._dataService.post('/api/material/add', JSON.stringify(this.entity))
                    .subscribe(function (response) {
                    _this.loadData();
                    _this.modalAddEdit.hide();
                    _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
                }, function (error) {
                    if (error.status == 409) {
                        _this._notificationService.printErrorMessage("Code exists");
                    }
                    else {
                        _this._dataService.handleError(error);
                    }
                });
            }
            else {
                this._dataService.put('/api/material/update', JSON.stringify(this.entity))
                    .subscribe(function (response) {
                    _this.loadData();
                    _this.modalAddEdit.hide();
                    _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].UPDATED_OK_MSG);
                }, function (error) { return _this._dataService.handleError(error); });
            }
        }
    };
    MaterialComponent.prototype.deleteItem = function (id) {
        var _this = this;
        this._notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () { return _this.deleteItemConfirm(id); });
    };
    MaterialComponent.prototype.deleteItemConfirm = function (id) {
        var _this = this;
        this._dataService.delete('/api/material/delete', 'id', id).subscribe(function (response) {
            _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
            _this.loadData();
        });
    };
    return MaterialComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalAddEdit'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _a || Object)
], MaterialComponent.prototype, "modalAddEdit", void 0);
MaterialComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-material',
        template: __webpack_require__("../../../../../src/app/main/material/material.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/material/material.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */]) === "function" && _c || Object])
], MaterialComponent);

var _a, _b, _c;
//# sourceMappingURL=material.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/material/material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__material_component__ = __webpack_require__("../../../../../src/app/main/material/material.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_pagination__ = __webpack_require__("../../../../ngx-bootstrap/pagination/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var materialRoutes = [
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_2__material_component__["a" /* MaterialComponent */] }
];
var MaterialModule = (function () {
    function MaterialModule() {
    }
    return MaterialModule;
}());
MaterialModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_pagination__["a" /* PaginationModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["b" /* ModalModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["g" /* RouterModule */].forChild(materialRoutes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__material_component__["a" /* MaterialComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__core_services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_5__core_services_notification_service__["a" /* NotificationService */]]
    })
], MaterialModule);

//# sourceMappingURL=material.module.js.map

/***/ })

});
//# sourceMappingURL=material.module.chunk.js.map