webpackJsonp(["parameter.module"],{

/***/ "../../../../../src/app/main/parameter/parameter.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/parameter/parameter.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Parameters List</h3>\n  </div>\n\n  <div class=\"title_right\">\n    <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n      <div class=\"input-group\">\n        <input type=\"text\" class=\"form-control\" name=\"filter\" (keyup.enter)=\"loadData()\" [(ngModel)]=\"filter\" placeholder=\"Search...\">\n        <span class=\"input-group-btn\">\n          <button class=\"btn btn-default\" (click)=\"loadData()\" type=\"button\">Go</button>\n        </span>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"clearfix\"></div>\n<div class=\"row\">\n  <div class=\"col-md-12 col-sm-12 col-xs-12\">\n    <div class=\"x_panel\">\n      <div class=\"x_title\">\n        <ul class=\"nav navbar-right panel_toolbox\">\n          <li>\n            <button class=\"btn btn-success\" (click)=\"showAddModal()\">Add new</button>\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"x_content\">\n\n        <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th>Key</th>\n              <th>Value</th>\n              <th>Description</th>\n              <th></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let parameter of parameters\">\n              <td>{{parameter.Code}}</td>\n              <td>{{parameter.Name}}</td>\n              <td>{{parameter.Description}}</td>\n              <td>\n                <button  class=\"btn btn-primary\" (click)=\"showEditModal(parameter.Id)\">\n                  <i class=\"fa fa-pencil-square-o\"></i>\n                </button>\n                <button class=\"btn btn-danger\" (click)=\"deleteItem(parameter.Id)\">\n                  <i class=\"fa fa-trash\"></i>\n                </button>\n\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"col-md-12\">\n          <pagination [boundaryLinks]=\"true\" [itemsPerPage]=\"pageSize\" (pageChanged)=\"pageChanged($event)\" [totalItems]=\"totalRow\"\n            [(ngModel)]=\"pageIndex\" class=\"pagination-sm\" previousText=\"&lsaquo;\" nextText=\"&rsaquo;\" firstText=\"&laquo;\" lastText=\"&raquo;\"></pagination>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Modal add and edit-->\n<div bsModal #modalAddEdit=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Add/edit</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"modalAddEdit.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChange(addEditForm.valid)\"\n          *ngIf=\"entity\">\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Key</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"hidden\" [(ngModel)]=\"entity.Id\" name=\"id\" />\n              <input type=\"text\" #code=\"ngModel\" [(ngModel)]=\"entity.Code\" required  name=\"code\" class=\"form-control\">\n              <small [hidden]=\"code.valid || (code.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input Key\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Value </label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" class=\"form-control\" #name=\"ngModel\" required [(ngModel)]=\"entity.Name\" name=\"name\">\n              <small [hidden]=\"name.valid || (name.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input Value\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Description </label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <textarea class=\"form-control\" #description=\"ngModel\" [(ngModel)]=\"entity.Description\" name=\"description\" rows=\"3\"></textarea>\n            </div>\n          </div>\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addEditForm.form.valid\">Update</button>\n              <button type=\"button\" (click)=\"modalAddEdit.hide()\" class=\"btn btn-primary\">Cancel</button>\n\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/parameter/parameter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParameterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ParameterComponent = (function () {
    function ParameterComponent(_dataService, _notificationService) {
        this._dataService = _dataService;
        this._notificationService = _notificationService;
        this.pageIndex = 1;
        this.pageSize = 20;
        this.pageDisplay = 10;
        this.filter = '';
    }
    ParameterComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    ParameterComponent.prototype.loadData = function () {
        var _this = this;
        this._dataService.get('/api/parameter/getlistpaging?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&filter=' + this.filter)
            .subscribe(function (response) {
            _this.parameters = response.Items;
            _this.totalRow = response.TotalRows;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ParameterComponent.prototype.loadparameter = function (id) {
        var _this = this;
        this._dataService.get('/api/parameter/detail/' + id)
            .subscribe(function (response) {
            _this.entity = response;
            //console.log(this.entity);
        });
    };
    ParameterComponent.prototype.pageChanged = function (event) {
        this.pageIndex = event.page;
        this.loadData();
    };
    ParameterComponent.prototype.showAddModal = function () {
        this.entity = {};
        this.modalAddEdit.show();
    };
    ParameterComponent.prototype.showEditModal = function (id) {
        this.loadparameter(id);
        this.modalAddEdit.show();
    };
    ParameterComponent.prototype.saveChange = function (valid) {
        var _this = this;
        if (valid) {
            if (this.entity.Id == undefined) {
                this._dataService.post('/api/parameter/add', JSON.stringify(this.entity))
                    .subscribe(function (response) {
                    _this.loadData();
                    _this.modalAddEdit.hide();
                    _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
                }, function (error) {
                    if (error.status == 409) {
                        _this._notificationService.printErrorMessage("Key exists");
                    }
                    else {
                        _this._dataService.handleError(error);
                    }
                });
            }
            else {
                this._dataService.put('/api/parameter/update', JSON.stringify(this.entity))
                    .subscribe(function (response) {
                    _this.loadData();
                    _this.modalAddEdit.hide();
                    _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].UPDATED_OK_MSG);
                }, function (error) { return _this._dataService.handleError(error); });
            }
        }
    };
    ParameterComponent.prototype.deleteItem = function (id) {
        var _this = this;
        this._notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () { return _this.deleteItemConfirm(id); });
    };
    ParameterComponent.prototype.deleteItemConfirm = function (id) {
        var _this = this;
        this._dataService.delete('/api/parameter/delete', 'id', id).subscribe(function (response) {
            _this._notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
            _this.loadData();
        });
    };
    return ParameterComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('modalAddEdit'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _a || Object)
], ParameterComponent.prototype, "modalAddEdit", void 0);
ParameterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-parameter',
        template: __webpack_require__("../../../../../src/app/main/parameter/parameter.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/parameter/parameter.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */]) === "function" && _c || Object])
], ParameterComponent);

var _a, _b, _c;
//# sourceMappingURL=parameter.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/parameter/parameter.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParameterModule", function() { return ParameterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__parameter_component__ = __webpack_require__("../../../../../src/app/main/parameter/parameter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_pagination__ = __webpack_require__("../../../../ngx-bootstrap/pagination/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var parameterRoutes = [
    //localhost:4200/main/user
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    //localhost:4200/main/home/index
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_2__parameter_component__["a" /* ParameterComponent */] }
];
var ParameterModule = (function () {
    function ParameterModule() {
    }
    return ParameterModule;
}());
ParameterModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_pagination__["a" /* PaginationModule */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["b" /* ModalModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["g" /* RouterModule */].forChild(parameterRoutes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__parameter_component__["a" /* ParameterComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__core_services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_5__core_services_notification_service__["a" /* NotificationService */]]
    })
], ParameterModule);

//# sourceMappingURL=parameter.module.js.map

/***/ })

});
//# sourceMappingURL=parameter.module.chunk.js.map