webpackJsonp(["home.module"],{

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <div class=\"logo-home\">\n        <img src=\"../../../assets/images/logo-home.png\" alt=\"logo\" />\n        <div style=\"font-size: 16px;\">\n            unboxing your supply chain\n        </div>\n        <br/>\n        <div class=\"big-search\">\n\n            <form id=\"search\" method=\"get\" action=\"\" class=\"intro-big-search\">\n                <div class=\"center\">\n                    <input type=\"text\" placeholder=\"Enter keyword here...\" name=\"part\" id=\"part\" [(ngModel)]=\"keyWord\" autocomplete=\"off\" autofocus=\"\"\n                        class=\"search-field\">\n                    <button [disabled]=\"keyWord == ''\" [routerLink]=\"['/home/materiallist/index',keyWord]\" class=\"secondary-button\">Search</button>\n                </div>\n            </form>\n\n        </div>\n        <div class=\"pro-action\">\n\n            <div class=\"advance\">\n                <a routerLink=\"/home/advancesearch/index\" class=\"primary-button\">Advance Search</a>\n            </div>\n            <div class=\"upload\">\n                <form class=\"j-bom-upload\" action=\"\" method=\"post\" encoding=\"multipart/form-data\" target=\"bomiframe\">\n                    <div class=\"upload-button is-bigger j-upload-button\">\n                        <i class=\"icon-upload\">\n                            <img src=\"assets/images/icon-upload.png\" />\n                        </i>\n                        <a routerLink=\"/home/uploadbom/index\" class=\"primary-button\">Upload BOM</a>\n                        \n                        \n                    </div>\n                </form>\n            </div>\n\n        </div>\n        <!--end.pro-action-->\n        <!--end .big-search-->\n    </div>\n    <!--end.logo-home-->\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
        this.keyWord = '';
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/home.component.css")]
    }),
    __metadata("design:paramtypes", [])
], HomeComponent);

//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_signalr_service__ = __webpack_require__("../../../../../src/app/core/services/signalr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var homeRoutes = [
    //localhost:4200
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_2__home_component__["a" /* HomeComponent */] },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterModule' },
    { path: 'product', loadChildren: './product/product.module#ProductModule' },
    { path: 'materiallist', loadChildren: './materiallist/materiallist.module#MaterialListModule' },
    { path: 'advancesearch', loadChildren: './advancesearch/advancesearch.module#AdvanceSearchModule' },
    { path: 'uploadbom', loadChildren: './uploadbom/uploadbom.module#UploadBomModule' }
];
var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["g" /* RouterModule */].forChild(homeRoutes)
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__home_component__["a" /* HomeComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__core_services_utility_service__["a" /* UtilityService */], __WEBPACK_IMPORTED_MODULE_5__core_services_authen_service__["a" /* AuthenService */], __WEBPACK_IMPORTED_MODULE_6__core_services_signalr_service__["a" /* SignalrService */], __WEBPACK_IMPORTED_MODULE_7__core_services_notification_service__["a" /* NotificationService */]]
    })
], HomeModule);

//# sourceMappingURL=home.module.js.map

/***/ })

});
//# sourceMappingURL=home.module.chunk.js.map