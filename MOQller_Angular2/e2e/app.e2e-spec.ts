import { MOQllerAngular2Page } from './app.po';

describe('MOQller-angular2 App', () => {
  let page: MOQllerAngular2Page;

  beforeEach(() => {
    page = new MOQllerAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
