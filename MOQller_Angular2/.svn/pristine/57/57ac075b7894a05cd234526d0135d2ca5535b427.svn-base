import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { DataService } from '../../core/services/data.service';
import { NotificationService } from '../../core/services/notification.service';
import { ShareService } from '../../core/services/share.service';
import { AuthenService } from '../../core/services/authen.service';
import { Router } from '@angular/router';
import { UrlConstants } from '../../core/common/url.constants';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.css']
})
export class SellerComponent implements OnInit {
  public checkSeller: boolean;

  public orderStatistics : any;

  // Biểu đồ đường
  public LineEmptyData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  public LineChartData:any[];
  public LineChartLabels:string[] = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'];
  public LineChartType:string = 'bar';
  public LineChartOptions:any = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {             
              min: 0,
              beginAtZero:true
          }
      }]
    }
  };

  public LineChartColors:Array<any> =[
    {
      backgroundColor: '#f41515'
    },
    {
      backgroundColor: '#55afe2'
    }
  ];

  
  // Biểu đồ status invent
  public ChartDataStatusInvent:number[] = [];
  public ChartLabelsStatusInvent:string[] = ['Confirmed', 'Unconfirmed'];
  public ChartColorsStatusInven:any[] = [{ backgroundColor: ["#008000", "#e2e21f"] }];
  public ChartTypeStatusInvent:string = 'doughnut';
  public ChartOptionsStatusInvent = {
    animation: false,
    responsive: true,
    legend: {
      labels : {
          boxWidth: 20,
          fontSize: 12,
          padding: 5
      },
      position: 'bottom'
    }
  };
  
  //Biểu đồ Chart Day
  public ChartLabelsDay:string[] = ['0 - 30 Days','31 - 60 Days', '61 - 120 Days',  '> 120 Days' ];
  public ChartColorsDay:any[] = [{ backgroundColor: ["#008000", "#0000ff", "#ff0000", "#e2e21f"] }];
  public ChartDataDay:number[] = [];
  public ChartTypeDay:string ="doughnut";
  public ChartOptionsDay = {
    animation: false,
    responsive: true,
    legend: {
      labels : {
          boxWidth: 20,
          fontSize:10,
          padding:5
      },
      position: 'bottom'
    }
  };

  constructor(private _dataService: DataService, 
    private _notification: NotificationService, 
    private _share: ShareService, 
    private _user: AuthenService,
    private _router: Router) { 
      this._share.getSeller().subscribe(m=>{
        this.checkSeller =m
      })
    }

  ngOnInit() 
  {
    if(!this.checkSeller || !this._user.isUserAuthenticated())
    {
      return this._router.navigate([UrlConstants.HOME]);
    }
    this.LoadData();
  }

  LoadData()
  {
    this.LoadOrderStatistics();
    this.LoadOrderMonthStatistics();
    this.LoadCurrentInventory();
  }

  LoadOrderStatistics()
  {
    this._dataService.get('/api/statistic/getorderstatistic').subscribe(m => {
      this.orderStatistics = m;
    }, error=> this._dataService.handleError(error))
  }

  LoadOrderMonthStatistics()
  {
    this._dataService.get('/api/statistic/getordermonthstatistic').subscribe( m=> {
      let tempDelivered = m.find(m => m.Label ==='Delivered') ;
      let tempCancelled = m.find(m => m.Label ==='Cancelled') ;
      // Gán giá trị mặc định
      this.LineChartData = [];
      
      if(tempCancelled == null)
      {
        this.LineChartData.push({ data: this.LineEmptyData, label: 'Cancelled',pointStyle : 'line'});
      }
      else
      {
        var result1 = Object.keys(tempCancelled).map(function(key) {
          return tempCancelled[key];
        }).filter(i => i != 'Cancelled');

        this.LineChartData.push({data: result1, label: 'Cancelled',pointStyle : 'line' });
      }
      
      if(tempDelivered == null)
      { 
        this.LineChartData.push({data: this.LineEmptyData, label:'Delivered',pointStyle : 'line'});
      }
      else
      {
          var result = Object.keys(tempDelivered).map(function(key) {
            return tempDelivered[key];
          }).filter(i => i != 'Delivered');

          this.LineChartData.push({data: result, label: 'Delivered',pointStyle : 'line'});
      }
      
    });
  }

  LoadCurrentInventory(){
    this._dataService.get('/api/materiallist/getcurrentinventory').subscribe(m=>{
      this.ChartDataDay = [];
      this.ChartDataDay.push(m.Less30Day);
      this.ChartDataDay.push(m.From30To60Day);
      this.ChartDataDay.push(m.From60To120Day);
      this.ChartDataDay.push(m.To120Day);

      this.ChartDataStatusInvent = [];
      this.ChartDataStatusInvent.push(m.Confirm);
      this.ChartDataStatusInvent.push(m.Unconfirm);

      }, error => this._dataService.handleError(error));
    
  }

  GoToAddMaterial(){
    this._router.navigate(['home/seller/addmaterialtext/index']);
  }

  GoToOrder(){
    this._router.navigate(['home/sellerordermanagement/index']);
  }
  GoToInventory(){
    this._router.navigate(['home/seller/inventorymanagement']);
  }
  GoToTransaction(){
    this._router.navigate(['home/transactionhistory', '']);
  }

  FilterTransaction(filter:string){
    this._router.navigate(['home/transactionhistory', filter])
  }

  FilterOrder(filter:string){
    this._router.navigate(['home/sellerordermanagement', filter])
  }
}