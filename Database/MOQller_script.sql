USE [master]
GO
/****** Object:  Database [MOQller]    Script Date: 1/10/2018 3:05:08 PM ******/
CREATE DATABASE [MOQller] ON  PRIMARY 
( NAME = N'MOQller', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.DEV2008R2\MSSQL\DATA\MOQller.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MOQller_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.DEV2008R2\MSSQL\DATA\MOQller_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MOQller] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MOQller].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MOQller] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MOQller] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MOQller] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MOQller] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MOQller] SET ARITHABORT OFF 
GO
ALTER DATABASE [MOQller] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MOQller] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MOQller] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MOQller] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MOQller] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MOQller] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MOQller] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MOQller] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MOQller] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MOQller] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MOQller] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MOQller] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MOQller] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MOQller] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MOQller] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MOQller] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MOQller] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MOQller] SET RECOVERY FULL 
GO
ALTER DATABASE [MOQller] SET  MULTI_USER 
GO
ALTER DATABASE [MOQller] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MOQller] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MOQller', N'ON'
GO
USE [MOQller]
GO
/****** Object:  Table [dbo].[AppLog]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionType] [nvarchar](50) NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[Actor1] [nvarchar](50) NOT NULL,
	[Actor2] [nvarchar](50) NULL,
	[BriefContent] [nvarchar](200) NOT NULL,
	[Content] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BillingInfo]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](30) NOT NULL,
	[City] [nvarchar](100) NOT NULL,
	[State] [nvarchar](100) NULL,
	[Address2] [nvarchar](30) NOT NULL,
	[ZipCode] [nvarchar](10) NOT NULL,
	[CountryId] [int] NULL,
 CONSTRAINT [PK__BillingI__3214EC076477ECF3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__BillingI__1788CC4D6754599E] UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BOM]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BOM](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UploadDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BOMDataUploaded]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BOMDataUploaded](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BOMId] [bigint] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[URL] [nvarchar](100) NULL,
	[BOMData] [varbinary](max) NULL,
 CONSTRAINT [PK__BOMDataU__3214EC0759FA5E80] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BOMItem]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BOMItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BOMId] [bigint] NOT NULL,
	[PartNumber] [nvarchar](100) NOT NULL,
	[MaterialId] [bigint] NULL,
	[Manufacturer] [nvarchar](100) NULL,
	[Quantity] [int] NOT NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK__BOMItem__3214EC075629CD9C] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cart]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BuyerId] [int] NOT NULL,
	[TotalNet] [numeric](20, 6) NOT NULL,
	[TotalSellerFee] [numeric](20, 6) NOT NULL,
	[TotalBuyerFee] [numeric](20, 6) NOT NULL,
	[Total] [numeric](20, 6) NOT NULL,
	[ShippingMethodId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CartDetail]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CartId] [bigint] NOT NULL,
	[MaterialListId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [numeric](20, 6) NOT NULL,
	[ExtendedPrice] [numeric](20, 6) NOT NULL,
	[SellerFee] [numeric](20, 6) NOT NULL,
	[BuyerFee] [numeric](20, 6) NOT NULL,
	[SellerFeeRate] [numeric](20, 6) NOT NULL,
	[BuyerFeeRate] [numeric](20, 6) NOT NULL,
	[BuyerTotal] [numeric](20, 6) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Country]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[PhoneCode] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeliveryStatus]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DiliveryInfo]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiliveryInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](30) NOT NULL,
	[City] [nvarchar](100) NOT NULL,
	[State] [nvarchar](100) NULL,
	[Address2] [nvarchar](30) NOT NULL,
	[ZipCode] [nvarchar](10) NOT NULL,
	[CountryId] [int] NULL,
 CONSTRAINT [PK__Dilivery__3214EC075DCAEF64] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Dilivery__1788CC4D60A75C0F] UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Functions]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Functions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [int] NOT NULL,
	[URL] [nvarchar](100) NOT NULL,
	[ParentId] [int] NULL,
	[Note] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Material]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Material](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PartNumber] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[Manufacturer] [nvarchar](100) NULL,
	[IntNumber] [nvarchar](100) NULL,
	[DescriptionDetail] [nvarchar](1000) NULL,
	[Status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialDataUploaded]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialDataUploaded](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SellerId] [int] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[UploadDate] [datetime] NOT NULL,
	[URL] [nvarchar](100) NULL,
	[Data] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MaterialList]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialList](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SellerId] [int] NOT NULL,
	[MaterialId] [bigint] NOT NULL,
	[AvailableQty] [int] NOT NULL,
	[MaxOfQuantity] [int] NOT NULL,
	[MOQ] [int] NOT NULL,
	[PartNumber] [nvarchar](100) NULL,
	[Manufacturer] [nvarchar](100) NULL,
	[Distributer] [nvarchar](100) NULL,
	[Supplier] [nvarchar](100) NULL,
	[Price] [numeric](20, 6) NOT NULL,
	[LeadTime] [numeric](6, 2) NOT NULL,
	[UploadDate] [datetime] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[TechLink] [nvarchar](100) NULL,
	[ShippingStatusId] [int] NULL,
	[ApproveStatus] [int] NULL,
 CONSTRAINT [PK__Material__3214EC076B24EA82] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Material__C50610F66E01572D] UNIQUE NONCLUSTERED 
(
	[MaterialId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialMedia]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialMedia](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MaterialId] [bigint] NOT NULL,
	[MediaName] [nvarchar](50) NOT NULL,
	[DocumentType] [int] NOT NULL,
	[DocumentURL] [nvarchar](200) NULL,
	[DocumentData] [varbinary](max) NULL,
 CONSTRAINT [PK__Material__3214EC07164452B1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[MaterialListId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [numeric](20, 6) NOT NULL,
	[ExtendedPrice] [numeric](20, 6) NOT NULL,
	[SellerFee] [numeric](20, 6) NOT NULL,
	[BuyerFee] [numeric](20, 6) NOT NULL,
	[SellerFeeRate] [numeric](20, 6) NOT NULL,
	[BuyerFeeRate] [numeric](20, 6) NOT NULL,
	[BuyerTotal] [numeric](20, 6) NOT NULL,
	[PaymentStatusId] [int] NOT NULL,
	[DeliveryStatusId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetailStatus]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetailStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderDetailId] [bigint] NOT NULL,
	[StatusDate] [bigint] NOT NULL,
	[PaymentStatusId] [int] NULL,
	[DeliveryStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BuyerId] [int] NOT NULL,
	[TotalNet] [numeric](20, 6) NOT NULL,
	[TotalSellerFee] [numeric](20, 6) NOT NULL,
	[TotalBuyerFee] [numeric](20, 6) NOT NULL,
	[Total] [numeric](20, 6) NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[ShippingMethodId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentStatus]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [int] NOT NULL,
	[Note] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolePermission]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[FunctionId] [int] NOT NULL,
	[Permission] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setting]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SettingKey] [nvarchar](50) NOT NULL,
	[SettingValue] [nvarchar](100) NOT NULL,
	[DataType] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShippingMethod]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShippingMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ShippingStatus]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShippingStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[State]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CountryId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[FunctionId] [int] NOT NULL,
	[Permission] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/10/2018 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[Salt] [nvarchar](200) NOT NULL,
	[CompanyName] [nvarchar](100) NOT NULL,
	[Occupation] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](30) NOT NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [nvarchar](30) NOT NULL,
	[StateId] [int] NULL,
	[ZipCode] [nvarchar](10) NOT NULL,
	[TaxRate] [numeric](6, 3) NULL,
	[CountryId] [int] NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK__Users__3214EC0729572725] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Users__89C60F112C3393D0] UNIQUE NONCLUSTERED 
(
	[FullName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Users__C9F284562F10007B] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AppLog] ADD  DEFAULT (getdate()) FOR [ActionTime]
GO
ALTER TABLE [dbo].[MaterialList] ADD  CONSTRAINT [DF__MaterialL__Appro__6FE99F9F]  DEFAULT ((1)) FOR [ApproveStatus]
GO
ALTER TABLE [dbo].[Setting] ADD  DEFAULT ('nvarchar(50)') FOR [DataType]
GO
ALTER TABLE [dbo].[BillingInfo]  WITH CHECK ADD  CONSTRAINT [FK_BillingInfo_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[BillingInfo] CHECK CONSTRAINT [FK_BillingInfo_Users]
GO
ALTER TABLE [dbo].[BOMDataUploaded]  WITH CHECK ADD  CONSTRAINT [FK_BOMDataUploaded_BOM] FOREIGN KEY([BOMId])
REFERENCES [dbo].[BOM] ([Id])
GO
ALTER TABLE [dbo].[BOMDataUploaded] CHECK CONSTRAINT [FK_BOMDataUploaded_BOM]
GO
ALTER TABLE [dbo].[BOMItem]  WITH CHECK ADD  CONSTRAINT [FK_BOMItem_BOM] FOREIGN KEY([BOMId])
REFERENCES [dbo].[BOM] ([Id])
GO
ALTER TABLE [dbo].[BOMItem] CHECK CONSTRAINT [FK_BOMItem_BOM]
GO
ALTER TABLE [dbo].[BOMItem]  WITH CHECK ADD  CONSTRAINT [FK_BOMItem_Material] FOREIGN KEY([MaterialId])
REFERENCES [dbo].[Material] ([Id])
GO
ALTER TABLE [dbo].[BOMItem] CHECK CONSTRAINT [FK_BOMItem_Material]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK_Cart_ShippingMethod] FOREIGN KEY([ShippingMethodId])
REFERENCES [dbo].[ShippingMethod] ([Id])
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK_Cart_ShippingMethod]
GO
ALTER TABLE [dbo].[CartDetail]  WITH CHECK ADD  CONSTRAINT [FK_CartDetail_Cart] FOREIGN KEY([CartId])
REFERENCES [dbo].[Cart] ([Id])
GO
ALTER TABLE [dbo].[CartDetail] CHECK CONSTRAINT [FK_CartDetail_Cart]
GO
ALTER TABLE [dbo].[CartDetail]  WITH CHECK ADD  CONSTRAINT [FK_CartDetail_MaterialList] FOREIGN KEY([MaterialListId])
REFERENCES [dbo].[MaterialList] ([Id])
GO
ALTER TABLE [dbo].[CartDetail] CHECK CONSTRAINT [FK_CartDetail_MaterialList]
GO
ALTER TABLE [dbo].[DiliveryInfo]  WITH CHECK ADD  CONSTRAINT [FK_DiliveryInfo_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[DiliveryInfo] CHECK CONSTRAINT [FK_DiliveryInfo_Users]
GO
ALTER TABLE [dbo].[Functions]  WITH CHECK ADD  CONSTRAINT [FK_Functions_Functions] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Functions] ([Id])
GO
ALTER TABLE [dbo].[Functions] CHECK CONSTRAINT [FK_Functions_Functions]
GO
ALTER TABLE [dbo].[MaterialDataUploaded]  WITH CHECK ADD  CONSTRAINT [FK_MaterialDataUploaded_Users] FOREIGN KEY([SellerId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MaterialDataUploaded] CHECK CONSTRAINT [FK_MaterialDataUploaded_Users]
GO
ALTER TABLE [dbo].[MaterialList]  WITH CHECK ADD  CONSTRAINT [FK_MaterialList_Material] FOREIGN KEY([MaterialId])
REFERENCES [dbo].[Material] ([Id])
GO
ALTER TABLE [dbo].[MaterialList] CHECK CONSTRAINT [FK_MaterialList_Material]
GO
ALTER TABLE [dbo].[MaterialList]  WITH CHECK ADD  CONSTRAINT [FK_MaterialList_ShippingStatus] FOREIGN KEY([ShippingStatusId])
REFERENCES [dbo].[ShippingStatus] ([Id])
GO
ALTER TABLE [dbo].[MaterialList] CHECK CONSTRAINT [FK_MaterialList_ShippingStatus]
GO
ALTER TABLE [dbo].[MaterialMedia]  WITH CHECK ADD  CONSTRAINT [FK_MaterialMedia_Material] FOREIGN KEY([MaterialId])
REFERENCES [dbo].[Material] ([Id])
GO
ALTER TABLE [dbo].[MaterialMedia] CHECK CONSTRAINT [FK_MaterialMedia_Material]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_DeliveryStatus] FOREIGN KEY([DeliveryStatusId])
REFERENCES [dbo].[DeliveryStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_DeliveryStatus]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_MaterialList] FOREIGN KEY([MaterialListId])
REFERENCES [dbo].[MaterialList] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_MaterialList]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Orders]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_PaymentStatus] FOREIGN KEY([PaymentStatusId])
REFERENCES [dbo].[PaymentStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_PaymentStatus]
GO
ALTER TABLE [dbo].[OrderDetailStatus]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetailStatus_DeliveryStatus] FOREIGN KEY([DeliveryStatusId])
REFERENCES [dbo].[DeliveryStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderDetailStatus] CHECK CONSTRAINT [FK_OrderDetailStatus_DeliveryStatus]
GO
ALTER TABLE [dbo].[OrderDetailStatus]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetailStatus_OrderDetail] FOREIGN KEY([OrderDetailId])
REFERENCES [dbo].[OrderDetail] ([Id])
GO
ALTER TABLE [dbo].[OrderDetailStatus] CHECK CONSTRAINT [FK_OrderDetailStatus_OrderDetail]
GO
ALTER TABLE [dbo].[OrderDetailStatus]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetailStatus_PaymentStatus] FOREIGN KEY([PaymentStatusId])
REFERENCES [dbo].[PaymentStatus] ([Id])
GO
ALTER TABLE [dbo].[OrderDetailStatus] CHECK CONSTRAINT [FK_OrderDetailStatus_PaymentStatus]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_ShippingMethod] FOREIGN KEY([ShippingMethodId])
REFERENCES [dbo].[ShippingMethod] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_ShippingMethod]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Users] FOREIGN KEY([BuyerId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Users]
GO
ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_Functions] FOREIGN KEY([FunctionId])
REFERENCES [dbo].[Functions] ([Id])
GO
ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_Functions]
GO
ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_Role]
GO
ALTER TABLE [dbo].[State]  WITH CHECK ADD  CONSTRAINT [FK_State_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[State] CHECK CONSTRAINT [FK_State_Country]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Functions] FOREIGN KEY([FunctionId])
REFERENCES [dbo].[Functions] ([Id])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_Functions]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_Users]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Country]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_State]
GO
USE [master]
GO
ALTER DATABASE [MOQller] SET  READ_WRITE 
GO
