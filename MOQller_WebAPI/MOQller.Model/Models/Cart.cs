﻿using MOQller.Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Model.Models
{
    [Table("Carts")]
    public class Cart : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [Required]
        public int MaterialListId { set; get; }

        [Required]
        public int Quantity { set; get; }

        [Required]
        [DecimalPrecision(20, 6)]
        public Decimal UnitPrice { set; get; }

        [Required]
        [DecimalPrecision(20, 6)]
        public Decimal ExtendedPrice { set; get; }

        [Required]
        [StringLength(128)]
        public string BuyerId { get; set; }


        [ForeignKey("BuyerId")]
        public virtual AppUser AppUser { set; get; }

        [ForeignKey("MaterialListId")]
        public virtual MaterialList MaterialList { set; get; }

    }
}
