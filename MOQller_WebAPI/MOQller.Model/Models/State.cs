﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MOQller.Model.Abstract;

namespace MOQller.Model.Models
{
    [Table("States")]
    public class State : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        [StringLength(250)]
        [Required]
        public string Code { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { set; get; }
        
        [Required]
        public int CountryId { set; get; }

        [ForeignKey("CountryId")]
        public virtual Country Country { set; get; }
        
    }
}