﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MOQller.Model.Models
{
    [Table("AppUsers")]
    public class AppUser : IdentityUser
    {
        [MaxLength(256)]
        public string FullName { set; get; }

        [MaxLength(256)]
        public string Address { set; get; }

        public string Avatar { get; set; }

        public DateTime? BirthDay { set; get; }

        public bool? Status { get; set; }

        public bool? Gender { get; set; }

        [Required]
        public int CountryId { set; get; }

        [MaxLength(256)]
        [Required]
        public string FirstName { set; get; }

        [MaxLength(256)]
        [Required]
        public string LastName { set; get; }

        [MaxLength(256)]
        [Required]
        public string Company { set; get; }

        [MaxLength(256)]
        public string Occupation { set; get; }

        [MaxLength(256)]
        public string Address2 { set; get; }

        [MaxLength(256)]
        [Required]
        public string City { set; get; }

        [MaxLength(256)]
        public string State { set; get; }


        [MaxLength(20)]
        [Required]
        public string ZipCode { set; get; }


        public decimal TaxRate { set; get; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual IEnumerable<Order> Orders { set; get; }
    }
}