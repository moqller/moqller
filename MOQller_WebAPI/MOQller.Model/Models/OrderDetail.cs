﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MOQller.Model.Models
{
    [Table("OrderDetails")]
    public class OrderDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public Int64 Id { set; get; }

        [Required]
        public Int64 OrderId { set; get; }

        // Cái này theo thiết kế là Int64 nhưng do tạo trước là int nên phải theo
        public int MaterialListId { set; get; }

        [Required]
        public int Quantity { set; get; }


        [Required]
        [DecimalPrecision(20, 4)]
        public decimal UnitPrice { set; get; }


        [Required]
        [DecimalPrecision(20, 2)]
        public decimal ExtendedPrice { set; get; }


        [Required]
        [DecimalPrecision(20, 6)]
        public decimal SellerFee { set; get; }


        [Required]
        [DecimalPrecision(20, 6)]
        public decimal BuyerFee { set; get; }

        [Required]
        [DecimalPrecision(20, 6)]
        public decimal SellerFeeRate { set; get; }


        [Required]
        [DecimalPrecision(20, 6)]
        public decimal BuyerFeeRate { set; get; }


        [Required]
        [DecimalPrecision(20, 2)]
        public decimal BuyerTotal { set; get; }

        public int? OrderStatusId { get; set; }

        public string TrackingNumber { get; set; }

        [ForeignKey("OrderStatusId")]
        public virtual OrderStatus OrderStatuses { set; get; }

        [Required]
        public int DeliveryStatusId { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { set; get; }

        [ForeignKey("MaterialListId")]
        public virtual MaterialList MaterialList { set; get; }

        [ForeignKey("DeliveryStatusId")]
        public virtual DeliveryStatus DeliveryStatuses { set; get; }

        public virtual IEnumerable<OrderDetailStatus> OrderDetailStatuses { set; get; }
    }
}