﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Abstract;

namespace MOQller.Model.Models
{
    [Table("ShippingStatuses")]
    public class ShippingStatus : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(250)]
        [Required]
        public string Code { get; set; }

        [StringLength(250)]
        [Required]
        public string Name { get; set; }
    }
}