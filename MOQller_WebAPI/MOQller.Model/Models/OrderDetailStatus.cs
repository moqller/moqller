﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace MOQller.Model.Models
{
    [Table("OrderDetailStatuses")]
    public class OrderDetailStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 1)]
        public Int64 Id { set; get; }

        [Column(Order = 2)]
        [Required]
        public Int64 OrderDetailId { set; get; }

        [Column(Order = 3)]
        [Required]
        public DateTime StatusDate { set; get; }

        [Column(Order = 5)]
        public int DeliveryStatusId { get; set; }

        [ForeignKey("OrderDetailId")]
        public virtual OrderDetail OrderDetail { set; get; }

        [ForeignKey("DeliveryStatusId")]
        public virtual DeliveryStatus DeliveryStatus { set; get; }
    }
}
