﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MOQller.Model.Abstract;

namespace MOQller.Model.Models
{
    [Table("BOM")]
    public class BOM: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        [StringLength(100)]
        [Required]
        public string Name { get; set; }
        

        [Required]
        public DateTime UploadDate { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUser { set; get; }

        public virtual IEnumerable<BOMItem> BomItem { get; set; }
        public virtual ICollection<BOMItem> newBomItem { get; set; }
    }
}
