﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Abstract;
using System.ComponentModel;
using MOQller.Common.Enums;

namespace MOQller.Model.Models
{
    [Table("MaterialLists")]
    public class MaterialList : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(128)]
        [Required]
        public string SellerId { get; set; }

        public int? MaterialId { get; set; }

        [Required]
        public int AvailableQty { get; set; }

        [Required]
        public int MaxOfQuantity { get; set; }

        [Required]
        public int MOQ { get; set; }

        [StringLength(20)]
        public string DateCode { get; set; }

        [Required]
        public ConditionEnum Condition { get; set; }

        [StringLength(250)]
        public string Distributer { get; set; }

        [StringLength(250)]
        public string Supplier { get; set; }

        [DecimalPrecision(20, 6)]
        [Required]
        public decimal Price { get; set; }

        [Required]
        public DateTime UploadDate { get; set; }

        [Required]
        public DateTime DueDate { get; set; }

        [StringLength(250)]
        public string PartNumber { get; set; }

        [StringLength(250)]
        public string Manufacturer { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(250)]
        public string TechLink { get; set; }

        [DefaultValue(1)]
        public int ApproveStatus { get; set; }

        public int ShippingStatusId { get; set; }

        public Int64? WorkOrderId { get; set; }

        [ForeignKey("SellerId")]
        public virtual AppUser AppUser { set; get; }

        //[ForeignKey("MaterialId")]
        public virtual Material Material { set; get; }

        [ForeignKey("ShippingStatusId")]
        public virtual ShippingStatus ShippingStatus { set; get; }

        [ForeignKey("WorkOrderId")]
        public virtual MaterialTempData MaterialTempDatas { set; get; }

    }
}
