﻿using MOQller.Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MOQller.Model.Models
{
    [Table("Orders")]
    public class Order : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        [StringLength(50)]
        public string OrderNumber { get; set; }

        public Int64 BillingInfoId { get; set; }

        public Int64 DeliveryInfoId { get; set; }

        [Required]
        [StringLength(128)]
        public string BuyerId { get; set; }

        [Required]
        [DecimalPrecision(20, 6)]
        public decimal TotalQuantity { get; set; }

        [Required]
        [DecimalPrecision(20, 2)]
        public decimal SubTotal { get; set; }

        [Required]
        [DecimalPrecision(20, 6)]
        public decimal TotalSellerFee { get; set; }

        [Required]
        [DecimalPrecision(20, 6)]
        public decimal TotalBuyerFee { get; set; }

        [Required]
        [DecimalPrecision(20, 2)]
        public decimal TransactionAmount { get; set; }

        [Required]
        [DecimalPrecision(20, 2)]
        public decimal Total { get; set; }

        public int? OrderStatus { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public int ShippingMethodId { get; set; }

        [Required]
        public int PaymentStatusId { get; set; }

        [ForeignKey("ShippingMethodId")]
        public virtual ShippingMethod ShippingMethod { set; get; }

        [ForeignKey("BuyerId")]
        public virtual AppUser AppUser { set; get; }

        [ForeignKey("PaymentStatusId")]
        public virtual PaymentStatus PaymentStatuses { set; get; }

        [ForeignKey("BillingInfoId")]
        public virtual Address AddressBilling { set; get; }

        [ForeignKey("DeliveryInfoId")]
        public virtual Address AddressDelivery { set; get; }

        // DUng lazzy loading lấy dữ liệu
        public virtual IEnumerable<OrderDetail> OrderDetails { set; get; }


        // Thêm mới sẽ tự động thêm nếu truyền Order đủ time sô
        public virtual ICollection<OrderDetail> NewOrderDetails { set; get; }

    }
}