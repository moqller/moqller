﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MOQller.Model.Abstract;

namespace MOQller.Model.Models
{
    [Table("BomItem")]
    public class BOMItem:Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        [Required]
        public Int64 BOMId { get; set; }

        [StringLength(100)]
        [Required]
        public string PartNumber { get; set; }

        [Required]
        public int Quantity { get; set; }

        
        [ForeignKey("BOMId")]
        public virtual BOM BOM { get; set; }
    }
}
