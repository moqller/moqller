﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Abstract;

namespace MOQller.Model.Models
{
    [Table("Materials")]
    public class Material : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(250)]
        [Required]
        public string PartNumber { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(250)]
        public string Manufacturer { get; set; }

        [StringLength(250)]
        public string TechLink { get; set; }

        public byte[] Image1 { get; set; }

        public byte[] Image2 { get; set; }

        public byte[] Image3 { get; set; }

        public byte[] Image4 { get; set; }

        //public int Status { get; set; }

        [StringLength(1000)]
        public string DescriptionDetail { get; set; }

        public virtual IEnumerable<MaterialList> MaterialLists { get; set; }

        public virtual ICollection<MaterialList> NewMaterialList { set; get; }
        

    }
}
