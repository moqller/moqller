﻿using MOQller.Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Model.Models
{
    [Table("MaterialTempDatas")]
    public class MaterialTempData 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }

        [Required]
        [StringLength(128)]
        public string SellerId { get; set; }

        [Required]
        public DateTime UploadDate { get; set; }

        [Required]
        [MaxLength]
        public string Data { get; set; }
        
        [Required]
        [DefaultValue(0)]
        public int Status { get; set; } 

        public string DataEntryId { get; set; }

        public DateTime? CreatedDate { set; get; }
        public DateTime? UpdatedDate { set; get; }

        [ForeignKey("SellerId")]
        public virtual AppUser Seller { set; get; }
       
        public virtual AppUser DataEntry { set; get; }
    }
}
