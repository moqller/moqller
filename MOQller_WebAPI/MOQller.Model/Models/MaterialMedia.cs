﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MOQller.Model.Models
{
    [Table("MaterialMedia")]
    public class MaterialMedia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int MaterialId { get; set; }

        [StringLength(50)]
        [Required]
        public string MediaName { get; set; }

        [Required]
        public int MaterialType { get; set; }

        [StringLength(200)]
        public string DocumentURL { get; set; }

        [Column(TypeName = "varbinary")]
        public byte[] DocumentData { get; set; }

        [ForeignKey("MaterialId")]
        public virtual IEnumerable<Material> Materials { set; get; }
    }
}
