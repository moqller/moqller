﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IParameterService
    {
        Parameter Add(Parameter Parameter);

        void Update(Parameter Parameter);

        Parameter Delete(int id);

        IEnumerable<Parameter> GetAll();

        IEnumerable<Parameter> GetAll(string keyword);

        Parameter GetById(int id);
        Parameter GetByCode(string code);

        bool CheckCode (int Id, string Code);

        void Save();
    }

    public class ParameterService : IParameterService
    {
        private IParameterRepository _ParameterRepository;
        private IUnitOfWork _unitOfWork;

        public ParameterService(IParameterRepository ParameterRepository, IUnitOfWork unitOfWork)
        {
            this._ParameterRepository = ParameterRepository;
            this._unitOfWork = unitOfWork;
        }

        public Parameter Add(Parameter Parameter)
        {
            return _ParameterRepository.Add(Parameter);
        }

        public Parameter Delete(int id)
        {
            return _ParameterRepository.Delete(id);
        }

        public IEnumerable<Parameter> GetAll()
        {
            return _ParameterRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<Parameter> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _ParameterRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()) || x.Code.ToLower().Contains(keyword.ToLower()) || x.Description.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.Name);
            else
                return _ParameterRepository.GetAll().OrderBy(x => x.Name);
        }


        public Parameter GetById(int id)
        {
            return _ParameterRepository.GetSingleById(id);
        }

        public Parameter GetByCode(string code)
        {
            return _ParameterRepository.GetByCode(code);
        }

        public bool CheckCode (int Id, string Code)
        {
            if (!string.IsNullOrEmpty(Code))
            {
                if (_ParameterRepository.Count(x => x.Code == Code.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Parameter Parameter)
        {
            _ParameterRepository.Update(Parameter);
        }
    }
}
