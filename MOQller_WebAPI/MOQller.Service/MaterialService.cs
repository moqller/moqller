﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IMaterialService
    {
        Material Add(Material Material);

        void Update(Material Material);

        Material Delete(int id);

        IEnumerable<Material> GetAll();

        IEnumerable<Material> GetAll(string keyword);

        Material GetById(int id);

        Material GetByPartNumber (string PartNumber);


        void Save();
    }

    public class MaterialService : IMaterialService
    {
        private IMaterialRepository _MaterialRepository;
        private IUnitOfWork _unitOfWork;

        public MaterialService(IMaterialRepository MaterialRepository, IUnitOfWork unitOfWork)
        {
            this._MaterialRepository = MaterialRepository;
            this._unitOfWork = unitOfWork;
        }

        public Material Add(Material Material)
        {
            return _MaterialRepository.Add(Material);
        }

        public Material Delete(int id)
        {
            return _MaterialRepository.Delete(id);
        }

        public IEnumerable<Material> GetAll()
        {
            return _MaterialRepository.GetAll().OrderBy(x => x.PartNumber);
        }

        public IEnumerable<Material> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _MaterialRepository.GetMulti(x => x.PartNumber.ToLower().Contains(keyword.ToLower()) || x.Description.ToLower().Contains(keyword.ToLower()) || x.Manufacturer.ToLower().Contains(keyword.ToLower()) || x.DescriptionDetail.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.PartNumber);
            else
                return _MaterialRepository.GetAll().OrderBy(x => x.PartNumber);
        }


        public Material GetById(int id)
        {
            return _MaterialRepository.GetSingleById(id);
        }

        public Material GetByPartNumber (string PartNumber)
        {
            return _MaterialRepository.GetSingleByCondition(x => x.PartNumber.ToLower() == PartNumber.Trim().ToLower());
        }


        public void Save()
        {
             _unitOfWork.Commit();
        }

        public void Update(Material Material)
        {
            _MaterialRepository.Update(Material);
        }

    }
}
