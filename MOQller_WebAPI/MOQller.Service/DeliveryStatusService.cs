﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Common;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;

namespace MOQller.Service
{
    public class DeliveryStatusService : IDeliveryStatusService
    {
        private IDeliveryStatusRepository _DeliveryStatusRepository;
        private IUnitOfWork _unitOfWork;

        public DeliveryStatusService (IDeliveryStatusRepository DeliveryStatusRepository, IUnitOfWork unitOfWork)
        {
            this._DeliveryStatusRepository = DeliveryStatusRepository;
            this._unitOfWork = unitOfWork;
        }

        public DeliveryStatus Add (DeliveryStatus DeliveryStatus)
        {
            return _DeliveryStatusRepository.Add(DeliveryStatus);
        }

        public DeliveryStatus Delete (int id)
        {
            return _DeliveryStatusRepository.Delete(id);
        }

        public IEnumerable<DeliveryStatus> GetAll ()
        {
            return _DeliveryStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<DeliveryStatus> GetAll (string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _DeliveryStatusRepository.GetMulti(x => x.Name.Contains(keyword))
                    .OrderBy(x => x.Name);
            else
                return _DeliveryStatusRepository.GetAll().OrderBy(x => x.Name);
        }


        public DeliveryStatus GetById (int id)
        {
            return _DeliveryStatusRepository.GetSingleById(id);
        }

        public DeliveryStatus GetByName (string Name)
        {
            return _DeliveryStatusRepository.GetSingleByCondition(x => x.Name.ToLower().Contains(Name.ToLower()));
        }

        public bool CheckNameDeliveryStatus (int Id, string Name)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (_DeliveryStatusRepository.Count(x => x.Name == Name.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public DeliveryStatus GetByCode (string Code)
        {
            return _DeliveryStatusRepository.GetSingleByCondition(x => x.Code.ToLower() == Code.Trim().ToLower());
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (DeliveryStatus DeliveryStatus)
        {
            _DeliveryStatusRepository.Update(DeliveryStatus);
        }
    }

    public interface IDeliveryStatusService
    {
        DeliveryStatus Add (DeliveryStatus DeliveryStatus);

        void Update (DeliveryStatus DeliveryStatus);

        DeliveryStatus Delete (int id);

        IEnumerable<DeliveryStatus> GetAll ();

        IEnumerable<DeliveryStatus> GetAll (string keyword);

        DeliveryStatus GetById (int id);

        DeliveryStatus GetByName (string Name);

        DeliveryStatus GetByCode (string Code);

        bool CheckNameDeliveryStatus(int Id, string Name);

        void Save ();
    }
}
