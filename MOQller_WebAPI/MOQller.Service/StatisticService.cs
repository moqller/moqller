﻿using System;
using System.Collections.Generic;
using System.Linq;
using MOQller.Common.ViewModels;
using MOQller.Data.Repositories;

namespace MOQller.Service
{
    public interface IStatisticService
    {
        IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate);
        OrderStatisticsViewModel GetOrderStatistics(string userId, DateTime fromDate, DateTime toDate);
        List<OrderMonthStatisticsViewModel> GetOrderMonthStatistics(string useId, int year);
        IEnumerable<MaterialAlertDay> GetMaterialAlertDay(string userId, int year, int month);
    }

    public class StatisticService : IStatisticService
    {
        private IOrderRepository _orderRepository;

        public StatisticService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate)
        {
            return _orderRepository.GetRevenueStatistic(fromDate, toDate);
        }

        public OrderStatisticsViewModel GetOrderStatistics( string userId, DateTime fromDate, DateTime toDate)
        {
            
            return _orderRepository.GetOrderStatistic(userId, fromDate, toDate);

        }

        public List<OrderMonthStatisticsViewModel> GetOrderMonthStatistics(string userId, int year)
        {
            return _orderRepository.GetMonthOrderStatistic(userId, year);
        }

        public IEnumerable<MaterialAlertDay> GetMaterialAlertDay(string userId, int year, int month)
        {
            return _orderRepository.GetMaterialAlertDay(userId, year, month);
        }
    }
}