﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Common;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;

namespace MOQller.Service
{
    public class MaterialMediaService : IMaterialMediaService
    {
        private IMaterialMediaRepository _MaterialMediaRepository;
        private IUnitOfWork _unitOfWork;

        public MaterialMediaService (IMaterialMediaRepository MaterialMediaRepository, IUnitOfWork unitOfWork)
        {
            this._MaterialMediaRepository = MaterialMediaRepository;
            this._unitOfWork = unitOfWork;
        }

        public MaterialMedia Add (MaterialMedia MaterialMedia)
        {
            return _MaterialMediaRepository.Add(MaterialMedia);
        }

        public MaterialMedia Delete (int id)
        {
            return _MaterialMediaRepository.Delete(id);
        }

        public IEnumerable<MaterialMedia> GetAll ()
        {
            return _MaterialMediaRepository.GetAll().OrderBy(x => x.MediaName);
        }

        public IEnumerable<MaterialMedia> GetAll (string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _MaterialMediaRepository.GetMulti(x => x.MediaName.Contains(keyword))
                    .OrderBy(x => x.MediaName);
            else
                return _MaterialMediaRepository.GetAll().OrderBy(x => x.MediaName);
        }


        public MaterialMedia GetById (int id)
        {
            return _MaterialMediaRepository.GetSingleById(id);
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (MaterialMedia MaterialMedia)
        {
            _MaterialMediaRepository.Update(MaterialMedia);
        }
    }

    public interface IMaterialMediaService
    {
        MaterialMedia Add (MaterialMedia MaterialMedia);

        void Update (MaterialMedia MaterialMedia);

        MaterialMedia Delete (int id);

        IEnumerable<MaterialMedia> GetAll ();

        IEnumerable<MaterialMedia> GetAll (string keyword);

        MaterialMedia GetById (int id);

        void Save ();
    }
}
