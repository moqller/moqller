﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface ICountryService
    {
        Country Add(Country Country);

        void Update(Country Country);

        Country Delete(int id);

        IEnumerable<Country> GetAll();

        IEnumerable<Country> GetAll(string keyword);

        Country GetById(int id);
        Country GetByCode(string code);

        bool CheckCode (int Id, string Name);
        void Save();
    }

    public class CountryService : ICountryService
    {
        private ICountryRepository _CountryRepository;
        private IUnitOfWork _unitOfWork;

        public CountryService(ICountryRepository CountryRepository, IUnitOfWork unitOfWork)
        {
            this._CountryRepository = CountryRepository;
            this._unitOfWork = unitOfWork;
        }

        public Country Add(Country Country)
        {
            return _CountryRepository.Add(Country);
        }

        public Country Delete(int id)
        {
            return _CountryRepository.Delete(id);
        }

        public IEnumerable<Country> GetAll()
        {
            return _CountryRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<Country> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _CountryRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()) || x.Code.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.Name);
            else
                return _CountryRepository.GetAll().OrderBy(x => x.Name);
        }

        
        public Country GetById(int id)
        {
            return _CountryRepository.GetSingleById(id);
        }

        public Country GetByCode(string code)
        {
            return _CountryRepository.GetByCode(code);
        }

        public bool CheckCode(int Id, string Code)
        {
            if (!string.IsNullOrEmpty(Code))
            {
                if (_CountryRepository.Count(x => x.Code == Code.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Country Country)
        {
            _CountryRepository.Update(Country);
        }
    }
}
