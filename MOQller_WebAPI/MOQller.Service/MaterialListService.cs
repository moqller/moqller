﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;
using MOQller.Common.ViewModels;

namespace MOQller.Service
{
    public interface IMaterialListService
    {
        MaterialList Add(MaterialList MaterialList);

        void Update(MaterialList MaterialList);

        MaterialList Delete(int id);

        IEnumerable<MaterialList> GetAll();

        IEnumerable<MaterialList> GetAll(List<string> keywords, string userid);

        IEnumerable<MaterialList> GetAllExact(string keyword);

        IEnumerable<MaterialList> GetAll(string keyword,int searchField);

        IEnumerable<MaterialList> GetByMaterialId (int MaterialId);

        IEnumerable<MaterialList> GetByPartNumber(string PartNumber);

        IEnumerable<MaterialList> GetByWorkOrder (int id);

        MaterialList GetInstock (int MaterialID, int MOQ, decimal Price, string SellerId);

        MaterialList GetById (int id);

        CurrentInventoryViewModel GetInventory(string userId);

        bool CheckQuantityMaterialList (int MaterialListId, int quantity);

        bool CheckMoqMaterialList (int MaterialListId, int quantity);

        bool CheckMaxMoqMaterialList (int MaterialListId, int quantity);

        void Save();
    }

    public class MaterialListService : IMaterialListService
    {
        private IMaterialListRepository _MaterialListRepository;
        private IUnitOfWork _unitOfWork;
        private IShippingStatusService _ShippingStatusService;

        public MaterialListService(IMaterialListRepository MaterialListRepository, IUnitOfWork unitOfWork, IShippingStatusService ShippingStatusService)
        {
            this._MaterialListRepository = MaterialListRepository;
            this._unitOfWork = unitOfWork;
            this._ShippingStatusService = ShippingStatusService;
        }

        public MaterialList Add(MaterialList MaterialList)
        {
            return _MaterialListRepository.Add(MaterialList);
        }

        public MaterialList Delete(int id)
        {
            return _MaterialListRepository.Delete(id);
        }

        public IEnumerable<MaterialList> GetAll()
        {
            return _MaterialListRepository.GetAll(new string[] { "AppUser", "Material", "ShippingStatus"}).OrderBy(x => x.UploadDate);
        }

        public IEnumerable<MaterialList> GetAll(List<string> keywords, string userid)
        {
            List<MaterialList> listResults = new List<MaterialList>();
            var query = _MaterialListRepository.GetAll(new string[] { "AppUser", "Material", "ShippingStatus" });
            foreach (var item in keywords)
            {
                listResults.AddRange(query.Where(x => x.ApproveStatus == 1 && (x.Material.PartNumber.ToLower().Contains(item.ToLower()) || x.Material.Description.ToLower().Contains(item.ToLower())) && x.AvailableQty > 0 && x.SellerId != userid ).ToList());
            }            
            
            return (IEnumerable<MaterialList>)listResults.OrderBy(x => x.Material.PartNumber);
        }

        public IEnumerable<MaterialList> GetAllExact(string keyword)
        {
            var query = _MaterialListRepository.GetAll(new string[] { "AppUser", "Material", "ShippingStatus" });
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.ApproveStatus == 1 && x.Material.PartNumber.ToLower()== keyword.ToLower() && x.AvailableQty > 0);

            return query.OrderBy(x => x.Material.PartNumber);
        }

        public IEnumerable<MaterialList> GetAll(string keyword, int searchField)
        {
            var query = _MaterialListRepository.GetAll(new string[] { "AppUser", "Material", "ShippingStatus" });
            if (!string.IsNullOrEmpty(keyword))
                if (searchField==1)
                {
                    query = query.Where(x => x.ApproveStatus == 1 && x.Material.PartNumber.ToLower().Contains(keyword.ToLower()) && x.AvailableQty > 0 );
                }
                else if (searchField == 2)
                {
                    query = query.Where(x => x.ApproveStatus == 1 && x.Material.Description.ToLower().Contains(keyword.ToLower()) && x.AvailableQty > 0  );
                }
                else if (searchField == 3)
                {
                    query = query.Where(x => x.ApproveStatus == 1 && x.Material.Manufacturer.ToLower().Contains(keyword.ToLower()) && x.AvailableQty > 0 );
                }
            return query.OrderBy(x => x.Material.PartNumber);
        }

        public IEnumerable<MaterialList> GetByMaterialId (int MaterialId)
        {
            return _MaterialListRepository.GetMulti(x => x.MaterialId == MaterialId);
        }

        public IEnumerable<MaterialList> GetByPartNumber(string PartNumber)
        {
            return _MaterialListRepository.GetMulti(x => x.PartNumber.Trim() == PartNumber.Trim());
        }


        public MaterialList GetById(int id)
        {
            return _MaterialListRepository.GetSingleById(id);
        }

        public IEnumerable<MaterialList> GetByWorkOrder (int id)
        {
            return _MaterialListRepository.GetMulti(wo => wo.WorkOrderId == id);
        }


        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(MaterialList MaterialList)
        {
            _MaterialListRepository.Update(MaterialList);
        }

        // check số lượng trong kho còn để mua ko
        public bool CheckQuantityMaterialList (int MaterialListId, int quantity)
        {
            var MaterialList = _MaterialListRepository.GetSingleById(MaterialListId);

            return (MaterialList.AvailableQty < quantity) ? true : false;
        }

        // check số lương nhỏ nhất MOQ
        public bool CheckMoqMaterialList (int MaterialListId, int quantity)
        {
            var MaterialList = _MaterialListRepository.GetSingleById(MaterialListId);

            return (MaterialList.MOQ <= quantity) ? true : false;
        }

        // check số lương lớn nhất MOQ
        public bool CheckMaxMoqMaterialList (int MaterialListId, int quantity)
        {
            var MaterialList = _MaterialListRepository.GetSingleById(MaterialListId);

            return (quantity <= MaterialList.MaxOfQuantity) ? true : false;
        }

        public CurrentInventoryViewModel GetInventory(string userId)
        {
            CurrentInventoryViewModel curr = new CurrentInventoryViewModel();
            var materiallist = _MaterialListRepository.GetAll(new string[] { "ShippingStatus"}).Where(x => x.SellerId == userId);
            curr.Less30Day = materiallist.Count(x => (DateTime.Now - x.UploadDate).TotalDays >= 0 && (DateTime.Now - x.UploadDate).TotalDays <= 30);
            curr.From30To60Day = materiallist.Count(x => (DateTime.Now - x.UploadDate).TotalDays > 31 && (DateTime.Now - x.UploadDate).TotalDays <= 60);
            curr.From60To120Day = materiallist.Count(x => (DateTime.Now - x.UploadDate).TotalDays > 60 && (DateTime.Now - x.UploadDate).TotalDays <= 120);
            curr.To120Day = materiallist.Count(x => (DateTime.Now - x.UploadDate).TotalDays > 120);
            curr.Unconfirm = materiallist.Count(x => x.ShippingStatus.Code == "UNC");
            curr.Confirm = materiallist.Count(x => x.ShippingStatus.Code == "COF");
            return curr;
        }

        public MaterialList GetInstock (int MaterialID, int MOQ, decimal Price, string SellerId)
        {
            return _MaterialListRepository.GetSingleByCondition(x => x.MaterialId == MaterialID && x.MOQ == MOQ && x.Price == Price && x.SellerId == SellerId);
        }
    }
}
