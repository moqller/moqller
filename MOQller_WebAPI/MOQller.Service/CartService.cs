﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface ICartService
    {
        Cart Add(Cart Cart);

        void Update(Cart Cart);

        Cart Delete(int id);

        IEnumerable<Cart> GetAll();

        IEnumerable<Cart> GetAll(string UserId);

        Cart GetById(int id);
        Cart CheckCart(string buyerId,int MaterialistId);

        void Save();

        void DeleteByUserId (string UserId);
    }

    public class CartService : ICartService
    {
        private ICartRepository _CartRepository;
        private IUnitOfWork _unitOfWork;

        public CartService(ICartRepository CartRepository, IUnitOfWork unitOfWork)
        {
            this._CartRepository = CartRepository;
            this._unitOfWork = unitOfWork;
        }

        public Cart Add(Cart Cart)
        {
            return _CartRepository.Add(Cart);
        }

        public Cart Delete(int id)
        {
            return _CartRepository.Delete(id);
        }

        public IEnumerable<Cart> GetAll()
        {
            var query = _CartRepository.GetAll(new string[] { "AppUser", "MaterialList", "MaterialList.Material" });
            return query.OrderBy(x => x.MaterialList.Material.PartNumber);
        }

        public IEnumerable<Cart> GetAll(string UserId)
        {
            var query = _CartRepository.GetAll(new string[] { "MaterialList", "MaterialList.AppUser" });
            if (!string.IsNullOrEmpty(UserId))
                query = query.Where(x => x.BuyerId == UserId);

            return query.OrderBy(x => x.MaterialList.Material.PartNumber);
        }

        public void DeleteByUserId (string UserId)
        {
            _CartRepository.DeleteMulti(x => x.BuyerId == UserId);
        }

        public Cart GetById(int id)
        {
            return _CartRepository.GetSingleById(id);
        }

        public Cart CheckCart(string buyerId,int materialListId)
        {
            var cart =_CartRepository.GetAll().Where(x => x.MaterialListId == materialListId && x.BuyerId == buyerId).SingleOrDefault();
            
            return cart;
            
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Cart Cart)
        {
            _CartRepository.Update(Cart);
        }
    }
}
