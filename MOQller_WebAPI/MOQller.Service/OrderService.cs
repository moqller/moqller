﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MOQller.Common.ViewModels;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;

namespace MOQller.Service
{
    public interface IOrderService
    {
        Order Create(Order order);
        List<Order> GetList(string startDate, string endDate, string customerName, string status,
            int pageIndex, int pageSize, out int totalRow);

        IEnumerable<Order> GetAll();

        IEnumerable<Order> GetByUser (string UserId);

        IEnumerable<OrderDetail> GetBySellerUser (string UserId);

        IEnumerable<BuyingViewModel> GetBying (string UserId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId);

        IEnumerable<SellingViewModel> GetSelling (string UserId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter);

        IEnumerable<OrderDetail> GetOrderDetailsNew (string UserId, int id);

        IEnumerable<OrderDetail> GeSellerOrderDetails(string UserId);

        IEnumerable<OrderDetail> FilterSellerOrderDetail(string userId, string filter);

        IEnumerable<Order> GetOrderOne(string userId, int id);

        Order GetDetail (int orderId);

        OrderDetail CreateDetail(OrderDetail order);

        OrderDetail GetOrderDetailById (Int64 orderDetailId);

        void UpdateOderDetailStatus (OrderDetail orderDetail);

        void DeleteDetail(int productId, int orderId, int colorId, int sizeId);

        Order UpdateOrderNumber (Int64 orderId, string OrderNumber);

        void UpdateStatus(int orderId);

        void UpdateDeliveredStatus(Int64 OrderDetailId, int DeliveredStatusId);

        IEnumerable<OrderDetail> GetOrderDetails(int orderId);

        AppUser GetSellerByMaterialListId(int materialListId);

        void Save();
    }

    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;
        private IOrderDetailRepository _orderDetailRepository;
        private IUnitOfWork _unitOfWork;

        public OrderService(IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IUnitOfWork unitOfWork)
        {
            this._orderRepository = orderRepository;
            this._orderDetailRepository = orderDetailRepository;
            this._unitOfWork = unitOfWork;
        }

        public Order Create(Order order)
        {
            try
            {
                _orderRepository.Add(order);
                _unitOfWork.Commit();
                return order;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<Order> GetAll()
        {
            return _orderRepository.GetAll(new string[] { "AppUser", "ShippingMethod", "PaymentStatuses" }).OrderByDescending(x => x.OrderDate);
        }

        public IEnumerable<Order> GetByUser (string UserId)
        {
            return _orderRepository.GetByUserId(UserId);
        }

        public IEnumerable<OrderDetail> GetBySellerUser (string UserId)
        {
            return _orderRepository.GetBySellerUserId(UserId);
        }

        public IEnumerable<BuyingViewModel> GetBying (string UserId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId)
        {
            return _orderRepository.GetBuying(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId);
        }

        public IEnumerable<SellingViewModel> GetSelling (string UserId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter)
        {
            return _orderRepository.GetSelling(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId, filter);
        }

        public int GetOderDetailByStatus(Int64 OrderId)
        {
            // Code CAN là canncelled, FIN là Close
            return _orderDetailRepository.GetMulti(x => x.OrderStatuses.Code != "CAN" && x.OrderId == OrderId && x.OrderStatuses.Code != "FIN").Count();
        }

        public IEnumerable<Order> GetOrderOne(string userId, int id)
        {
            return _orderRepository.GetAll(new string[] { "ShippingMethod", "PaymentStatuses", "AddressBilling" }).Where(x=>x.Id==id && x.BuyerId == userId).OrderByDescending(x => x.OrderDate);
        }

        public void UpdateStatus(int orderId)
        {
            var order = _orderRepository.GetSingleById(orderId);
            order.Status = true;
            _orderRepository.Update(order);
        }

        public void UpdateDeliveredStatus(Int64 OrderDetailId, int DeliveredStatusId)
        {
            var orderDetail = _orderDetailRepository.GetSingleById(OrderDetailId);

            orderDetail.DeliveryStatusId = DeliveredStatusId;

            _orderDetailRepository.Update(orderDetail);
        }

        public Order UpdateOrderNumber (Int64 orderId, string OrderNumber)
        {
            var order = _orderRepository.GetSingleById(orderId);
           
            order.OrderNumber = OrderNumber;
            _orderRepository.Update(order);
            return order;
        }

        public AppUser GetSellerByMaterialListId(int materialListId)
        {
            return _orderRepository.GetSellerByMaterialListId(materialListId);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public List<Order> GetList(string startDate, string endDate, string customerName,
            string paymentStatus, int pageIndex, int pageSize, out int totalRow)
        {
            var query = _orderRepository.GetAll();
            if (!string.IsNullOrEmpty(startDate))
            {
                DateTime start = DateTime.ParseExact(startDate, "MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));
                query = query.Where(x => x.CreatedDate >= start);

            }
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime end = DateTime.ParseExact(endDate, "MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));
                query = query.Where(x => x.CreatedDate <= end);
            }
            //if (!string.IsNullOrEmpty(paymentStatus))
            //    query = query.Where(x => x.PaymentStatus == paymentStatus);
            totalRow = query.Count();
            return query.OrderByDescending(x => x.CreatedDate).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
        }

        public Order GetDetail(int orderId)
        {
            return _orderRepository.GetSingleByCondition(x => x.Id == orderId, new string[] { "OrderDetails" });
        }

        public IEnumerable<OrderDetail> GetOrderDetails(int orderId)
        {
            return _orderDetailRepository.GetMulti(x => x.OrderId == orderId, new string[] {"MaterialList", "MaterialList.Material", "DeliveryStatuses" }).ToList();
        }

        public IEnumerable<OrderDetail> GeSellerOrderDetails(string UserId)
        {
            return _orderDetailRepository.GetMulti(od => od.MaterialList.SellerId == UserId && (od.DeliveryStatuses.Code == "DEF" || od.DeliveryStatuses.Code == "OTW") && od.OrderStatuses.Code != "CAN" );
        }

        public IEnumerable<OrderDetail> FilterSellerOrderDetail(string userId, string filter)
        {
            return _orderDetailRepository.GetMulti(od => od.MaterialList.SellerId == userId && od.DeliveryStatuses.Code == filter && od.OrderStatuses.Code != "CAN");
        }

        public OrderDetail CreateDetail(OrderDetail order)
        {
            return _orderDetailRepository.Add(order);
        }

        public OrderDetail GetOrderDetailById (Int64 orderDetailId)
        {
            return _orderDetailRepository.GetSingleById(orderDetailId);
        }

        public void UpdateOderDetailStatus (OrderDetail orderDetail)
        {
            _orderDetailRepository.Update(orderDetail);
        }

        public void DeleteDetail(int productId, int orderId, int colorId, int sizeId)
        {
           // var detail = _orderDetailRepository.GetSingleByCondition(x => x.ProductID == productId
           //&& x.OrderID == orderId && x.ColorId == colorId && x.SizeId == sizeId);
           // _orderDetailRepository.Delete(detail);
        }

        public IEnumerable<OrderDetail> GetOrderDetailsNew (string UserId, int id)
        {
            return _orderDetailRepository.GetAll().Where(x => x.OrderId == id && x.Order.BuyerId == UserId);
        }

    }
}