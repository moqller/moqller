﻿using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Service
{
    public interface IAddressService
    {
        Address Add (Address Address);

        void Update (Address Address);

        Address Delete (int id);

        IEnumerable<Address> GetAll ();

        Address GetById (Int64 id);

        IEnumerable<Address> GetByUsrId (string UsrId); 

        void Save ();
    }
    public class AddressService : IAddressService
    {
        private IAddressRepository _addressRepository;
        private IUnitOfWork _unitOfWork;

        public AddressService (IAddressRepository addressRepository, IUnitOfWork unitOfWork)
        {
            this._addressRepository = addressRepository;
            this._unitOfWork = unitOfWork;
        }

        public Address Add (Address Address)
        {
            return _addressRepository.Add(Address);
        }

        public Address Delete (int id)
        {
            return _addressRepository.Delete(id);
        }

        public IEnumerable<Address> GetAll ()
        {
            return _addressRepository.GetAll().OrderBy(x => x.City);
        }

        public Address GetById (Int64 id)
        {
            return _addressRepository.GetSingleById(id);
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (Address Address)
        {
            _addressRepository.Update(Address);
        }

        public IEnumerable<Address> GetByUsrId (string UsrId)
        {
            return _addressRepository.GetAll().OrderBy(x => x.City).Where(u => u.UserId == UsrId);
        }
    }
}
