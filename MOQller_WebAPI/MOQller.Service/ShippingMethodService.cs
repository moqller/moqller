﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Common;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;

namespace MOQller.Service
{
    public class ShippingMethodService : IShippingMethodService
    {
        private IShippingMethodRepository _ShippingMethodRepository;
        private IUnitOfWork _unitOfWork;

        public ShippingMethodService (IShippingMethodRepository ShippingMethodRepository, IUnitOfWork unitOfWork)
        {
            this._ShippingMethodRepository = ShippingMethodRepository;
            this._unitOfWork = unitOfWork;
        }

        public ShippingMethod Add (ShippingMethod ShippingMethod)
        {
            return _ShippingMethodRepository.Add(ShippingMethod);
        }

        public ShippingMethod Delete (int id)
        {
            return _ShippingMethodRepository.Delete(id);
        }

        public IEnumerable<ShippingMethod> GetAll ()
        {
            return _ShippingMethodRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<ShippingMethod> GetAll (string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _ShippingMethodRepository.GetMulti(x => x.Name.Contains(keyword))
                    .OrderBy(x => x.Name);
            else
                return _ShippingMethodRepository.GetAll().OrderBy(x => x.Name);
        }


        public ShippingMethod GetById (int id)
        {
            return _ShippingMethodRepository.GetSingleById(id);
        }

        public bool CheckNameShippingMethod (int Id, string Name)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (_ShippingMethodRepository.Count(x => x.Name == Name.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (ShippingMethod ShippingMethod)
        {
            _ShippingMethodRepository.Update(ShippingMethod);
        }
    }

    public interface IShippingMethodService
    {
        ShippingMethod Add (ShippingMethod ShippingMethod);

        void Update (ShippingMethod ShippingMethod);

        ShippingMethod Delete (int id);

        IEnumerable<ShippingMethod> GetAll ();

        IEnumerable<ShippingMethod> GetAll (string keyword);

        ShippingMethod GetById (int id);

        bool CheckNameShippingMethod (int Id, string Name);

        void Save ();
    }
}
