﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IConfirmStatusService
    {
        ConfirmStatus Add(ConfirmStatus ConfirmStatus);

        void Update(ConfirmStatus ConfirmStatus);

        ConfirmStatus Delete(int id);

        IEnumerable<ConfirmStatus> GetAll();

        IEnumerable<ConfirmStatus> GetAll(string keyword);

        ConfirmStatus GetById(int id);


        void Save();
    }

    public class ConfirmStatusService : IConfirmStatusService
    {
        private IConfirmStatusRepository _ConfirmStatusRepository;
        private IUnitOfWork _unitOfWork;

        public ConfirmStatusService(IConfirmStatusRepository ConfirmStatusRepository, IUnitOfWork unitOfWork)
        {
            this._ConfirmStatusRepository = ConfirmStatusRepository;
            this._unitOfWork = unitOfWork;
        }

        public ConfirmStatus Add(ConfirmStatus ConfirmStatus)
        {
            return _ConfirmStatusRepository.Add(ConfirmStatus);
        }

        public ConfirmStatus Delete(int id)
        {
            return _ConfirmStatusRepository.Delete(id);
        }

        public IEnumerable<ConfirmStatus> GetAll()
        {
            return _ConfirmStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<ConfirmStatus> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _ConfirmStatusRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.Name);
            else
                return _ConfirmStatusRepository.GetAll().OrderBy(x => x.Name);
        }


        public ConfirmStatus GetById(int id)
        {
            return _ConfirmStatusRepository.GetSingleById(id);
        }



        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ConfirmStatus ConfirmStatus)
        {
            _ConfirmStatusRepository.Update(ConfirmStatus);
        }
    }
}
