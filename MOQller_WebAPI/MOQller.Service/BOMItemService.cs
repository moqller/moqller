﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IBOMItemService
    {
        BOMItem Add(BOMItem BOM);

        void Update(BOMItem BOM);

        BOMItem Delete(int id);

        IEnumerable<BOMItem> GetAll();
        

        BOMItem GetById(Int64 id);

        void Save();
    }

    public class BOMItemService : IBOMItemService
    {
        private IBOMItemRepository _BOMItemRepository;
        private IUnitOfWork _unitOfWork;

        public BOMItemService(IBOMItemRepository BOMItemRepository, IUnitOfWork unitOfWork)
        {
            this._BOMItemRepository = BOMItemRepository;
            this._unitOfWork = unitOfWork;
        }

        public BOMItem Add(BOMItem bomItem)
        {
            return _BOMItemRepository.Add(bomItem);
        }

        public BOMItem Delete(int id)
        {
            return _BOMItemRepository.Delete(id);
        }
        public IEnumerable<BOMItem> GetAll()
        {
            return _BOMItemRepository.GetAll();
        }

        public BOMItem GetById(Int64 id)
        {
            return _BOMItemRepository.GetSingleById(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(BOMItem BOM)
        {
            _BOMItemRepository.Update(BOM);
        }
    }
}
