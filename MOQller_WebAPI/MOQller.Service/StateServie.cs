﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IStateService
    {
        State Add(State State);

        void Update(State State);

        State Delete(int id);

        IEnumerable<State> GetAll();

        IEnumerable<State> GetAll(int? countryId,string keyword);

        State GetById(int id);
        State GetByCode(string code);
        IEnumerable<State> GetByCountryId(int countryId);
        bool CheckCode (int Id, string Name);

        void Save();
    }

    public class StateService : IStateService
    {
        private IStateRepository _StateRepository;
        private IUnitOfWork _unitOfWork;

        public StateService(IStateRepository StateRepository, IUnitOfWork unitOfWork)
        {
            this._StateRepository = StateRepository;
            this._unitOfWork = unitOfWork;
        }

        public State Add(State State)
        {
            return _StateRepository.Add(State);
        }

        public State Delete(int id)
        {
            return _StateRepository.Delete(id);
        }

        public IEnumerable<State> GetAll()
        {
            return _StateRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<State> GetAll(int? countryId,string keyword)
        {
            var query = _StateRepository.GetAll(new string[] { "Country"});
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.ToLower().Contains(keyword.ToLower()) || x.Code.ToLower().Contains(keyword.ToLower()));
            if (countryId.HasValue)
                query = query.Where(x => x.CountryId == countryId.Value);
            return query.OrderBy(x=>x.Name);
        }


        public State GetById(int id)
        {
            return _StateRepository.GetSingleById(id);
        }

        public State GetByCode(string code)
        {
            return _StateRepository.GetByCode(code);
        }
        public IEnumerable<State> GetByCountryId(int countryId)
        {
            return _StateRepository.GetByCountryId(countryId);
        }

        public bool CheckCode (int Id, string Code)
        {
            if (!string.IsNullOrEmpty(Code))
            {
                if (_StateRepository.Count(x => x.Code == Code.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(State State)
        {
            _StateRepository.Update(State);
        }
    }
}
