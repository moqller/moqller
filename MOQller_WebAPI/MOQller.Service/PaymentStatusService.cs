﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Common;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;

namespace MOQller.Service
{
    public class PaymentStatusService : IPaymentStatusService
    {
        private IPaymentStatusRepository _PaymentStatusRepository;
        private IUnitOfWork _unitOfWork;

        public PaymentStatusService (IPaymentStatusRepository PaymentStatusRepository, IUnitOfWork unitOfWork)
        {
            this._PaymentStatusRepository = PaymentStatusRepository;
            this._unitOfWork = unitOfWork;
        }

        public PaymentStatus Add (PaymentStatus PaymentStatus)
        {
            return _PaymentStatusRepository.Add(PaymentStatus);
        }

        public PaymentStatus Delete (int id)
        {
            return _PaymentStatusRepository.Delete(id);
        }

        public IEnumerable<PaymentStatus> GetAll ()
        {
            return _PaymentStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<PaymentStatus> GetAll (string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _PaymentStatusRepository.GetMulti(x => x.Name.Contains(keyword))
                    .OrderBy(x => x.Name);
            else
                return _PaymentStatusRepository.GetAll().OrderBy(x => x.Name);
        }


        public PaymentStatus GetById (int id)
        {
            return _PaymentStatusRepository.GetSingleById(id);
        }

        public bool CheckNamePaymentStatus (int Id, string Name)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (_PaymentStatusRepository.Count(x => x.Name == Name.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (PaymentStatus PaymentStatus)
        {
            _PaymentStatusRepository.Update(PaymentStatus);
        }

        public PaymentStatus GetByCode (string Code)
        {
            return _PaymentStatusRepository.GetSingleByCondition(x => x.Code.ToLower() == Code.Trim().ToLower());
        }
    }

    public interface IPaymentStatusService
    {
        PaymentStatus Add (PaymentStatus PaymentStatus);

        void Update (PaymentStatus PaymentStatus);

        PaymentStatus Delete (int id);

        PaymentStatus GetByCode (string Code);

        IEnumerable<PaymentStatus> GetAll ();

        IEnumerable<PaymentStatus> GetAll (string keyword);

        PaymentStatus GetById (int id);
        bool CheckNamePaymentStatus (int Id, string Name);

        void Save ();

    }
}
