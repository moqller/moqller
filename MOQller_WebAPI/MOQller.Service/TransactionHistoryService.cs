﻿using MOQller.Common.ViewModels;
using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Service
{
    public interface ITransactionHistoryService
    {
        IEnumerable<BuyingViewModel> GetBuyings (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId);

        IEnumerable<SellingViewModel> GetSellings (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter);
    }

    public class TransactionHistoryService : ITransactionHistoryService
    {
        private IOrderRepository _orderRepository;
        public TransactionHistoryService (IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public IEnumerable<BuyingViewModel> GetBuyings (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId)
        {
            return _orderRepository.GetBuying(username, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId);
        }

        public IEnumerable<SellingViewModel> GetSellings (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter)
        {
            return _orderRepository.GetSelling(username, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId, filter );
        }
    }
}
