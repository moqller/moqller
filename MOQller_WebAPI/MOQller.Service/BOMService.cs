﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IBOMService
    {
        BOM Add(BOM BOM);

        void Update(BOM BOM);

        BOM Delete(int id);

        IEnumerable<BOM> GetAll();

        IEnumerable<BOM> GetAll(string keyword);

        BOM GetById(int id);

        void Save();
    }

    public class BOMService : IBOMService
    {
        private IBOMRepository _BOMRepository;
        private IUnitOfWork _unitOfWork;

        public BOMService(IBOMRepository BOMRepository, IUnitOfWork unitOfWork)
        {
            this._BOMRepository = BOMRepository;
            this._unitOfWork = unitOfWork;
        }

        public BOM Add(BOM bom)
        {
            _BOMRepository.Add(bom);
            this._unitOfWork.Commit();
            return bom;
        }

        public BOM Delete(int id)
        {
            return _BOMRepository.Delete(id);
        }

        public IEnumerable<BOM> GetAll()
        {
            return _BOMRepository.GetAll().OrderBy(x => x.CreatedDate);
        }

        public IEnumerable<BOM> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _BOMRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.CreatedDate);
            else
                return _BOMRepository.GetAll().OrderBy(x => x.CreatedDate);
        }

        public BOM GetById(int id)
        {
            return _BOMRepository.GetSingleById(id);
        }


        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(BOM BOM)
        {
            _BOMRepository.Update(BOM);
        }
    }
}
