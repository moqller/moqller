﻿using MOQller.Data.Infrastructure;
using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Service
{
    public interface IOrderStatusService
    {
        OrderStatus Add (OrderStatus OrderStatus);

        void Update (OrderStatus OrderStatus);

        OrderStatus Delete (int id);

        IEnumerable<OrderStatus> GetAll ();

        IEnumerable<OrderStatus> GetAll (string keyword);

        OrderStatus GetById (int id);

        OrderStatus GetByName (string NameStatus);

        OrderStatus GetByCode (string Code);

        bool CheckNameOrderStatus (int Id, string Name);

        void Save ();
    }

    public class OrderStatusService : IOrderStatusService
    {
        private IOrderStatusRepository _OrderStatusRepository;
        private IUnitOfWork _unitOfWork;

        public OrderStatusService (IOrderStatusRepository OrderStatusRepository, IUnitOfWork unitOfWork)
        {
            this._OrderStatusRepository = OrderStatusRepository;
            this._unitOfWork = unitOfWork;
        }

        public OrderStatus Add (OrderStatus OrderStatus)
        {
            return _OrderStatusRepository.Add(OrderStatus);
        }

        public OrderStatus Delete (int id)
        {
            return _OrderStatusRepository.Delete(id);
        }

        public IEnumerable<OrderStatus> GetAll ()
        {
            return _OrderStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<OrderStatus> GetAll (string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _OrderStatusRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()) || x.Name.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.Name);
            else
                return _OrderStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public OrderStatus GetById (int id)
        {
            return _OrderStatusRepository.GetSingleById(id);
        }

        public OrderStatus GetByName (string NameStatus)
        {
            return _OrderStatusRepository.GetSingleByCondition(x => x.Name.ToLower().Contains(NameStatus.ToLower()));
        }

        public OrderStatus GetByCode (string Code)
        {
            return _OrderStatusRepository.GetSingleByCondition(x => x.Code.ToLower() == Code.Trim().ToLower());
        }

        public bool CheckNameOrderStatus (int Id, string Name)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (_OrderStatusRepository.Count(x => x.Name == Name.Trim() && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public void Save ()
        {
            _unitOfWork.Commit();
        }

        public void Update (OrderStatus OrderStatus)
        {
            _OrderStatusRepository.Update(OrderStatus);
        }
    }
}
