﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IMaterialTempDataService
    {
        MaterialTempData Add(MaterialTempData Country);

        void Update(MaterialTempData Country);

        MaterialTempData Delete(int id);

        IEnumerable<MaterialTempData> GetAll();

        IEnumerable<MaterialTempData> GetWork ();

        MaterialTempData GetById(Int64 id);

        void Save();
    }

    public class MaterialTempDataService : IMaterialTempDataService
    {
        private IMaterialTempDataRepository _MaterialTempDataRepository;
        private IUnitOfWork _unitOfWork;

        public MaterialTempDataService(IMaterialTempDataRepository MaterialTempDataRepository, IUnitOfWork unitOfWork)
        {
            this._MaterialTempDataRepository = MaterialTempDataRepository;
            this._unitOfWork = unitOfWork;
        }

        public MaterialTempData Add(MaterialTempData MaterialTempData)
        {
            return _MaterialTempDataRepository.Add(MaterialTempData);
        }

        public MaterialTempData Delete(int id)
        {
            return _MaterialTempDataRepository.Delete(id);
        }

        public IEnumerable<MaterialTempData> GetAll()
        {
            return _MaterialTempDataRepository.GetAll().OrderBy(x => x.UploadDate);
        }

        public IEnumerable<MaterialTempData> GetWork()
        {
            return _MaterialTempDataRepository.GetMulti(x => x.Status != 1).OrderBy(x => x.UploadDate);
        }


        public MaterialTempData GetById(Int64 id)
        {
            return _MaterialTempDataRepository.GetSingleById(id);
        }


        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(MaterialTempData MaterialData)
        {
            _MaterialTempDataRepository.Update(MaterialData);
        }
    }
}
