﻿namespace MOQller.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}