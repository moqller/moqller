﻿using System;

namespace MOQller.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        MOQllerDbContext Init();
    }
}