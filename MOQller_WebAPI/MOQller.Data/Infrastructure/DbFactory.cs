﻿namespace MOQller.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private MOQllerDbContext dbContext;

        public MOQllerDbContext Init()
        {
            return dbContext ?? (dbContext = new MOQllerDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}