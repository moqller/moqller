﻿using Microsoft.AspNet.Identity.EntityFramework;
using MOQller.Model.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MOQller.Data
{
    public class MOQllerDbContext : IdentityDbContext<AppUser>
    {
        public MOQllerDbContext () : base("MOQllerConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Footer> Footers { set; get; }

        public DbSet<Order> Orders { set; get; }
        public DbSet<OrderDetail> OrderDetails { set; get; }
        public DbSet<OrderDetailStatus> OrderDetailStatuses { set; get; }

        public DbSet<Page> Pages { set; get; }
        public DbSet<Post> Posts { set; get; }
        public DbSet<PostCategory> PostCategories { set; get; }
        public DbSet<PostTag> PostTags { set; get; }
        public DbSet<Product> Products { set; get; }

        public DbSet<ProductCategory> ProductCategories { set; get; }
        public DbSet<ProductTag> ProductTags { set; get; }
        public DbSet<Slide> Slides { set; get; }
        public DbSet<SupportOnline> SupportOnlines { set; get; }
        public DbSet<SystemConfig> SystemConfigs { set; get; }

        public DbSet<Tag> Tags { set; get; }

        public DbSet<VisitorStatistic> VisitorStatistics { set; get; }
        public DbSet<Error> Errors { set; get; }
        public DbSet<ContactDetail> ContactDetails { set; get; }
        public DbSet<Feedback> Feedbacks { set; get; }

        public DbSet<Function> Functions { set; get; }
        public DbSet<Permission> Permissions { set; get; }
        public DbSet<AppRole> AppRoles { set; get; }
        public DbSet<IdentityUserRole> UserRoles { set; get; }


        public DbSet<Color> Colors { set; get; }
        public DbSet<Size> Sizes { set; get; }
        public DbSet<ProductQuantity> ProductQuantities { set; get; }
        public DbSet<ProductImage> ProductImages { set; get; }

        public DbSet<Announcement> Announcements { set; get; }
        public DbSet<AnnouncementUser> AnnouncementUsers { set; get; }
        public DbSet<Country> Countries { set; get; }
        public DbSet<Parameter> Parameters { set; get; }
        public DbSet<ShippingStatus> ShippingStatuses { set; get; }
        public DbSet<ConfirmStatus> ConfirmStatuses { set; get; }
        public DbSet<State> States { set; get; }
        public DbSet<Material> Materials { set; get; }
        public DbSet<MaterialList> MaterialLists { set; get; }

        public DbSet<MaterialMedia> MaterialMedia { set; get; }
        public DbSet<PaymentStatus> PaymentStatuses { set; get; }
        public DbSet<DeliveryStatus> DeliveryStatuses { set; get; }
        public DbSet<ShippingMethod> ShippingMethods { set; get; }
        public DbSet<BOM> BOM { get; set; }
        public DbSet<BOMItem> BOMItem { get; set; }

        public DbSet<Cart> Carts { set; get; }
        // public DbSet<CartDetail> CartDetails { set; get; }

        public DbSet<OrderStatus> OrderStatuses { get; set; }

        public DbSet<Address> Address { get; set; }

        public DbSet<MaterialTempData> MaterialTempData { get; set; }

        public static MOQllerDbContext Create ()
        {
            return new MOQllerDbContext();
        }

        protected override void OnModelCreating (DbModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasKey<string>(r => r.Id).ToTable("AppRoles");
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("AppUserRoles");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("AppUserLogins");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("AppUserClaims");


            // Khong cho phep tu động xóa orderdetailstatus khi oderstatus bi xoa, ko hieu tai sao 
            builder.Entity<OrderDetailStatus>().HasRequired(c => c.OrderDetail).WithMany().WillCascadeOnDelete(false);
            //builder.Entity<BillingInfo>().HasRequired(c => c.States).WithMany().WillCascadeOnDelete(false);
            //builder.Entity<DeliveryInfo>().HasRequired(c => c.States).WithMany().WillCascadeOnDelete(false);



            // rewrite lại các Custom Attribute như decimall ...
            foreach (Type classType in from t in Assembly.GetAssembly(typeof(DecimalPrecisionAttribute)).GetTypes()
                                       where t.IsClass && t.Namespace == "MOQller.Model.Models"
                                       select t)
            {
                foreach (var propAttr in classType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.GetCustomAttribute<DecimalPrecisionAttribute>() != null).Select(
                       p => new { prop = p, attr = p.GetCustomAttribute<DecimalPrecisionAttribute>(true) }))
                {

                    var entityConfig = builder.GetType().GetMethod("Entity").MakeGenericMethod(classType).Invoke(builder, null);
                    ParameterExpression param = ParameterExpression.Parameter(classType, "c");
                    Expression property = Expression.Property(param, propAttr.prop.Name);
                    LambdaExpression lambdaExpression = Expression.Lambda(property, true,
                                                                             new ParameterExpression[]
                                                                                 {param});
                    DecimalPropertyConfiguration decimalConfig;
                    if (propAttr.prop.PropertyType.IsGenericType && propAttr.prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        MethodInfo methodInfo = entityConfig.GetType().GetMethods().Where(p => p.Name == "Property").ToList()[7];
                        decimalConfig = methodInfo.Invoke(entityConfig, new[] { lambdaExpression }) as DecimalPropertyConfiguration;
                    }
                    else
                    {
                        MethodInfo methodInfo = entityConfig.GetType().GetMethods().Where(p => p.Name == "Property").ToList()[6];
                        decimalConfig = methodInfo.Invoke(entityConfig, new[] { lambdaExpression }) as DecimalPropertyConfiguration;
                    }

                    decimalConfig.HasPrecision(propAttr.attr.Precision, propAttr.attr.Scale);
                }
            }
        }
    }
}