namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateBOM : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BomItem", "BOMId", "dbo.BOM");
            DropIndex("dbo.BomItem", new[] { "BOMId" });
            DropPrimaryKey("dbo.BOM");
            DropPrimaryKey("dbo.BomItem");
            AddColumn("dbo.BOM", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.BOM", "CreatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.BOM", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.BOM", "UpdatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.BOM", "MetaKeyword", c => c.String(maxLength: 256));
            AddColumn("dbo.BOM", "MetaDescription", c => c.String(maxLength: 256));
            AddColumn("dbo.BOM", "Status", c => c.Boolean(nullable: false));
            AddColumn("dbo.BomItem", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.BomItem", "CreatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.BomItem", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.BomItem", "UpdatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.BomItem", "MetaKeyword", c => c.String(maxLength: 256));
            AddColumn("dbo.BomItem", "MetaDescription", c => c.String(maxLength: 256));
            AddColumn("dbo.BomItem", "Status", c => c.Boolean(nullable: false));
            AlterColumn("dbo.BOM", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.BOM", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.BomItem", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.BomItem", "BOMId", c => c.Long(nullable: false));
            AlterColumn("dbo.BomItem", "PartNumber", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.BOM", "Id");
            AddPrimaryKey("dbo.BomItem", "Id");
            CreateIndex("dbo.BomItem", "BOMId");
            AddForeignKey("dbo.BomItem", "BOMId", "dbo.BOM", "Id", cascadeDelete: true);
            DropColumn("dbo.BOM", "CreateDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BOM", "CreateDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.BomItem", "BOMId", "dbo.BOM");
            DropIndex("dbo.BomItem", new[] { "BOMId" });
            DropPrimaryKey("dbo.BomItem");
            DropPrimaryKey("dbo.BOM");
            AlterColumn("dbo.BomItem", "PartNumber", c => c.String(nullable: false));
            AlterColumn("dbo.BomItem", "BOMId", c => c.Int(nullable: false));
            AlterColumn("dbo.BomItem", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.BOM", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.BOM", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.BomItem", "Status");
            DropColumn("dbo.BomItem", "MetaDescription");
            DropColumn("dbo.BomItem", "MetaKeyword");
            DropColumn("dbo.BomItem", "UpdatedBy");
            DropColumn("dbo.BomItem", "UpdatedDate");
            DropColumn("dbo.BomItem", "CreatedBy");
            DropColumn("dbo.BomItem", "CreatedDate");
            DropColumn("dbo.BOM", "Status");
            DropColumn("dbo.BOM", "MetaDescription");
            DropColumn("dbo.BOM", "MetaKeyword");
            DropColumn("dbo.BOM", "UpdatedBy");
            DropColumn("dbo.BOM", "UpdatedDate");
            DropColumn("dbo.BOM", "CreatedBy");
            DropColumn("dbo.BOM", "CreatedDate");
            AddPrimaryKey("dbo.BomItem", "Id");
            AddPrimaryKey("dbo.BOM", "Id");
            CreateIndex("dbo.BomItem", "BOMId");
            AddForeignKey("dbo.BomItem", "BOMId", "dbo.BOM", "Id", cascadeDelete: true);
        }
    }
}
