namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConfirmStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConfirmStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        MetaKeyword = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ConfirmStatuses");
        }
    }
}
