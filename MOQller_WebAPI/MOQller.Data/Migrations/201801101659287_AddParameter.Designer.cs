// <auto-generated />
namespace MOQller.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddParameter : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddParameter));
        
        string IMigrationMetadata.Id
        {
            get { return "201801101659287_AddParameter"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
