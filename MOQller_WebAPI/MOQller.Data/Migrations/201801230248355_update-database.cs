namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MaterialLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SellerId = c.String(nullable: false, maxLength: 128),
                        MaterialId = c.Int(nullable: false),
                        AvailableQty = c.Int(nullable: false),
                        MaxOfQuantity = c.Int(nullable: false),
                        MOQ = c.Int(nullable: false),
                        Distributer = c.String(maxLength: 250),
                        Supplier = c.String(maxLength: 250),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LeadTime = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UploadDate = c.DateTime(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 250),
                        TechLink = c.String(maxLength: 250),
                        ShippingStatusId = c.Int(nullable: false),
                        ApproveStatus = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        MetaKeyword = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.SellerId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.MaterialId, cascadeDelete: true)
                .ForeignKey("dbo.ShippingStatuses", t => t.ShippingStatusId, cascadeDelete: true)
                .Index(t => t.SellerId)
                .Index(t => t.MaterialId)
                .Index(t => t.ShippingStatusId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaterialLists", "ShippingStatusId", "dbo.ShippingStatuses");
            DropForeignKey("dbo.MaterialLists", "MaterialId", "dbo.Materials");
            DropForeignKey("dbo.MaterialLists", "SellerId", "dbo.AppUsers");
            DropIndex("dbo.MaterialLists", new[] { "ShippingStatusId" });
            DropIndex("dbo.MaterialLists", new[] { "MaterialId" });
            DropIndex("dbo.MaterialLists", new[] { "SellerId" });
            DropTable("dbo.MaterialLists");
        }
    }
}
