namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(nullable: false, maxLength: 100),
                        Address1 = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 100),
                        PhoneNumber = c.String(nullable: false, maxLength: 30),
                        City = c.String(nullable: false, maxLength: 100),
                        StadeId = c.Int(nullable: true),
                        Address2 = c.String(nullable: false, maxLength: 200),
                        ZipCode = c.String(nullable: false, maxLength: 20),
                        CountryId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        MetaKeyword = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.StadeId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.StadeId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.OrderStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        MetaKeyword = c.String(maxLength: 256),
                        MetaDescription = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Carts", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.Carts", "CreatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.Carts", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.Carts", "UpdatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.Carts", "MetaKeyword", c => c.String(maxLength: 256));
            AddColumn("dbo.Carts", "MetaDescription", c => c.String(maxLength: 256));
            AddColumn("dbo.Carts", "Status", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "BillingInfoId", c => c.Long(nullable: true));
            AddColumn("dbo.Orders", "DeliveryInfoId", c => c.Long(nullable: true));
            AddColumn("dbo.Orders", "TotalQuantity", c => c.Decimal(nullable: false, precision: 20, scale: 6));
            AddColumn("dbo.Orders", "SubTotal", c => c.Decimal(nullable: false, precision: 20, scale: 6));
            AddColumn("dbo.Orders", "TransactionAmount", c => c.Decimal(nullable: false, precision: 20, scale: 6));
            AddColumn("dbo.Orders", "OrderStatusId", c => c.Int(nullable: true));
            CreateIndex("dbo.Orders", "BillingInfoId");
            CreateIndex("dbo.Orders", "DeliveryInfoId");
            CreateIndex("dbo.Orders", "OrderStatusId");
            AddForeignKey("dbo.Orders", "BillingInfoId", "dbo.Address", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Orders", "DeliveryInfoId", "dbo.Address", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatuses", "Id", cascadeDelete: true);
            DropColumn("dbo.Orders", "TotalNet");
            DropColumn("dbo.Orders", "OrderStatus");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "OrderStatus", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "TotalNet", c => c.Decimal(nullable: false, precision: 20, scale: 6));
            DropForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatuses");
            DropForeignKey("dbo.Orders", "DeliveryInfoId", "dbo.Address");
            DropForeignKey("dbo.Orders", "BillingInfoId", "dbo.Address");
            DropForeignKey("dbo.Address", "StadeId", "dbo.States");
            DropForeignKey("dbo.Address", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Address", "UserId", "dbo.AppUsers");
            DropIndex("dbo.Orders", new[] { "OrderStatusId" });
            DropIndex("dbo.Orders", new[] { "DeliveryInfoId" });
            DropIndex("dbo.Orders", new[] { "BillingInfoId" });
            DropIndex("dbo.Address", new[] { "CountryId" });
            DropIndex("dbo.Address", new[] { "StadeId" });
            DropIndex("dbo.Address", new[] { "UserId" });
            DropColumn("dbo.Orders", "OrderStatusId");
            DropColumn("dbo.Orders", "TransactionAmount");
            DropColumn("dbo.Orders", "SubTotal");
            DropColumn("dbo.Orders", "TotalQuantity");
            DropColumn("dbo.Orders", "DeliveryInfoId");
            DropColumn("dbo.Orders", "BillingInfoId");
            DropColumn("dbo.Carts", "Status");
            DropColumn("dbo.Carts", "MetaDescription");
            DropColumn("dbo.Carts", "MetaKeyword");
            DropColumn("dbo.Carts", "UpdatedBy");
            DropColumn("dbo.Carts", "UpdatedDate");
            DropColumn("dbo.Carts", "CreatedBy");
            DropColumn("dbo.Carts", "CreatedDate");
            DropTable("dbo.OrderStatuses");
            DropTable("dbo.Address");
        }
    }
}
