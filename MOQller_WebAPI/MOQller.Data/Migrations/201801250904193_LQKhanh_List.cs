namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LQKhanh_List : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaterialMedia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialId = c.Int(nullable: false),
                        MediaName = c.String(nullable: false, maxLength: 50),
                        MaterialType = c.Int(nullable: false),
                        DocumentURL = c.String(maxLength: 200),
                        DocumentData = c.Binary(maxLength: 8000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ShippingMethods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ShippingMethods");
            DropTable("dbo.PaymentStatuses");
            DropTable("dbo.MaterialMedia");
            DropTable("dbo.DeliveryStatuses");
        }
    }
}
