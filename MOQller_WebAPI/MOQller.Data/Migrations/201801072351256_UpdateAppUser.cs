namespace MOQller.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAppUser : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.AppUsers", "CountryId", c => c.Int(nullable: false));
            //AddColumn("dbo.AppUsers", "FirstName", c => c.String(nullable: false, maxLength: 256));
            //AddColumn("dbo.AppUsers", "LastName", c => c.String(nullable: false, maxLength: 256));
            //AddColumn("dbo.AppUsers", "Company", c => c.String(nullable: false, maxLength: 256));
            //AddColumn("dbo.AppUsers", "Occupation", c => c.String(maxLength: 256));
            //AddColumn("dbo.AppUsers", "Address2", c => c.String(maxLength: 256));
            //AddColumn("dbo.AppUsers", "City", c => c.String(nullable: false, maxLength: 256));
            //AddColumn("dbo.AppUsers", "State", c => c.String(maxLength: 256));
            //AddColumn("dbo.AppUsers", "ZipCode", c => c.String(nullable: false, maxLength: 20));
            //AddColumn("dbo.AppUsers", "TaxRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }

        public override void Down()
        {
            //DropColumn("dbo.AppUsers", "TaxRate");
            //DropColumn("dbo.AppUsers", "ZipCode");
            //DropColumn("dbo.AppUsers", "State");
            //DropColumn("dbo.AppUsers", "City");
            //DropColumn("dbo.AppUsers", "Address2");
            //DropColumn("dbo.AppUsers", "Occupation");
            //DropColumn("dbo.AppUsers", "Company");
            //DropColumn("dbo.AppUsers", "LastName");
            //DropColumn("dbo.AppUsers", "FirstName");
            //DropColumn("dbo.AppUsers", "CountryId");
        }
    }
}
