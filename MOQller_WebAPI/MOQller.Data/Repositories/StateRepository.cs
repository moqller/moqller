﻿using System.Collections.Generic;
using System.Linq;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;

namespace MOQller.Data.Repositories
{
    public interface IStateRepository : IRepository<State>
    {
        State GetByCode(string code);
        IEnumerable<State> GetByCountryId(int countryId);
    }

    public class StateRepository : RepositoryBase<State>, IStateRepository
    {
        public StateRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public State GetByCode(string code)
        {
            return this.DbContext.States.Where(x => x.Code == code).SingleOrDefault();
        }
        public IEnumerable<State> GetByCountryId(int countryId)
        {
            return this.DbContext.States.Where(x => x.CountryId == countryId);
        }
    }
}