﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Models;
using MOQller.Data.Infrastructure;
using System.Data.SqlClient;

namespace MOQller.Data.Repositories
{
    public interface IBOMItemRepository : IRepository<BOMItem>
    {
        List<ResultBOM> GetAll(Int64 bomId, string buyer);
        
    }
    public class BOMItemRepository : RepositoryBase<BOMItem>, IBOMItemRepository
    {
        public BOMItemRepository(IDbFactory dbFactory)
            : base(dbFactory)
        { }
        public List<ResultBOM> GetAll(Int64 bomId, string buyer)
        {
            return this.DbContext.Database.SqlQuery<ResultBOM>(
            "EXEC SearchBom @param1,@buyerId",
            new SqlParameter("param1", bomId),
            new SqlParameter("buyerId", buyer)
            ).ToList();

        }
    }
    public class ResultBOM
    {
        public Int64? Id { get; set; }
        public int? MaterialListId { get; set; }
        public string PartNumber { get; set; }
        public string Manufacturer { get; set; }
        public  string Description { get; set; }
        public string SellerId { get; set; }
        public int? AvailableQty { get; set; }
        public int? MOQ { get; set; }
        public string DueDate  { get; set; }
        public int? RequestQty { get; set; }
        public Decimal? Price { get; set; }
        public Decimal? ExtendedPrice { get; set; }
        public string ShippingStatus { get; set; }
        public int MatchedType { get; set; }
        public string TechLink { get; set; }
        public int InCart { get; set; }
        public string DateCode { get; set;}
        public string Condition { get; set; }
		public int? MaterialId { get; set; }
    }
}
