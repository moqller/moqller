﻿using MOQller.Data.Infrastructure;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Data.Repositories
{
    public interface IMaterialTempDataRepository : IRepository<MaterialTempData>
    {

    }

    public class MaterialTempDataRepository : RepositoryBase<MaterialTempData>, IMaterialTempDataRepository
    { 
        public MaterialTempDataRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
