﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Models;
using MOQller.Data.Infrastructure;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace MOQller.Data.Repositories
{
    public interface IBOMRepository : IRepository<BOM>
    {
        List<BOM> GetByUserId(string userId);
        object DeleteById(int Id);
        object AddBOM(string fileName, string userId, string xml);
        object UpdateBOM(Int64 bomId, string xml);
        List<ResultBOM> SearchRowBom(Int64? id, string partNumber, int requestQty, string BuyerId);
        List<ResultBOM> GetByPartNumber(ResultBOM bomitem);
        object UpdateBomChangeName(Int64 bomId, string bomName, string xml);
    }
    public class BOMRepository : RepositoryBase<BOM>, IBOMRepository
    {
        public BOMRepository(IDbFactory dbFactory)
            : base(dbFactory)
        { }

        public List<BOM> GetByUserId(string userId)
        {   

            return this.DbContext.Database.SqlQuery<BOM>(
            "EXEC GetBOMByUserId @param1",
            new SqlParameter("param1", userId)
            ).ToList();

        }

        public object DeleteById(int Id)
        {
            return this.DbContext.Database.ExecuteSqlCommand("EXEC DeleteBOM @Id", new SqlParameter("@Id", Id));
        }

        public object AddBOM(string fileName, string userId, string xml)
        {
            return this.DbContext.Database.ExecuteSqlCommand(
            "EXEC AddBom @fileName, @userId, @xml",
            new SqlParameter("fileName", fileName),
            new SqlParameter("userId", userId),
            new SqlParameter("xml", xml)
            );
        }

        public object UpdateBOM( Int64 bomId, string xml)
        {
            return this.DbContext.Database.ExecuteSqlCommand(
            "EXEC UpdateBom @bomId, @xml",
            new SqlParameter("bomId", bomId),
            new SqlParameter("xml", xml)
            );
        }

        public List<ResultBOM> SearchRowBom(Int64? id, string partNumber, int requestQty, string BuyerId)
        {
            return this.DbContext.Database.SqlQuery<ResultBOM>(
                "EXEC SearchRowBom @Id,@PartNumber, @RequestQty,@BuyerId",
                new SqlParameter("Id", id ?? SqlInt64.Null),
                new SqlParameter("PartNumber", partNumber),
                new SqlParameter("RequestQty", requestQty),
                new SqlParameter("BuyerId", BuyerId)
                ).ToList();
        }
        
        public List<ResultBOM> GetByPartNumber(ResultBOM bom)
        {
            return this.DbContext.Database.SqlQuery<ResultBOM>(
                "EXEC GetByPartNumber @Id, @PartNumber, @SellerId",
                new SqlParameter("Id", bom.Id ?? SqlInt64.Null),
                new SqlParameter("PartNumber", bom.PartNumber),
                new SqlParameter("MaterialListId", bom.MaterialListId)).ToList();
        }

        public object UpdateBomChangeName(Int64 bomId, string bomName, string xml)
        {
            //string result = string.Empty;
            return this.DbContext.Database.ExecuteSqlCommand(
            "EXEC UpdateBomChangeName @bomId,@bomName, @xml",
            new SqlParameter("bomId", bomId),
            new SqlParameter("bomName",bomName),
            new SqlParameter("xml", xml)       
            
            );
        }
    }
}

