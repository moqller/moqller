﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Data.Repositories
{
    public interface IConfirmStatusRepository : IRepository<ConfirmStatus>
    {

    }

    public class ConfirmStatusRepository : RepositoryBase<ConfirmStatus>, IConfirmStatusRepository
    {
        public ConfirmStatusRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
