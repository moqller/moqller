﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;

namespace MOQller.Data.Repositories
{
    public interface IShippingMethodRepository : IRepository<ShippingMethod>
    {

    }

    public class ShippingMethodRepository : RepositoryBase<ShippingMethod>, IShippingMethodRepository
    {
        public ShippingMethodRepository (IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

    }
}
