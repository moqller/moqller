﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;

namespace MOQller.Data.Repositories
{
    public interface IDeliveryStatusRepository : IRepository<DeliveryStatus>
    {

    }

    public class DeliveryStatusRepository : RepositoryBase<DeliveryStatus>, IDeliveryStatusRepository
    {
        public DeliveryStatusRepository (IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

    }
}
