﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Data.Repositories
{
    public interface IMaterialRepository : IRepository<Material>
    {
        
    }

    public class MaterialRepository : RepositoryBase<Material>, IMaterialRepository
    {
        public MaterialRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

    }
}
