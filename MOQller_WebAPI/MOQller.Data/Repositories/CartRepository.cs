﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;
using System.Data.SqlClient;

namespace MOQller.Data.Repositories
{
    public interface ICartRepository : IRepository<Cart>
    {

    }

    public class cartRepository : RepositoryBase<Cart>, ICartRepository
    {
        public cartRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
    
}
