﻿using System.Collections.Generic;
using System.Data.SqlClient;
using MOQller.Common.ViewModels;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;
using System.Linq;
using System;
using System.Globalization;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;


namespace MOQller.Data.Repositories
{
    public interface IOrderDetailStatusRepository : IRepository<OrderDetailStatus>
    {
    }

    public class OrderDetailStatusRepository : RepositoryBase<OrderDetailStatus>, IOrderDetailStatusRepository
    {
        public OrderDetailStatusRepository (IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
