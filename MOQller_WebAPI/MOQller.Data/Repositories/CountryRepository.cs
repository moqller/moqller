﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Data.Repositories
{
    public interface ICountryRepository : IRepository<Country>
    {
        Country GetByCode(string code);
    }

    public class CountryRepository : RepositoryBase<Country>, ICountryRepository
    {
        public CountryRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public Country GetByCode(string code)
        {
            return this.DbContext.Countries.Where(x => x.Code == code).SingleOrDefault();
        }
    }
}
