﻿using System.Collections.Generic;
using System.Data.SqlClient;
using MOQller.Common.ViewModels;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;
using System.Linq;
using System;
using System.Globalization;
using System.Data.Entity;

namespace MOQller.Data.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate);

        OrderStatisticsViewModel GetOrderStatistic(string userId, DateTime fromDate, DateTime toDate);

        List<OrderMonthStatisticsViewModel> GetMonthOrderStatistic(string userId, int year);

        List<Order> GetByUserId (string userId);

        List<OrderDetail> GetBySellerUserId (string userId);

        AppUser GetSellerByMaterialListId(int materialListId);

        AppUser GetUserById(string BuyerId);

        IEnumerable<BuyingViewModel> GetBuying (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId);

        IEnumerable<SellingViewModel> GetSelling (string username, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter);

        IEnumerable<MaterialAlertDay> GetMaterialAlertDay(string userId, int year, int month);
    }

    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public OrderStatisticsViewModel GetOrderStatistic(string userId, DateTime fromDate, DateTime toDate)
        {

            return this.DbContext.Database.SqlQuery<OrderStatisticsViewModel>(
                "EXEC GetOrderStatistic @UserId,@FromDate, @ToDate",
                new SqlParameter("UserId", userId),
                new SqlParameter("FromDate", fromDate),
                new SqlParameter("ToDate", toDate)).SingleOrDefault();
        }

        public List<OrderMonthStatisticsViewModel> GetMonthOrderStatistic(string userId, int year)
        {
            return this.DbContext.Database.SqlQuery<OrderMonthStatisticsViewModel>(
                "EXEC GetOrderMonthStatistic @UserId, @Year",
                new SqlParameter("@UserId", userId),
                new SqlParameter("Year", year)
                ).ToList();
        }
        public List<Order> GetByUserId(string userId)
        {
            return this.DbContext.Database.SqlQuery<Order>(
            "EXEC GetOrderManager @param1",
            new SqlParameter("param1", userId)
            ).ToList();
        }

        public List<OrderDetail> GetBySellerUserId(string userId)
        {
            return this.DbContext.Database.SqlQuery<OrderDetail>(
            "EXEC GetSellerOrderManager @param1",
            new SqlParameter("param1", userId)
            ).ToList();
        }

        public IEnumerable<BuyingViewModel> GetBuying(string userId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId)
        {
            return this.DbContext.Database.SqlQuery<BuyingViewModel>(
            "EXEC GetOrderBuyingTransaction @userID, @StartDate, @EndDate, @PaymentId, @deliveryId, @OrderStatusId",
            new SqlParameter("userID", userId),
            new SqlParameter("StartDate", StartDate ?? ""),
            new SqlParameter("EndDate", EndDate ?? ""),
            new SqlParameter("PaymentId", PaymentId),
            new SqlParameter("deliveryId", deliveryId),
            new SqlParameter("OrderStatusId", OrderStatusId)
            ).ToList();
        }

        public IEnumerable<SellingViewModel> GetSelling(string userId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, string filter)
        {
            return this.DbContext.Database.SqlQuery<SellingViewModel>(
            "EXEC GetOrderSellingTransaction  @userID, @StartDate, @EndDate, @PaymentId, @DeliveryId, @OrderStatusId, @Filter",
            new SqlParameter("userID", userId),
            new SqlParameter("StartDate", StartDate ?? ""),
            new SqlParameter("EndDate", EndDate ?? ""),
            new SqlParameter("PaymentId", PaymentId),
            new SqlParameter("DeliveryId", deliveryId),
            new SqlParameter("OrderStatusId", OrderStatusId),
            new SqlParameter("Filter", filter ?? "")
            ).ToList();
        }

        public AppUser GetSellerByMaterialListId(int materialListId)
        {
            var query = from ml in DbContext.MaterialLists
                        join u in DbContext.Users
                        on ml.SellerId equals u.Id
                        where ml.Id == materialListId
                        select u;
            return query.FirstOrDefault();
        }

        public AppUser GetUserById(string BuyerId)
        {
            var query = from ml in DbContext.Users
                        where ml.Id == BuyerId
                        select ml;
            return query.FirstOrDefault();
        }

        public IEnumerable<RevenueStatisticViewModel> GetRevenueStatistic(string fromDate, string toDate)
        {
            var query = from o in DbContext.Orders
                        join od in DbContext.OrderDetails
                        on o.Id equals od.OrderId
                        join p in DbContext.MaterialLists
                        on od.MaterialListId equals p.Id
                        select new
                        {
                            CreatedDate = o.CreatedDate,
                            Quantity = od.Quantity
                            //Price = od.Price,
                            //OriginalPrice = p.OriginalPrice
                        };
            if (!string.IsNullOrEmpty(fromDate))
            {
                DateTime start = DateTime.ParseExact(fromDate, "MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));

                query = query.Where(x => x.CreatedDate >= start);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                DateTime endDate = DateTime.ParseExact(toDate, "MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));

                query = query.Where(x => x.CreatedDate <= endDate);
            }

            var result = query.GroupBy(x => DbFunctions.TruncateTime(x.CreatedDate ?? DateTime.Now))
                .Select(r => new
                {
                    Date = r.Key.Value
                    //TotalBuy = r.Sum(x => x.OriginalPrice * x.Quantity),
                    //TotalSell = r.Sum(x => x.Price * x.Quantity),
                }).Select(x => new RevenueStatisticViewModel()
                {
                    Date = x.Date
                    //Benefit = x.TotalSell - x.TotalBuy,
                    //Revenues = x.TotalSell
                });
            return result.ToList();
        }

        public IEnumerable<MaterialAlertDay> GetMaterialAlertDay(string userId, int year, int month)
        {
            return this.DbContext.Database.SqlQuery<MaterialAlertDay>(
            "EXEC GetMaterialAlertDay  @UserId, @nYear, @nMonth",
            new SqlParameter("UserId", userId),
            new SqlParameter("nYear", year),
            new SqlParameter("nMonth", month)).ToList();
        }
    }
}