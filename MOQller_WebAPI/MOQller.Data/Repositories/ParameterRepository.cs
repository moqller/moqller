﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Data.Repositories
{
    public interface IParameterRepository : IRepository<Parameter>
    {
        Parameter GetByCode(string code);
    }

    public class ParameterRepository : RepositoryBase<Parameter>, IParameterRepository
    {
        public ParameterRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public Parameter GetByCode(string code)
        {
            return this.DbContext.Parameters.Where(x => x.Code == code).SingleOrDefault();
        }
    }
}
