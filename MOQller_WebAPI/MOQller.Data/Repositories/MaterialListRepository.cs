﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Common.ViewModels;
using MOQller.Data.Infrastructure;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace MOQller.Data.Repositories
{
    public interface IMaterialListRepository : IRepository<MaterialList>
    {
        List<UpLoadBom> GetUploadBom(string xmml);
        List<UpLoadBom> GetUploadMaterial(string xml, string userId);
        UpLoadBom GetInstock(UpLoadBom entity, string userId);

        List<ReviewMaterialByDataEntryViewModel> GetReviewMaterialByDataEntry(string StartDate, string EndDate, bool description, bool manufacturer, bool tecklink);
    }

    public class MaterialListRepository : RepositoryBase<MaterialList>, IMaterialListRepository
    {
        public MaterialListRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
        public List<UpLoadBom> GetUploadBom(string xml)
        {
            return this.DbContext.Database.SqlQuery<UpLoadBom>(
            "EXEC UploadBom @param1",
            new SqlParameter("param1", xml)
            ).ToList();

        }
        public List<UpLoadBom> GetUploadMaterial(string xml, string userId)
        {
            return this.DbContext.Database.SqlQuery<UpLoadBom>(
            "EXEC UploadMaterial @param1, @userId",
            new SqlParameter("param1", xml),
            new SqlParameter("userId", userId ?? SqlString.Null)
            ).ToList();

        }

        public UpLoadBom GetInstock(UpLoadBom entity, string userId)
        {
            return this.DbContext.Database.SqlQuery<UpLoadBom>(
                "EXEC GetInstock @PartNumber,@Manufacturer, @Description,@MOQ, @Price, @DueDate, @Stastus, @TechLink, @userId",
                new SqlParameter("PartNumber", entity.PartNumber),
                new SqlParameter("Manufacturer", entity.Manufacturer),
                new SqlParameter("Description", entity.Description),
                new SqlParameter("MOQ", entity.MOQ),
                new SqlParameter("Price", entity.Price),
                new SqlParameter("DueDate", entity.DueDate ?? SqlString.Null),
                new SqlParameter("Stastus", entity.ShippingStatusId),
                new SqlParameter("TechLink", entity.TechLink),
                new SqlParameter("userId", userId)
                ).SingleOrDefault();
        }

        public List<ReviewMaterialByDataEntryViewModel> GetReviewMaterialByDataEntry(string StartDate, string EndDate, bool description, bool manufacturer, bool tecklink)
        {
            return this.DbContext.Database.SqlQuery<ReviewMaterialByDataEntryViewModel>(
                "EXEC GetReviewMaterialByDataEntry @StartDate, @EndDate, @Description, @Manufacturer, @TechLink",
                new SqlParameter("StartDate", StartDate ?? SqlString.Null),
                new SqlParameter("EndDate", EndDate ?? SqlString.Null),
                new SqlParameter("Description", description),
                new SqlParameter("Manufacturer", manufacturer),
                new SqlParameter("TechLink", tecklink)).ToList();
        }
    }
    public class UpLoadBom
    {

        //public int? Id { get; set; }

        public string Sheet { get; set; }

        //public int? MaterialId { get; set; }

        public string SellerId { get; set; }

        public string RequestQty { get; set; }

        public int? AvailableQty { get; set; }

        public int? MOQ { get; set; }
		
		public int? Condition { get; set; }

        public string DateCode { get; set; }  

        public Decimal? Price { get; set; }

        public string Manufacturer { get; set; }

        public string DueDate { get; set; }

        public string Description { get; set; }

        public string TechLink { get; set; }

        public int ShippingStatusId { get; set; }

        public Int32? InStock { get; set; }

        //public int ApproveStatus { get; set; }

        public virtual AppUser AppUser { set; get; }

        public virtual Material Material { set; get; }

        public virtual ShippingStatus ShippingStatus { set; get; }

        public string PartNumber { get; set; }

        public int? MatchedType { get; set; }// lấy ra kết quả search

        //public int? MatchedUploadMaterial { get; set; } //1 : new partnumber, 2:duplicate khác qty, 3: duplicate with all cirterial, 4: missing infor, 5: other errors
    }
}
