﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;
using MOQller.Model.Models;

namespace MOQller.Data.Repositories
{
    public interface IPaymentStatusRepository : IRepository<PaymentStatus>
    {

    }

    public class PaymentStatusRepository : RepositoryBase<PaymentStatus>, IPaymentStatusRepository
    {
        public PaymentStatusRepository (IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

    }
}
