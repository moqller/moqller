﻿using MOQller.Data.Infrastructure;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Data.Repositories
{
    public interface IAddressRepository : IRepository<Address>
    {

    }

    public class AddressRepository : RepositoryBase<Address>, IAddressRepository
    {
        public AddressRepository (IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
