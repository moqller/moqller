﻿using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Data.Repositories
{
    public interface IShippingStatusRepository : IRepository<ShippingStatus>
    {
        
    }

    public class ShippingStatusRepository : RepositoryBase<ShippingStatus>, IShippingStatusRepository
    {
        public ShippingStatusRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
