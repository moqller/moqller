﻿using MOQller.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Model.Models;

namespace MOQller.Data.Repositories
{
    public interface IOrderStatusRepository : IRepository<OrderStatus>
    {

    }

    public class OrderStatusRepository : RepositoryBase<OrderStatus>, IOrderStatusRepository
    {
        public OrderStatusRepository (IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

    }
}