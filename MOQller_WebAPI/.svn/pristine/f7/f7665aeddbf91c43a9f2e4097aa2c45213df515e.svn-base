﻿using MOQller.Data.Repositories;
using MOQller.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MOQller.Data.Infrastructure;

namespace MOQller.Service
{
    public interface IShippingStatusService
    {
        ShippingStatus Add(ShippingStatus ShippingStatus);

        void Update(ShippingStatus ShippingStatus);

        ShippingStatus Delete(int id);

        IEnumerable<ShippingStatus> GetAll();

        IEnumerable<ShippingStatus> GetAll(string keyword);

        ShippingStatus GetById(int id);

        ShippingStatus GetByCode(string code);

        bool CheckNameShippingStatus (int Id, string Name, string Code);

        ShippingStatus GetIdByName(string name);

        void Save();
    }

    public class ShippingStatusService : IShippingStatusService
    {
        private IShippingStatusRepository _ShippingStatusRepository;
        private IUnitOfWork _unitOfWork;

        public ShippingStatusService(IShippingStatusRepository ShippingStatusRepository, IUnitOfWork unitOfWork)
        {
            this._ShippingStatusRepository = ShippingStatusRepository;
            this._unitOfWork = unitOfWork;
        }

        public ShippingStatus Add(ShippingStatus ShippingStatus)
        {
            return _ShippingStatusRepository.Add(ShippingStatus);
        }

        public ShippingStatus Delete(int id)
        {
            return _ShippingStatusRepository.Delete(id);
        }

        public IEnumerable<ShippingStatus> GetAll()
        {
            return _ShippingStatusRepository.GetAll().OrderBy(x => x.Name);
        }

        public IEnumerable<ShippingStatus> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
                return _ShippingStatusRepository.GetMulti(x => x.Name.ToLower().Contains(keyword.ToLower()))
                    .OrderBy(x => x.Name);
            else
                return _ShippingStatusRepository.GetAll().OrderBy(x => x.Name);
        }


        public ShippingStatus GetById(int id)
        {
            return _ShippingStatusRepository.GetSingleById(id);
        }

        public ShippingStatus GetByCode(string code)
        {
            return _ShippingStatusRepository.GetSingleByCondition(x => x.Code.Trim().ToLower() == code.Trim().ToLower());
        }

        public bool CheckNameShippingStatus (int Id, string Name, string Code)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (_ShippingStatusRepository.Count(x => (x.Name == Name.Trim().ToLower() || x.Code == Code.Trim().ToLower()) && x.Id != Id) > 0) return false;
                else
                    return true;
            }
            else
                return true;
        }

        public ShippingStatus GetIdByName(string name)
        {
            var status = _ShippingStatusRepository.GetAll().Where(x => x.Code.Trim().ToLower() == name.Trim().ToLower()).FirstOrDefault();
            return status;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ShippingStatus ShippingStatus)
        {
            _ShippingStatusRepository.Update(ShippingStatus);
        }
    }
}
