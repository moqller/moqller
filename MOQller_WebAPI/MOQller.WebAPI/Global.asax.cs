﻿using Microsoft.AspNet.SignalR;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using MOQller.Web.Mappings;
using System.IO;
using System.Web;

namespace MOQller.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfiguration.Configure();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Context.Response.AppendHeader("Access-Control-Allow-Credentials", "true");
            var referrer = Request.UrlReferrer;
            if (Context.Request.Path.Contains("signalr/") && referrer != null)
            {
                Context.Response.AppendHeader("Access-Control-Allow-Origin", referrer.Scheme + "://" + referrer.Authority);
                //Context.Response.AppendHeader("Access-Control-Allow-Origin", "http://moqller.ddns.net/");
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            string logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs.txt");
            using (StreamWriter sw = new StreamWriter(logFilePath, true, System.Text.Encoding.UTF8))
            {
                sw.Write(HttpContext.Current.Error);
            }
        }
    }
}