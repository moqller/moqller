﻿using AutoMapper;
using MOQller.Model.Models;
using MOQller.Web.Models.Country;
using MOQller.Web.Models;
using MOQller.Web.Models.Common;
using MOQller.Web.Models.Parameter;
using MOQller.Web.Models.ShippingStatus;
using MOQller.Web.Models.ConfirmStatus;
using MOQller.Web.Models.State;
using MOQller.Web.Models.MaterialList;
using MOQller.Web.Models.PaymentStatus;
using MOQller.Web.Models.DeliveryStatus;
using MOQller.Web.Models.ShippingMethod;
using MOQller.Web.Models.BOM;
using MOQller.Web.Models.BOMItem;
using MOQller.Web.Models.Cart;
using MOQller.Web.Models.OrderStatus;
using MOQller.Web.Models.Order;
using MOQller.Common.ViewModels;
using MOQller.Web.Models.MaterialTempData;
using MOQller.Common;
using System;
using MOQller.Common.Enums;

namespace MOQller.Web.Mappings
{
    //public class EnumValueResolver : ValueResolver<MaterialList, string>
    //{
    //    protected override string ResolveCore(MaterialList src)
    //    {
    //        var value = (ConditionEnum)Enum.Parse(typeof(ConditionEnum), src.Condition.GetType().ToString());

    //        return EnumHelper.GetEnumDescription(value);
    //    }
    //}

    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Post, PostViewModel>();
                cfg.CreateMap<PostCategory, PostCategoryViewModel>();
                cfg.CreateMap<Tag, TagViewModel>();
                cfg.CreateMap<ProductCategory, ProductCategoryViewModel>();
                cfg.CreateMap<Product, ProductViewModel>();
                cfg.CreateMap<ProductTag, ProductTagViewModel>();
                cfg.CreateMap<Footer, FooterViewModel>();
                cfg.CreateMap<Slide, SlideViewModel>();
                cfg.CreateMap<Page, PageViewModel>();
                cfg.CreateMap<ContactDetail, ContactDetailViewModel>();
                cfg.CreateMap<AppRole, ApplicationRoleViewModel>();
                cfg.CreateMap<AppUser, AppUserViewModel>();
                cfg.CreateMap<Function, FunctionViewModel>();
                cfg.CreateMap<Permission, PermissionViewModel>();
                cfg.CreateMap<ProductImage, ProductImageViewModel>();
                cfg.CreateMap<ProductQuantity, ProductQuantityViewModel>();
                cfg.CreateMap<Color, ColorViewModel>();
                cfg.CreateMap<Size, SizeViewModel>();
                cfg.CreateMap<Order, OrderViewModel>();
                cfg.CreateMap<OrderDetail, OrderDetailViewModel>();
                cfg.CreateMap<Announcement, AnnouncementViewModel>();
                cfg.CreateMap<AnnouncementUser, AnnouncementUserViewModel>();
                cfg.CreateMap<Country, CountryViewModel>();
                cfg.CreateMap<Parameter, ParameterViewModel>();
                cfg.CreateMap<ShippingStatus, ShippingStatusViewModel>();
                cfg.CreateMap<ConfirmStatus, ConfirmStatusViewModel>();
                cfg.CreateMap<State, StateViewModel>();

                cfg.CreateMap<Material, MaterialViewModel>();

                cfg.CreateMap<MaterialList, MaterialListViewModel>()
                .ForMember(
                    dest => dest.ConditionName,
                    opts => opts.ResolveUsing(src =>
                    {
                        // Get enum 
                        var value = (ConditionEnum)Enum.Parse(
                            typeof(ConditionEnum), src.Condition.ToString());
                        // return enum description
                        return EnumHelper.GetEnumDescription(value);
                    }));

                cfg.CreateMap<PaymentStatus, PaymentStatusViewModel>();
                cfg.CreateMap<DeliveryStatus, DeliveryStatusViewModel>();
                cfg.CreateMap<ShippingMethod, ShippingMethodViewModel>();
                cfg.CreateMap<BOM, BOMViewModel>();
                cfg.CreateMap<BOMItem, BOMItemViewModel>();

                cfg.CreateMap<Cart, CartViewModel>()
                .ForMember(dest => dest.MOQ, opts => opts.MapFrom(src => src.MaterialList.MOQ));

                cfg.CreateMap<OrderStatus, OrderStatusViewModel>();
                cfg.CreateMap<Address, AddressViewModel>();
                cfg.CreateMap<MaterialTempData, MaterialTempDataViewModel>();

                cfg.CreateMap<MaterialTempData, DetailTempDataViewModel>()
                .ForMember(dest => dest.SellerEmail, opts => opts.MapFrom(src => src.Seller.Email))
                .ForMember(dest => dest.SellerName, opts => opts.MapFrom(src => src.Seller.UserName));

                cfg.CreateMap<OrderDetail, OrderManagerSellerViewModel>()
                .ForMember(dest => dest.OrderNumber, opts => opts.MapFrom(src => src.Order.OrderNumber))
                .ForMember(dest => dest.PartNumber, opts => opts.MapFrom(src => src.MaterialList.Material.PartNumber))
                .ForMember(dest => dest.MOQ, opts => opts.MapFrom(src => src.MaterialList.MOQ))
                .ForMember(dest => dest.AvailableQty, opts => opts.MapFrom(src => src.MaterialList.AvailableQty))
                .ForMember(dest => dest.ShippingMethodName, opts => opts.MapFrom(src => src.Order.ShippingMethod.Name))
                .ForMember(dest => dest.DeliveryStatusesName, opts => opts.MapFrom(src => src.DeliveryStatuses.Name))
                .ForMember(dest => dest.OrderStatusesName, opts => opts.MapFrom(src => src.OrderStatuses.Name))
                .ForMember(dest => dest.OrderDetailId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.OrderId, opts => opts.MapFrom(src => src.Order.Id))
                .ForMember(dest => dest.DeliveryStatusCode, opts => opts.MapFrom(src => src.DeliveryStatuses.Code))
                .ForMember(dest => dest.MaterialId, opts => opts.MapFrom(src => src.MaterialList.Material.Id));
            });
        }

    }
}