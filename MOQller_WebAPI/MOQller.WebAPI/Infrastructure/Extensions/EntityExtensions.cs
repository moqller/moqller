﻿using MOQller.Model.Models;
using MOQller.Web.Models.Country;
using System;
using System.Globalization;
using MOQller.Web.Models;
using MOQller.Web.Models.Parameter;
using MOQller.Web.Models.ShippingStatus;
using MOQller.Web.Models.ConfirmStatus;
using MOQller.Web.Models.State;
using MOQller.Web.Models.MaterialList;
using MOQller.Web.Models.DeliveryStatus;
using MOQller.Web.Models.PaymentStatus;
using MOQller.Web.Models.ShippingMethod;
using MOQller.Web.Models.BOM;
using MOQller.Web.Models.Cart;
using MOQller.Web.Models.OrderStatus;
using MOQller.Web.Models.Order;
using System.Text;
using MOQller.Web.Models.MaterialTempData;

namespace MOQller.Web.Infrastructure.Extensions
{
    public static class EntityExtensions
    {
        public static void UpdatePostCategory (this PostCategory postCategory, PostCategoryViewModel postCategoryVm)
        {
            postCategory.ID = postCategoryVm.ID;
            postCategory.Name = postCategoryVm.Name;
            postCategory.Description = postCategoryVm.Description;
            postCategory.Alias = postCategoryVm.Alias;
            postCategory.ParentID = postCategoryVm.ParentID;
            postCategory.DisplayOrder = postCategoryVm.DisplayOrder;
            postCategory.Image = postCategoryVm.Image;
            postCategory.HomeFlag = postCategoryVm.HomeFlag;

            postCategory.CreatedDate = postCategoryVm.CreatedDate;
            postCategory.CreatedBy = postCategoryVm.CreatedBy;
            postCategory.UpdatedDate = postCategoryVm.UpdatedDate;
            postCategory.UpdatedBy = postCategoryVm.UpdatedBy;
            postCategory.MetaKeyword = postCategoryVm.MetaKeyword;
            postCategory.MetaDescription = postCategoryVm.MetaDescription;
            postCategory.Status = postCategoryVm.Status;
        }

        public static void UpdateProductCategory (this ProductCategory productCategory, ProductCategoryViewModel productCategoryVm)
        {
            productCategory.ID = productCategoryVm.ID;
            productCategory.Name = productCategoryVm.Name;
            productCategory.Description = productCategoryVm.Description;
            productCategory.Alias = productCategoryVm.Alias;
            productCategory.ParentID = productCategoryVm.ParentID;
            productCategory.DisplayOrder = productCategoryVm.DisplayOrder;
            productCategory.HomeOrder = productCategoryVm.HomeOrder;
            productCategory.Image = productCategoryVm.Image;
            productCategory.HomeFlag = productCategoryVm.HomeFlag;
            productCategory.HomeOrder = productCategoryVm.HomeOrder;
            productCategory.CreatedDate = productCategoryVm.CreatedDate;
            productCategory.CreatedBy = productCategoryVm.CreatedBy;
            productCategory.UpdatedDate = productCategoryVm.UpdatedDate;
            productCategory.UpdatedBy = productCategoryVm.UpdatedBy;
            productCategory.MetaKeyword = productCategoryVm.MetaKeyword;
            productCategory.MetaDescription = productCategoryVm.MetaDescription;
            productCategory.Status = productCategoryVm.Status;
        }

        public static void UpdatePost (this Post post, PostViewModel postVm)
        {
            post.ID = postVm.ID;
            post.Name = postVm.Name;
            post.Description = postVm.Description;
            post.Alias = postVm.Alias;
            post.CategoryID = postVm.CategoryID;
            post.Content = postVm.Content;
            post.Image = postVm.Image;
            post.HomeFlag = postVm.HomeFlag;
            post.ViewCount = postVm.ViewCount;

            post.CreatedDate = postVm.CreatedDate;
            post.CreatedBy = postVm.CreatedBy;
            post.UpdatedDate = postVm.UpdatedDate;
            post.UpdatedBy = postVm.UpdatedBy;
            post.MetaKeyword = postVm.MetaKeyword;
            post.MetaDescription = postVm.MetaDescription;
            post.Status = postVm.Status;
        }

        public static void UpdateProduct (this Product product, ProductViewModel productVm)
        {
            product.ID = productVm.ID;
            product.Name = productVm.Name;
            product.Description = productVm.Description;
            product.Alias = productVm.Alias;
            product.CategoryID = productVm.CategoryID;
            product.Content = productVm.Content;
            product.ThumbnailImage = productVm.ThumbnailImage;
            product.Price = productVm.Price;
            product.PromotionPrice = productVm.PromotionPrice;
            product.Warranty = productVm.Warranty;
            product.HomeFlag = productVm.HomeFlag;
            product.HotFlag = productVm.HotFlag;
            product.ViewCount = productVm.ViewCount;

            product.CreatedDate = productVm.CreatedDate;
            product.CreatedBy = productVm.CreatedBy;
            product.UpdatedDate = productVm.UpdatedDate;
            product.UpdatedBy = productVm.UpdatedBy;
            product.MetaKeyword = productVm.MetaKeyword;
            product.MetaDescription = productVm.MetaDescription;
            product.Status = productVm.Status;
            product.Tags = productVm.Tags;
            product.OriginalPrice = productVm.OriginalPrice;
        }

        public static void UpdateFeedback (this Feedback feedback, FeedbackViewModel feedbackVm)
        {
            feedback.Name = feedbackVm.Name;
            feedback.Email = feedbackVm.Email;
            feedback.Message = feedbackVm.Message;
            feedback.Status = feedbackVm.Status;
            feedback.CreatedDate = DateTime.Now;
        }

        public static void UpdateProductQuantity (this ProductQuantity quantity, ProductQuantityViewModel quantityVm)
        {
            quantity.ColorId = quantityVm.ColorId;
            quantity.ProductId = quantityVm.ProductId;
            quantity.SizeId = quantityVm.SizeId;
            quantity.Quantity = quantityVm.Quantity;
        }
        public static void UpdateOrder (this Order order, OrderViewModel orderVm)
        {
            order.BuyerId = orderVm.BuyerId;
            order.TotalQuantity = orderVm.TotalQuantity;
            order.SubTotal = orderVm.SubTotal;
            order.TotalSellerFee = orderVm.TotalSellerFee;
            order.TotalBuyerFee = orderVm.TotalBuyerFee;
            order.TransactionAmount = orderVm.TransactionAmount;
            order.OrderDate = orderVm.OrderDate;
            order.Total = orderVm.Total;
            order.CreatedBy = orderVm.CreatedBy;
            order.ShippingMethodId = orderVm.ShippingMethodId;
            order.UpdatedDate = orderVm.UpdatedDate;
            order.UpdatedBy = orderVm.UpdatedBy;
            order.PaymentStatusId = orderVm.PaymentStatusId;
            order.BillingInfoId = orderVm.BillingInfoId;
            order.DeliveryInfoId = orderVm.DeliveryInfoId;
        }

        public static void UpdateProductImage (this ProductImage image, ProductImageViewModel imageVm)
        {
            image.ProductId = imageVm.ProductId;
            image.Path = imageVm.Path;
            image.Caption = imageVm.Caption;
        }
        public static void UpdateFunction (this Function function, FunctionViewModel functionVm)
        {
            function.Name = functionVm.Name;
            function.DisplayOrder = functionVm.DisplayOrder;
            function.IconCss = functionVm.IconCss;
            function.Status = functionVm.Status;
            function.ParentId = functionVm.ParentId;
            function.Status = functionVm.Status;
            function.URL = functionVm.URL;
            function.ID = functionVm.ID;
        }
        public static void UpdatePermission (this Permission permission, PermissionViewModel permissionVm)
        {
            permission.RoleId = permissionVm.RoleId;
            permission.FunctionId = permissionVm.FunctionId;
            permission.CanCreate = permissionVm.CanCreate;
            permission.CanDelete = permissionVm.CanDelete;
            permission.CanRead = permissionVm.CanRead;
            permission.CanUpdate = permissionVm.CanUpdate;
        }

        public static void UpdateApplicationRole (this AppRole appRole, ApplicationRoleViewModel appRoleViewModel, string action = "add")
        {
            if (action == "update")
                appRole.Id = appRoleViewModel.Id;
            else
                appRole.Id = Guid.NewGuid().ToString();
            appRole.Name = appRoleViewModel.Name;
            appRole.Description = appRoleViewModel.Description;
        }

        public static void UpdateUser (this AppUser appUser, AppUserViewModel appUserViewModel, string action = "add")
        {
            appUser.Id = appUserViewModel.Id;
            appUser.FirstName = appUserViewModel.FirstName;
            appUser.LastName = appUserViewModel.LastName;
            appUser.FullName = appUserViewModel.FirstName + " " + appUserViewModel.LastName;
            appUser.CountryId = appUserViewModel.CountryId;
            appUser.Company = appUserViewModel.Company;
            appUser.Occupation = appUserViewModel.Occupation;
            appUser.Address2 = appUserViewModel.Address2;
            appUser.City = appUserViewModel.City;
            appUser.State = appUserViewModel.State;
            appUser.ZipCode = appUserViewModel.ZipCode;
            appUser.TaxRate = appUserViewModel.TaxRate;
            if (!string.IsNullOrEmpty(appUserViewModel.BirthDay))
            {
                DateTime dateTime = DateTime.ParseExact(appUserViewModel.BirthDay, "MM/dd/yyyy", new CultureInfo("en-US"));
                appUser.BirthDay = dateTime;
            }

            appUser.Email = appUserViewModel.Email;
            appUser.Address = appUserViewModel.Address;
            appUser.UserName = appUserViewModel.UserName;
            appUser.PhoneNumber = appUserViewModel.PhoneNumber;
            appUser.Gender = appUserViewModel.Gender == "True" ? true : false;
            appUser.Status = appUserViewModel.Status;
            appUser.Avatar = appUserViewModel.Avatar;
        }
        public static void UpdateCountry (this Country country, CountryViewModel countryVm)
        {
            country.Id = countryVm.Id;
            country.Code = countryVm.Code;
            country.Name = countryVm.Name;
        }
        public static void UpdateParameter (this Parameter parameter, ParameterViewModel parameterVm)
        {
            parameter.Id = parameterVm.Id;
            parameter.Code = parameterVm.Code;
            parameter.Name = parameterVm.Name;
            parameter.Description = parameterVm.Description;
        }
        public static void UpdateShippingStatus (this ShippingStatus shippingStatus, ShippingStatusViewModel shippingStatusVm)
        {
            shippingStatus.Id = shippingStatusVm.Id;
            shippingStatus.Code = shippingStatusVm.Code;
            shippingStatus.Name = shippingStatusVm.Name;
        }
        public static void UpdateConfirmStatus (this ConfirmStatus confirmStatus, ConfirmStatusViewModel confirmStatusVm)
        {
            confirmStatus.Id = confirmStatusVm.Id;
            confirmStatus.Name = confirmStatusVm.Name;
        }
        public static void UpdateState (this State state, StateViewModel stateVm)
        {
            state.Id = stateVm.Id;
            state.Code = stateVm.Code;
            state.Name = stateVm.Name;
            state.CountryId = stateVm.CountryId;
        }
        public static void UpdateMaterial (this Material material, MaterialViewModel materialVm)
        {
            material.Id = materialVm.Id;
            material.PartNumber = materialVm.PartNumber;
            material.Description = materialVm.Description;
            material.Manufacturer = materialVm.Manufacturer;
            //material.IntNumber = materialVm.IntNumber;
            material.DescriptionDetail = materialVm.DescriptionDetail;
            material.TechLink = materialVm.TechLink;
        }
        public static void UpdateCart(this Cart cart, CartViewModel cartVm)
        {
            cart.Id = cartVm.Id;
            cart.MaterialListId = cartVm.MaterialListId;
            cart.BuyerId = cartVm.BuyerId;
            cart.Quantity = cartVm.Quantity;
            cart.UnitPrice = cartVm.UnitPrice;
            cart.ExtendedPrice = cartVm.Quantity* cartVm.UnitPrice;

        }
        public static void UpdateMaterialList (this MaterialList materialList, MaterialListViewModel materialListVm)
        {
            materialList.Id = materialListVm.Id;
            materialList.SellerId = materialListVm.SellerId;
            materialList.AvailableQty = materialListVm.AvailableQty;
            materialList.MaxOfQuantity = materialListVm.AvailableQty; 
            materialList.MOQ = materialListVm.MOQ;
            materialList.Price = materialListVm.Price;
            materialList.DueDate = (materialListVm.DueDate.Trim() == "Now" || materialListVm.DueDate.Trim() == "") ? DateTime.Now.AddDays(-1) : DateTime.Parse(materialListVm.DueDate);
            materialList.ShippingStatusId = materialListVm.ShippingStatusId;
            materialList.Condition = materialListVm.Condition;
            materialList.DateCode = materialListVm.DateCode;
        }

        public static void UpdatePaymentStatus (this PaymentStatus paymentstatus, PaymentStatusViewModel paymentstatusVm)
        {
            paymentstatus.Id = paymentstatusVm.Id;
            paymentstatus.Name = paymentstatusVm.Name;
        }

        public static void UpdateDeliveryStatus (this DeliveryStatus deliveryStatus, DeliveryStatusViewModel deliveryStatusVm)
        {
            deliveryStatus.Id = deliveryStatusVm.Id;
            deliveryStatus.Name = deliveryStatusVm.Name;
        }

        public static void UpdateShippingMethod (this ShippingMethod shippingMethod, ShippingMethodViewModel shippingMethodVm)
        {
            shippingMethod.Id = shippingMethodVm.Id;
            shippingMethod.Name = shippingMethodVm.Name;
            shippingMethod.Description = shippingMethodVm.Description;
            shippingMethod.ShippingFee = shippingMethodVm.ShippingFee;
        }
        public static void UpdateBOM (this BOM bom, BOMViewModel bomVm)
        {
            bom.Id = bomVm.Id;
            bom.UserId = bomVm.UserId;
            bom.Name = bomVm.Name;
            bom.UploadDate = bomVm.UploadDate;
        }

        public static void UpdateOrderStatus (this OrderStatus orderStatus, OrderStatusViewModel orderStatusVm)
        {
            orderStatus.Id = orderStatusVm.Id;
            orderStatus.Name = orderStatusVm.Name;
        }

        public static void UpdateAddress (this Address address, AddressViewModel addressVm)
        {
            address.Id = addressVm.Id;
            address.UserId = addressVm.UserId;
            address.FullName = addressVm.FullName;
            address.Address1 = addressVm.Address1;
            address.Email = addressVm.Email;
            address.PhoneNumber = addressVm.PhoneNumber;
            address.City = addressVm.City;
            address.StateId = addressVm.StateId;
            address.Address2 = addressVm.Address2;
            address.ZipCode = addressVm.ZipCode;
            address.CountryId = addressVm.CountryId;
        }
        public static void UpdateMaterialTempData(this MaterialTempData materialTempData, MaterialTempDataViewModel TempDataVm)
        {
            materialTempData.Id = TempDataVm.Id;
            materialTempData.SellerId = TempDataVm.SellerId;
            materialTempData.UploadDate = TempDataVm.UploadDate;
            materialTempData.Data = TempDataVm.Data;
            materialTempData.Status = TempDataVm.Status;
            materialTempData.DataEntryId = TempDataVm.DataEntryId;
        }
    }
}