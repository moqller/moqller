﻿using MOQller.Web.Models.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.State
{
    public class StateViewModel
    {
        public int Id { get; set; }
        
        public string Code { get; set; }

        public string Name { get; set; }
        public int CountryId { get; set; }
        public virtual CountryViewModel Country { set; get; }
    }
}