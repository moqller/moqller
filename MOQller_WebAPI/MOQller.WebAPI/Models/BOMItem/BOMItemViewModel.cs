﻿using MOQller.Web.Models.BOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.BOMItem
{
    public class BOMItemViewModel
    {
        
        public Int64 Id { get; set; }

        public int BOMId { get; set; }

        public string PartNumber { get; set; }

        public int Quantity { get; set; }

        public virtual BOMViewModel BOM { get; set; }
    }
}