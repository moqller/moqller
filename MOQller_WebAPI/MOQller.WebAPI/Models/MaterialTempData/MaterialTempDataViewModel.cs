﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.MaterialTempData
{
    public class MaterialTempDataViewModel
    {
        public Int64 Id { get; set; }

        public string SellerId { get; set; }
       
        public DateTime UploadDate { get; set; }

        public int Status { get; set; }

        public string Data { get; set; }

        public string DataEntryId { get; set; }
    }

    public class DetailTempDataViewModel
    {
        public Int64 Id { get; set; }

        public string SellerName { get; set; }

        public string Data { get; set; }

        public DateTime UploadDate { get; set; }

        public string SellerEmail { get; set; }
    }
}