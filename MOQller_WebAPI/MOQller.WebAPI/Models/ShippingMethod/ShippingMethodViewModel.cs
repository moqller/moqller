﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.ShippingMethod
{
    public class ShippingMethodViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal ShippingFee { get; set; }
    }
}