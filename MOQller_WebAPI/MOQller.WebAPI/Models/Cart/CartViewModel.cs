﻿using MOQller.Model.Models;
using MOQller.Web.Models.MaterialList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.Cart
{
    public class CartViewModel
    {
        
        public int Id { set; get; }

        public int MaterialListId { set; get; }

        public int Quantity { set; get; }

        public Decimal UnitPrice { set; get; }

        public Decimal ExtendedPrice { set; get; }

        public int MOQ { get; set; }

        public string BuyerId { get; set; }
        
        public virtual AppUserViewModel AppUser { set; get; }

        public virtual MaterialListViewModel MaterialList { set; get; }

    }
}