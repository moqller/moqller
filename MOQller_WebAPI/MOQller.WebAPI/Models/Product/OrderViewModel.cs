﻿using MOQller.Web.Models.OrderStatus;
using MOQller.Web.Models.PaymentStatus;
using MOQller.Web.Models.ShippingMethod;
using MOQller.Web.Models.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MOQller.Web.Models
{
    public class OrderViewModel
    {
        public Int64 ID { set; get; }

        [StringLength(50)]
        public string OrderNumber { get; set; }

        public string BuyerId { get; set; }

        [Required]
        public decimal TotalQuantity { get; set; }

        [Required]
        public decimal SubTotal { get; set; }

        [Required]
        public decimal TotalSellerFee { get; set; }

        [Required]
        public decimal TotalBuyerFee { get; set; }

        [Required]
        public decimal TransactionAmount { get; set; }

        [Required]
        public decimal Total { get; set; }

        public int? OrderStatus { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public int ShippingMethodId { get; set; }


        public DateTime? CreatedDate { set; get; }

        public string CreatedBy { set; get; }

        public DateTime? UpdatedDate { set; get; }

        public string UpdatedBy { set; get; }

        public int PaymentStatusId { get; set; }

        public Int64 BillingInfoId { get; set; }

        public Int64 DeliveryInfoId { get; set; }

        public virtual ShippingMethodViewModel ShippingMethod { set; get; }

        public virtual PaymentStatusViewModel PaymentStatuses { set; get; }



        public virtual AddressViewModel AddressBilling { set; get; }

        public virtual AddressViewModel AddressDelivery { set; get; }

        public IEnumerable<OrderDetailViewModel> OrderDetails { set; get; }
    }
}