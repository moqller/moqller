﻿using MOQller.Model.Models;
using MOQller.Web.Models.DeliveryStatus;
using MOQller.Web.Models.MaterialList;
using MOQller.Web.Models.OrderStatus;
using System;
using System.Collections.Generic;

namespace MOQller.Web.Models
{
    public class OrderDetailViewModel
    {
        public Int64 Id { set; get; }

        public Int64 OrderId { set; get; }
        
        public int MaterialListId { set; get; }
        
        public int Quantity { set; get; }


        public decimal UnitPrice { set; get; }


        public decimal ExtendedPrice { set; get; }


        public decimal SellerFee { set; get; }

     
        public decimal BuyerFee { set; get; }

  
        public decimal SellerFeeRate { set; get; }

        
        public decimal BuyerFeeRate { set; get; }

        public decimal BuyerTotal { set; get; }

        public int DeliveryStatusId { get; set; }

        public int? OrderStatusId { get; set; }

        public virtual OrderViewModel Order { set; get; }

        public virtual MaterialListViewModel MaterialList { set; get; }

        public virtual OrderStatusViewModel OrderStatuses { set; get; }

        public virtual DeliveryStatusViewModel DeliveryStatuses { set; get; }

        public virtual ICollection<OrderDetailStatusViewModel> OrderDetailStatuses { set; get; }
    }

    public class OrderDetailStatusViewModel
    {
        
        public Int64 Id { set; get; }
        
        public Int64 OrderDetailId { set; get; }

        public DateTime StatusDate { set; get; }
        
        public int DeliveryStatusId { get; set; }
        
        public virtual OrderDetailViewModel OrderDetail { set; get; }
        
        public virtual DeliveryStatusViewModel DeliveryStatus { set; get; }
    }

    public class OrderManagerSellerViewModel
    {
        public Int64 Id { get; set; }
        public string OrderNumber { get; set; }

        public string PartNumber { get; set; }

        public int Quantity { get; set; }

        public int MOQ { get; set; }

        public int AvailableQty { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal ExtendedPrice { get; set; }

        public string ShippingMethodName { get; set; }

        public string DeliveryStatusesName { get; set; }

        public string DeliveryStatusId { get; set; }

        public string DeliveryStatusCode { get; set; }

        public string OrderStatusesName { get; set; }

        public int OrderDetailId { get; set; }

        public int OrderId { get; set; }

        public int MaterialId { get; set; }
    }
}