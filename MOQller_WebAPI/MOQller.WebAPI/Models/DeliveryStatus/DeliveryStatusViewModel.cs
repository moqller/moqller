﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.DeliveryStatus
{
    public class DeliveryStatusViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}