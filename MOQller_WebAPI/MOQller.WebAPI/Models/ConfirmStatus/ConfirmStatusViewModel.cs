﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.ConfirmStatus
{
    public class ConfirmStatusViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}