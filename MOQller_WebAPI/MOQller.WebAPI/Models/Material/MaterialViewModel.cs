﻿using System;
using System.Collections.Generic;
using MOQller.Model.Models;
using System.Web;
using MOQller.Web.Models.MaterialList;

namespace MOQller.Web.Models
{
    public class MaterialViewModel
    {
        public int Id { get; set; }
        
        public string PartNumber { get; set; }
        
        public string Description { get; set; }

        public string Manufacturer { get; set; }

        //public string IntNumber { get; set; }
        public string TechLink { get; set; }

        //public string MOQ { get; set; }

        public byte[] Image1 { get; set; }

        //public DateTime AvailableDate { get; set; }

        public string DescriptionDetail { get; set; }

        public virtual ICollection<MaterialListViewModel> MaterialList { get; set; }
    }
}