﻿using MOQller.Data.Repositories;
using MOQller.Web.Models.BOMItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.BOM
{
    public class BOMViewModel
    {
        public Int64 Id { get; set; }

        public string Name { get; set; }
        
        public DateTime UploadDate { get; set; }

        public string UserId { get; set; }
        
        public virtual AppUserViewModel AppUser { set; get; }
        
        public IEnumerable<BOMItemViewModel> BomItem { get; set; }
    }
}