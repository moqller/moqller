﻿using MOQller.Common.Enums;
using MOQller.Web.Models.ShippingStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.MaterialList
{
    public class MaterialListViewModel
    {

        public int Id { get; set; }

        public string SellerId { get; set; }

        public int MaterialId { get; set; }

        public int AvailableQty { get; set; }

        public int MaxOfQuantity { get; set; }

        public int MOQ { get; set; }

        public string DateCode { get; set; }

        public ConditionEnum Condition { get; set; }

        public string ConditionName { get; set; }

        public string Distributer { get; set; }

        public string Supplier { get; set; }

        public decimal Price { get; set; }

        public DateTime UploadDate { get; set; }

        public string DueDate { get; set; }

        public string PartNumber { get; set; }

        public string Manufacturer { get; set; }

        public string Description { get; set; }

        public string TechLink { get; set; }

        public int ShippingStatusId { get; set; }

        public int? WorkOrderId { get; set; }

        public int ApproveStatus { get; set; }

        public byte[] ImagePartNumber { get; set; }

        public string FileName { get; set; }

        public virtual AppUserViewModel AppUser { set; get; }


        public virtual MaterialViewModel Material { set; get; }


        public virtual ShippingStatusViewModel ShippingStatus { set; get; }
    }


}