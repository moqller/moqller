﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.ShippingStatus
{
    public class ShippingStatusViewModel
    {

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}