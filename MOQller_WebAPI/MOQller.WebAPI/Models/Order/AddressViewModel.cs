﻿using MOQller.Web.Models.Country;
using MOQller.Web.Models.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOQller.Web.Models.Order
{
    public class AddressViewModel
    {
        public Int64 Id { set; get; }

        public string UserId { get; set; }

        public string FullName { get; set; }

        public string Address1 { get; set; }


        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string City { get; set; }

        public int? StateId { get; set; }

        public string Address2 { get; set; }

        public string ZipCode { get; set; }

        public int CountryId { get; set; }

        public virtual AppUserViewModel AppUser { set; get; }

        public virtual StateViewModel States { set; get; }

        public virtual CountryViewModel Country { set; get; }
    }
}