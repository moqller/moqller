﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using OfficeOpenXml;
using System.Linq;
using AutoMapper;
using System.Data;
using MOQller.Data.Repositories;
using System.Xml.Linq;
using System.Globalization;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/upload")]
    //[Authorize]
    public class UploadController : ApiControllerBase
    {
        private IMaterialListService _MaterialListService;
        private IMaterialListRepository _MaterialListRepository;
        public UploadController(IErrorService errorService, IMaterialListService MaterialListService, IMaterialListRepository MaterialListRepository) : base(errorService)
        {
            this._MaterialListService = MaterialListService;
            this._MaterialListRepository = MaterialListRepository;
        }

        [HttpPost]
        [Route("saveImage")]
        public HttpResponseMessage SaveImage(string type = "")
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string directory = string.Empty;
                            if (type == "avatar")
                            {
                                directory = "/UploadedFiles/Avatars/";
                            }
                            else if (type == "product")
                            {
                                directory = "/UploadedFiles/Products/";
                            }
                            else if (type == "news")
                            {
                                directory = "/UploadedFiles/News/";
                            }
                            else if (type == "banner")
                            {
                                directory = "/UploadedFiles/Banners/";
                            }
                            else
                            {
                                directory = "/UploadedFiles/";
                            }
                            if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory)))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory));
                            }

                            string path = Path.Combine(HttpContext.Current.Server.MapPath(directory), postedFile.FileName);
                            //Userimage myfolder name where i want to save my image
                            postedFile.SaveAs(path);
                            return Request.CreateResponse(HttpStatusCode.OK, Path.Combine(directory, postedFile.FileName));
                        }
                    }

                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1);
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }


        [HttpPost]
        [Route("materialfile")]
        [Authorize]
        public HttpResponseMessage MaterialFile(string partNumberField, string qtyField, string moqField, string dateField, string priceField, string statusField, string manufacturerField, string descriptionField, string techLinkField, string ConditionField, string DateCodeField, int page, int pageSize)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
            try
            {
                var directory1 = "/UploadedFiles/Material/";
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory1)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory1));
                }
                string path11 = Path.Combine(HttpContext.Current.Server.MapPath(directory1), "WriteLines.txt");
                var httpRequest = HttpContext.Current.Request;
                string[] lines = { "First line" };
                System.IO.File.WriteAllLines(path11, lines);
                if (partNumberField != null)
                {
                    partNumberField = partNumberField.Replace("[[]]", "#");

                }
                path11 = Path.Combine(HttpContext.Current.Server.MapPath(directory1), "WriteLines.txt");
                httpRequest = HttpContext.Current.Request;
                string[] lines2 = { "second line" };
                System.IO.File.WriteAllLines(path11, lines2);
                if (qtyField != null)
                {
                    qtyField = qtyField.Replace("[[]]", "#");
                }


                if (moqField != null)
                {
                    moqField = moqField.Replace("[[]]", "#");
                }
                if (dateField != null)
                {
                    dateField = dateField.Replace("[[]]", "#");
                }
                if (priceField != null)
                {
                    priceField = priceField.Replace("[[]]", "#");
                }
                if (statusField != null)
                {
                    statusField = statusField.Replace("[[]]", "#");
                }
                if (manufacturerField != null)
                {
                    manufacturerField = manufacturerField.Replace("[[]]", "#");
                }
                if (descriptionField != null)
                {
                    descriptionField = descriptionField.Replace("[[]]", "#");
                }
                if (techLinkField != null)
                {
                    techLinkField = techLinkField.Replace("[[]]", "#");
                }
                if (DateCodeField != null)
                {
                    DateCodeField = DateCodeField.Replace("[[]]", "#");
                }
                if (ConditionField != null)
                {
                    ConditionField = ConditionField.Replace("[[]]", "#");
                }

                path11 = Path.Combine(HttpContext.Current.Server.MapPath(directory1), "WriteLines.txt");

                string[] lines4 = { "lin 4e" };
                System.IO.File.WriteAllLines(path11, lines4);

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    string[] lines1 = { "third line" };
                    System.IO.File.WriteAllLines(path11, lines1);
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB

                        IList<string> AllowedFileExtensions = new List<string> { ".xls", ".xlsx", ".csv" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .xls,.xlsx,.csv");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Upload image of type .xls,.xlsx,.csv");
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please upload a file upto 5 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadGateway, "Please upload a file upto 5 mb.");
                        }
                        else
                        {
                            string directory = string.Empty;


                            string path = Path.Combine(HttpContext.Current.Server.MapPath(directory1), Guid.NewGuid() + postedFile.FileName);

                            postedFile.SaveAs(path);

                            string csvFileName = path;
                            string excelFileName = "";
                            if (path.Substring(path.Length - 4) == ".csv")
                            {
                                excelFileName = path.Substring(0, path.Length - 4) + ".xlsx";
                            }
                            else if (path.Substring(path.Length - 4) == ".xls")
                            {
                                excelFileName = path.Substring(0, path.Length - 4) + ".xlsx";
                            }
                            else
                            {
                                excelFileName = path;
                            }

                            if (path.Substring(path.Length - 4) == ".csv")
                            {
                                string worksheetsName = "Sheet1";
                                bool firstRowIsHeader = false;
                                var format = new ExcelTextFormat();
                                format.Delimiter = ',';
                                format.EOL = "\r";
                                using (ExcelPackage package = new ExcelPackage(new FileInfo(excelFileName)))
                                {
                                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetsName);
                                    worksheet.Cells["A1"].LoadFromText(new FileInfo(csvFileName), format, OfficeOpenXml.Table.TableStyles.Medium27, firstRowIsHeader);
                                    package.Save();
                                }

                            }

                            DataTable dt = new DataTable();
                            dt.TableName = "UploadMaterial";
                            dt.Columns.Add("Sheet", typeof(string));
                            dt.Columns.Add("PartNumber", typeof(string));
                            dt.Columns.Add("ShippingStatusId", typeof(string));
                            dt.Columns.Add("Manufacturer", typeof(string));
                            dt.Columns.Add("Description", typeof(string));
                            dt.Columns.Add("DueDate", typeof(DateTime));
                            dt.Columns.Add("TechLink", typeof(string));
                            dt.Columns.Add("DateCode", typeof(string));
                            dt.Columns.Add("Condition", typeof(string));
                            dt.Columns.Add("AvailableQty", typeof(string));
                            dt.Columns.Add("Price", typeof(string));
                            dt.Columns.Add("MOQ", typeof(string));

                            // dt.Columns.Add("MatchedType", typeof(int));

                            var result = new List<UpLoadBom>();
                            var dtColumn = new DataTable();
                            dtColumn.Columns.Add("Name", typeof(string));
                            dtColumn.Columns.Add("Error", typeof(string));
                            dtColumn.Columns.Add("Sheet", typeof(string));

                            var ListUploadBomm = new List<UpLoadBom>();
                            int AdressPartNumber = 0, AdressMoq = 0, AdressMoqDate = 0, AdressPrice = 0, AdressStatus = 0, AdressManufacturer = 0, AdressDescription = 0, AdressTechlink = 0;
                            int AdressQTY = 0, AdresssDateCode = 0, AdressCondition = 0;

                            using (FileStream templateDocumentStream = File.OpenRead(excelFileName))
                            {
                                using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                                {
                                    ExcelWorksheet Firtsheet = package.Workbook.Worksheets[1];
                                    templateDocumentStream.Close();

                                    var tab = "\t";
                                    var enter = "\n";
                                    foreach (var header in Firtsheet.Cells[Firtsheet.Dimension.Start.Row, Firtsheet.Dimension.Start.Column, 1, Firtsheet.Dimension.End.Column])
                                    {
                                        string _header = header.Text.ToLower().Trim().Replace(tab, "").Replace(enter, "").Replace("!", "").Replace("_", "");

                                        if (partNumberField != null) // Kiem tra neu partNumberField khac rong thi la truyen tham so
                                        {
                                            if (_header == partNumberField.ToLower().Trim()) // Kiem tra xem thang nao chinh xac la partnumber
                                            {
                                                AdressPartNumber = header.Start.Column;
                                                continue;
                                            }
                                        } else if (_header == "partnumber"
                                            || _header == "part number"
                                            || _header == "mpn"
                                            || _header == "mfg part no#")
                                        {
                                            AdressPartNumber = header.Start.Column;
                                            continue;
                                        }
                                        //-------------------------------------------
                                        if (qtyField != null)
                                        {
                                            if (_header == qtyField.ToLower().Trim())
                                            {
                                                AdressQTY = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "quantity"
                                            || _header == "qty"
                                            || _header == "available quantity"
                                            || _header == "available qty"
                                            || _header == "availableqty")
                                        {
                                            AdressQTY = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- moq Field
                                        if (moqField != null)
                                        {
                                            if (_header == moqField.ToLower().Trim())
                                            {
                                                AdressMoq = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "moq")
                                        {
                                            AdressMoq = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- Date Field
                                        if (dateField != null)
                                        {
                                            if (_header == dateField.ToLower().Trim())
                                            {
                                                AdressMoqDate = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "duedate" || _header == "due date" || _header == "availabledate" || _header == "available date")
                                        {
                                            AdressMoqDate = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- Price Field
                                        if (priceField != null)
                                        {
                                            if (_header == priceField.ToLower().Trim())
                                            {
                                                AdressPrice = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "price" || _header == "unitprice" || _header == "unit price")
                                        {
                                            AdressPrice = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- status Field statusField
                                        if (statusField != null)
                                        {
                                            if (_header == statusField.ToLower().Trim())
                                            {
                                                AdressStatus = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "status")
                                        {
                                            AdressStatus = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- manufacturer Field manufacturerField
                                        if (manufacturerField != null)
                                        {
                                            if (_header == manufacturerField.ToLower().Trim())
                                            {
                                                AdressManufacturer = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "manufacturer")
                                        {
                                            AdressManufacturer = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- description Field descriptionField
                                        if (descriptionField != null)
                                        {
                                            if (_header == descriptionField.ToLower().Trim())
                                            {
                                                AdressDescription = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "description")
                                        {
                                            AdressDescription = header.Start.Column;
                                            continue;
                                        }
                                        //------------------------------------------- tech link Field techLinkField
                                        if (techLinkField != null)
                                        {
                                            if (_header == techLinkField.ToLower().Trim())
                                            {
                                                AdressTechlink = header.Start.Column;
                                                continue;
                                            }
                                        }
                                        else if (_header == "tech link")
                                        {
                                            AdressTechlink = header.Start.Column;
                                            continue;
                                        }
                                    }

                                    List<UploadFileSeller> info = new List<UploadFileSeller>();

                                    var totalRows = Firtsheet.Dimension.End.Row;

                                    //Neu khong thay dia chi cot cua partnumber thi map cot
                                    if (AdressPartNumber == 0)
                                    {
                                        var dr = dtColumn.NewRow();

                                        foreach (var header in Firtsheet.Cells[Firtsheet.Dimension.Start.Row, Firtsheet.Dimension.Start.Column, 1, Firtsheet.Dimension.End.Column])
                                        {
                                            dr = dtColumn.NewRow();
                                            dr["Sheet"] = "";
                                            dr["Name"] = header.Text;
                                            dr["Error"] = "Can’t find any column to map. Please map columns yourself.";
                                            dtColumn.Rows.Add(dr);
                                        }
                                        return Request.CreateResponse(HttpStatusCode.OK, dtColumn);
                                    }
                                    else
                                    {
                                        for (int i = 2; i <= totalRows; i++)
                                        {
                                            var item = new UploadFileSeller();

                                            item.Sheet = "";

                                            if (Firtsheet.Cells[i, AdressPartNumber].Value != null)
                                                item.PartNumber = Firtsheet.Cells[i, AdressPartNumber].Value.ToString().Trim();
                                            else continue; // Nếu dòng nào không có part number tiep tuc luôn ko kiểm tra nữa
                                            
                                            if (AdressQTY != 0) // Nếu không tím thấy địa chỉ cột quantity tức là header á
                                                if (Firtsheet.Cells[i, AdressQTY].Value == null) // check cột đó có null hay ko
                                                    item.AvailableQty = ""; // nếu null cho bằng rỗng
       
                                                else item.AvailableQty = TryParseInt(Firtsheet.Cells[i, AdressQTY].Value.ToString().Trim()) == null ? "" : Firtsheet.Cells[i, AdressQTY].Value.ToString().Trim(); // Nếu không null lấy giá trị
                                            else item.AvailableQty = "";// Không có địa chỉ cột cho bằng rỗng luôn

                                            if (AdressMoq != 0)
                                                if (Firtsheet.Cells[i, AdressMoq].Value == null)
                                                    item.moq = "";
                                                else item.moq = TryParseInt(Firtsheet.Cells[i, AdressMoq].Value.ToString().Trim()) == null ? "" : Firtsheet.Cells[i, AdressMoq].Value.ToString().Trim();
                                            else item.moq = "";

                                            if (AdressPrice != 0)
                                                if (Firtsheet.Cells[i, AdressPrice].Value == null)
                                                    item.Price = "";
                                                else item.Price = TryParseDecimal(Firtsheet.Cells[i, AdressPrice].Value.ToString().Trim()) == null ?  "" : Firtsheet.Cells[i, AdressPrice].Value.ToString().Trim();
                                            else item.Price = "";

                                            if (AdressMoqDate != 0)
                                                if (Firtsheet.Cells[i, AdressMoqDate].Value == null)
                                                    item.DueDate = "";
                                                else item.DueDate = TryParseDateTime( Firtsheet.Cells[i, AdressMoqDate].Value.ToString().Trim()) == null ?  "" : Firtsheet.Cells[i, AdressMoqDate].Value.ToString().Trim();
                                            else item.DueDate = "";

                                            if (AdressStatus != 0)
                                                if (Firtsheet.Cells[i, AdressStatus].Value == null)
                                                    item.Status = "";
                                                else item.Status = Firtsheet.Cells[i, AdressStatus].Value.ToString().Trim();
                                            else item.Status = "";

                                            if (AdressDescription != 0)
                                                if (Firtsheet.Cells[i, AdressDescription].Value == null)
                                                    item.Description = "";
                                                else item.Description = Firtsheet.Cells[i, AdressDescription].Value.ToString().Trim();
                                            else item.Description = "";

                                            if (AdressManufacturer != 0)
                                                if (Firtsheet.Cells[i, AdressManufacturer].Value == null)
                                                    item.Manufacturer = "";
                                                else item.Manufacturer = Firtsheet.Cells[i, AdressManufacturer].Value.ToString().Trim();
                                            else item.Manufacturer = "";

                                            if (AdressTechlink != 0)
                                                if (Firtsheet.Cells[i, AdressTechlink].Value == null)
                                                    item.TechLink = "";
                                                else item.TechLink = Firtsheet.Cells[i, AdressTechlink].Value.ToString().Trim();
                                            else item.TechLink = "";

                                            if (AdresssDateCode != 0)
                                                if (Firtsheet.Cells[i, AdresssDateCode].Value == null)
                                                    item.Datecode = "";
                                                else item.Datecode = Firtsheet.Cells[i, AdresssDateCode].Value.ToString().Trim();
                                            else item.Datecode = "";

                                            if (AdressCondition != 0)
                                                if (Firtsheet.Cells[i, AdressCondition].Value == null)
                                                    item.Condition = "";
                                                else item.Condition = Firtsheet.Cells[i, AdressCondition].Value.ToString().Trim();
                                            else item.Condition = "";

                                            info.Add(item);
                                        }

                                        try
                                        {
                                            var xEle = new XElement("DocumentElement",
                                            from emp in info
                                            select new XElement("UploadMaterial",
                                                        new XElement("Sheet", emp.Sheet),
                                                        new XElement("PartNumber", emp.PartNumber),
                                                        new XElement("AvailableQty", emp.AvailableQty),
                                                        new XElement("MOQ", emp.moq),
                                                        new XElement("Description", emp.Description),
                                                        new XElement("Manufacturer", emp.Manufacturer),
                                                        new XElement("DueDate", emp.DueDate),
                                                        new XElement("Price", emp.Price),
                                                        new XElement("Status", emp.Status),
                                                        new XElement("TechLink", emp.TechLink),
                                                        new XElement("Condition", emp.Condition),
                                                        new XElement("DateCode", emp.Datecode)
                                                        ));

                                            var a = new List<UpLoadBom>();

                                            string dataTableXml = xEle.ToString();
                                            a = _MaterialListRepository.GetUploadMaterial(dataTableXml, UserId);


                                            for (int q = 0; q < a.Count; q++)
                                            {
                                                if (!string.IsNullOrEmpty(a[q].PartNumber))
                                                {

                                                    try
                                                    {
                                                        result.Add(a[q]);

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                                                    }


                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                                        }
                                    }

                                    return Request.CreateResponse(HttpStatusCode.OK, result);
                                }

                            }
                            var dtset = new DataSet();
                            dtset.Tables.Add(dt);

                            return Request.CreateResponse(HttpStatusCode.OK, dt);
                        }
                    }


                    var message1 = string.Format("Image Updated Successfully.");

                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1);
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }
        }

        public static DateTime? TryParseDateTime(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;
            DateTime result;
            if (DateTime.TryParse(s, out result))
                return result;
            return null;
        }

        public static Decimal? TryParseDecimal(string s)
        {

            if (string.IsNullOrEmpty(s))
                return null;
            Decimal result;
            if (Decimal.TryParse(s, out result))
                return result;
            return null;
        }

        public static Int32? TryParseInt(string s)
        {

            if (string.IsNullOrEmpty(s))
                return null;
            Int32 result;
            if (Int32.TryParse(s, out result))
                return result;
            return null;
        }

        [HttpPost]
        [Route("checkvaliduploadfile")]
        [AllowAnonymous]
        public HttpResponseMessage CheckValidUploadFile(string partNumberField, string qtyField, string fileName)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string pathFile = string.Empty;
            var directoryFile = "/UploadedFiles/BOM/";
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(directoryFile)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directoryFile));
            }
            if (!string.IsNullOrEmpty(fileName))
                pathFile = Path.Combine(HttpContext.Current.Server.MapPath(directoryFile), fileName);// nếu phải map cột thì lấy luôn file đã đc lưu từ trước

            string directory = string.Empty;

            try
            {

                var httpRequest = HttpContext.Current.Request;
                if (partNumberField != null)
                {
                    partNumberField = partNumberField.Replace("[[]]", "#");

                }

                if (qtyField != null)
                {
                    qtyField = qtyField.Replace("[[]]", "#");
                }

                if (string.IsNullOrEmpty(fileName))
                {
                    foreach (string file in httpRequest.Files)
                    {
                        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                        var postedFile = httpRequest.Files[file];
                        if (postedFile != null && postedFile.ContentLength > 0)
                        {

                            int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB
                            IList<string> AllowedFileExtensions = new List<string> { ".xls", ".xlsx", ".csv" };
                            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                            var extension = ext.ToLower();
                            if (!AllowedFileExtensions.Contains(extension))
                            {

                                var message = string.Format("Please Upload image of type .xls,.xlsx,.csv");

                                dict.Add("error", message);
                                return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Upload image of type .xls,.xlsx,.csv");
                            }
                            else if (postedFile.ContentLength > MaxContentLength)
                            {

                                var message = string.Format("Please upload a file upto 5 mb.");

                                dict.Add("error", message);
                                return Request.CreateResponse(HttpStatusCode.BadGateway, "Please upload a file upto 5 mb.");
                            }
                            else //lưu lại file
                            {

                                fileName = string.Format("{0}{1}{2}", postedFile.FileName.Substring(0, postedFile.FileName.LastIndexOf(".")), DateTime.Now.ToString("yyyyMMddhhmmsss"), postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.')));
                                pathFile = Path.Combine(HttpContext.Current.Server.MapPath(directoryFile), fileName);
                                //Userimage myfolder name where i want to save my image
                                postedFile.SaveAs(pathFile);
                                string csvFileName = pathFile;
                                string excelFileName = pathFile;
                                if (pathFile.Substring(pathFile.Length - 4) == ".csv")
                                {
                                    excelFileName = pathFile.Substring(0, pathFile.Length - 4) + ".xlsx";
                                }
                                else if (pathFile.Substring(pathFile.Length - 4) == ".xls")
                                {
                                    excelFileName = pathFile.Substring(0, pathFile.Length - 4) + ".xlsx";
                                }
                                else
                                {
                                    excelFileName = pathFile;
                                }

                                if (pathFile.Substring(pathFile.Length - 4) == ".csv")
                                {
                                    string worksheetsName = "Sheet1";

                                    bool firstRowIsHeader = false;

                                    var format = new ExcelTextFormat();
                                    format.Delimiter = ',';
                                    format.EOL = "\r";              // DEFAULT IS "\r\n";
                                                                    // format.TextQualifier = '"';

                                    using (ExcelPackage package = new ExcelPackage(new FileInfo(excelFileName)))
                                    {
                                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetsName);
                                        worksheet.Cells["A1"].LoadFromText(new FileInfo(csvFileName), format, OfficeOpenXml.Table.TableStyles.Medium27, firstRowIsHeader);
                                        package.Save();
                                    }

                                }
                            }
                        }
                    }
                }


                DataTable dt = new DataTable();
                dt.TableName = "UploadBom";
                dt.Columns.Add("Sheet", typeof(string));
                dt.Columns.Add("PartNumber", typeof(string));
                dt.Columns.Add("Description", typeof(string));
                dt.Columns.Add("Manufacturer", typeof(string));
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("SellerId", typeof(string));
                dt.Columns.Add("RequestQty", typeof(string));
                dt.Columns.Add("AvailableQty", typeof(string));
                dt.Columns.Add("Price", typeof(string));
                dt.Columns.Add("MOQ", typeof(string));
                dt.Columns.Add("ExtendedPrice", typeof(string));
                dt.Columns.Add("MatchedType", typeof(int));


                var dtColumn = new DataTable();
                dtColumn.Columns.Add("Name", typeof(string));
                dtColumn.Columns.Add("Error", typeof(string));
                dtColumn.Columns.Add("Sheet", typeof(string));
                dtColumn.Columns.Add("FileName", typeof(string));

                var ListUploadBomm = new List<UpLoadBom>();
                int AdressPartNumber = 0;
                int AdressQTY = 0;
                using (FileStream templateDocumentStream = File.OpenRead(pathFile))
                {
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // Lấy sheet đầu tiên
                        ExcelWorksheet Firtsheet = package.Workbook.Worksheets[1];
                        //var headerColum = GetHeaderColumns(Firtsheet);
                        templateDocumentStream.Close();

                        //tim cot partnumber va Qty
                        var tab = "\t";
                        var enter = "\n";

                        // Lọc tất cả header để lấy địa chỉ trên sheet ví dụ A1 B1
                        foreach (var header in Firtsheet.Cells[Firtsheet.Dimension.Start.Row, Firtsheet.Dimension.Start.Column, 1, Firtsheet.Dimension.End.Column])
                        {
                            string _header = header.Text.ToLower().Trim().Replace(tab, "").Replace(enter, "").Replace("!", "").Replace("_", "");

                            // Kiem tra neu truyen lai ten field partnumber thi tim theo ten do
                            if (partNumberField != null)
                            {
                                if (_header == partNumberField.ToLower().Trim())
                                {
                                    AdressPartNumber = header.Start.Column;
                                    continue; // Tim thay roi thi tiep tuc vong lap khong chay tiep lam gi ca
                                }
                            }
                            // Neu khong co partNumberField tuc Upload lan dau tu dong tim
                            else if (_header == "partnumber"
                                || _header == "part number"
                                || _header == "mpn"
                                || _header == "mfg part no#"
                                || _header == "mfg part no.")
                            {
                                // Lấy địa chỉ cột tránh trường hợp cột nó nằm ở 2 chữ nên chém chuỗi ví dự AA
                                AdressPartNumber = header.Start.Column;
                                continue;
                            }


                            // Kiem tra neu truyen lai field QTY thi tim theo ten do
                            if (qtyField != null)
                            {
                                if (_header == qtyField.ToLower().Trim())
                                {
                                    AdressQTY = header.Start.Column;
                                    continue;
                                }
                            }
                            // Neu khong co qtyField tuc Upload lan dau tu dong tim
                            else if (_header == "quantity"
                                || _header == "qty"
                                || _header == "requested quantity"
                                || _header == "requested qty"
                                || _header == "request quantity"
                                || _header == "request qty"
                                || _header == "requestedquantity"
                                || _header == "requestedqty"
                                || _header == "requestquantity"
                                || _header == "requestqty")
                            {
                                AdressQTY = header.Start.Column;
                                continue;
                            }
                        }

                        var totalRows = Firtsheet.Dimension.Rows;

                        List<UploadFilePartNumberAndQuantity> info = new List<UploadFilePartNumberAndQuantity>();

                        //Neu khong thay dia chi cot cua partnumber hoac qty thi map cot
                        if ((AdressPartNumber == 0))
                        {
                            var dr = dtColumn.NewRow();

                            foreach (var header in Firtsheet.Cells[Firtsheet.Dimension.Start.Row, Firtsheet.Dimension.Start.Column, 1, Firtsheet.Dimension.End.Column])
                            {
                                dr = dtColumn.NewRow();
                                dr["FileName"] = fileName;
                                dr["Sheet"] = "";
                                dr["Name"] = header.Text;
                                dr["Error"] = "Can’t find any column to map. Please map columns yourself.";
                                dtColumn.Rows.Add(dr);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, dtColumn);
                        }
                        else
                        {

                            // có địa chỉ của AdressPartNumber vs AdressQTY rồi bỏ các cột khác chỉ đọc 2 địa chỉ đo
                            for (int i = 2; i <= totalRows; i++)
                            {
                                var item = new UploadFilePartNumberAndQuantity();

                                item.Sheet = "";

                                if (Firtsheet.Cells[i, AdressPartNumber].Value != null)
                                    item.PartNumber = Firtsheet.Cells[i, AdressPartNumber].Value.ToString().Trim();
                                else continue; // Nếu dòng nào không có part number tiep tuc luôn ko kiểm tra nữa

                                if (Firtsheet.Cells[i, AdressQTY].Value != null)
                                    item.RequestQty = Firtsheet.Cells[i, AdressQTY].Value.ToString().Trim();
                                else continue; // Nếu dòng nào không có Request Qty tiep tuc luôn ko kiểm tra nữa

                                info.Add(item);
                            }

                            try
                            {
                                var xEle = new XElement("DocumentElement",
                                from emp in info
                                select new XElement("UploadBom",
                                            new XElement("Sheet", emp.Sheet),
                                            new XElement("PartNumber", emp.PartNumber),
                                            new XElement("RequestQty", emp.RequestQty)
                                            ));


                                string dataTableXml = xEle.ToString();
                                ListUploadBomm = _MaterialListRepository.GetUploadBom(dataTableXml);

                                for (int q = 0; q < ListUploadBomm.Count; q++)
                                {
                                    if (!string.IsNullOrEmpty(ListUploadBomm[q].PartNumber))
                                    {
                                        var dr = dt.NewRow();
                                        dr["Sheet"] = ListUploadBomm[q].Sheet;
                                        dr["PartNumber"] = ListUploadBomm[q].PartNumber;
                                        dr["SellerId"] = ListUploadBomm[q].SellerId;
                                        dr["RequestQty"] = ListUploadBomm[q].RequestQty;
                                        dr["AvailableQty"] = ListUploadBomm[q].AvailableQty;
                                        dr["Manufacturer"] = ListUploadBomm[q].Manufacturer;
                                        dr["MOQ"] = ListUploadBomm[q].MOQ;
                                        dr["Price"] = ListUploadBomm[q].Price;
                                        dr["ExtendedPrice"] = Convert.ToDouble(ListUploadBomm[q].Price) * Convert.ToDouble(ListUploadBomm[q].AvailableQty);
                                        dr["MatchedType"] = ListUploadBomm[q].MatchedType;
                                        dt.Rows.Add(dr);

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, dt);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex); ;

            }
        }
    }

    public class UploadFilePartNumberAndQuantity
    {
        public string Sheet { get; set; }
        public string PartNumber { get; set; }
        public string RequestQty { get; set; }
    }

    public class UploadFileSeller
    {
        public string Sheet { get; set; }
        public string PartNumber { get; set; }
        public string AvailableQty { get; set; }
        public string moq { get; set; }
        public string Price { get; set; }
        public string DueDate { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public string TechLink { get; set; }
        public string Datecode { get; set; }
        public string Condition { get; set; }
    }
}