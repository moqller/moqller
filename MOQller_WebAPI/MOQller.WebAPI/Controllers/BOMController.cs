﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Models.BOM;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Data.Repositories;
using System.Xml.Linq;
using MOQller.Common;
using System.Web;
using System.IO;
using OfficeOpenXml;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/BOM")]
    
    public class BOMController : ApiControllerBase
    {
        private IBOMService _BOMService;
        private IBOMRepository _BOMRepository;
        public BOMController(IErrorService errorService, IBOMService BOMService, IBOMRepository BOMRepository)
            :base(errorService)
        {
            this._BOMService = BOMService;
            this._BOMRepository = BOMRepository;
        }

        [Route("getall")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _BOMService.GetAll();

                var responseData = Mapper.Map<IEnumerable<BOM>, IEnumerable<BOMViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
        [Route("detail")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage GetById(HttpRequestMessage request, ResultBOM bomitem)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _BOMRepository.GetByPartNumber(bomitem);
                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("userid")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetByUserId(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var userId = ClaimsPrincipal.Current.Identity.GetUserId();
                var model = _BOMService.GetAll().Where(b => b.UserId == userId);
                //var model = _BOMRepository.GetByUserId(userId);
                var responseData = Mapper.Map<IEnumerable<BOM>,IEnumerable<BOMViewModel>>(model); 
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                
                return response;
            });
        }

        [Route("addbom")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Create(HttpRequestMessage request, BOMViewModel bomVm)
        {
            if (ModelState.IsValid)
            {
                var newBom = new BOM();
                bomVm.UploadDate = DateTime.Now;
                newBom.UpdateBOM(bomVm);
                if (_BOMRepository.Count(x => x.Name == bomVm.Name && x.UserId == bomVm.UserId) == 0)
                    try
                    {
                        var listBomItem = new List<BOMItem>();

                        foreach (var item in bomVm.BomItem)
                        {
                            if (item.PartNumber.Trim() == "")
                            {
                                return request.CreateErrorResponse(HttpStatusCode.Forbidden, "Some partnumbers are null value, please check your excel file");
                            }
                            else
                            {
                                listBomItem.Add(new BOMItem()
                                {
                                    PartNumber = item.PartNumber,
                                    Quantity = item.Quantity,
                                    CreatedDate = DateTime.Now
                                });
                            }
                        }
                        if (listBomItem != null)
                        {
                            newBom.newBomItem = listBomItem;
                            var bomCreate = _BOMService.Add(newBom);
                            var model = Mapper.Map<BOM, BOMViewModel>(bomCreate);
                            return request.CreateResponse(HttpStatusCode.OK, model);
                        }
                        else
                        {
                            return request.CreateErrorResponse(HttpStatusCode.Created, "List bom null");
                        }


                    }
                    catch (Exception exc)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Forbidden, exc);
                    }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exist");
                }
            }

            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
       

        [Route("addrowbom")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage AddRowBom(HttpRequestMessage request, Int64? Id,string partNumber, int requestQty)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage resp = null;
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var buyerId = ClaimsPrincipal.Current.Identity.GetUserId();
                try
                {
                    var response = _BOMRepository.SearchRowBom(Id, partNumber, requestQty, buyerId);

                    resp = request.CreateResponse(HttpStatusCode.OK, response);
                }
                catch (Exception ex)
                {
                    resp = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }

                return resp;

            });
        }

        [Route("updatebom/{bomId:int}")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Update(HttpRequestMessage request, Int64 bomId, ResultBOM[] BOMVm)
        {

            var xEle = new XElement("BomItems",
            from bomItem in BOMVm
            select new XElement("BomItem",
            new XElement("PartNumber", bomItem.PartNumber),
            new XElement("RequestQty", bomItem.RequestQty)

            ));
            string dataTableXml = xEle.ToString();
            return CreateHttpResponse(request, () =>
            {

                HttpResponseMessage resp = null;
                try
                {
                    var response = _BOMRepository.UpdateBOM(bomId, dataTableXml);

                    resp = request.CreateResponse(HttpStatusCode.OK);

                }
                catch (Exception ex)
                {
                    resp = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                }

                return resp;

            });
        }
        [Route("updatebom")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage UpdateChangeName(HttpRequestMessage request, BOMViewModel bomVm)
        {

            var xEle = new XElement("BomItems",
            from bomItem in bomVm.BomItem
            select new XElement("BomItem",
            
            new XElement("PartNumber", bomItem.PartNumber),
            new XElement("RequestQty", bomItem.Quantity)

            ));
            string dataTableXml = xEle.ToString();
            return CreateHttpResponse(request, () =>
            {

                HttpResponseMessage resp = null;
                
                   // string userId = _BOMRepository.GetSingleById(bomVm.Id).UserId;
                    if (_BOMRepository.Count(x => x.Name == bomVm.Name && x.UserId == bomVm.UserId && x.Id != bomVm.Id) == 0)
                    {

                        object response = _BOMRepository.UpdateBomChangeName(bomVm.Id, bomVm.Name, dataTableXml);
                        try
                        {
                            int rowAffected = Convert.ToInt32(response);
                             return request.CreateResponse(HttpStatusCode.OK, bomVm.Name);
                        }
                        catch (Exception ex)
                        {
                          return  resp = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                        }
                    
                    }
                    else
                    {
                      return   resp = request.CreateErrorResponse(HttpStatusCode.Conflict, "File name exist");
                    }   
                
                
                
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldBOM = _BOMRepository.Delete(id);
                    _BOMService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, "Delete success");
                }

                return response;
            });
        }

        [Route("exportExcel")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage ExportResultBOM(HttpRequestMessage request, ResultBOM[] bomList)
        {
            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = GenerateFileExport(bomList);
            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error export");
            }
        }

        private string GenerateFileExport( ResultBOM[] arrayList)
        {
            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            // Template File
            string templateDocument =
                    HttpContext.Current.Server.MapPath("~/Templates/SearchResultTemplate.xlsx");
            string documentName = string.Format("BOM-{0}.xlsx", DateTime.Now.ToString("yyyyMMdd"));
            string fullPath = Path.Combine(filePath, documentName);
            // Results Output
            MemoryStream output = new MemoryStream();
            try
            {
                // Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    // Create Excel EPPlus Package based on template stream
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet_buy = package.Workbook.Worksheets["ResultBOM"];

                        // Data Acces, load order header data.
            
                        // Start Row for Detail Rows
                        int rowIndex = 5;

                        int count_buy = 1;
                        foreach (var item in arrayList.OrderByDescending(x=>x.MatchedType))
                        {
                            // Cell 1, Carton Count
                            sheet_buy.Cells[rowIndex, 1].Value = count_buy.ToString();
                            // Cell 2, Order Number (Outline around columns 2-7 make it look like 1 column)
                            sheet_buy.Cells[rowIndex, 2].Value = item.PartNumber;
                            // Cell 8, Weight in LBS (convert KG to LBS, and rounding to whole number)
                            sheet_buy.Cells[rowIndex, 3].Value = item.Manufacturer;

                            sheet_buy.Cells[rowIndex, 4].Value = item.SellerId;
                            sheet_buy.Cells[rowIndex, 5].Value = item.AvailableQty;
                            sheet_buy.Cells[rowIndex, 6].Value = item.MOQ;
                            sheet_buy.Cells[rowIndex, 7].Value = item.DueDate;
                            sheet_buy.Cells[rowIndex, 8].Value = item.RequestQty;
                            sheet_buy.Cells[rowIndex, 9].Value = item.Price;
                            sheet_buy.Cells[rowIndex, 10].Value = item.ExtendedPrice;
                            sheet_buy.Cells[rowIndex, 11].Value = item.ShippingStatus;
                            
                            // Increment Row Counter
                            rowIndex++;
                            count_buy++;
                        }


                        package.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }

    
}
}
