﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MOQller.Common.Exceptions;
using MOQller.Data;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.App_Start;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Web.Models;
using MOQller.Web.Providers;
using MOQller.Web.Models.Country;
using MOQller.Web.Models.State;
using MOQller.Common;
using System.Net.Mail;
using System.Web;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/register")]

    public class RegiterController : ApiControllerBase
    {
        private ICountryService _CountryService;
        private IStateService _StateService;
        public RegiterController(IErrorService errorService, ICountryService CountryService, IStateService StateService)
            : base(errorService)
        {
            this._CountryService = CountryService;
            this._StateService = StateService;
        }
        [HttpPost]
        [Route("add")]
        [AllowAnonymous]
        
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, AppUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var manager = new UserManager<AppUser>(new UserStore<AppUser>(new MOQllerDbContext()));
                var newAppUser = new AppUser();
                newAppUser.UpdateUser(applicationUserViewModel);

                var userByEmail = await manager.FindByEmailAsync(applicationUserViewModel.Email);
                if (userByEmail != null)
                {
                    //ModelState.AddModelError("email", "Email is already exists");
                    return request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Email is already exists");
                }
                var userByUserName = await manager.FindByNameAsync(applicationUserViewModel.UserName);
                if (userByUserName != null)
                {
                    //ModelState.AddModelError("email", "Account is already exists");
                    return request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Account is already exists");
                }
                if (applicationUserViewModel.Password != applicationUserViewModel.ConfirmPassword)
                {
                    return request.CreateErrorResponse(HttpStatusCode.NotFound, "Password Mismatch");
                }

                try
                {
                    newAppUser.Id = Guid.NewGuid().ToString();
                    var result = await AppUserManager.CreateAsync(newAppUser, applicationUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        
                        var registerUser = manager.FindByName(applicationUserViewModel.UserName);

                        manager.AddToRoles(registerUser.Id, new string[] {"Member" });
                        return request.CreateResponse(HttpStatusCode.OK, newAppUser);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpGet]
        [Route("verify")]
        [AllowAnonymous]

        public async Task<HttpResponseMessage> Verify(HttpRequestMessage request, string id)
        {
            if (ModelState.IsValid)
            {
                var appUser = AppUserManager.FindById(id);
                appUser.Status = true;
                try
                {
                    var result = await AppUserManager.UpdateAsync(appUser);
                    if (result.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, appUser);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [Route("getallcountry")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _CountryService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getallstatebycountry")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAllStateByCountry(HttpRequestMessage request, int countryId)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _StateService.GetByCountryId(countryId);

                var responseData = Mapper.Map<IEnumerable<State>, IEnumerable<StateViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("sendmail")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage SendMail(HttpRequestMessage request, string toEmail, string userName, string id)
        {
            try
            {
                var host = ConfigHelper.GetByKey("SMTPHost");
                var port = int.Parse(ConfigHelper.GetByKey("SMTPPort"));
                var fromEmail = ConfigHelper.GetByKey("FromEmailAddress");
                var password = ConfigHelper.GetByKey("FromEmailPassword");
                var fromName = ConfigHelper.GetByKey("FromName");
                var subject = ConfigHelper.GetByKey("Subject");

                var smtpClient = new SmtpClient(host, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = false,
                    Timeout = 100000
                };
                
                string content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Templates/newuser.html"));
                content = content.Replace("{{UserName}}", userName);
                content = content.Replace("{{Link}}", ConfigHelper.GetByKey("CurrentLink") + id);
                var mail = new MailMessage
                {
                    Body = content,
                    Subject = subject,
                    From = new MailAddress(fromEmail, fromName)
                };

                mail.To.Add(new MailAddress(toEmail));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                smtpClient.Send(mail);

                var response = request.CreateResponse(HttpStatusCode.OK, "Send Sccucsess");
                return response;
            }
            catch (SmtpException smex)
            {

                var response = request.CreateResponse(HttpStatusCode.OK, "Send Failed");
                return response;
            }
            
        }
    }
}