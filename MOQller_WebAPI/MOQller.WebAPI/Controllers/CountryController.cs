﻿using AutoMapper;
using MOQller.Web.Models.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/country")]
    
    public class CountryController : ApiControllerBase
    {
        private ICountryService _CountryService;

        public CountryController(IErrorService errorService, ICountryService CountryService)
            : base(errorService)
        {
            this._CountryService = CountryService;
        }

        [Route("getall")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _CountryService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("checkauthen")]
        [HttpGet]

        public HttpResponseMessage CheckAuthen(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                

                var response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _CountryService.GetById(id);

                var responseData = Mapper.Map<Country, CountryViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _CountryService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryViewModel>>(query);

                var paginationSet = new PaginationSet<CountryViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, CountryViewModel CountryVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_CountryService.CheckCode(0, CountryVm.Code))
                    {
                        var newCountry = new Country();
                        newCountry.UpdateCountry(CountryVm);
                        newCountry.CreatedDate = DateTime.Now;
                        _CountryService.Add(newCountry);
                        _CountryService.Save();

                        var responseData = Mapper.Map<Country, CountryViewModel>(newCountry);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                    }
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, CountryViewModel CountryVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_CountryService.CheckCode(CountryVm.Id, CountryVm.Code))
                    {
                        var dbCountry = _CountryService.GetById(CountryVm.Id);

                        dbCountry.UpdateCountry(CountryVm);
                        dbCountry.UpdatedDate = DateTime.Now;

                        _CountryService.Update(dbCountry);
                        try
                        {
                            _CountryService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<Country, CountryViewModel>(dbCountry);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                    }

                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldCountry = _CountryService.Delete(id);
                    _CountryService.Save();

                    var responseData = Mapper.Map<Country, CountryViewModel>(oldCountry);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listCountry = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listCountry)
                    {
                        _CountryService.Delete(item);
                    }

                    _CountryService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listCountry.Count);
                }

                return response;
            });
        }
        
    }
}