﻿using AutoMapper;
using MOQller.Web.Models.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/parameter")]
    [Authorize]
    public class ParameterController : ApiControllerBase
    {



        private IParameterService _ParameterService;

        public ParameterController(IErrorService errorService, IParameterService ParameterService)
            : base(errorService)
        {
            this._ParameterService = ParameterService;
        }



        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ParameterService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Parameter>, IEnumerable<ParameterViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ParameterService.GetById(id);

                var responseData = Mapper.Map<Parameter, ParameterViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _ParameterService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<Parameter>, IEnumerable<ParameterViewModel>>(query);

                var paginationSet = new PaginationSet<ParameterViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, ParameterViewModel ParameterVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                
                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ParameterService.CheckCode(0, ParameterVm.Code))
                    {
                        var newParameter = new Parameter();
                        newParameter.UpdateParameter(ParameterVm);
                        newParameter.CreatedDate = DateTime.Now;
                        _ParameterService.Add(newParameter);
                        _ParameterService.Save();

                        var responseData = Mapper.Map<Parameter, ParameterViewModel>(newParameter);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                        
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, ParameterViewModel ParameterVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ParameterService.CheckCode(ParameterVm.Id, ParameterVm.Code))
                    {

                        var dbParameter = _ParameterService.GetById(ParameterVm.Id);

                        dbParameter.UpdateParameter(ParameterVm);
                        dbParameter.UpdatedDate = DateTime.Now;

                        _ParameterService.Update(dbParameter);
                        try
                        {
                            _ParameterService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<Parameter, ParameterViewModel>(dbParameter);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldParameter = _ParameterService.Delete(id);
                    _ParameterService.Save();

                    var responseData = Mapper.Map<Parameter, ParameterViewModel>(oldParameter);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listParameter = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listParameter)
                    {
                        _ParameterService.Delete(item);
                    }

                    _ParameterService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listParameter.Count);
                }

                return response;
            });
        }

    }
}