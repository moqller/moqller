﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using System.Web.Script.Serialization;
using AutoMapper;
using System.Net;
using MOQller.Web.Models.PaymentStatus;
using MOQller.Web.Infrastructure.Extensions;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/PaymentStatus")]
    [Authorize]
    public class PaymentStatusController : ApiControllerBase
    {
        private IPaymentStatusService _PaymentStatusService;

        public PaymentStatusController (IErrorService errorService, IPaymentStatusService PaymentStatusService)
            : base(errorService)
        {
            this._PaymentStatusService = PaymentStatusService;
        }

        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _PaymentStatusService.GetAll();

                var responseData = Mapper.Map<IEnumerable<PaymentStatus>, IEnumerable<PaymentStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _PaymentStatusService.GetById(id);

                var responseData = Mapper.Map<PaymentStatus, PaymentStatusViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging (HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _PaymentStatusService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.Name).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<PaymentStatus>, IEnumerable<PaymentStatusViewModel>>(query);

                var paginationSet = new PaginationSet<PaymentStatusViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create (HttpRequestMessage request, PaymentStatusViewModel PaymentStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    // truyền Id là 0 khi thêm mưới
                    if (_PaymentStatusService.CheckNamePaymentStatus(0, PaymentStatusVm.Name))
                    {
                        var newPaymentStatus = new PaymentStatus();
                        newPaymentStatus.UpdatePaymentStatus(PaymentStatusVm);
                        //newPaymentStatus.CreatedDate = DateTime.Now;
                        _PaymentStatusService.Add(newPaymentStatus);
                        _PaymentStatusService.Save();

                        var responseData = Mapper.Map<PaymentStatus, PaymentStatusViewModel>(newPaymentStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exists");
                    }
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update (HttpRequestMessage request, PaymentStatusViewModel PaymentStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_PaymentStatusService.CheckNamePaymentStatus(PaymentStatusVm.Id, PaymentStatusVm.Name))
                    {
                        var dbPaymentStatus = _PaymentStatusService.GetById(PaymentStatusVm.Id);

                        dbPaymentStatus.UpdatePaymentStatus(PaymentStatusVm);
                        //dbPaymentStatus.UpdatedDate = DateTime.Now;

                        _PaymentStatusService.Update(dbPaymentStatus);
                        try
                        {
                            _PaymentStatusService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<PaymentStatus, PaymentStatusViewModel>(dbPaymentStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exists");
                    }
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldPaymentStatus = _PaymentStatusService.Delete(id);
                    _PaymentStatusService.Save();

                    var responseData = Mapper.Map<PaymentStatus, PaymentStatusViewModel>(oldPaymentStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti (HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listPaymentStatus = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listPaymentStatus)
                    {
                        _PaymentStatusService.Delete(item);
                    }

                    _PaymentStatusService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listPaymentStatus.Count);
                }

                return response;
            });
        }
    }
}