﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;
using MOQller.Web.Models;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/material")]
    [Authorize]
    public class MaterialController : ApiControllerBase
    {
        
        private IMaterialService _MaterialService;

        public MaterialController(IErrorService errorService, IMaterialService MaterialService)
            : base(errorService)
        {
            this._MaterialService = MaterialService;
        }


        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Material>, IEnumerable<MaterialViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialService.GetById(id);

                var responseData = Mapper.Map<Material, MaterialViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _MaterialService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<Material>, IEnumerable<MaterialViewModel>>(query);

                var paginationSet = new PaginationSet<MaterialViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, MaterialViewModel MaterialVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                
                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newMaterial = new Material();
                    newMaterial.UpdateMaterial(MaterialVm);
                    newMaterial.CreatedDate = DateTime.Now;
                    _MaterialService.Add(newMaterial);
                    _MaterialService.Save();

                    var responseData = Mapper.Map<Material, MaterialViewModel>(newMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, MaterialViewModel MaterialVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbMaterial = _MaterialService.GetById(MaterialVm.Id);

                    dbMaterial.UpdateMaterial(MaterialVm);
                    dbMaterial.UpdatedDate = DateTime.Now;

                    _MaterialService.Update(dbMaterial);
                    try
                    {
                        _MaterialService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<Material, MaterialViewModel>(dbMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldMaterial = _MaterialService.Delete(id);
                    _MaterialService.Save();

                    var responseData = Mapper.Map<Material, MaterialViewModel>(oldMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listMaterial = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listMaterial)
                    {
                        _MaterialService.Delete(item);
                    }

                    _MaterialService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listMaterial.Count);
                }

                return response;
            });
        }

    }
}