﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MOQller.Common.Exceptions;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.App_Start;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Web.Models;
using MOQller.Web.Providers;
using System.Web.Security;
using MOQller.Common;
using System.Web;

namespace MOQller.Web.Controllers
{
    [Authorize]
    [RoutePrefix("api/appUser")]
    public class AppUserController : ApiControllerBase
    {
        public AppUserController(IErrorService errorService)
            : base(errorService)
        {
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "USER")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = AppUserManager.Users;
                if (!string.IsNullOrWhiteSpace(filter))
                    model = model.Where(x => x.UserName.Contains(filter) || x.FullName.Contains(filter));

                totalRow = model.Count();

                var data = model.OrderBy(x => x.FullName).Skip((page - 1) * pageSize).Take(pageSize);
                IEnumerable<AppUserViewModel> modelVm = Mapper.Map<IEnumerable<AppUser>, IEnumerable<AppUserViewModel>>(data);

                PaginationSet<AppUserViewModel> pagedSet = new PaginationSet<AppUserViewModel>()
                {
                    PageIndex = page,
                    PageSize = pageSize,
                    TotalRows = totalRow,
                    Items = modelVm,
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("detail/{id}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "USER")]
        [Authorize]
        public async Task<HttpResponseMessage> Details(HttpRequestMessage request, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var user = await AppUserManager.FindByIdAsync(id);
            if (user == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var roles = await AppUserManager.GetRolesAsync(user.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(user);
                applicationUserViewModel.Roles = roles;
                return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
            }
        }

        [HttpPost]
        [Route("add")]
        //[Authorize(Roles = "AddUser")]
        [Permission(Action = "Create", Function = "USER")]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, AppUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var newAppUser = new AppUser();
                newAppUser.UpdateUser(applicationUserViewModel);
                try
                {
                    newAppUser.Id = Guid.NewGuid().ToString();
                    var result = await AppUserManager.CreateAsync(newAppUser, applicationUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        var roles = applicationUserViewModel.Roles.ToArray();
                        await AppUserManager.AddToRolesAsync(newAppUser.Id, roles);

                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("update")]
        //[Authorize(Roles = "UpdateUser")]
        [Permission(Action = "Update", Function = "USER")]
        public async Task<HttpResponseMessage> Update(HttpRequestMessage request, AppUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var appUser = await AppUserManager.FindByIdAsync(applicationUserViewModel.Id);
                try
                {
                    appUser.UpdateUser(applicationUserViewModel);
                    var result = await AppUserManager.UpdateAsync(appUser);
                    if (result.Succeeded)
                    {
                        var userRoles = await AppUserManager.GetRolesAsync(appUser.Id);
                        var selectedRole = applicationUserViewModel.Roles.ToArray();

                        selectedRole = selectedRole ?? new string[] { };

                        await AppUserManager.AddToRolesAsync(appUser.Id, selectedRole.Except(userRoles).ToArray());
                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpDelete]
        [Route("delete")]
        [Permission(Action = "Delete", Function = "USER")]
        //[Authorize(Roles ="DeleteUser")]
        public async Task<HttpResponseMessage> Delete(HttpRequestMessage request, string id)
        {
            var appUser = await AppUserManager.FindByIdAsync(id);
            var result = await AppUserManager.DeleteAsync(appUser);
            if (result.Succeeded)
                return request.CreateResponse(HttpStatusCode.OK, id);
            else
                return request.CreateErrorResponse(HttpStatusCode.OK, string.Join(",", result.Errors));
        }

        [HttpPut]
        [Route("ChangePassword")]
        public async Task<HttpResponseMessage> ChangePassword(HttpRequestMessage request, ChangePasswordViewModel changepassVm)
        {
            // kiểm tra dữ liệu trả về
            if (ModelState.IsValid)
            {
                var appUser = await AppUserManager.FindByNameAsync(changepassVm.UserName);
                // kiểm tra user có tồn tại nếu không badrequest
                if (appUser != null)
                {
                    try
                    {
                        var result = await AppUserManager.ChangePasswordAsync(appUser.Id, changepassVm.OldPassword, changepassVm.NewPassword);
                        // Kiếm tra nếu thành công không thành công thì pass cũ sai
                        if (result.Succeeded)
                        {
                            return request.CreateResponse(HttpStatusCode.OK, changepassVm.UserName);
                        }
                        else
                        {
                            return request.CreateResponse(HttpStatusCode.NotFound, changepassVm.UserName);
                        }
                    }
                    catch (NameDuplicatedException dex)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                    }
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // cho phép setpass nếu có quền xóa
        [HttpPost]
        [Route("SetPassword")]
        [Permission(Action = "Delete", Function = "USER")]
        public async Task<HttpResponseMessage> SetPassword(HttpRequestMessage request, SetPasswordModel SetpassVm)
        {
            if (ModelState.IsValid)
            {
                var appUser = await AppUserManager.FindByIdAsync(SetpassVm.Id);
                if (appUser != null)
                {
                    // Add pass mới không cần pass cũ
                    var result = await AppUserManager.AddPasswordAsync(SetpassVm.Id, SetpassVm.NewPassword);
                    if (result.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);
                    }
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpGet]
        [Route("RequestResetPasswordToken/{UserName}/{Email}")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> GetResetPasswordToken(HttpRequestMessage request, string UserName, string Email)
        {
            if (!ModelState.IsValid)
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var user = await AppUserManager.FindByEmailAsync(Email.Trim());

            if (user != null && UserName.Trim() == user.UserName)
            {
                var callbackUrl = ConfigHelper.GetByKey("ResetPwdLink");

                if (callbackUrl == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Contact to admin check config link reset password");
                }
                else
                {
                    var token = await AppUserManager.GeneratePasswordResetTokenAsync(user.Id);

                    callbackUrl = callbackUrl + "?email=" + user.Email + "&token=" + HttpUtility.UrlEncode(token);

                    var subject = "Moqller - Password reset.";

                    var body = "<html><body>" +
                               "<h2>Password reset</h2>" +
                               $"<p>Hi {user.UserName}, <a href=\"{callbackUrl}\"> please click this link to reset your password </a></p>" +
                               "</body></html>";

                    if (MailHelper.SendMail(user.Email, subject, body) == true)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.OK, "Email reset password is send!");
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email reset password cannot send! Please contact to admin.");
                    }
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, "UserName or Email does not exist!");
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> ResetPasswordAsync(HttpRequestMessage request, ResetPasswordRequestModel model)
        {
            if (!ModelState.IsValid)
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var user = await AppUserManager.FindByEmailAsync(model.Email);
            
            if (user != null)
            {
                if (!await AppUserManager.UserTokenProvider.ValidateAsync("ResetPassword", model.Token, AppUserManager, user))
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error token!");
                }

                var result = await AppUserManager.ResetPasswordAsync(user.Id, model.Token, model.NewPassword);

                if (result.Succeeded)
                {

                    const string subject = "Moqller - Password reset success.";
                    var body = "<html><body>" +
                               "<h1>Your password for Client was reset</h1>" +
                               $"<p>Hi {user.FullName}!</p>" +
                               "<p>Your password for Moqller was reset. Please inform us if you did not request this change.</p>" +
                               "</body></html>";

                    MailHelper.SendMail(user.Email, subject, body);
                }

                return request.CreateErrorResponse(HttpStatusCode.OK, "Password reset success!");
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email does not exist!");
            }
            
        }

    }
}