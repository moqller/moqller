﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using MOQller.Common;
using MOQller.Common.ViewModels;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Models;
using MOQller.Web.Models.DeliveryStatus;
using MOQller.Web.Models.OrderStatus;
using MOQller.Web.Models.PaymentStatus;
using MOQller.Web.Providers;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/transactionhistory")]
    public class TransactionHistoryController : ApiControllerBase
    {
        private IPaymentStatusService _PaymentStatusService;
        private IDeliveryStatusService _DeliveryStatusService;
        private IOrderStatusService _OrderStatusService;
        private IOrderService _orderService;

        public TransactionHistoryController (IErrorService errorService, IPaymentStatusService PaymentStatusService, IDeliveryStatusService DeliveryStatusService,
            IOrderStatusService orderStatusService,
            IOrderService orderService)
            : base(errorService)
        {
            this._PaymentStatusService = PaymentStatusService;
            this._DeliveryStatusService = DeliveryStatusService;
            this._OrderStatusService = orderStatusService;
            this._orderService = orderService;
        }

        [Route("getallpayment")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllPayment (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _PaymentStatusService.GetAll().Where(ds => ds.Code != "DEF");

                var responseData = Mapper.Map<IEnumerable<PaymentStatus>, IEnumerable<PaymentStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getalldelivery")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllShipping (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _DeliveryStatusService.GetAll();

                var responseData = Mapper.Map<IEnumerable<DeliveryStatus>, IEnumerable<DeliveryStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getallorderstatus")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetAllOrderStatus (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _OrderStatusService.GetAll().Where(ds => ds.Code != "DEF");

                var responseData = Mapper.Map<IEnumerable<OrderStatus>, IEnumerable<OrderStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }


        [Route("getbuying")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetBuying (HttpRequestMessage request, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId ,int pageIndex, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRowBuying = 0;

                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();

                var model = _orderService.GetBying(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId);

                totalRowBuying = model.Count();

                var query = model.OrderByDescending(x => x.OrderDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<BuyingViewModel>, IEnumerable<BuyingViewModel>>(query);

                var paginationSet = new PaginationSet<BuyingViewModel>()
                {
                    Items = responseData,
                    PageIndex = pageIndex,
                    TotalRows = totalRowBuying,
                    PageSize = pageSize
                };

                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getselling")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetSelling (HttpRequestMessage request, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, int pageIndex, int pageSize, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();

                int totalRowSelling = 0;

                var model = _orderService.GetSelling(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId, filter);

                totalRowSelling = model.Count();

                var query = model.OrderByDescending(x => x.OrderDate).Skip((pageIndex - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<SellingViewModel>, IEnumerable<SellingViewModel>>(query);

                var paginationSet = new PaginationSet<SellingViewModel>()
                {
                    Items = responseData,
                    PageIndex = pageIndex,
                    TotalRows = totalRowSelling,
                    PageSize = pageSize
                };

                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("exportExcel")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage ExportTransaction (HttpRequestMessage request, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, bool isSeller)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            var UserId = ClaimsPrincipal.Current.Identity.GetUserId();

            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = GenerateFileExport(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId, isSeller);
            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error export");
            }
        }

        private string GenerateFileExport (string UserId, string StartDate, string EndDate, int PaymentId, int deliveryId, int OrderStatusId, bool isSeller)
        {
            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument;
            // Template File Seller or Buyer
            if (isSeller == true)
            {
                templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TransactionTemplateSeller.xlsx");
            }
            else
            {
                templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TransactionTemplateBuyer.xlsx");
            }
            
            string documentName = string.Format("Transaction-{0}-{1}.xlsx", UserId, DateTime.Now.ToString("yyyyMMddhhmmsss"));
            string fullPath = Path.Combine(filePath, documentName);
            // Results Output
            MemoryStream output = new MemoryStream();
            try
            {
                // Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    // Create Excel EPPlus Package based on template stream
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        int rowIndex = 5;

                        int count_buy = 1;

                        if (isSeller == true)
                        {
                            ExcelWorksheet sheet_sell = package.Workbook.Worksheets["Selling"];

                            var transactions_selling = _orderService.GetSelling(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId, "");

                            sheet_sell.Cells[2, 2].Value = StartDate;
                            sheet_sell.Cells[2, 4].Value = EndDate;
                            // Start Row for Detail Rows
                            int rowIndex_sell = 5;

                            int count_sell = 1;
                            foreach (var item in transactions_selling)
                            {
                                // Cell 1, Carton Count
                                sheet_sell.Cells[rowIndex_sell, 1].Value = count_buy.ToString();
                                // Cell 2, Order Number (Outline around columns 2-7 make it look like 1 column)
                                sheet_sell.Cells[rowIndex_sell, 2].Value = item.OrderNumber;
                                // Cell 8, Weight in LBS (convert KG to LBS, and rounding to whole number)
                                sheet_sell.Cells[rowIndex_sell, 3].Value = item.OrderDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));

                                sheet_sell.Cells[rowIndex_sell, 4].Value = item.PartNumber;
                                sheet_sell.Cells[rowIndex_sell, 5].Value = item.Quantity;
                                sheet_sell.Cells[rowIndex_sell, 6].Value = item.Total;
                                sheet_sell.Cells[rowIndex_sell, 7].Value = item.DeliveryStatusName;
                                sheet_sell.Cells[rowIndex_sell, 8].Value = item.OrderStatusName;
                                sheet_sell.Cells[rowIndex_sell, 9].Value = item.TrackingNumber;
                                // Increment Row Counter
                                rowIndex_sell++;
                                count_sell++;
                            }

                        }
                        else
                        {
                            ExcelWorksheet sheet_buy = package.Workbook.Worksheets["Buying"];

                            // Data Acces, load order header data.
                            var transactions_buying = _orderService.GetBying(UserId, StartDate, EndDate, PaymentId, deliveryId, OrderStatusId);



                            sheet_buy.Cells[2, 2].Value = StartDate;
                            sheet_buy.Cells[2, 4].Value = EndDate;
                            // Start Row for Detail Rows

                            foreach (var item in transactions_buying)
                            {
                                // Cell 1, Carton Count
                                sheet_buy.Cells[rowIndex, 1].Value = count_buy.ToString();
                                // Cell 2, Order Number (Outline around columns 2-7 make it look like 1 column)
                                sheet_buy.Cells[rowIndex, 2].Value = item.OrderNumber;
                                // Cell 8, Weight in LBS (convert KG to LBS, and rounding to whole number)
                                sheet_buy.Cells[rowIndex, 3].Value = item.OrderDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));
                                sheet_buy.Cells[rowIndex, 4].Value = item.PartNumber;
                                sheet_buy.Cells[rowIndex, 5].Value = item.Quantity;
                                sheet_buy.Cells[rowIndex, 6].Value = item.Total;
                                sheet_buy.Cells[rowIndex, 7].Value = item.DeliveryStatusName;
                                sheet_buy.Cells[rowIndex, 8].Value = item.OrderStatusName;
                                sheet_buy.Cells[rowIndex, 9].Value = item.SellerName;
                                // Increment Row Counter
                                rowIndex++;
                                count_buy++;
                            }
                        }

                        package.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }

    }
}