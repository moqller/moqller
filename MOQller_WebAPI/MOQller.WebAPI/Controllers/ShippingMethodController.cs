﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using System.Web.Script.Serialization;
using AutoMapper;
using System.Net;
using MOQller.Web.Models.ShippingMethod;
using MOQller.Web.Infrastructure.Extensions;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/ShippingMethod")]
    [Authorize]
    public class ShippingMethodController : ApiControllerBase
    {
        private IShippingMethodService _ShippingMethodService;

        public ShippingMethodController (IErrorService errorService, IShippingMethodService ShippingMethodService)
            : base(errorService)
        {
            this._ShippingMethodService = ShippingMethodService;
        }

        [Route("getall")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetAll (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ShippingMethodService.GetAll();

                var responseData = Mapper.Map<IEnumerable<ShippingMethod>, IEnumerable<ShippingMethodViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ShippingMethodService.GetById(id);

                var responseData = Mapper.Map<ShippingMethod, ShippingMethodViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging (HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _ShippingMethodService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.Name).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ShippingMethod>, IEnumerable<ShippingMethodViewModel>>(query);

                var paginationSet = new PaginationSet<ShippingMethodViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create (HttpRequestMessage request, ShippingMethodViewModel ShippingMethodVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ShippingMethodService.CheckNameShippingMethod(0, ShippingMethodVm.Name))
                    {
                        var newShippingMethod = new ShippingMethod();
                        newShippingMethod.UpdateShippingMethod(ShippingMethodVm);
                        //newShippingMethod.CreatedDate = DateTime.Now;
                        _ShippingMethodService.Add(newShippingMethod);
                        _ShippingMethodService.Save();

                        var responseData = Mapper.Map<ShippingMethod, ShippingMethodViewModel>(newShippingMethod);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exists");
                    }
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update (HttpRequestMessage request, ShippingMethodViewModel ShippingMethodVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ShippingMethodService.CheckNameShippingMethod(ShippingMethodVm.Id, ShippingMethodVm.Name))
                    {

                        var dbShippingMethod = _ShippingMethodService.GetById(ShippingMethodVm.Id);

                        dbShippingMethod.UpdateShippingMethod(ShippingMethodVm);
                        //dbShippingMethod.UpdatedDate = DateTime.Now;

                        _ShippingMethodService.Update(dbShippingMethod);
                        try
                        {
                            _ShippingMethodService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<ShippingMethod, ShippingMethodViewModel>(dbShippingMethod);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exists");
                    }

                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldShippingMethod = _ShippingMethodService.Delete(id);
                    _ShippingMethodService.Save();

                    var responseData = Mapper.Map<ShippingMethod, ShippingMethodViewModel>(oldShippingMethod);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti (HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listShippingMethod = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listShippingMethod)
                    {
                        _ShippingMethodService.Delete(item);
                    }

                    _ShippingMethodService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listShippingMethod.Count);
                }

                return response;
            });
        }
    }
}