﻿using AutoMapper;
using MOQller.Web.Models.ConfirmStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/confirmstatus")]
    [Authorize]
    public class ConfirmStatusController : ApiControllerBase
    {



        private IConfirmStatusService _ConfirmStatusService;

        public ConfirmStatusController(IErrorService errorService, IConfirmStatusService ConfirmStatusService)
            : base(errorService)
        {
            this._ConfirmStatusService = ConfirmStatusService;
        }



        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ConfirmStatusService.GetAll();

                var responseData = Mapper.Map<IEnumerable<ConfirmStatus>, IEnumerable<ConfirmStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ConfirmStatusService.GetById(id);

                var responseData = Mapper.Map<ConfirmStatus, ConfirmStatusViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _ConfirmStatusService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ConfirmStatus>, IEnumerable<ConfirmStatusViewModel>>(query);

                var paginationSet = new PaginationSet<ConfirmStatusViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, ConfirmStatusViewModel ConfirmStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newConfirmStatus = new ConfirmStatus();
                    newConfirmStatus.UpdateConfirmStatus(ConfirmStatusVm);
                    newConfirmStatus.CreatedDate = DateTime.Now;
                    _ConfirmStatusService.Add(newConfirmStatus);
                    _ConfirmStatusService.Save();

                    
                    var responseData = Mapper.Map<ConfirmStatus, ConfirmStatusViewModel>(newConfirmStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, ConfirmStatusViewModel ConfirmStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbConfirmStatus = _ConfirmStatusService.GetById(ConfirmStatusVm.Id);

                    dbConfirmStatus.UpdateConfirmStatus(ConfirmStatusVm);
                    dbConfirmStatus.UpdatedDate = DateTime.Now;

                    _ConfirmStatusService.Update(dbConfirmStatus);
                    try
                    {
                        _ConfirmStatusService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<ConfirmStatus, ConfirmStatusViewModel>(dbConfirmStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldConfirmStatus = _ConfirmStatusService.Delete(id);
                    _ConfirmStatusService.Save();

                    var responseData = Mapper.Map<ConfirmStatus, ConfirmStatusViewModel>(oldConfirmStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listConfirmStatus = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listConfirmStatus)
                    {
                        _ConfirmStatusService.Delete(item);
                    }

                    _ConfirmStatusService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listConfirmStatus.Count);
                }

                return response;
            });
        }

    }
}