﻿using AutoMapper;
using MOQller.Web.Models.ShippingStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/shippingstatus")]
    [Authorize]
    public class ShippingStatusController : ApiControllerBase
    {



        private IShippingStatusService _ShippingStatusService;

        public ShippingStatusController(IErrorService errorService, IShippingStatusService ShippingStatusService)
            : base(errorService)
        {
            this._ShippingStatusService = ShippingStatusService;
        }



        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ShippingStatusService.GetAll();

                var responseData = Mapper.Map<IEnumerable<ShippingStatus>, IEnumerable<ShippingStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ShippingStatusService.GetById(id);

                var responseData = Mapper.Map<ShippingStatus, ShippingStatusViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _ShippingStatusService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ShippingStatus>, IEnumerable<ShippingStatusViewModel>>(query);

                var paginationSet = new PaginationSet<ShippingStatusViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, ShippingStatusViewModel ShippingStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                
                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ShippingStatusService.CheckNameShippingStatus(0, ShippingStatusVm.Name, ShippingStatusVm.Code))
                    {
                        var newShippingStatus = new ShippingStatus();
                        newShippingStatus.UpdateShippingStatus(ShippingStatusVm);
                        newShippingStatus.CreatedDate = DateTime.Now;
                        _ShippingStatusService.Add(newShippingStatus);
                        _ShippingStatusService.Save();

                        var responseData = Mapper.Map<ShippingStatus, ShippingStatusViewModel>(newShippingStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Duplicate Name Or Code");
                    }
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, ShippingStatusViewModel ShippingStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_ShippingStatusService.CheckNameShippingStatus(ShippingStatusVm.Id, ShippingStatusVm.Name, ShippingStatusVm.Code))
                    {
                        var dbShippingStatus = _ShippingStatusService.GetById(ShippingStatusVm.Id);

                        dbShippingStatus.UpdateShippingStatus(ShippingStatusVm);
                        dbShippingStatus.UpdatedDate = DateTime.Now;

                        _ShippingStatusService.Update(dbShippingStatus);
                        try
                        {
                            _ShippingStatusService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<ShippingStatus, ShippingStatusViewModel>(dbShippingStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name or Code exists");
                    }

                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldShippingStatus = _ShippingStatusService.Delete(id);
                    _ShippingStatusService.Save();

                    var responseData = Mapper.Map<ShippingStatus, ShippingStatusViewModel>(oldShippingStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listShippingStatus = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listShippingStatus)
                    {
                        _ShippingStatusService.Delete(item);
                    }

                    _ShippingStatusService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listShippingStatus.Count);
                }

                return response;
            });
        }

    }
}