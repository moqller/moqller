﻿using AutoMapper;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Web.Models.OrderStatus;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/OrderStatus")]
    [Authorize]
    public class OrderStatusController : ApiControllerBase
    {
        private IOrderStatusService _OrderStatusService;

        public OrderStatusController (IErrorService errorService, IOrderStatusService OrderStatusService)
            : base(errorService)
        {
            this._OrderStatusService = OrderStatusService;
        }

        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _OrderStatusService.GetAll();

                var responseData = Mapper.Map<IEnumerable<OrderStatus>, IEnumerable<OrderStatusViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _OrderStatusService.GetById(id);

                var responseData = Mapper.Map<OrderStatus, OrderStatusViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging (HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _OrderStatusService.GetAll(filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<OrderStatus>, IEnumerable<OrderStatusViewModel>>(query);

                var paginationSet = new PaginationSet<OrderStatusViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create (HttpRequestMessage request, OrderStatusViewModel OrderStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_OrderStatusService.CheckNameOrderStatus(0, OrderStatusVm.Name))
                    {
                        var newOrderStatus = new OrderStatus();
                        newOrderStatus.UpdateOrderStatus(OrderStatusVm);
                        newOrderStatus.CreatedDate = DateTime.Now;
                        _OrderStatusService.Add(newOrderStatus);
                        _OrderStatusService.Save();

                        var responseData = Mapper.Map<OrderStatus, OrderStatusViewModel>(newOrderStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Name exists");
                    }
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update (HttpRequestMessage request, OrderStatusViewModel OrderStatusVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_OrderStatusService.CheckNameOrderStatus(OrderStatusVm.Id, OrderStatusVm.Name))
                    {
                        var dbOrderStatus = _OrderStatusService.GetById(OrderStatusVm.Id);

                        dbOrderStatus.UpdateOrderStatus(OrderStatusVm);
                        dbOrderStatus.UpdatedDate = DateTime.Now;

                        _OrderStatusService.Update(dbOrderStatus);
                        try
                        {
                            _OrderStatusService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<OrderStatus, OrderStatusViewModel>(dbOrderStatus);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                    }

                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldOrderStatus = _OrderStatusService.Delete(id);
                    _OrderStatusService.Save();

                    var responseData = Mapper.Map<OrderStatus, OrderStatusViewModel>(oldOrderStatus);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti (HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listOrderStatus = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listOrderStatus)
                    {
                        _OrderStatusService.Delete(item);
                    }

                    _OrderStatusService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listOrderStatus.Count);
                }

                return response;
            });
        }
    }
}