﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;
using MOQller.Data.Repositories;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using MOQller.Web.Models.Cart;
using MOQller.Common.ViewModels;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/cart")]

    public class CartController : ApiControllerBase
    {

        private ICartService _CartService;
        private ICartRepository _CartRepository;
        private IBOMItemService _BOMItemService;
        private IMaterialListService _materialListService;

        public CartController (IErrorService errorService, 
            ICartService CartService, 
            ICartRepository CartRepository, 
            IBOMItemService BOMItemService,
            IMaterialListService materialListService)
            : base(errorService)
        {
            this._CartService = CartService;
            this._CartRepository = CartRepository;
            this._BOMItemService = BOMItemService;
            this._materialListService = materialListService;
        }



        [Route("countitem")]
        [HttpGet]
        public HttpResponseMessage CountItem(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var userId = ClaimsPrincipal.Current.Identity.GetUserId();

                var response = request.CreateResponse(HttpStatusCode.OK, new { Count = _CartService.GetAll(userId).Count() });

                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetById (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _CartService.GetById(id);

                var responseData = Mapper.Map<Cart, CartViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetListPaging (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var userId = ClaimsPrincipal.Current.Identity.GetUserId();

                var model = _CartService.GetAll(userId).OrderByDescending(x => x.CreatedDate);

                int CountPartNumber = 0;

                if (model.GroupBy(x => x.MaterialList.MaterialId).Count() > 0)
                {
                    CountPartNumber = model.GroupBy(x => x.MaterialList.MaterialId).Count();
                }

                var responseData = Mapper.Map<IEnumerable<Cart>, IEnumerable<CartViewModel>>(model);

                var result = new ResultCart<CartViewModel>()
                {
                    Items = responseData,
                    CountByPartNumber = CountPartNumber
                };

                var response = request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            });
        }


        private class ResultCart<T>
        {
            public int CountByPartNumber { get; set; }
            public IEnumerable<T> Items { set; get; }
        }


	    [Route("add")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage CreateCart(HttpRequestMessage request, CartViewModel CartVm)
        {
            var userId = ClaimsPrincipal.Current.Identity.GetUserId();

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                CartVm.BuyerId = userId; // thêm vào userid thay vì truyền

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var cart = _CartService.CheckCart(CartVm.BuyerId, CartVm.MaterialListId);
                    if(cart  == null)
                    { 
                        if(_materialListService.CheckMoqMaterialList(CartVm.MaterialListId, CartVm.Quantity))
                        {
                            var newCart = new Cart();
                            newCart.UpdateCart(CartVm);
                            newCart.CreatedDate = DateTime.Now;
                            _CartService.Add(newCart);
                            _CartService.Save();
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.Conflict, "Quantity must be greater than MOQ");
                            return response;
                        }
                    }
                    else
                    {
                        cart.Quantity += CartVm.Quantity;
                        cart.ExtendedPrice = CartVm.Quantity * CartVm.UnitPrice;
                        cart.UpdatedDate = DateTime.Now;
                        _CartService.Update(cart);
                        _CartService.Save();
                    }

                    var responseData = _CartService.GetAll(userId).Count();

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }
        //thêm cart và trừ số lượng ở bom
        [Route("AddCartAndUpdateBomitem")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddToCartAndDivideBomItem(HttpRequestMessage request, CartAndBomItemViewModel CartVm)
        {
            var userId = ClaimsPrincipal.Current.Identity.GetUserId();

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                CartVm.BuyerId = userId; // thêm vào userid thay vì truyền

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var cart = _CartService.CheckCart(CartVm.BuyerId, CartVm.MaterialListId);
                    var bomitem = _BOMItemService.GetById(CartVm.BomItemId);
                    if (cart == null)
                    {
                        if (_materialListService.CheckMoqMaterialList(CartVm.MaterialListId, CartVm.Quantity))
                        {
                            var newCart = new Cart();
                           
                            newCart.MaterialListId = CartVm.MaterialListId;
                            newCart.Quantity = CartVm.Quantity;
                            newCart.UnitPrice = CartVm.UnitPrice;
                            newCart.ExtendedPrice = CartVm.UnitPrice * CartVm.Quantity;
                            newCart.BuyerId = CartVm.BuyerId;
                            newCart.CreatedDate = DateTime.Now;
                            
                            _CartService.Add(newCart);
                            _CartService.Save();
                            
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.Conflict, "Quantity must be greater than MOQ");
                            return response;
                        }
                    }
                    else
                    {
                        cart.Quantity += CartVm.Quantity;
                        //bomitem.Quantity -= CartVm.Quantity;
                        cart.ExtendedPrice = CartVm.Quantity * CartVm.UnitPrice;
                        cart.UpdatedDate = DateTime.Now;
                        _CartService.Update(cart);
                        _CartService.Save();
                    }

                    bomitem.Quantity -= CartVm.Quantity;
                    bomitem.UpdatedDate = DateTime.Now;
                    _BOMItemService.Update(bomitem);
                    _BOMItemService.Save();

                    var responseData = _CartService.GetAll(userId).Count();

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize]
        public HttpResponseMessage Update (HttpRequestMessage request, CartViewModel CartVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbCart = _CartService.GetById(CartVm.Id);

                    dbCart.UpdateCart(CartVm);
                    dbCart.UpdatedDate = DateTime.Now;

                    _CartService.Update(dbCart);
                    try
                    {
                        _CartService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<Cart, CartViewModel>(dbCart);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("updateqty")]
        [HttpPut]
        [Authorize]
        public HttpResponseMessage UpdateQty (HttpRequestMessage request, int CartId, int Quantity)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbCart = _CartService.GetById(CartId);
                    if (dbCart != null)
                    {
                        if (_materialListService.CheckMoqMaterialList(dbCart.MaterialListId, Quantity))
                        {
                            if (_materialListService.CheckMaxMoqMaterialList(dbCart.MaterialListId, Quantity))
                            {
                                dbCart.Quantity = Quantity;
                                dbCart.ExtendedPrice = Quantity * dbCart.UnitPrice;
                                dbCart.UpdatedDate = DateTime.Now;

                                _CartService.Update(dbCart);
                                try
                                {
                                    _CartService.Save();
                                }
                                catch (DbEntityValidationException e)
                                {
                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                                ve.PropertyName, ve.ErrorMessage);
                                        }
                                    }
                                    throw;
                                }
                                var responseData = Mapper.Map<Cart, CartViewModel>(dbCart);
                                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                            }
                            else
                            {
                                response = request.CreateResponse(HttpStatusCode.Conflict, "Quantity must be smaller than MaxOfQuantity");
                            }
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.Conflict, "Quantity must be greater than MOQ");
                        }
                    }
                }
                return response;
            });
        }

        [Route("CheckQtyByPay")]
        [HttpPut]
        [Authorize]
        public HttpResponseMessage CheckQtyByPay(HttpRequestMessage request)
        {
            var userId = ClaimsPrincipal.Current.Identity.GetUserId();

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var CartMaterial = _CartService.GetAll(userId).OrderByDescending(x => x.CreatedDate);

                    if (CartMaterial != null)
                    {
                        foreach (var item in CartMaterial)
                        {
                            if (_materialListService.CheckQuantityMaterialList(item.MaterialListId, item.Quantity))
                            {
                                response = request.CreateErrorResponse(HttpStatusCode.Conflict, "Quantity partnumber '" + item.MaterialList.Material.PartNumber + "' in stock only '" + item.MaterialList.AvailableQty + "'");
                                break;
                            }
                            else
                            {
                                response = request.CreateErrorResponse(HttpStatusCode.OK, "OK");
                            }
                        }
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }

                return response;
            });
        }


        [Route("delete")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldCart = _CartService.Delete(id);
                    _CartService.Save();

                    var responseData = Mapper.Map<Cart, CartViewModel>(oldCart);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage DeleteMulti (HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listCart = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listCart)
                    {
                        _CartService.Delete(item);
                    }

                    _CartService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listCart.Count);
                }

                return response;
            });
        }

	    [Route("addlist")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage AddList(HttpRequestMessage request,  List<CartAndBomItemViewModel> listItems)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listReturn = new List<CartViewModel>();
                    try
                    {
                        
                        if (listItems != null)
                        {

                            foreach (var item in listItems)
                            {
                                var availableQty = _materialListService.GetById(item.MaterialListId).AvailableQty;
                                var qty = availableQty > item.Quantity ? item.Quantity : availableQty;
                                var newCart = new Cart();
                                newCart.MaterialListId = item.MaterialListId;
                                newCart.Quantity = item.Quantity;
                                newCart.UnitPrice = item.UnitPrice;
                                newCart.ExtendedPrice = item.UnitPrice * item.Quantity;
                                newCart.BuyerId = item.BuyerId;
                                newCart.CreatedDate = DateTime.Now;

                                var bomitem = _BOMItemService.GetById(item.BomItemId);
                                bomitem.UpdatedDate = DateTime.Now;
                                
                                if (item.Quantity != 0 && availableQty != 0)
                                {
                                    var cart = _CartService.CheckCart(item.BuyerId, item.MaterialListId);
                                    if (cart == null)
                                    {
                                        if (_materialListService.CheckMoqMaterialList(item.MaterialListId, qty))
                                        {
                                            
                                            if (_materialListService.CheckQuantityMaterialList(item.MaterialListId, item.Quantity))
                                            
                                            { 
                                                newCart.Quantity = availableQty;
                                                bomitem.Quantity -= availableQty;                                               
                                                _CartService.Add(newCart);
                                                _CartService.Save();
                                                
                                                var a = Mapper.Map<Cart, CartViewModel>(newCart);
                                                listReturn.Add(a);
                                            }
                                            else
                                            {
                                                //newCart.Quantity = newCart.Quantity;
                                                _CartService.Add(newCart);
                                                _CartService.Save();
                                                bomitem.Quantity -= newCart.Quantity;
                                                var a = Mapper.Map<Cart, CartViewModel>(newCart);
                                                a.Quantity = - (availableQty - a.Quantity);
                                                listReturn.Add(a);
                                            }

                                            
                                        }
                                        
                                    }
                                    else
                                    {
                                        if (_materialListService.CheckQuantityMaterialList(item.MaterialListId, item.Quantity))
                                        {
                                            newCart.Quantity = availableQty;                                           
                                            cart.Quantity += availableQty;
                                            bomitem.Quantity -= availableQty;
                                            cart.ExtendedPrice = cart.Quantity * cart.UnitPrice;
                                        }
                                        else
                                        {
                                            newCart.Quantity = - (availableQty - newCart.Quantity);                                            
                                            cart.Quantity += item.Quantity ;
                                            bomitem.Quantity -= item.Quantity;
                                            cart.ExtendedPrice = cart.Quantity * cart.UnitPrice;
                                        }
                                         cart.UpdatedDate = DateTime.Now;
                                        _CartService.Update(cart);
                                        _CartService.Save();
                                        var a = Mapper.Map<Cart, CartViewModel>(newCart);
                                        listReturn.Add(a);
                                    }

                                }
                                _BOMItemService.Update(bomitem);
                                _BOMItemService.Save();
                                //else
                                //{
                                //    item.Quantity = 0; 
                                //    listReturn.Add(item);
                                //}
                            }
                            response = request.CreateResponse(HttpStatusCode.Created, listReturn);
                        }
                        else
                        {
                            response = request.CreateErrorResponse(HttpStatusCode.NoContent, "Nothing");
                        }
                    }
                    catch
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "insert errors");
                    }
                }

                return response;
            });
        }

    }
}