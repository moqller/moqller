﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/statistic")]
    public class StatisticController : ApiControllerBase
    {
        private IStatisticService _statisticService;
        private IOrderStatusService _orderStatusService;
        private IDeliveryStatusService _deliveryStatusService;

        public StatisticController(IErrorService errorService, IStatisticService statisticService) : base(errorService)
        {
            _statisticService = statisticService;
        }

        [Route("getrevenue")]
        [HttpGet]
        public HttpResponseMessage GetRevenueStatistic(HttpRequestMessage request, string fromDate, string toDate)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _statisticService.GetRevenueStatistic(fromDate, toDate);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getorderstatistic")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetOrderStatistic(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                string fromDate = "01/01/" + DateTime.Now.Year.ToString();
                DateTime start = DateTime.ParseExact(fromDate, "MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"));
                DateTime end = DateTime.Now;

               // var can = _orderStatusService.GetByCode("CAN");
               // var fin = _orderStatusService.GetByCode("FIN");
                //var def = _orderStatusService.GetByCode("DEF");

                //var ddef = _deliveryStatusService.GetByCode("DEF");
                //var ddone = _deliveryStatusService.GetByCode("DONE");
                //if(can == null || fin == null || fin == null || ddef == null || ddone == null)
                //{
                //    return request.CreateErrorResponse(HttpStatusCode.NotFound, " Please, feedback to Admin to check Order status and Delivery status");
                //}
               
                var model = _statisticService.GetOrderStatistics(UserId, start, end);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getordermonthstatistic")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetOrderMonthStatistic(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                DateTime end = DateTime.Now;
                //var can = _orderStatusService.GetByCode("CAN");
                //var fin = _orderStatusService.GetByCode("FIN");
                
                //if (can == null || fin == null)
                //{
                //    return request.CreateErrorResponse(HttpStatusCode.NotFound, "Can't find default Order status. Please, feedback to Admin");
                //}
                var model = _statisticService.GetOrderMonthStatistics(UserId, end.Year);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }


        [Route("getMaterialAlertDay")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetMaterialAlertDay(HttpRequestMessage request, int year, int month)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
              
                var model = _statisticService.GetMaterialAlertDay(UserId, year, month);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
    }
}