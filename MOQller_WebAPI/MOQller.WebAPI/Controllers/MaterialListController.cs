﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;
using MOQller.Web.Models;
using MOQller.Web.Models.MaterialList;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using MOQller.Data.Repositories;
using MOQller.Common.ViewModels;
using MOQller.Common;
using System.Net.Mail;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/materiallist")]
    [Authorize]
    public class MaterialListController : ApiControllerBase
    {

        private IMaterialListService _MaterialListService;
        private IMaterialService _MaterialService;
        private IShippingStatusService _ShippingStatusService;
        private IMaterialListRepository _MaterialListRespository;
        public MaterialListController(IErrorService errorService, IMaterialListService MaterialListService, IMaterialService MateriaService, IShippingStatusService ShippingStatusService, IMaterialListRepository MaterialListRespository)
            : base(errorService)
        {
            this._MaterialListService = MaterialListService;
            this._MaterialService = MateriaService;
            this._ShippingStatusService = ShippingStatusService;
            this._MaterialListRespository = MaterialListRespository;
        }



        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialListService.GetAll();

                var responseData = Mapper.Map<IEnumerable<MaterialList>, IEnumerable<MaterialListViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getreviewmaterial")]
        [HttpGet]
        public HttpResponseMessage GetReViewMaterial(HttpRequestMessage request, int page, int pageSize, string startdate, string enddate, bool description, bool manufacturer, bool tecklink)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _MaterialListRespository.GetReviewMaterialByDataEntry(startdate, enddate, description, manufacturer, tecklink);

                totalRow = model.Count();

                var query = model.Skip((page - 1) * pageSize).Take(pageSize);

                var paginationSet = new PaginationSet<ReviewMaterialByDataEntryViewModel>()
                {
                    Items = query,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialListService.GetById(id);

                var responseData = Mapper.Map<MaterialList, MaterialListViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyworkorder/{id:int}")]
        [HttpGet]
        public HttpResponseMessage getbyworkorder (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialListService.GetByWorkOrder(id);

                var responseData = Mapper.Map<IEnumerable<MaterialList>, IEnumerable<MaterialListViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var filterString = filter.ToLower().Split(' ').ToList();
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                var model = _MaterialListService.GetAll(filterString, UserId);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate);

                var responseData = Mapper.Map<IEnumerable<MaterialList>, IEnumerable<MaterialListViewModel>>(query);

                //var paginationSet = new PaginationSet<MaterialListViewModel>()
                //{
                //    Items = responseData,
                //    PageIndex = page,
                //    TotalRows = totalRow,
                //    PageSize = pageSize
                //};
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
        [Route("getlistseller")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetListPage(HttpRequestMessage request, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                var model = _MaterialListService.GetAll();
                model = model.Where(x => x.SellerId == UserId && (x.ApproveStatus == 1 || x.ApproveStatus == 0)).OrderByDescending(x => x.UploadDate); 

                totalRow = model.Count();
                var query = model.Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<MaterialList>, IEnumerable<MaterialListViewModel>>(query);

                var paginationSet = new PaginationSet<MaterialListViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getlistpagingadvance")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetListPagingAdvance(HttpRequestMessage request, int page, int pageSize, int searchField, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _MaterialListService.GetAll(filter, searchField);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate);

                var responseData = Mapper.Map<IEnumerable<MaterialList>, IEnumerable<MaterialListViewModel>>(query);

                //var paginationSet = new PaginationSet<MaterialListViewModel>()
                //{
                //    Items = responseData,
                //    PageIndex = page,
                //    TotalRows = totalRow,
                //    PageSize = pageSize
                //};
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Create(HttpRequestMessage request, List<MaterialListViewModel> MaterialListVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Dont match data type");
                }
                else
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                    var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                    
                    try
                    {
                        ProcessMaterial(MaterialListVm, UserId);
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, e);
                    }

                    response = request.CreateResponse(HttpStatusCode.Created, "insert successfully");
                }
                return response;   
            });
        }

        [Route("createbydataentry")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage CreateByDataEntry (HttpRequestMessage request, List<MaterialListViewModel> DataEntryVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, "Dont match data type");
                }
                else
                {
                    var SellerName = DataEntryVm.Where(x => x.SellerId != "").FirstOrDefault();

                    AppUser appUser = AppUserManager.FindByName(SellerName.SellerId);

                    try
                    {
                        ProcessMaterial(DataEntryVm, appUser.Id);
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, e);
                    }

                    response = request.CreateResponse(HttpStatusCode.Created, "insert successfully");
                }

                return response;
            });
        }

        private void ProcessMaterial(List<MaterialListViewModel> materialListVm, string UserId)
        {
            foreach (var item in materialListVm)
            {
                
                item.SellerId = UserId;

                var newMaterialList = new MaterialList();
                var newMaterial = new Material();

                newMaterial.PartNumber = item.PartNumber;
                newMaterial.Manufacturer = item.Manufacturer;
                newMaterial.TechLink = item.TechLink;
                newMaterial.Description = item.Description;
                newMaterial.CreatedDate = DateTime.Now;
                newMaterial.Image1 = item.ImagePartNumber;
                newMaterialList.UpdateMaterialList(item);
                newMaterialList.CreatedDate = DateTime.Now;
                newMaterialList.UploadDate = DateTime.Now;

                if (item.WorkOrderId > 0)
                {
                    newMaterialList.WorkOrderId = item.WorkOrderId;
                }

                var MaterialByPartNumber = _MaterialService.GetByPartNumber(item.PartNumber);

                // Nếu tồn tai PartNumber
                if (MaterialByPartNumber != null)
                {
                    
                        newMaterialList.MaterialId = MaterialByPartNumber.Id;
                        newMaterialList.ApproveStatus = 1;

                        // Lấy danh sách các Materialist theo PartNumber
                        var materiallist = _MaterialListService.GetByMaterialId(MaterialByPartNumber.Id);

                        // nếu k tồn tại Materialist cùng người bán, cùng status, cùng duedate
                        if (materiallist == null)
                        {
                            _MaterialListService.Add(newMaterialList);
                        }
                        else
                        {
                            // Kiêm tra Shipping
                            var shippingStatus = _ShippingStatusService.GetById(item.ShippingStatusId);

                            var shippingStatusID = item.ShippingStatusId;


                            if (shippingStatus.Code == "STOCK")
                            {
                                // Không check shipping vì nó có hàng rồi và có thẻ hàng cũ trong danh mục chưa đc cập nhập và dueddate < ngày hiện tại. 
                                //Có thể phải check status ở đây nếu như status unconfirm?? and hàng k tới, bị cancelled???
                                materiallist = materiallist.Where(m => m.SellerId == item.SellerId && m.MOQ == item.MOQ && m.Price == item.Price && m.DueDate.Date < DateTime.Now.Date);

                            }
                            else
                            {
                                materiallist = materiallist.Where(x => x.SellerId == item.SellerId && x.ShippingStatusId == item.ShippingStatusId && x.MOQ == item.MOQ && x.Price == item.Price && x.DueDate.Date == Convert.ToDateTime(item.DueDate).Date);
                            }

                            var abss = materiallist;

                            // nếu k tồn tại materillist cùng người bán, cùng status, cùng duedate
                            if (materiallist.Count() == 0)
                            {
                                _MaterialListService.Add(newMaterialList);
                            }
                            else
                            {
                                // Trường hợp có 2 record ở trong kho thì lấy record đầu tiên
                                MaterialList MaterialListUpdate = materiallist.FirstOrDefault();
                                MaterialListUpdate.AvailableQty += item.AvailableQty;
                                MaterialListUpdate.UpdatedDate = DateTime.Now;

                                if (item.WorkOrderId > 0)
                                {
                                    MaterialListUpdate.WorkOrderId = item.WorkOrderId;
                                }
                                _MaterialListService.Update(MaterialListUpdate);
                            }
                        }
                    
                }
                else
                {

                    //if ( string.IsNullOrEmpty(item.Manufacturer) || string.IsNullOrEmpty(item.Description) || string.IsNullOrEmpty(item.TechLink))
                    if (string.IsNullOrEmpty(item.Manufacturer) || string.IsNullOrEmpty(item.Description))
                    {
                        newMaterialList.ApproveStatus = 2;
                        newMaterialList.PartNumber = item.PartNumber;
                        newMaterialList.Manufacturer = item.Manufacturer;
                        newMaterialList.TechLink = item.TechLink;
                        newMaterialList.Description = item.Description;
                        _MaterialListService.Add(newMaterialList);
                    }
                    else
                    {
                        newMaterialList.Material = newMaterial;
                        newMaterialList.ApproveStatus = 1;
                        var listTemp = new List<MaterialList>();
                        listTemp.Add(newMaterialList);
                        newMaterial.NewMaterialList = listTemp;

                        _MaterialService.Add(newMaterial);
                    }
                }
            }
            _MaterialService.Save();
            _MaterialListService.Save();
        }

        [Route("getinstockbyorderwork")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage getinstockbyorderwork (HttpRequestMessage request, MaterialListViewModel dataEntryVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    AppUser appUser = AppUserManager.FindByName(dataEntryVm.SellerId);

                    var dbMaterial = _MaterialService.GetByPartNumber(dataEntryVm.PartNumber);

                    if (dbMaterial != null)
                    {
                        // Nếu tồn tại Manufaturer
                        var man = string.IsNullOrEmpty(dbMaterial.Manufacturer) ? "" : dbMaterial.Manufacturer.ToLower();

                        if (man == dbMaterial.Manufacturer.ToLower() || string.IsNullOrEmpty(dbMaterial.Manufacturer))
                        {
                            var dbMaterialList = _MaterialListService.GetInstock(dbMaterial.Id, dataEntryVm.MOQ, dataEntryVm.Price, appUser.Id);
                            if (dbMaterialList != null)
                            {
                                response = request.CreateResponse(HttpStatusCode.Created, dbMaterialList.AvailableQty);
                            }
                            else
                            {
                                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                            }
                        }
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                    }

                }
                return response;
            });
        }

        [Route("getinstock")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Update(HttpRequestMessage request, UpLoadBom uploadbom)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                    var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                    var dbMaterialList = _MaterialListRespository.GetInstock(uploadbom, UserId);
                    response = request.CreateResponse(HttpStatusCode.Created, dbMaterialList);
                }

                return response;
            });
        }

        [Route("getcurrentinventory")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetCurrentInventory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                    var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                    var unstatus = _ShippingStatusService.GetByCode("UNC");
                    var cfstatus = _ShippingStatusService.GetByCode("COF");
                    if (unstatus == null || cfstatus == null)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.NotFound, "Can't find default Status. Please feedback to Admin!");
                    }
                    else
                    {
                        var dbMaterialList = _MaterialListService.GetInventory(UserId);
                        response = request.CreateResponse(HttpStatusCode.Created, dbMaterialList);
                    }
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Authorize]
        public HttpResponseMessage Update(HttpRequestMessage request, MaterialListViewModel MaterialListVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var shippingStatus = _ShippingStatusService.GetByCode("CAN");
                    if(shippingStatus == null)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.NoContent, "Can't find shipping status default. Please, feedback to addmin");
                    }
                    
                    var dbMaterialList = _MaterialListService.GetById(MaterialListVm.Id);

                    dbMaterialList.UpdateMaterialList(MaterialListVm);
                    dbMaterialList.UpdatedDate = DateTime.Now;
                    if (MaterialListVm.ShippingStatusId == shippingStatus.Id)
                    {
                        dbMaterialList.ApproveStatus = 0;
                    }
					else
					{
						dbMaterialList.ApproveStatus = 1;
					}
                    _MaterialListService.Update(dbMaterialList);
                    try
                    {
                        _MaterialListService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<MaterialList, MaterialListViewModel>(dbMaterialList);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("UpdateByEntryData")]
        [HttpPut]
        [Authorize]
        public HttpResponseMessage UpdateByEntryData(HttpRequestMessage request, MaterialListViewModel MaterialListVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                    var UserId = ClaimsPrincipal.Current.Identity.GetUserId();


                    var dbMaterialList = _MaterialListService.GetById(MaterialListVm.Id);

                    var PartNumberNew = _MaterialService.GetByPartNumber(MaterialListVm.PartNumber.Trim());

                    if (dbMaterialList != null)
                    {
                        if (PartNumberNew == null)
                        {
                            // Insert vào Material
                            var newMaterial = new Material();
                            newMaterial.PartNumber = MaterialListVm.PartNumber.Trim();
                            newMaterial.Description = MaterialListVm.Description.Trim();
                            newMaterial.Manufacturer = MaterialListVm.Manufacturer.Trim();
                            newMaterial.TechLink = MaterialListVm.TechLink.Trim();
                            newMaterial.CreatedBy = newMaterial.UpdatedBy = UserId;
                            newMaterial.CreatedDate = DateTime.Now;

                            _MaterialService.Add(newMaterial);
                            _MaterialService.Save();

                            var List_MaterialList = _MaterialListService.GetByPartNumber(MaterialListVm.PartNumber.Trim());

                            foreach (var item in List_MaterialList)
                            {
                                item.MaterialId = newMaterial.Id;
                                item.TechLink = item.Manufacturer = item.PartNumber = item.Manufacturer = string.Empty;
                                item.UpdatedDate = DateTime.Now;
                                item.ApproveStatus = 1;
                                _MaterialListService.Update(item);
                            }
                            try
                            {
                                _MaterialListService.Save();
                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                            ve.PropertyName, ve.ErrorMessage);
                                    }
                                }
                                throw;
                            }
                            response = request.CreateResponse(HttpStatusCode.Created, "success");
                        }
                        else
                        {
                            response = request.CreateResponse(HttpStatusCode.Conflict, "PartNumber exist");
                        }
                    }
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldMaterialList = _MaterialListService.Delete(id);
                    _MaterialListService.Save();

                    var responseData = Mapper.Map<MaterialList, MaterialListViewModel>(oldMaterialList);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listMaterialList = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listMaterialList)
                    {
                        _MaterialListService.Delete(item);
                    }

                    _MaterialListService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listMaterialList.Count);
                }

                return response;
            });
        }

        

        [Route("sendmail")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage SendMail(HttpRequestMessage request, string toEmail, string userName)
        {
            try
            {
                var host = ConfigHelper.GetByKey("SMTPHost");
                var port = int.Parse(ConfigHelper.GetByKey("SMTPPort"));
                var fromEmail = ConfigHelper.GetByKey("FromEmailAddress");
                var password = ConfigHelper.GetByKey("FromEmailPassword");
                var fromName = ConfigHelper.GetByKey("FromName");
                var subject = "Your materials have been uploaded on MOQller";
                var smtpClient = new SmtpClient(host, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = false,
                    Timeout = 100000
                };

                string content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Templates/uploadseller.html"));
                content = content.Replace("{{UserName}}", userName);
                
                var mail = new MailMessage
                {
                    Body = content,
                    Subject = subject,
                    From = new MailAddress(fromEmail, fromName)
                };

                mail.To.Add(new MailAddress(toEmail));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                smtpClient.Send(mail);

                var response = request.CreateResponse(HttpStatusCode.OK, "Send Sccucsess");
                return response;
            }
            catch (SmtpException smex)
            {

                var response = request.CreateResponse(HttpStatusCode.OK, "Send Failed");
                return response;
            }

        }

    }
}