﻿using AutoMapper;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MOQller.Common;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Web.Models;
using MOQller.Web.Providers;
using MOQller.Web.Models.Country;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using MOQller.Data.Repositories;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/Order")]
    public class OrderController : ApiControllerBase
    {
        private IOrderService _orderService;
        private ICountryService _CountryService;
        private IOrderRepository _orderRepository;
        private ICartService _cartService;
        private IMaterialListService _materialListService;
        private IOrderStatusService _orderStatusService;
        private IDeliveryStatusService _deliveryStatusService;
        private IShippingMethodService _shippingMethodService;
        private IPaymentStatusService _paymentStatusService;

        public OrderController(IOrderService orderService, 
            IErrorService errorService,
            ICountryService countryService,
            ICartService cartService,
            IMaterialListService materialListService,
            IOrderStatusService orderStatusService, 
            IDeliveryStatusService deliveryStatusService,
            IShippingMethodService shippingMethodService,
            IPaymentStatusService paymentStatusService,
            IOrderRepository orderRespository) : base(errorService)
        {
            _orderService = orderService;
            _CountryService = countryService;
            _cartService = cartService;
            _materialListService = materialListService;
            _orderStatusService = orderStatusService;
            _deliveryStatusService = deliveryStatusService;
            _shippingMethodService = shippingMethodService;
            _paymentStatusService = paymentStatusService;
            _orderRepository = orderRespository;
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "ORDER")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, string startDate, string endDate,
            string customerName, string paymentStatus, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _orderService.GetList(startDate, endDate, customerName, paymentStatus, page, pageSize, out totalRow);
                List<OrderViewModel> modelVm = Mapper.Map<List<Order>, List<OrderViewModel>>(model);

                PaginationSet<OrderViewModel> pagedSet = new PaginationSet<OrderViewModel>()
                {
                    PageIndex = page,
                    PageSize = pageSize,
                    TotalRows = totalRow,
                    Items = modelVm,
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyorderpaging")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getbyorderpaging (HttpRequestMessage request, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                if (_orderStatusService.GetByCode("DEF") == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from order status please contact to admin");
                }
                else
                { 
                    if (_deliveryStatusService.GetByCode("DEF") == null)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from delivery status please contact to admin");
                    }
                    else
                    {
                        ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                        var UserId = ClaimsPrincipal.Current.Identity.GetUserId();

                       

                        int totalRow = 0;
                        var model = _orderService.GetByUser(UserId);

                        foreach (var item in model)
                        {
                            item.ShippingMethod = _shippingMethodService.GetById(item.ShippingMethodId);
                        }

                        totalRow = model.Count();
                        var query = model.OrderByDescending(x => x.OrderDate).Skip((page - 1) * pageSize).Take(pageSize);

                        var responseData = Mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(query);

                        var paginationSet = new PaginationSet<OrderViewModel>()
                        {
                            Items = responseData,
                            PageIndex = page,
                            TotalRows = totalRow,
                            PageSize = pageSize
                        };
                        var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                        return response;
                    }
                }
            });
        }

        [Route("getbysellerorderpaging")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getbysellerorderpaging (HttpRequestMessage request, int page, int pageSize, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                if (_orderStatusService.GetByCode("DEF") == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from order status please contact to admin");
                }
                else
                {
                    if (_deliveryStatusService.GetByCode("DEF") == null)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from delivery status please contact to admin");
                    }
                    else
                    {
                        ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                        var UserId = ClaimsPrincipal.Current.Identity.GetUserId();



                        int totalRow = 0;
                        IEnumerable<OrderDetail> model = Enumerable.Empty<OrderDetail>();
                        if (string.IsNullOrEmpty(filter))
                        {
                             model = _orderService.GeSellerOrderDetails(UserId);
                        }
                        else
                        {
                            model = _orderService.FilterSellerOrderDetail(UserId, filter);
                        }

                        totalRow = model.Count();

                        var query = model.OrderByDescending(x => x.Order.OrderNumber).Skip((page - 1) * pageSize).Take(pageSize);

                        var responseData = Mapper.Map<IEnumerable<OrderDetail>, IEnumerable<OrderManagerSellerViewModel>>(query);

                        var paginationSet = new PaginationSet<OrderManagerSellerViewModel>()
                        {
                            Items = responseData,
                            PageIndex = page,
                            TotalRows = totalRow,
                            PageSize = pageSize
                        };

                        var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                        return response;
                    }
                }
            });
        }

        [Route("getorderone")]
        [HttpGet]
        [Authorize]
        //[Permission(Action = "Read", Function = "ORDER")]
        public HttpResponseMessage GetOrderOne(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();

                int totalRow = 0;
                var model = _orderService.GetOrderOne(UserId, id);

                totalRow = model.Count();
               

                var responseData = Mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(model);

                var paginationSet = new PaginationSet<OrderViewModel>()
                {
                    Items = responseData,
                    PageIndex = 1,
                    TotalRows = totalRow,
                    PageSize = 20
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("getorderdetail")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetOrderDetail(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {

                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

                var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                
                if (id > 0)
                {
                    var model = _orderService.GetOrderDetailsNew(UserId, id);

                    var responseData = Mapper.Map<IEnumerable<OrderDetail>, IEnumerable<OrderDetailViewModel>>(model);

                    return request.CreateResponse(HttpStatusCode.OK, responseData);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "");
                }
            });
        }

        //[Route("getsellerorderdetail")]
        //[HttpGet]
        //[Authorize]
        //public HttpResponseMessage GetSellerOrderDetail(HttpRequestMessage request, int id)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {

        //        ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

        //        var UserId = ClaimsPrincipal.Current.Identity.GetUserId();
                
        //        if (id > 0)
        //        {
        //            var model = _orderService.GeSellerOrderDetails(UserId, id);

        //            var responseData = Mapper.Map<IEnumerable<OrderDetail>, IEnumerable<OrderDetailViewModel>>(model);

        //            return request.CreateResponse(HttpStatusCode.OK, responseData);
        //        }
        //        else
        //        {
        //            return request.CreateResponse(HttpStatusCode.BadRequest, "");
        //        }
        //    });
        //}


        [Route("detail/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "ORDER")]
        //[Authorize(Roles = "ViewUser")]
        public HttpResponseMessage Details(HttpRequestMessage request, int id)
        {
            if (id == 0)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var order = _orderService.GetDetail(id);
            if (order == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var orderVm = Mapper.Map<Order, OrderViewModel>(order);

                return request.CreateResponse(HttpStatusCode.OK, orderVm);
            }
        }

        [Route("getalldetails/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "ORDER")]
        //[Authorize(Roles = "ViewUser")]
        public HttpResponseMessage GetOrderDetails(HttpRequestMessage request, int id)
        {
            if (id == 0)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var details = _orderService.GetOrderDetails(id);
            if (details == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var detailVms = Mapper.Map<IEnumerable<OrderDetail>, IEnumerable<OrderDetailViewModel>>(details);

                return request.CreateResponse(HttpStatusCode.OK, detailVms);
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        //[Permission(Action = "Create", Function = "USER")]
        public HttpResponseMessage Create(HttpRequestMessage request, OrderViewModel orderVm)
        {
            int SellerFeeRateSettings = 10;
            int BuyerFeeRateSettings = 10;

            if (ModelState.IsValid)
            {
                var newOrder = new Order();
                orderVm.CreatedBy = orderVm.UpdatedBy = orderVm.BuyerId;
                orderVm.OrderDate = DateTime.Now;
                var paymenstatus = _paymentStatusService.GetByCode("DEF");

                if (paymenstatus == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from payment status please contact to admin");
                }

                orderVm.PaymentStatusId = paymenstatus.Id;
                newOrder.UpdateOrder(orderVm);
                // Lay orderstatus va delivery mac dinh
                var orderstatus = _orderStatusService.GetByCode("DEF");
                var delivery = _deliveryStatusService.GetByCode("DEF");
                if (orderstatus == null)
                {
                    return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from order status please contact to admin");
                }
                else
                {
                    if (delivery == null)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Gone, "Cannot find default from delivery status please contact to admin");
                    }
                    else
                    {
                        try
                        {
                            
                            var listOrderDetails = new List<OrderDetail>();
                            var listEmail = new List<AppUser>();
                            foreach (var item in orderVm.OrderDetails)
                            {
                                listOrderDetails.Add(new OrderDetail()
                                {
                                    MaterialListId = item.MaterialListId,
                                    Quantity = item.Quantity,
                                    UnitPrice = item.UnitPrice,
                                    ExtendedPrice = item.Quantity * item.UnitPrice,
                                    SellerFee = ((item.Quantity * item.UnitPrice) * SellerFeeRateSettings) / 100,
                                    BuyerFee = ((item.Quantity * item.UnitPrice) * BuyerFeeRateSettings) / 100,
                                    SellerFeeRate = SellerFeeRateSettings,
                                    BuyerFeeRate = BuyerFeeRateSettings,
                                    BuyerTotal = item.Quantity + item.UnitPrice + 0,
                                    DeliveryStatusId = delivery.Id,
                                    OrderStatusId = orderstatus.Id
                                });
                                // check quantity số lượng nhỏ hơn trả lại giá trị false
                                if (_materialListService.CheckQuantityMaterialList(item.MaterialListId, item.Quantity))
                                {
                                    return request.CreateErrorResponse(HttpStatusCode.Conflict, "Quantity partnumber '" + item.MaterialList.Material.PartNumber + "' in stock only '" + item.MaterialList.AvailableQty + "'");
                                }
                            }

                            if (listOrderDetails != null)
                            {
                                newOrder.NewOrderDetails = listOrderDetails;
                                // Thêm mới vào order và order detail
                                var OrderCreated = _orderService.Create(newOrder);
                                // Tao OderNumber
                                var OrderNumber_Id = "0000" + OrderCreated.Id.ToString();
                                string ordernumber = ((DateTime.Now.Year) % 100).ToString()+ GetIso8601WeekOfYear(DateTime.Now).ToString() +  OrderNumber_Id.Substring(OrderNumber_Id.Length - 4);

                                var result = _orderService.UpdateOrderNumber(OrderCreated.Id, ordernumber);

                                if (result != null)
                                {
                                    // Sửa số lượng trong order
                                    foreach (var item in result.NewOrderDetails)
                                    {
                                        var dbMaterialList = _materialListService.GetById(item.MaterialListId);
                                        var sellermail = _orderService.GetSellerByMaterialListId(item.MaterialListId);
                                        if(!listEmail.Contains(sellermail))
                                            listEmail.Add(sellermail);

                                        dbMaterialList.AvailableQty -= item.Quantity;
                                        dbMaterialList.UpdatedDate = DateTime.Now;
                                        _materialListService.Update(dbMaterialList);
                                    }

                                    _materialListService.Save();
                                    _cartService.DeleteByUserId(result.BuyerId);
                                    _cartService.Save();
                                }

                                var model = Mapper.Map<Order, OrderViewModel>(result);
                                var buyerMail = _orderRepository.GetUserById(orderVm.BuyerId);
                                string sellerId = listEmail.Count() > 1 ? listEmail[0].FirstName + "..." : listEmail[0].FirstName;
                                string buyerId = buyerMail.FirstName;
                                //send email to seller

                                foreach (var email in listEmail)
                                 {
                                    try
                                    {
                                        SendMail(email.Email, email.FirstName, true, sellerId, buyerId);
                                    }
                                    catch (SmtpException sex)
                                    {
                                        return request.CreateErrorResponse(HttpStatusCode.ServiceUnavailable, sex.Message);
                                    }
                                 }
                                //send email to buyer
                                
                                try
                                {
                                    SendMail(buyerMail.Email, buyerMail.FirstName, true, sellerId, buyerId);
                                }
                                catch (SmtpException sex)
                                {
                                    return request.CreateErrorResponse(HttpStatusCode.ServiceUnavailable, sex.Message);
                                }

                                return request.CreateResponse(HttpStatusCode.OK, sellerId);
                            }
                            else
                            {
                                return request.CreateErrorResponse(HttpStatusCode.LengthRequired, "Null list of cart");
                            }
                        }
                        catch (Exception ex)
                        {
                            return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                        }
                    }
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //[HttpPut]
        //[Route("updateStatus")]
        //[Authorize]
        //[Permission(Action = "Update", Function = "ORDER")]
        //public HttpResponseMessage UpdateStatus(HttpRequestMessage request, int orderId)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _orderService.UpdateStatus(orderId);
        //            return request.CreateResponse(HttpStatusCode.OK, orderId);
        //        }
        //        catch (Exception dex)
        //        {
        //            return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
        //        }
        //    }
        //    else
        //    {
        //        return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }
        //}

        [HttpPut]
        [Route("UpdateDeliveredStatus")]
        [Authorize]
        //[Permission(Action = "Update", Function = "ORDER")]
        public HttpResponseMessage UpdateDeliveredStatus(HttpRequestMessage request, Int64 OrderDetailId, int DeliveryStatusId)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var delivery = _deliveryStatusService.GetById(DeliveryStatusId);

                    var orderdetail = _orderService.GetOrderDetailById(OrderDetailId);

                    if (delivery != null)
                    {
                        switch (delivery.Code)
                        {
                            case "DEF": // Không làm gì vì nó là DEF sẵn rồi
                                break;

                            case "OTW":
                                if (orderdetail.DeliveryStatuses.Code == "DEF")
                                {
                                    _orderService.UpdateDeliveredStatus(OrderDetailId, delivery.Id);
                                    _orderService.Save();
                                }
                                break;
                            case "DEL":
                                if (orderdetail.DeliveryStatuses.Code == "DEF" | orderdetail.DeliveryStatuses.Code == "OTW")
                                {
                                    var statusOder = _orderStatusService.GetAll().Where(so => so.Code == "FIN").SingleOrDefault();

                                    orderdetail.DeliveryStatusId = delivery.Id;
                                    
                                    if (statusOder != null)
                                    {
                                        orderdetail.OrderStatusId = statusOder.Id;
                                    }

                                    _orderService.UpdateOderDetailStatus(orderdetail);
                                    _orderService.Save();
                                }
                                break;
                            default:
                                break;
                        }

                        return request.CreateResponse(HttpStatusCode.OK, OrderDetailId);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    }
                }
                catch (Exception dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("CancelOrderDetail")]
        [Authorize]
        public HttpResponseMessage CancelOrderDetail(HttpRequestMessage request, Int64 Id)
        {
            var OldDetail = _orderService.GetOrderDetailById(Id);
            if (OldDetail.DeliveryStatuses.Code == "DEF" && Id > 0)
            {
                var StatusOrder = _orderStatusService.GetByCode("CAN");
                if (StatusOrder != null)
                {
                    if (OldDetail.OrderStatusId != StatusOrder.Id)
                    {
                        OldDetail.OrderStatusId = StatusOrder.Id;
                        _orderService.UpdateOderDetailStatus(OldDetail);

                        // Update so luong lai vao kho
                        var material = _materialListService.GetById(OldDetail.MaterialListId);
                        material.AvailableQty += OldDetail.Quantity;
                        _materialListService.Update(material);

                        _orderService.Save();
                        return request.CreateResponse(HttpStatusCode.OK, StatusOrder.Id);
                    }
                    else
                    {
                        return request.CreateResponse(HttpStatusCode.BadRequest, "Material has cancelled. Please check agian.");
                    }
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, "The status 'cancelled' is not define. Please contact to admin");
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.Conflict, "Cannot cancel");
            }
        }

        [Route("exportExcel/{id}")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage ExportOrder(HttpRequestMessage request, int id)
        {
            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = GenerateOrder(id);
            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error export");

            }

            // If something fails or somebody calls invalid URI, throw error.
        }

        #region send email
        public void SendMail(string toEmail, string userName, bool seller, string sellerId, string buyerId)
        {
            string subject = "";
            string content = "";
            var host = ConfigHelper.GetByKey("SMTPHost");
            var port = int.Parse(ConfigHelper.GetByKey("SMTPPort"));
            var fromEmail = ConfigHelper.GetByKey("FromEmailAddress");
            var password = ConfigHelper.GetByKey("FromEmailPassword");
            var fromName = ConfigHelper.GetByKey("FromName");
            if (seller)
            {
                subject = "You have a new paid order from buyerID";
                content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Templates/sellerAfterPaid.html"));
                content = content.Replace("{{UserName}}", userName);
                content = content.Replace("{{Content}}", buyerId);
            }
            else
            {
                subject = "our new order has submitted to sellerID";
                content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Templates/buyerAfterPaid.html"));
                content = content.Replace("{{Content}}", sellerId);
            }
                var smtpClient = new SmtpClient(host, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = false,
                    Timeout = 100000
                };

                 
                content = content.Replace("{{UserName}}", userName);
                
                var mail = new MailMessage
                {
                    Body = content,
                    Subject = subject,
                    From = new MailAddress(fromEmail, fromName)
                };

                mail.To.Add(new MailAddress(toEmail));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                smtpClient.Send(mail);

        }
        #endregion

        #region Export to Excel
        private string GenerateOrder(int orderId)
        {
            var folderReport = ConfigHelper.GetByKey("ReportFolder");
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            // Template File
            string templateDocument =
                    HttpContext.Current.Server.MapPath("~/Templates/OrderTemplate.xlsx");
            string documentName = string.Format("Order-{0}-{1}.xlsx", orderId,DateTime.Now.ToString("yyyyMMddhhmmsss"));
            string fullPath = Path.Combine(filePath, documentName);
            // Results Output
            MemoryStream output = new MemoryStream();
            try
            {
                // Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    // Create Excel EPPlus Package based on template stream
                    using (ExcelPackage package = new ExcelPackage(templateDocumentStream))
                    {
                        // Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = package.Workbook.Worksheets["TEDUOrder"];
                        // Data Acces, load order header data.
                        var order = _orderService.GetDetail(orderId);

                        // Insert customer data into template
                        //sheet.Cells[4, 1].Value = "Tên khách hàng: " + order.CustomerName;
                        //sheet.Cells[5, 1].Value = "Địa chỉ: " + order.CustomerAddress;
                        //sheet.Cells[6, 1].Value = "Điện thoại: " + order.CustomerMobile;
                        // Start Row for Detail Rows
                        int rowIndex = 9;

                        // load order details
                        var orderDetails = _orderService.GetOrderDetails(orderId);
                        int count = 1;
                        foreach (var orderDetail in orderDetails)
                        {
                            // Cell 1, Carton Count
                            sheet.Cells[rowIndex, 1].Value = count.ToString();
                            // Cell 2, Order Number (Outline around columns 2-7 make it look like 1 column)
                            //sheet.Cells[rowIndex, 2].Value = orderDetail.Product.Name;
                            // Cell 8, Weight in LBS (convert KG to LBS, and rounding to whole number)
                            sheet.Cells[rowIndex, 3].Value = orderDetail.Quantity.ToString();

                            //sheet.Cells[rowIndex, 4].Value = orderDetail.Price.ToString("N0");
                            //sheet.Cells[rowIndex, 5].Value = (orderDetail.Price * orderDetail.Quantity).ToString("N0");
                            // Increment Row Counter
                            rowIndex++;
                            count++;
                        }
                        double total = (double)(orderDetails.Sum(x => x.Quantity * x.BuyerTotal));
                        sheet.Cells[24, 5].Value = total.ToString("N0");

                        var numberWord = "Thành tiền (viết bằng chữ): " + NumberHelper.ToString(total);
                        sheet.Cells[26, 1].Value = numberWord;
                        if (order.CreatedDate.HasValue)
                        {
                            var date = order.CreatedDate.Value;
                            sheet.Cells[28, 3].Value = "Ngày " + date.Day + " tháng " + date.Month + " năm " + date.Year;

                        }
                        package.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }
        public static int GetIso8601WeekOfYear (DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        #endregion
    }
}