﻿using AutoMapper;
using MOQller.Web.Models.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/state")]
    [Authorize]
    public class StateController : ApiControllerBase
    {

        private IStateService _StateService;

        public StateController(IErrorService errorService, IStateService StateService)
            : base(errorService)
        {
            this._StateService = StateService;
        }

        [Route("getall")]
        [HttpGet]

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _StateService.GetAll();

                var responseData = Mapper.Map<IEnumerable<State>, IEnumerable<StateViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _StateService.GetById(id);

                var responseData = Mapper.Map<State, StateViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int? countryId, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;
                var model = _StateService.GetAll(countryId,filter);

                totalRow = model.Count();
                var query = model.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<State>, IEnumerable<StateViewModel>>(query);

                var paginationSet = new PaginationSet<StateViewModel>()
                {
                    Items = responseData,
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize
                };
                var response = request.CreateResponse(HttpStatusCode.OK, paginationSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, StateViewModel StateVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_StateService.CheckCode(0, StateVm.Code))
                    {
                        var newState = new State();
                        newState.UpdateState(StateVm);
                        newState.CreatedDate = DateTime.Now;
                        _StateService.Add(newState);
                        _StateService.Save();

                        var responseData = Mapper.Map<State, StateViewModel>(newState);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                        
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");

                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, StateViewModel StateVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    if (_StateService.CheckCode(StateVm.Id, StateVm.Code))
                    {
                        var dbState = _StateService.GetById(StateVm.Id);

                        dbState.UpdateState(StateVm);
                        dbState.UpdatedDate = DateTime.Now;

                        _StateService.Update(dbState);
                        try
                        {
                            _StateService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<State, StateViewModel>(dbState);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Code exists");
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldState = _StateService.Delete(id);
                    _StateService.Save();

                    var responseData = Mapper.Map<State, StateViewModel>(oldState);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [AllowAnonymous]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listState = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listState)
                    {
                        _StateService.Delete(item);
                    }

                    _StateService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listState.Count);
                }

                return response;
            });
        }

    }
}