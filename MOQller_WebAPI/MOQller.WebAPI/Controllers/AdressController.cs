﻿using AutoMapper;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Infrastructure.Extensions;
using MOQller.Web.Models.Order;
using MOQller.Web.Providers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/address")]
    [Authorize]
    public class AdressController : ApiControllerBase
    {
        private IAddressService _AddressService;


        public AdressController (IAddressService AddressService, IErrorService errorService) : base(errorService)
        {
            _AddressService = AddressService;
        }

        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Create (HttpRequestMessage request, AddressViewModel AddresssInfoVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {

                    try
                    {
                        var newAddresssInfoVm = new Address();
                        newAddresssInfoVm.UpdateAddress(AddresssInfoVm);
                        _AddressService.Add(newAddresssInfoVm);
                        _AddressService.Save();

                        var responseData = Mapper.Map<Address, AddressViewModel>(newAddresssInfoVm);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (Exception ex)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                    }
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                return response;
            });
        }


        [Route("detail/{id}")]
        [HttpGet]

        public HttpResponseMessage GetOrderDetail (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {

                var model = _AddressService.GetById(id);

                var responseData = Mapper.Map<Address, AddressViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll (HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _AddressService.GetAll();

                var responseData = Mapper.Map<IEnumerable<Address>, IEnumerable<AddressViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getbyUsrId/{UsrID}")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage GetbyUsrId (HttpRequestMessage request, string UsrId)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _AddressService.GetByUsrId(UsrId);

                var responseData = Mapper.Map<IEnumerable<Address>, IEnumerable<AddressViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update (HttpRequestMessage request, AddressViewModel AddressVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbAddress = _AddressService.GetById(AddressVm.Id);
                    if (dbAddress != null)
                    {
                        

                        dbAddress.UpdateAddress(AddressVm);

                        _AddressService.Update(dbAddress);
                        try
                        {
                            _AddressService.Save();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        var responseData = Mapper.Map<Address, AddressViewModel>(dbAddress);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    else
                    {
                        return request.CreateErrorResponse(HttpStatusCode.Conflict, "Cannot find Id");
                    }

                }

                return response;
            });
        }

    }
}