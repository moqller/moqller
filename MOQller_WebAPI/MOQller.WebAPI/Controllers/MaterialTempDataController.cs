﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Model.Models;
using System.Data.Entity.Validation;
using MOQller.Web.Infrastructure.Extensions;
using System.Web.Script.Serialization;
using MOQller.Web.Models;
using MOQller.Web.Models.MaterialTempData;
using MOQller.Common;
using System.Net.Mail;

namespace MOQller.Web.Controllers
{
    [RoutePrefix("api/materialTempData")]
    [Authorize]
    public class MaterialTempDataController : ApiControllerBase
    {

        private IMaterialTempDataService _MaterialTempDataService;

        public MaterialTempDataController(IErrorService errorService, IMaterialTempDataService MaterialTempDataService)
            : base(errorService)
        {
            this._MaterialTempDataService = MaterialTempDataService;
        }



        [Route("getwork")]
        [HttpGet]

        public HttpResponseMessage GetWork(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialTempDataService.GetWork();

                var responseData = Mapper.Map<IEnumerable<MaterialTempData>, IEnumerable<MaterialTempDataViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("detail/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _MaterialTempDataService.GetById(id);

                var responseData = Mapper.Map<MaterialTempData, DetailTempDataViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, MaterialTempDataViewModel MaterialVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {

                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    //var httpRequest = HttpContext.Current.Request;
                    //byte[] re = httpRequest.BinaryRead(httpRequest.ContentLength);
                    var newMaterial = new MaterialTempData();
                    newMaterial.UpdateMaterialTempData(MaterialVm);
                    //newMaterial.SellerId = userid;
                    newMaterial.CreatedDate = DateTime.Now;
                    newMaterial.UploadDate = DateTime.Now;
                    newMaterial.Status = 0;

                    _MaterialTempDataService.Add(newMaterial);
                    _MaterialTempDataService.Save();

                    //var responseData = Mapper.Map<MaterialTempData, MaterialTempDataViewModel>(newMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, "insert success" );
                }

                return response;
            });
        }

        [Route("completeorderwork/{id:int}")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage completeorderwork (HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (id < 1)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbMaterial = _MaterialTempDataService.GetById(id);

                    dbMaterial.Status = 1;

                    _MaterialTempDataService.Update(dbMaterial);

                    try
                    {
                        _MaterialTempDataService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<MaterialTempData, MaterialTempDataViewModel>(dbMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, MaterialTempDataViewModel MaterialVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbMaterial = _MaterialTempDataService.GetById(MaterialVm.Id);

                    dbMaterial.UpdateMaterialTempData(MaterialVm);
                    dbMaterial.UpdatedDate = DateTime.Now;

                    _MaterialTempDataService.Update(dbMaterial);
                    try
                    {
                        _MaterialTempDataService.Save();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    var responseData = Mapper.Map<MaterialTempData, MaterialTempDataViewModel>(dbMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);


                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldMaterial = _MaterialTempDataService.Delete(id);
                    _MaterialTempDataService.Save();

                    var responseData = Mapper.Map<MaterialTempData, MaterialTempDataViewModel>(oldMaterial);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string checkedProductCategories)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listMaterial = new JavaScriptSerializer().Deserialize<List<int>>(checkedProductCategories);
                    foreach (var item in listMaterial)
                    {
                        _MaterialTempDataService.Delete(item);
                    }

                    _MaterialTempDataService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listMaterial.Count);
                }

                return response;
            });
        }

        [Route("sendmail")]
        [HttpGet]
        public HttpResponseMessage SendMail (HttpRequestMessage request, string toEmail, string userName, string Content)
        {
            try
            {
                //var host = ConfigHelper.GetByKey("SMTPHost");
                //var port = int.Parse(ConfigHelper.GetByKey("SMTPPort"));
                //var fromEmail = ConfigHelper.GetByKey("FromEmailAddress");
                //var password = ConfigHelper.GetByKey("FromEmailPassword");
                //var fromName = ConfigHelper.GetByKey("FromName");
                //var subject = ConfigHelper.GetByKey("Subject");
                //var smtpClient = new SmtpClient(host, port)
                //{
                //    UseDefaultCredentials = false,
                //    Credentials = new System.Net.NetworkCredential(fromEmail, password),
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    EnableSsl = false,
                //    Timeout = 100000
                //};

                string content = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Templates/workorder.html"));
                content = content.Replace("{{UserName}}", userName);
                content = content.Replace("{{Content}}", Content);
                //var mail = new MailMessage
                //{
                //    Body = content,
                //    Subject = subject,
                //    From = new MailAddress(fromEmail, fromName)
                //};

                //mail.To.Add(new MailAddress(toEmail));
                //mail.BodyEncoding = System.Text.Encoding.UTF8;
                //mail.IsBodyHtml = true;
                //mail.Priority = MailPriority.High;

                //smtpClient.Send(mail);

                if (MailHelper.SendMail(toEmail, "Material", content) == true)
                {
                    return request.CreateErrorResponse(HttpStatusCode.OK, "Email reset password is send!");
                }
                else
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email reset password cannot send! Please contact to admin.");
                }

            }
            catch (SmtpException smex)
            {

                var response = request.CreateResponse(HttpStatusCode.OK, "Send Failed");
                return response;
            }

        }

    }
}