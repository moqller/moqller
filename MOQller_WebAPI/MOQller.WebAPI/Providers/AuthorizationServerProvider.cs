﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using MOQller.Model.Models;
using MOQller.Service;
using MOQller.Web.Infrastructure.Core;
using MOQller.Web.Models;
using System.Net;
using System.Net.Http;

namespace MOQller.Web.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public AuthorizationServerProvider()
        {
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            await Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            UserManager<AppUser> userManager = context.OwinContext.GetUserManager<UserManager<AppUser>>();
            AppUser user;
            try
            {
                user = await userManager.FindAsync(context.UserName, context.Password);
            }
            catch (Exception ex)
            {
                
                    // Could not retrieve the user due to error.
                    context.Rejected();
                    context.SetError("server_error", "UserName or Password is incorrect.");
                    return;
            }
            //if (user == null)
            //{
            //    try
            //    {
            //        var findUser = userManager.FindByEmail(context.UserName);
            //        user = await userManager.FindAsync(findUser.UserName, context.Password);
            //    }
            //    catch (Exception)
            //    {
            //        // Could not retrieve the user due to error.
            //        context.Rejected();
            //        context.SetError("server_error", "UserName Or Email or Password is incorrect.");

            //        return;
            //    }
            //}
            if (user != null)
            {
                if (user.Status==false)
                {
                    //Could not retrieve the user due to error.
                    context.Rejected();
                    context.SetError("not_activate", "This account has not verified yet. ");
                    
                    return;
                }
                var permissions = ServiceFactory.Get<IPermissionService>().GetByUserId(user.Id);
                var permissionViewModels = AutoMapper.Mapper.Map<ICollection<Permission>, ICollection<PermissionViewModel>>(permissions);
                var roles = userManager.GetRoles(user.Id);
                ClaimsIdentity identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ExternalBearer);
                string avatar = string.IsNullOrEmpty(user.Avatar) ? "" : user.Avatar;
                string email = string.IsNullOrEmpty(user.Email) ? "" : user.Email;
                identity.AddClaim(new Claim("fullName", user.FullName));
                identity.AddClaim(new Claim("firtName", user.FirstName));
                identity.AddClaim(new Claim("avatar", avatar));
                identity.AddClaim(new Claim("email", email));
                identity.AddClaim(new Claim("username", user.UserName));
                identity.AddClaim(new Claim("id", user.Id));
                identity.AddClaim(new Claim("roles", JsonConvert.SerializeObject(roles)));
                identity.AddClaim(new Claim("permissions", JsonConvert.SerializeObject(permissionViewModels)));
                var props = new AuthenticationProperties(new Dictionary<string, string>
                    {
                        {"fullName", user.FullName},
                        {"firtName", user.FirstName},
                        {"avatar", avatar },
                        {"email", email},
                        {"username", user.UserName},
                        {"id", user.Id},
                        {"permissions",JsonConvert.SerializeObject(permissionViewModels) },
                        {"roles",JsonConvert.SerializeObject(roles) }

                    });
                context.Validated(new AuthenticationTicket(identity, props));
            }
            else
            {
                context.Rejected();
                context.SetError("invalid_grant", "UserName or Password is invalid.");
                
            }
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }

    }
}