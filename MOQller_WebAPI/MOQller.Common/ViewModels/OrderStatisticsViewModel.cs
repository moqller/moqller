﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Common.ViewModels
{
   public class OrderStatisticsViewModel
    {
       public int? PrepareToShip { get; set; }
       public int? OnTheWay { get; set; }
       public int? Delivered { get; set; }
       public int? Cancelled { get; set; }
       public int? TotalSold { get; set; }
       public Decimal? TotalSales { get; set; }
       public Decimal? TotalFees { get; set; }
    }
}
