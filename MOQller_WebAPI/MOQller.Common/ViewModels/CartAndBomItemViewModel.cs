﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Common.ViewModels
{
    public class CartAndBomItemViewModel
    {
        public int Id { set; get; } 

        public int MaterialListId { set; get; }

        public int Quantity { set; get; }

        public Decimal UnitPrice { set; get; }

        public Decimal ExtendedPrice { set; get; }

        public int MOQ { get; set; }

        public string BuyerId { get; set; }

        public int BomItemId { get; set; }
    }
}
