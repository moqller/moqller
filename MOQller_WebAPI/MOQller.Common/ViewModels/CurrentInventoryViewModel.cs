﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Common.ViewModels
{
    public class CurrentInventoryViewModel
    {
        public int Less30Day { get; set; }
        public int From30To60Day { get; set; }
        public int From60To120Day { get; set; }
        public int To120Day { get; set; }
        public int Confirm { get; set; }
        public int Unconfirm { get; set; }
    }
}
