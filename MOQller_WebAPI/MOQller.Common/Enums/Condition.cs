﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Common.Enums
{
    public enum ConditionEnum
    {
        [Description("New")]
        New = 1,
        [Description("Used")]
        Used = 2,
        [Description("Refurbished")]
        Refurbished = 3,
        [Description("New other")]
        NewOther = 4
    }
}
