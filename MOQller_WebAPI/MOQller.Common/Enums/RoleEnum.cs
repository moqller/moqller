﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MOQller.Common.Enums
{
    public enum RoleEnum
    {
        Admin,
        DataEntry,
        Member
    }
}
