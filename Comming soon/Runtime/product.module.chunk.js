webpackJsonp(["product.module"],{

/***/ "../../../../../src/app/main/product/product.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mt-24{\n    margin-top:24px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/product/product.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Products List\n      \n    </h3>\n  </div>\n\n  <div class=\"title_right\">\n    <div class=\"col-md-4 col-sm-6 col-xs-12 form-group\">\n      <select class=\"form-control\" name=\"filterCategoryID\" [(ngModel)]=\"filterCategoryID\">\n        <option value=\"\">--Select Category--</option>\n        <option *ngFor=\"let x of productCategories\" [value]=\"x.ID\">{{x.Name}}</option>\n      </select>\n    </div>\n    <div class=\"col-md-3 col-sm-6 col-xs-12 form-group \">\n      <input type=\"text\" name=\"filter\" (keyup.enter)=\"search()\" [(ngModel)]=\"filterKeyword\" class=\"form-control\" placeholder=\"Search...\">\n    </div>\n    <div class=\"col-md-5 col-sm-6 col-xs-12 form-group\">\n      <button class=\"btn btn-primary\" type=\"button\" (click)=\"search()\">Go</button>\n      <button class=\"btn btn-default\" type=\"button\" (click)=\"reset()\">Reset</button>\n      <button class=\"btn btn-danger\" type=\"button\" (click)=\"deleteMulti()\">Delete Multi</button>\n\n    </div>\n\n  </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<div class=\"row\">\n\n  <div class=\"clearfix\"></div>\n\n  <div class=\"x_panel\">\n    <div class=\"x_content\">\n      <div class=\"x_title\">\n        <ul class=\"nav navbar-right panel_toolbox\">\n          <li>\n            <button *ngIf=\"_authenService.hasPermission('PRODUCT','create')\" class=\"btn btn-success\" (click)=\"showAdd()\">Add new</button>\n          </li>\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div *ngIf=\"products && products.length > 0\" class=\"col-md-12\">\n          <pagination [boundaryLinks]=\"true\" [itemsPerPage]=\"pageSize\" (pageChanged)=\"pageChanged($event)\" [totalItems]=\"totalRow\" [maxSize] =\"pageDisplay\"\n            [(ngModel)]=\"pageIndex\" class=\"pagination-sm\" previousText=\"&lsaquo;\" nextText=\"&rsaquo;\" firstText=\"&laquo;\" lastText=\"&raquo;\"></pagination>\n        </div>\n      <table *ngIf=\"products && products.length > 0\" class=\"table table-bordered\">\n        <thead>\n          <tr>\n            <th></th>\n            <th>Name</th>\n            <th>Category</th>\n            <th>Image</th>\n            <td>Created Date</td>\n            <td>Created By</td>\n            <th></th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let item of products\">\n            <td>\n              <div class=\"checkbox\">\n                <label>\n                  <input type=\"checkbox\" [(ngModel)]=\"item.Checked\" [name]=\"item.ID\" [ngModelOptions]=\"{standalone: true}\">\n                </label>\n              </div>\n            </td>\n            <td>{{item.Name}}</td>\n            <td>{{item.ProductCategory.Name}}</td>\n            <td>\n              <img width=\"128\" [src]=\"baseFolder + item.ThumbnailImage\" />\n            </td>\n            <td>{{item.CreatedDate | date}}</td>\n            <td>{{item.CreatedBy}}</td>\n            <td>\n              <button class=\"btn btn-default\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Image Management\" *ngIf=\"_authenService.hasPermission('PRODUCT','update')\"\n                (click)=\"showImageManage(item.ID)\">\n                <i class=\"fa fa-file-image-o\" aria-hidden=\"true\"></i>\n              </button>\n\n              <button class=\"btn btn-default\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Quantity Management\" *ngIf=\"_authenService.hasPermission('PRODUCT','update')\"\n                (click)=\"showQuantityManage(item.ID)\">\n                <i class=\"fa fa-bandcamp\" aria-hidden=\"true\"></i>\n              </button>\n\n              <button class=\"btn btn-primary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Update\" *ngIf=\"_authenService.hasPermission('PRODUCT','update')\"\n                (click)=\"showEdit(item.ID)\">\n                <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n              </button>\n              <button class=\"btn btn-danger\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\" *ngIf=\"_authenService.hasPermission('PRODUCT','delete')\"\n                (click)=\"delete(item.ID)\">\n                <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n              </button>\n            </td>\n          </tr>\n\n        </tbody>\n      </table>\n      <div *ngIf=\"products && products.length == 0\" class=\"col-md-12\">\n        <div class=\"alert alert-info\">\n          <strong>Notification!</strong> No Proucts\n        </div>\n\n      </div>\n      <div *ngIf=\"products && products.length > 0\" class=\"col-md-12\">\n        <pagination [boundaryLinks]=\"true\" [itemsPerPage]=\"pageSize\" (pageChanged)=\"pageChanged($event)\" [totalItems]=\"totalRow\" [maxSize] =\"pageDisplay\"\n          [(ngModel)]=\"pageIndex\" class=\"pagination-sm\" previousText=\"&lsaquo;\" nextText=\"&rsaquo;\" firstText=\"&laquo;\" lastText=\"&raquo;\"></pagination>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n<!--Thêm sửa-->\n<div bsModal #addEditModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Add new / Edit</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"addEditModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChanges(addEditForm.valid)\"\n          *ngIf=\"entity\">\n          <ul class=\"nav nav-tabs\">\n            <li class=\"active\">\n              <a data-toggle=\"tab\" href=\"#basic\">Basic Infomations</a>\n            </li>\n            <li>\n              <a data-toggle=\"tab\" href=\"#more-info\">Extra Infomations</a>\n            </li>\n          </ul>\n          <div class=\"tab-content\">\n            <div id=\"basic\" class=\"tab-pane fade in active\">\n              <h3>Basic Infomation</h3>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Name</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"hidden\" [(ngModel)]=\"entity.ID\" name=\"id\" />\n                  <input type=\"text\" #name=\"ngModel\" (keypress)=\"createAlias()\" [(ngModel)]=\"entity.Name\" required minlength=\"3\" name=\"name\"\n                    class=\"form-control\" />\n                  <small [hidden]=\"name.valid || (name.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                      You have to input at lease 3 characters\n                  </small>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Alias</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"text\" #alias=\"ngModel\" [(ngModel)]=\"entity.Alias\" required minlength=\"3\" name=\"alias\" class=\"form-control\" />\n                  <small [hidden]=\"alias.valid || (alias.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                    You have to input alias\n                  </small>\n                </div>\n              </div>\n\n\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">List</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <select class=\"form-control\" name=\"categoryID\" [(ngModel)]=\"entity.CategoryID\">\n                    <option value=\"\">--Select Category--</option>\n                    <option *ngFor=\"let x of productCategories\" [value]=\"x.ID\">{{x.Name}}</option>\n                  </select>\n                  <input type=\"hidden\" [(ngModel)]=\"entity.CategoryID\" name=\"categoryID\" class=\"form-control\" />\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Image</label>\n                <div class=\"col-md-6 col-sm-6 col-xs-12\">\n                  <input type=\"file\" #thumbnailImage name=\"thumbnailImage\" class=\"form-control\">\n                  <p class=\"help-block label label-danger\"> Only support *.png, *.jpg, *.jpeg </p>\n                  <input type=\"hidden\" [(ngModel)]=\"entity.ThumbnailImage\" name=\"thumbnailImage\" />\n                </div>\n                <div class=\"col-md-3 col-sm-3 col-xs-12\" *ngIf=\"entity.ThumbnailImage\">\n                  <img [src]=\"baseFolder + entity.ThumbnailImage\" width=\"128\" />\n                </div>\n              </div>\n\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Selling Price</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"number\" #price=\"ngModel\" [(ngModel)]=\"entity.Price\" required name=\"price\" class=\"form-control\" />\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"entity.IncludedVAT\" name=\"includedVAT\" #includedVAT=\"ngModel\" /> Include VAT\n                    </label>\n                  </div>\n                  <small [hidden]=\"price.valid || (price.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                      You have to input selling price\n                  </small>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Buying Price</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"number\" #originalPrice=\"ngModel\" [(ngModel)]=\"entity.OriginalPrice\" required name=\"originalPrice\" class=\"form-control\"\n                  />\n                  <small [hidden]=\"originalPrice.valid || (originalPrice.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                      You have to input buying price\n                  </small>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Promotion Price</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"number\" #promotionPrice=\"ngModel\" [(ngModel)]=\"entity.PromotionPrice\" name=\"promotionPrice\" class=\"form-control\"\n                  />\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Status</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"entity.Status\" name=\"status\" #status=\"ngModel\"> Active\n                    </label>\n                  </div>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"entity.HomeFlag\" name=\"homeFlag\" #status=\"ngModel\"> Display on Home Page\n                    </label>\n                  </div>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"entity.HotFlag\" name=\"hotFlag\" #status=\"ngModel\"> Hilight Product\n                    </label>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div id=\"more-info\" class=\"tab-pane fade\">\n              <h3>Extra Infomations</h3>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Description</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <textarea type=\"text\" #description=\"ngModel\" [(ngModel)]=\"entity.Description\" name=\"description\" class=\"form-control\"></textarea>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Insurance (Month)</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"number\" #warranty=\"ngModel\" [(ngModel)]=\"entity.Warranty\" name=\"warranty\" class=\"form-control\" />\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Product Detail</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <app-simple-tiny [elementId]=\"'my-editor-id'\" [content]=\"entity.Content\" (onEditorKeyup)=\"keyupHandlerContentFunction($event)\">\n                  </app-simple-tiny>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Keyword</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"text\" #metaKeyword=\"ngModel\" [(ngModel)]=\"entity.MetaKeyword\" name=\"keyword\" class=\"form-control\" />\n\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">SEO Description</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <textarea type=\"text\" #metaDescription=\"ngModel\" rows=4 [(ngModel)]=\"entity.MetaDescription\" name=\"metaDescription\" class=\"form-control\"></textarea>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Tags</label>\n                <div class=\"col-md-9 col-sm-9 col-xs-12\">\n                  <input type=\"text\" #tags=\"ngModel\" [(ngModel)]=\"entity.Tags\" name=\"tags\" class=\"form-control\" />\n                </div>\n              </div>\n\n            </div>\n          </div>\n\n\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addEditForm.form.valid\">Update</button>\n              <button type=\"button\" (click)=\"addEditModal.hide()\" class=\"btn btn-primary\">Cancel</button>\n\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Quản lý ảnh-->\n<div bsModal #imageManageModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Quản lý ảnh sản phẩm</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"imageManageModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" *ngIf=\"imageEntity\" novalidate #imageForm=\"ngForm\" (ngSubmit)=\"saveProductImage(imageForm.valid)\">\n          <div class=\"form-group row\">\n            <div class=\"col-xs-3\">\n              <label>Chọn ảnh</label>\n              <input type=\"file\" required #imagePath name=\"imagePath\" class=\"form-control\">\n              <p class=\"help-block label label-danger\"> chỉ hỗ trợ định dạng *.png, *.jpg, *.jpeg </p>\n              <input type=\"hidden\" [(ngModel)]=\"imageEntity.Path\" name=\"imagePath\" />\n              <input type=\"hidden\" [(ngModel)]=\"imageEntity.ProductId\" name=\"productId\" />\n            </div>\n            <div class=\"col-xs-3\">\n              <label for=\"ex2\">Mô tả</label>\n              <input class=\"form-control\" name=\"caption\" [(ngModel)]=\"imageEntity.Caption\" type=\"text\">\n            </div>\n            <div class=\"col-xs-4\">\n              <button type=\"submit\" class=\"btn btn-primary mt-24\" [disabled]=\"!imageForm.valid\">Thêm</button>\n            </div>\n          </div>\n        </form>\n        <table class=\"table\" *ngIf=\"productImages && productImages.length > 0\">\n          <thead>\n            <tr>\n              <th>\n                Hình ảnh\n              </th>\n              <th>\n                Mô tả\n              </th>\n              <th></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let image of productImages\">\n              <td>\n                <img [src]=\"baseFolder + image.Path\" width=\"100\" />\n              </td>\n              <td>\n                {{image.Caption}}\n              </td>\n              <td>\n                <button class=\"btn btn-danger\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Xóa số lượng\" (click)=\"deleteImage(image.ID)\">\n                  <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                </button>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"ln_solid\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Quản lý số lượng sản phẩm-->\n<!--Quản lý số lượng-->\n<div bsModal #quantityManageModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Quản lý số lượng sản phẩm</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"quantityManageModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" novalidate #quantityForm=\"ngForm\" (ngSubmit)=\"saveProductQuantity(quantityForm.valid)\"\n          *ngIf=\"quantityEntity\">\n          <div class=\"form-group row\">\n            <div class=\"col-xs-3\">\n              <label>Màu sắc</label>\n              <select class=\"form-control\" required #colorId=\"ngModel\" name=\"colorId\" [(ngModel)]=\"quantityEntity.ColorId\">\n                <option value=\"\">--Chọn màu--</option>\n                <option *ngFor=\"let x of colors\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <input type=\"hidden\" [(ngModel)]=\"quantityEntity.ProductId\" name=\"productId\" />\n\n              <small [hidden]=\"colorId.valid || (colorId.pristine && !quantityForm.submitted)\" class=\"text-danger\">\n                Bạn phải chọn màu sắc\n              </small>\n            </div>\n            <div class=\"col-xs-3\">\n              <label>Cỡ</label>\n              <select class=\"form-control\" required #sizeId=\"ngModel\" name=\"sizeId\" [(ngModel)]=\"quantityEntity.SizeId\">\n                <option value=\"\">--Chọn cỡ--</option>\n                <option *ngFor=\"let x of sizes\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <small [hidden]=\"sizeId.valid || (sizeId.pristine && !quantityForm.submitted)\" class=\"text-danger\">\n                Bạn phải nhập cỡ\n              </small>\n            </div>\n            <div class=\"col-xs-2\">\n              <label>Số lượng</label>\n              <input class=\"form-control\" type=\"number\" #quantity=\"ngModel\" name=\"quantity\" [(ngModel)]=\"quantityEntity.Quantity\" required>\n              <small [hidden]=\"quantity.valid || (quantity.pristine && !quantityForm.submitted)\" class=\"text-danger\">\n                Bạn phải nhập số lượng\n              </small>\n            </div>\n            <div class=\"col-xs-4\">\n              <button type=\"submit\" [disabled]=\"!quantityForm.valid\" class=\"btn btn-primary mt-24\">Thêm</button>\n            </div>\n          </div>\n        </form>\n        <table class=\"table\" *ngIf=\"productQuantities && productQuantities.length > 0\">\n          <thead>\n            <tr>\n              <th>\n                Màu sắc\n              </th>\n              <th>\n                Kích thước\n              </th>\n              <th>Số lượng</th>\n              <th></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let item of productQuantities\">\n              <td>\n                {{item.Color.Name}}\n              </td>\n              <td>\n                {{item.Size.Name}}\n              </td>\n              <td>{{item.Quantity}}</td>\n              <td>\n                <button class=\"btn btn-danger\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Xóa số lượng\" (click)=\"deleteQuantity(item.ProductId,item.ColorId,item.SizeId)\">\n                  <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                </button>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"ln_solid\"></div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/product/product.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_common_system_constants__ = __webpack_require__("../../../../../src/app/core/common/system.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_upload_service__ = __webpack_require__("../../../../../src/app/core/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ProductComponent = (function () {
    function ProductComponent(_authenService, _dataService, notificationService, utilityService, uploadService) {
        this._authenService = _authenService;
        this._dataService = _dataService;
        this.notificationService = notificationService;
        this.utilityService = utilityService;
        this.uploadService = uploadService;
        this.baseFolder = __WEBPACK_IMPORTED_MODULE_6__core_common_system_constants__["a" /* SystemConstants */].BASE_API;
        this.pageIndex = 1;
        this.pageSize = 20;
        this.pageDisplay = 10;
        this.filterKeyword = '';
        /*Product manage */
        this.imageEntity = {};
        this.productImages = [];
        this.sizeId = null;
        this.colorId = null;
        this.quantityEntity = {};
        this.productQuantities = [];
    }
    ProductComponent.prototype.ngOnInit = function () {
        this.search();
        this.loadProductCategories();
    };
    ProductComponent.prototype.createAlias = function () {
        this.entity.Alias = this.utilityService.MakeSeoTitle(this.entity.Name);
    };
    ProductComponent.prototype.search = function () {
        var _this = this;
        this._dataService.get('/api/product/getall?page=' + this.pageIndex + '&pageSize=' + this.pageSize + '&keyword=' + this.filterKeyword + '&categoryId=' + this.filterCategoryID)
            .subscribe(function (response) {
            _this.products = response.Items;
            _this.totalRow = response.TotalRows;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.pageChanged = function (event) {
        this.pageIndex = event.page;
        this.search();
    };
    ProductComponent.prototype.reset = function () {
        this.filterKeyword = '';
        this.filterCategoryID = null;
        this.search();
    };
    //Show add form
    ProductComponent.prototype.showAdd = function () {
        this.entity = { Content: '' };
        this.addEditModal.show();
    };
    //Show edit form
    ProductComponent.prototype.showEdit = function (id) {
        var _this = this;
        this._dataService.get('/api/product/detail/' + id).subscribe(function (response) {
            _this.entity = response;
            _this.addEditModal.show();
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.delete = function (id) {
        var _this = this;
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () {
            _this._dataService.delete('/api/product/delete', 'id', id).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
                _this.search();
            }, function (error) { return _this._dataService.handleError(error); });
        });
    };
    ProductComponent.prototype.loadProductCategories = function () {
        var _this = this;
        this._dataService.get('/api/productCategory/getallhierachy').subscribe(function (response) {
            _this.productCategories = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    //Save change for modal popup
    ProductComponent.prototype.saveChanges = function (valid) {
        var _this = this;
        if (valid) {
            var fi = this.thumbnailImage.nativeElement;
            if (fi.files.length > 0) {
                this.uploadService.postWithFile('/api/upload/saveImage?type=product', null, fi.files).then(function (imageUrl) {
                    _this.entity.ThumbnailImage = imageUrl;
                }).then(function () {
                    _this.saveData();
                });
            }
            else {
                this.saveData();
            }
        }
    };
    ProductComponent.prototype.saveData = function () {
        var _this = this;
        if (this.entity.ID == undefined) {
            this._dataService.post('/api/product/add', JSON.stringify(this.entity)).subscribe(function (response) {
                _this.search();
                _this.addEditModal.hide();
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
            });
        }
        else {
            this._dataService.put('/api/product/update', JSON.stringify(this.entity)).subscribe(function (response) {
                _this.search();
                _this.addEditModal.hide();
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].UPDATED_OK_MSG);
            }, function (error) { return _this._dataService.handleError(error); });
        }
    };
    ProductComponent.prototype.keyupHandlerContentFunction = function (e) {
        this.entity.Content = e;
    };
    ProductComponent.prototype.deleteMulti = function () {
        var _this = this;
        this.checkedItems = this.products.filter(function (x) { return x.Checked; });
        var checkedIds = [];
        for (var i = 0; i < this.checkedItems.length; ++i)
            checkedIds.push(this.checkedItems[i]["ID"]);
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () {
            _this._dataService.delete('/api/product/deletemulti', 'checkedProducts', JSON.stringify(checkedIds)).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
                _this.search();
            }, function (error) { return _this._dataService.handleError(error); });
        });
    };
    /*Image management*/
    ProductComponent.prototype.showImageManage = function (id) {
        this.imageEntity = {
            ProductId: id
        };
        this.loadProductImages(id);
        this.imageManageModal.show();
    };
    ProductComponent.prototype.loadProductImages = function (id) {
        var _this = this;
        this._dataService.get('/api/productImage/getall?productId=' + id).subscribe(function (response) {
            _this.productImages = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.deleteImage = function (id) {
        var _this = this;
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () {
            _this._dataService.delete('/api/productImage/delete', 'id', id.toString()).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
                _this.loadProductImages(_this.imageEntity.ProductId);
            }, function (error) { return _this._dataService.handleError(error); });
        });
    };
    ProductComponent.prototype.saveProductImage = function (isValid) {
        var _this = this;
        if (isValid) {
            var fi = this.imagePath.nativeElement;
            if (fi.files.length > 0) {
                this.uploadService.postWithFile('/api/upload/saveImage?type=product', null, fi.files).then(function (imageUrl) {
                    _this.imageEntity.Path = imageUrl;
                    _this._dataService.post('/api/productImage/add', JSON.stringify(_this.imageEntity)).subscribe(function (response) {
                        _this.loadProductImages(_this.imageEntity.ProductId);
                        _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
                    });
                });
            }
        }
    };
    /*Quản lý số lượng */
    ProductComponent.prototype.showQuantityManage = function (id) {
        this.quantityEntity = {
            ProductId: id
        };
        this.loadColors();
        this.loadSizes();
        this.loadProductQuantities(id);
        this.quantityManageModal.show();
    };
    ProductComponent.prototype.loadColors = function () {
        var _this = this;
        this._dataService.get('/api/productQuantity/getcolors').subscribe(function (response) {
            _this.colors = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.loadSizes = function () {
        var _this = this;
        this._dataService.get('/api/productQuantity/getsizes').subscribe(function (response) {
            _this.sizes = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.loadProductQuantities = function (id) {
        var _this = this;
        this._dataService.get('/api/productQuantity/getall?productId=' + id + '&sizeId=' + this.sizeId + '&colorId=' + this.colorId).subscribe(function (response) {
            _this.productQuantities = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    ProductComponent.prototype.saveProductQuantity = function (isValid) {
        var _this = this;
        if (isValid) {
            this._dataService.post('/api/productQuantity/add', JSON.stringify(this.quantityEntity)).subscribe(function (response) {
                _this.loadProductQuantities(_this.quantityEntity.ProductId);
                _this.quantityEntity = {
                    ProductId: _this.quantityEntity.ProductId
                };
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
            }, function (error) { return _this._dataService.handleError(error); });
        }
    };
    ProductComponent.prototype.deleteQuantity = function (productId, colorId, sizeId) {
        var _this = this;
        var parameters = { "productId": productId, "sizeId": sizeId, "colorId": colorId };
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () {
            _this._dataService.deleteWithMultiParams('/api/productQuantity/delete', parameters).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
                _this.loadProductQuantities(productId);
            }, function (error) { return _this._dataService.handleError(error); });
        });
    };
    return ProductComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addEditModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _a || Object)
], ProductComponent.prototype, "addEditModal", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("thumbnailImage"),
    __metadata("design:type", Object)
], ProductComponent.prototype, "thumbnailImage", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('imageManageModal'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _b || Object)
], ProductComponent.prototype, "imageManageModal", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("imagePath"),
    __metadata("design:type", Object)
], ProductComponent.prototype, "imagePath", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('quantityManageModal'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _c || Object)
], ProductComponent.prototype, "quantityManageModal", void 0);
ProductComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-product',
        template: __webpack_require__("../../../../../src/app/main/product/product.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/product/product.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__core_services_upload_service__["a" /* UploadService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_services_upload_service__["a" /* UploadService */]) === "function" && _h || Object])
], ProductComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=product.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/product/product.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductModule", function() { return ProductModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_component__ = __webpack_require__("../../../../../src/app/main/product/product.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_routes__ = __webpack_require__("../../../../../src/app/main/product/product.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_services_upload_service__ = __webpack_require__("../../../../../src/app/core/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_daterangepicker__ = __webpack_require__("../../../../ng2-daterangepicker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_daterangepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_daterangepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular_2_dropdown_multiselect__ = __webpack_require__("../../../../angular-2-dropdown-multiselect/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_simple_tiny_simple_tiny_component__ = __webpack_require__("../../../../../src/app/shared/simple-tiny/simple-tiny.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var ProductModule = (function () {
    function ProductModule() {
    }
    return ProductModule;
}());
ProductModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_3__product_routes__["a" /* ProductRouter */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["b" /* PaginationModule */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["a" /* ModalModule */],
            __WEBPACK_IMPORTED_MODULE_9_ng2_daterangepicker__["Daterangepicker"],
            __WEBPACK_IMPORTED_MODULE_10_angular_2_dropdown_multiselect__["a" /* MultiselectDropdownModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__product_component__["a" /* ProductComponent */], __WEBPACK_IMPORTED_MODULE_11__shared_simple_tiny_simple_tiny_component__["a" /* SimpleTinyComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_6__core_services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_7__core_services_utility_service__["a" /* UtilityService */], __WEBPACK_IMPORTED_MODULE_8__core_services_upload_service__["a" /* UploadService */]]
    })
], ProductModule);

//# sourceMappingURL=product.module.js.map

/***/ }),

/***/ "../../../../../src/app/main/product/product.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductRouter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__product_component__ = __webpack_require__("../../../../../src/app/main/product/product.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");


var routes = [
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_0__product_component__["a" /* ProductComponent */] }
];
var ProductRouter = __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes);
//# sourceMappingURL=product.routes.js.map

/***/ }),

/***/ "../../../../../src/app/shared/simple-tiny/simple-tiny.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/simple-tiny/simple-tiny.component.html":
/***/ (function(module, exports) {

module.exports = "<textarea id=\"{{elementId}}\"></textarea>"

/***/ }),

/***/ "../../../../../src/app/shared/simple-tiny/simple-tiny.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimpleTinyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SimpleTinyComponent = (function () {
    function SimpleTinyComponent() {
        this.onEditorKeyup = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    SimpleTinyComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        tinymce.baseURL = "/assets/tinymce";
        tinymce.init({
            selector: '#' + this.elementId,
            language: 'vi_VN',
            skin_url: '/assets/tinymce/skins/lightgray',
            language_url: '/assets/tinymce/langs/vi_VN.js',
            plugins: "autosave autolink code codesample colorpicker emoticons fullscreen hr image imagetools media preview table textcolor wordcount",
            toolbar: "imageupload forecolor cut copy paste fontselect styleselect bold italic bold link preview code image",
            setup: function (editor) {
                _this.editor = editor;
                editor.on('keyup', function () {
                    var content = editor.getContent();
                    _this.onEditorKeyup.emit(content);
                });
                editor.on('init', function () {
                    editor.setContent(_this.content);
                });
            },
        });
    };
    SimpleTinyComponent.prototype.ngOnDestroy = function () {
        tinymce.remove(this.editor);
    };
    return SimpleTinyComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], SimpleTinyComponent.prototype, "elementId", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], SimpleTinyComponent.prototype, "onEditorKeyup", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], SimpleTinyComponent.prototype, "content", void 0);
SimpleTinyComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-simple-tiny',
        template: __webpack_require__("../../../../../src/app/shared/simple-tiny/simple-tiny.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/simple-tiny/simple-tiny.component.css")]
    })
], SimpleTinyComponent);

//# sourceMappingURL=simple-tiny.component.js.map

/***/ })

});
//# sourceMappingURL=product.module.chunk.js.map