webpackJsonp(["main.module"],{

/***/ "../../../../../src/app/core/services/signalr.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignalrService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_system_constants__ = __webpack_require__("../../../../../src/app/core/common/system.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SignalrService = (function () {
    function SignalrService(_authenService) {
        this._authenService = _authenService;
        this.proxyName = 'MOQllerHub';
        // Constructor initialization  
        this.connectionEstablished = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.announcementReceived = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.connectionExists = false;
        // create hub connection  
        this.connection = $.hubConnection(__WEBPACK_IMPORTED_MODULE_1__common_system_constants__["a" /* SystemConstants */].BASE_API);
        this.connection.qs = { "access_token": _authenService.getLoggedInUser().access_token };
        // create new proxy as name already given in top  
        this.proxy = this.connection.createHubProxy(this.proxyName);
        // register on server events  
        this.registerOnServerEvents();
        // call the connecion start method to start the connection to send and receive events.  
        this.startConnection();
    }
    // check in the browser console for either signalr connected or not  
    SignalrService.prototype.startConnection = function () {
        var _this = this;
        this.connection.start().done(function (data) {
            console.log('Now connected ' + data.transport.name + ', connection ID= ' + data.id);
            _this.connectionEstablished.emit(true);
            _this.connectionExists = true;
        }).fail(function (error) {
            console.log('Could not connect ' + error);
            _this.connectionEstablished.emit(false);
        });
    };
    SignalrService.prototype.registerOnServerEvents = function () {
        var _this = this;
        this.proxy.on('addAnnouncement', function (announcement) {
            _this.announcementReceived.emit(announcement);
        });
    };
    return SignalrService;
}());
SignalrService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__authen_service__["a" /* AuthenService */]) === "function" && _a || Object])
], SignalrService);

var _a;
//# sourceMappingURL=signalr.service.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container body\">\n  <div class=\"main_container\">\n    <div class=\"col-md-3 left_col\">\n      <div class=\"left_col scroll-view\">\n        <div class=\"navbar nav_title\" style=\"border: 0;\">\n          <a routerLink=\"/main/home/index\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>MOQller</span></a>\n        </div>\n\n        <div class=\"clearfix\"></div>\n\n        <!-- menu profile quick info -->\n        <div class=\"profile\">\n          <div class=\"profile_pic\">\n            <img [src]=\"baseFolder + user.avatar\" height=\"50\" alt=\"...\" class=\"img-circle profile_img\">\n          </div>\n          <div class=\"profile_info\">\n            <span>Welcome,</span>\n            <h2>{{user.fullName}}</h2>\n          </div>\n        </div>\n        <!-- /menu profile quick info -->\n\n        <br />\n\n        <!-- sidebar menu -->\n        <app-sidebar-menu></app-sidebar-menu>\n        <!-- /sidebar menu -->\n\n        <!-- /menu footer buttons -->\n        <div class=\"sidebar-footer hidden-small\">\n          <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">\n            <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>\n          </a>\n          <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">\n            <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>\n          </a>\n          <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">\n            <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>\n          </a>\n          <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" (click)=\"logout\">\n            <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>\n          </a>\n        </div>\n        <!-- /menu footer buttons -->\n      </div>\n    </div>\n\n    <!-- top navigation -->\n    <app-top-menu></app-top-menu>\n    <!-- /top navigation -->\n\n    <!-- page content -->\n    <div class=\"right_col\" role=\"main\">\n      <div class=\"\">\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n    <!-- /page content -->\n\n    <!-- footer content -->\n    <footer>\n      <div class=\"pull-right\">\n        MOQller\n      </div>\n      <div class=\"clearfix\"></div>\n    </footer>\n    <!-- /footer content -->\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_common_system_constants__ = __webpack_require__("../../../../../src/app/core/common/system.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_common_url_constants__ = __webpack_require__("../../../../../src/app/core/common/url.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MainComponent = (function () {
    function MainComponent(utilityService, authenService) {
        this.utilityService = utilityService;
        this.authenService = authenService;
        this.baseFolder = __WEBPACK_IMPORTED_MODULE_1__core_common_system_constants__["a" /* SystemConstants */].BASE_API;
    }
    MainComponent.prototype.ngOnInit = function () {
        this.user = JSON.parse(localStorage.getItem(__WEBPACK_IMPORTED_MODULE_1__core_common_system_constants__["a" /* SystemConstants */].CURRENT_USER));
        //console.log(this.user.access_token);
        if (!(this.user.access_token)) {
            this.utilityService.navigateToLogin();
        }
        //console.log(this.user);
    };
    MainComponent.prototype.logout = function () {
        localStorage.removeItem(__WEBPACK_IMPORTED_MODULE_1__core_common_system_constants__["a" /* SystemConstants */].CURRENT_USER);
        this.utilityService.navigate(__WEBPACK_IMPORTED_MODULE_2__core_common_url_constants__["a" /* UrlConstants */].LOGIN);
    };
    return MainComponent;
}());
MainComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-main',
        template: __webpack_require__("../../../../../src/app/main/main.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/main.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */]) === "function" && _b || Object])
], MainComponent);

var _a, _b;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__main_routes__ = __webpack_require__("../../../../../src/app/main/main.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_user_module__ = __webpack_require__("../../../../../src/app/main/user/user.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_services_signalr_service__ = __webpack_require__("../../../../../src/app/core/services/signalr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_sidebar_menu_sidebar_menu_component__ = __webpack_require__("../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_top_menu_top_menu_component__ = __webpack_require__("../../../../../src/app/shared/top-menu/top-menu.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var MainModule = (function () {
    function MainModule() {
    }
    return MainModule;
}());
MainModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_5__user_user_module__["UserModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__main_routes__["a" /* mainRoutes */])
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__main_component__["a" /* MainComponent */], __WEBPACK_IMPORTED_MODULE_9__shared_sidebar_menu_sidebar_menu_component__["a" /* SidebarMenuComponent */], __WEBPACK_IMPORTED_MODULE_10__shared_top_menu_top_menu_component__["a" /* TopMenuComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__["a" /* UtilityService */], __WEBPACK_IMPORTED_MODULE_7__core_services_authen_service__["a" /* AuthenService */], __WEBPACK_IMPORTED_MODULE_8__core_services_signalr_service__["a" /* SignalrService */]]
    })
], MainModule);

//# sourceMappingURL=main.module.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mainRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");

var mainRoutes = [
    {
        //localhost:4200/main
        path: '', component: __WEBPACK_IMPORTED_MODULE_0__main_component__["a" /* MainComponent */], children: [
            //localhost:4200/main
            { path: '', redirectTo: 'product', pathMatch: 'full' },
            //localhost:4200/main/home
            { path: 'function', loadChildren: './function/function.module#FunctionModule' },
            //localhost:4200/main/user
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'role', loadChildren: './role/role.module#RoleModule' },
            { path: 'product-category', loadChildren: './product-category/product-category.module#ProductCategoryModule' },
            { path: 'product', loadChildren: './product/product.module#ProductModule' },
            { path: 'order', loadChildren: './order/order.module#OrderModule' },
            { path: 'announcement', loadChildren: './announcement/announcement.module#AnnouncementModule' },
            { path: 'report', loadChildren: './report/report.module#ReportModule' },
        ]
    }
];
//# sourceMappingURL=main.routes.js.map

/***/ }),

/***/ "../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<br/>\n<br/>\n<br/>\n\n<div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">\n  <div class=\"menu_section\">\n    <ul class=\"nav side-menu\">\n      \n      <li *ngFor=\"let item of functions\"><a><i class=\"fa {{item.IconCss}}\"></i> {{item.Name}} <span *ngIf=\"item.ChildFunctions.length>0\" class=\"fa fa-chevron-down\"></span></a>\n        <ul class=\"nav child_menu\">\n          <li *ngFor=\"let subItem of item.ChildFunctions\"><a routerLink=\"{{subItem.URL}}\" routerLinkActive=\"active\">{{subItem.Name}}</a></li>\n        </ul>\n      </li>\n\n    </ul>\n  </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SidebarMenuComponent = (function () {
    function SidebarMenuComponent(dataService, utilityService) {
        this.dataService = dataService;
        this.utilityService = utilityService;
    }
    SidebarMenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.get('/api/function/getlisthierarchy').subscribe(function (response) {
            _this.functions = response.sort(function (n1, n2) {
                if (n1.DisplayOrder > n2.DisplayOrder)
                    return 1;
                else if (n1.DisplayOrder < n2.DisplayOrder)
                    return -1;
                return 0;
            });
        }, function (error) {
            _this.dataService.handleError(error);
        });
    };
    return SidebarMenuComponent;
}());
SidebarMenuComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sidebar-menu',
        template: __webpack_require__("../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/sidebar-menu/sidebar-menu.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_utility_service__["a" /* UtilityService */]) === "function" && _b || Object])
], SidebarMenuComponent);

var _a, _b;
//# sourceMappingURL=sidebar-menu.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/top-menu/top-menu.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/top-menu/top-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"top_nav\">\n  <div class=\"nav_menu\">\n    <nav class=\"\" role=\"navigation\">\n      <div class=\"nav toggle\">\n        <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>\n      </div>\n\n      <ul class=\"nav navbar-nav navbar-right\">\n        <!-- <li class=\"\">\n          <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n                <img [src]=\"baseFolder + user.avatar\" alt=\"\">{{user.fullName}}\n                <span class=\" fa fa-angle-down\"></span>\n              </a>\n          <ul class=\"dropdown-menu dropdown-usermenu pull-right\">\n            <li><a href=\"javascript:;\">Profile</a></li>\n            <li>\n              <a href=\"javascript:;\">\n                    <span class=\"badge bg-red pull-right\">50%</span>\n                    <span>Cài đặt</span>\n                  </a>\n            </li>\n            <li><a href=\"javascript:;\">Trợ giúp</a></li>\n            <li><a href=\"#\" (click)=\"logout()\"><i class=\"fa fa-sign-out pull-right\"></i> logout</a></li>\n          </ul>\n        </li> -->\n\n        <li role=\"presentation\" class=\"dropdown\">\n          <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">\n                <i class=\"fa fa-envelope-o\"></i>\n                <span class=\"badge bg-green\" *ngIf=\"announcements && announcements.length>0\">{{announcements.length}}</span>\n              </a>\n          <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list\" role=\"menu\">\n            <li *ngFor=\"let item of announcements\">\n              <a (click)=\"markAsRead(item.ID)\">\n                <span class=\"image\"><img [src]=\"baseFolder+item.AppUser.Avatar\" height=\"30\" alt=\"item.AppUser.FullName\" /></span>\n                <span>\n                      <span>{{item.AppUser.FullName}}</span>\n                      <span class=\"time\">{{item.CreatedDate}}</span>\n                </span>\n                <span class=\"message\">\n                   {{item.Content}}\n                 \n                </span>\n              </a>\n            </li>\n            <li>\n              <div class=\"text-center\">\n                <a routerLink=\"/main/my-announcement\">\n                      <strong>Xem tất cả thông báo</strong>\n                      <i class=\"fa fa-angle-right\"></i>\n                    </a>\n              </div>\n            </li>\n          </ul>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/shared/top-menu/top-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_common_system_constants__ = __webpack_require__("../../../../../src/app/core/common/system.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_common_url_constants__ = __webpack_require__("../../../../../src/app/core/common/url.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_signalr_service__ = __webpack_require__("../../../../../src/app/core/services/signalr.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TopMenuComponent = (function () {
    function TopMenuComponent(_authenService, utilityService, _signalRService, _dataService, _ngZone) {
        this._authenService = _authenService;
        this.utilityService = utilityService;
        this._signalRService = _signalRService;
        this._dataService = _dataService;
        this._ngZone = _ngZone;
        this.baseFolder = __WEBPACK_IMPORTED_MODULE_2__core_common_system_constants__["a" /* SystemConstants */].BASE_API;
        // this can subscribe for events  
        this.subscribeToEvents();
        // this can check for conenction exist or not.  
        this.canSendMessage = _signalRService.connectionExists;
    }
    TopMenuComponent.prototype.ngOnInit = function () {
        this.user = this._authenService.getLoggedInUser();
        this.loadAnnouncements();
    };
    TopMenuComponent.prototype.logout = function () {
        localStorage.removeItem(__WEBPACK_IMPORTED_MODULE_2__core_common_system_constants__["a" /* SystemConstants */].CURRENT_USER);
        this.utilityService.navigate(__WEBPACK_IMPORTED_MODULE_3__core_common_url_constants__["a" /* UrlConstants */].LOGIN);
    };
    TopMenuComponent.prototype.subscribeToEvents = function () {
        var _this = this;
        var self = this;
        self.announcements = [];
        // if connection exists it can call of method.  
        this._signalRService.connectionEstablished.subscribe(function () {
            _this.canSendMessage = true;
        });
        // finally our service method to call when response received from server event and transfer response to some variable to be shwon on the browser.  
        this._signalRService.announcementReceived.subscribe(function (announcement) {
            _this._ngZone.run(function () {
                //console.log(announcement);
                moment.locale('vi');
                announcement.CreatedDate = moment(announcement.CreatedDate).fromNow();
                self.announcements.push(announcement);
            });
        });
    };
    TopMenuComponent.prototype.markAsRead = function (id) {
        var _this = this;
        var body = { announId: id };
        this._dataService.get('/api/Announcement/markAsRead?announId=' + id.toString()).subscribe(function (response) {
            if (response) {
                _this.loadAnnouncements();
            }
        });
    };
    TopMenuComponent.prototype.loadAnnouncements = function () {
        var _this = this;
        this._dataService.get('/api/Announcement/getTopMyAnnouncement').subscribe(function (response) {
            _this.announcements = [];
            moment.locale('vi');
            for (var _i = 0, response_1 = response; _i < response_1.length; _i++) {
                var item = response_1[_i];
                item.CreatedDate = moment(item.CreatedDate).fromNow();
                _this.announcements.push(item);
            }
        });
    };
    return TopMenuComponent;
}());
TopMenuComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-top-menu',
        template: __webpack_require__("../../../../../src/app/shared/top-menu/top-menu.component.html"),
        styles: [__webpack_require__("../../../../../src/app/shared/top-menu/top-menu.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_authen_service__["a" /* AuthenService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__["a" /* UtilityService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_signalr_service__["a" /* SignalrService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_signalr_service__["a" /* SignalrService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_services_data_service__["a" /* DataService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _e || Object])
], TopMenuComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=top-menu.component.js.map

/***/ })

});
//# sourceMappingURL=main.module.chunk.js.map