webpackJsonp(["order.module"],{

/***/ "../../../../../src/app/main/order/order-add/order-add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/order/order-add/order-add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Add new invoice</h3>\n  </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<div class=\"row\">\n\n  <div class=\"clearfix\"></div>\n\n  <div class=\"x_panel\">\n    <div class=\"x_title\">\n      <ul class=\"nav navbar-right panel_toolbox\">\n        <li>\n          <button class=\"btn btn-primary\" (click)=\"goBack()\">Back</button>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"x_content\">\n      <form class=\"form-horizontal form-label-left input_mask\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChanges(addEditForm.valid)\"\n        *ngIf=\"entity\">\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Customer Name</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"hidden\" [(ngModel)]=\"entity.ID\" name=\"id\" />\n            <input type=\"text\" #customerName=\"ngModel\" [(ngModel)]=\"entity.CustomerName\" required minlength=\"3\" name=\"customerName\" class=\"form-control\">\n            <small [hidden]=\"customerName.valid || (customerName.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n              You have to input product\n            </small>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Address </label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <textarea class=\"form-control\" #customerAddress=\"ngModel\" [(ngModel)]=\"entity.CustomerAddress\" name=\"customerAddress\" rows=\"3\"></textarea>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"text\" #customerEmail=\"ngModel\" [(ngModel)]=\"entity.CustomerEmail\" pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\"\n              required name=\"customerEmail\" class=\"form-control\">\n            <small [hidden]=\"customerEmail.valid || (customerEmail.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n              You have input the email\n            </small>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Phone</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"text\" #customerMobile=\"ngModel\" [(ngModel)]=\"entity.CustomerMobile\" required name=\"customerMobile\" class=\"form-control\">\n            <small [hidden]=\"customerMobile.valid || (customerMobile.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n              You have input the email\n            </small>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Customer Note </label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <textarea class=\"form-control\" #customerMessage=\"ngModel\" [(ngModel)]=\"entity.CustomerMessage\" name=\"customerMessage\" rows=\"3\"></textarea>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Payment Method</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <select class=\"form-control\" #paymentMethod=\"ngModel\" name=\"paymentMethod\" [(ngModel)]=\"entity.PaymentMethod\">\n              <option value=\"\">--Choose payment method--</option>\n              <option value=\"CASH\">Cash</option>\n              <option value=\"BANKING\">Bank Acount</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Payment Status</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <select class=\"form-control\" #paymentStatus=\"ngModel\" name=\"paymentStatus\" [(ngModel)]=\"entity.PaymentStatus\">\n              <option value=\"\">--Status--</option>\n              <option value=\"PAID\">Paid</option>\n              <option value=\"UNPAID\">Not Yet Pay</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Status</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <div class=\"checkbox\">\n              <label>\n                <input type=\"checkbox\" [(ngModel)]=\"entity.Status\" name=\"status\" #status=\"ngModel\"> Valid\n              </label>\n            </div>\n\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <div class=\"col-md-10 col-sm-12 col-xs-12\">\n\n          </div>\n          <div class=\"col-md-2 col-sm-12 col-xs-12\">\n            <button type=\"button\" (click)=\"showAddDetail()\" class=\"btn btn-success\">Add Detail</button>\n          </div>\n        </div>\n        <table class=\"table\" *ngIf=\"orderDetails && orderDetails.length > 0\">\n          <thead>\n            <tr>\n              <th>\n                Product\n              </th>\n              <th>\n                Quantity\n              </th>\n              <th>Price</th>\n              <th>Amount</th>\n              <th></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let item of orderDetails\">\n              <td>\n                {{item.Product.Name}}\n              </td>\n              <td>\n                {{item.Quantity | number}}\n              </td>\n              <td>{{item.Price | number}}</td>\n              <td>{{item.Price * item.Quantity | number}}</td>\n              <td>\n                <button class=\"btn btn-danger\" (click)=\"deleteDetail(item)\">\n                  <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                </button>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"ln_solid\"></div>\n        <div class=\"form-group\">\n          <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addEditForm.form.valid\">Update</button>\n          </div>\n        </div>\n\n      </form>\n    </div>\n  </div>\n\n</div>\n\n<div bsModal #addEditModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Add new Detail</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"addEditModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" novalidate #detailForm=\"ngForm\" (ngSubmit)=\"saveOrderDetail(detailForm.valid)\"\n          *ngIf=\"entity\">\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Product</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <select class=\"form-control\" name=\"productID\" #productID=\"ngModel\" required [(ngModel)]=\"detailEntity.ProductID\">\n                <option value=\"\">--Chọn sản phẩm--</option>\n                <option *ngFor=\"let x of products\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <small [hidden]=\"productID.valid || (productID.pristine && !detailForm.submitted)\" class=\"text-danger\">\n                You have to input at lease 3 characters\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Color</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <select class=\"form-control\" required #colorId=\"ngModel\" name=\"colorId\" [(ngModel)]=\"detailEntity.ColorId\">\n                <option value=\"\">--Chọn màu--</option>\n                <option *ngFor=\"let x of colors\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <small [hidden]=\"colorId.valid || (colorId.pristine && !detailForm.submitted)\" class=\"text-danger\">\n                You have to choose color\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Size</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <select class=\"form-control\" required #sizeId=\"ngModel\" name=\"sizeId\" [(ngModel)]=\"detailEntity.SizeId\">\n                <option value=\"\">--Choose Size--</option>\n                <option *ngFor=\"let x of sizes\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <small [hidden]=\"sizeId.valid || (sizeId.pristine && !detailForm.submitted)\" class=\"text-danger\">\n                You have to input size\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Quantity</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"number\" #quantity=\"ngModel\" [(ngModel)]=\"detailEntity.Quantity\" required name=\"quantity\" class=\"form-control\">\n              <small [hidden]=\"quantity.valid || (quantity.pristine && !detailForm.submitted)\" class=\"text-danger\">\n                You have to input quantity\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Price</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"number\" #price=\"ngModel\" [(ngModel)]=\"detailEntity.Price\" required name=\"price\" class=\"form-control\">\n              <small [hidden]=\"price.valid || (price.pristine && !detailForm.submitted)\" class=\"text-danger\">\n                You have to input price\n              </small>\n            </div>\n          </div>\n\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!detailForm.form.valid\">Update</button>\n              <button type=\"button\" (click)=\"addEditModal.hide()\" class=\"btn btn-primary\">Cancel</button>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/order/order-add/order-add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderAddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OrderAddComponent = (function () {
    function OrderAddComponent(utilityService, _dataService, notificationService) {
        this.utilityService = utilityService;
        this._dataService = _dataService;
        this.notificationService = notificationService;
        this.entity = { Status: true };
        this.sizeId = null;
        this.colorId = null;
        this.orderDetails = [];
        this.detailEntity = {
            ProductID: 0,
            Quantity: 0,
            Price: 0
        };
    }
    OrderAddComponent.prototype.ngOnInit = function () {
    };
    /*Product quantity management */
    OrderAddComponent.prototype.showAddDetail = function () {
        this.loadColors();
        this.loadSizes();
        this.loadProducts();
        this.addEditModal.show();
    };
    OrderAddComponent.prototype.loadProducts = function () {
        var _this = this;
        this._dataService.get('/api/product/getallparents').subscribe(function (response) {
            _this.products = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderAddComponent.prototype.loadColors = function () {
        var _this = this;
        this._dataService.get('/api/productQuantity/getcolors').subscribe(function (response) {
            _this.colors = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderAddComponent.prototype.loadSizes = function () {
        var _this = this;
        this._dataService.get('/api/productQuantity/getsizes').subscribe(function (response) {
            _this.sizes = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderAddComponent.prototype.goBack = function () {
        this.utilityService.navigate('/main/order/index');
    };
    //Save change for modal popup
    OrderAddComponent.prototype.saveChanges = function (valid) {
        var _this = this;
        if (valid) {
            this.entity.OrderDetails = this.orderDetails;
            this._dataService.post('/api/order/add', JSON.stringify(this.entity)).subscribe(function (response) {
                _this.entity = response;
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_4__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
            }, function (error) { return _this._dataService.handleError(error); });
        }
    };
    OrderAddComponent.prototype.saveOrderDetail = function (valid) {
        var _this = this;
        if (valid) {
            this.addEditModal.hide();
            this.detailEntity.Product = this.products.find(function (x) { return x.ID == _this.detailEntity.ProductID; });
            this.orderDetails.push(this.detailEntity);
            this.detailEntity = {
                ProductID: 0,
                Quantity: 0,
                Price: 0
            };
        }
    };
    //Action delete
    OrderAddComponent.prototype.deleteDetail = function (item) {
        for (var index = 0; index < this.orderDetails.length; index++) {
            var orderDetail = this.orderDetails[index];
            if (orderDetail.ProductID == item.ProductID
                && orderDetail.ColorId == item.ColorId
                && orderDetail.SizeId == item.SizeId) {
                this.orderDetails.splice(index, 1);
            }
        }
    };
    return OrderAddComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addEditModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _a || Object)
], OrderAddComponent.prototype, "addEditModal", void 0);
OrderAddComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order-add',
        template: __webpack_require__("../../../../../src/app/main/order/order-add/order-add.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/order/order-add/order-add.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */]) === "function" && _d || Object])
], OrderAddComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=order-add.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/order/order-detail/order-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/order/order-detail/order-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Chi tiết hóa đơn</h3>\n  </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<div class=\"row\">\n\n  <div class=\"clearfix\"></div>\n\n  <div class=\"x_panel\">\n    <div class=\"x_title\">\n      <ul class=\"nav navbar-right panel_toolbox\">\n        <li>\n          <button class=\"btn btn-primary\" (click)=\"goBack()\">Quay lại</button>\n        </li>\n         <li>\n          <button class=\"btn btn-success\" (click)=\"exportToExcel()\">Xuất ra Excel</button>\n        </li>\n      </ul>\n      <div class=\"clearfix\"></div>\n    </div>\n    <div class=\"x_content\">\n      <form class=\"form-horizontal form-label-left input_mask\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChanges(addEditForm.valid)\"\n        *ngIf=\"entity\">\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Tên khách hàng</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"hidden\" [(ngModel)]=\"entity.ID\" name=\"id\" />\n            <input type=\"text\" #customerName=\"ngModel\" disabled [(ngModel)]=\"entity.CustomerName\" required minlength=\"3\" name=\"customerName\" class=\"form-control\">\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Địa chỉ </label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <textarea class=\"form-control\" disabled #customerAddress=\"ngModel\" [(ngModel)]=\"entity.CustomerAddress\" name=\"customerAddress\" rows=\"3\"></textarea>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"text\" #customerEmail=\"ngModel\" disabled [(ngModel)]=\"entity.CustomerEmail\" pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\"\n               name=\"customerEmail\" class=\"form-control\">\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Số điện thoại</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <input type=\"text\" #customerMobile=\"ngModel\" [(ngModel)]=\"entity.CustomerMobile\" disabled name=\"customerMobile\" class=\"form-control\">\n\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Ghi chú khách hàng </label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <textarea class=\"form-control\" disabled #customerMessage=\"ngModel\" [(ngModel)]=\"entity.CustomerMessage\" name=\"customerMessage\" rows=\"3\"></textarea>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Phương thức thanh toán</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <select class=\"form-control\" #paymentMethod=\"ngModel\" disabled name=\"paymentMethod\" [(ngModel)]=\"entity.PaymentMethod\">\n              <option value=\"\">--Chọn phương thức tahnh toán--</option>\n              <option value=\"CASH\">Tiền mặt</option>\n              <option value=\"BANKING\">Chuyển khoản</option>\n          </select>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Trạng thái thanh toán</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <select class=\"form-control\" #paymentStatus=\"ngModel\" disabled name=\"paymentStatus\" [(ngModel)]=\"entity.PaymentStatus\">\n              <option value=\"\">--Trạng thái--</option>\n              <option value=\"PAID\">Đã thanh toán</option>\n              <option value=\"UNPAID\">Chưa thanh toán</option>\n          </select>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Trạng thái</label>\n          <div class=\"col-md-9 col-sm-9 col-xs-12\">\n            <div class=\"checkbox\">\n              <label>\n                   <input type=\"checkbox\" disabled [(ngModel)]=\"entity.Status\" name=\"status\" #status=\"ngModel\"> Hợp lệ\n                </label>\n            </div>\n\n          </div>\n        </div>\n        <table class=\"table\" *ngIf=\"orderDetails && orderDetails.length > 0\">\n          <thead>\n            <tr>\n              <th>\n                Sản phẩm\n              </th>\n              <th>\n                Số lượng\n              </th>\n              <th>Giá</th>\n              <th>Thành tiền</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let item of orderDetails\">\n              <td>\n                {{item.Product.Name}}\n              </td>\n              <td>\n                {{item.Quantity | number}}\n              </td>\n              <td>{{item.Price | number}}</td>\n              <td>{{item.Price * item.Quantity | number}}</td>\n            </tr>\n            <tr>\n                <td colspan=\"4\">\n                  Tổng tiền: {{totalAmount}}\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        <div class=\"ln_solid\"></div>\n      </form>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/order/order-detail/order-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_common_system_constants__ = __webpack_require__("../../../../../src/app/core/common/system.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OrderDetailComponent = (function () {
    function OrderDetailComponent(utilityService, _dataService, activatedRoute, notificationService) {
        this.utilityService = utilityService;
        this._dataService = _dataService;
        this.activatedRoute = activatedRoute;
        this.notificationService = notificationService;
        this.baseFolder = __WEBPACK_IMPORTED_MODULE_4__core_common_system_constants__["a" /* SystemConstants */].BASE_API;
    }
    OrderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.orderId = params['id'];
            _this.loadOrder(_this.orderId);
            _this.loadOrderDetail(_this.orderId);
        });
    };
    OrderDetailComponent.prototype.goBack = function () {
        this.utilityService.navigate('/main/order/index');
    };
    OrderDetailComponent.prototype.loadOrder = function (id) {
        var _this = this;
        this._dataService.get('/api/order/detail/' + id.toString()).subscribe(function (response) {
            _this.entity = response;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderDetailComponent.prototype.exportToExcel = function () {
        var _this = this;
        this._dataService.get('/api/order/exportExcel/' + this.orderId.toString()).subscribe(function (response) {
            //console.log(response);
            window.open(_this.baseFolder + response.Message);
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderDetailComponent.prototype.loadOrderDetail = function (id) {
        var _this = this;
        this._dataService.get('/api/order/getalldetails/' + id.toString()).subscribe(function (response) {
            _this.orderDetails = response;
            _this.totalAmount = 0;
            for (var _i = 0, _a = _this.orderDetails; _i < _a.length; _i++) {
                var item = _a[_i];
                _this.totalAmount = _this.totalAmount + (item.Quantity * item.Price);
            }
            //console.log(this.totalAmount);
        }, function (error) { return _this._dataService.handleError(error); });
    };
    return OrderDetailComponent;
}());
OrderDetailComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order-detail',
        template: __webpack_require__("../../../../../src/app/main/order/order-detail/order-detail.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/order/order-detail/order-detail.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */]) === "function" && _d || Object])
], OrderDetailComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=order-detail.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/order/order-routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderRouter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__order_component__ = __webpack_require__("../../../../../src/app/main/order/order.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__order_add_order_add_component__ = __webpack_require__("../../../../../src/app/main/order/order-add/order-add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_detail_order_detail_component__ = __webpack_require__("../../../../../src/app/main/order/order-detail/order-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");




var routes = [
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_0__order_component__["a" /* OrderComponent */] },
    { path: 'add', component: __WEBPACK_IMPORTED_MODULE_1__order_add_order_add_component__["a" /* OrderAddComponent */] },
    { path: 'detail/:id', component: __WEBPACK_IMPORTED_MODULE_2__order_detail_order_detail_component__["a" /* OrderDetailComponent */] }
];
var OrderRouter = __WEBPACK_IMPORTED_MODULE_3__angular_router__["g" /* RouterModule */].forChild(routes);
//# sourceMappingURL=order-routes.js.map

/***/ }),

/***/ "../../../../../src/app/main/order/order.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/order/order.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Invoice</h3>\n  </div>\n\n  <div class=\"title_right\">\n\n\n  </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<div class=\"row\">\n\n  <div class=\"clearfix\"></div>\n\n  <div class=\"x_panel\">\n    <div class=\"x_content\">\n      <div class=\"x_title\">\n        <div class=\"col-md-2 col-sm-2 col-xs-12 form-group\">\n          <input type=\"text\" name=\"startDate\" daterangepicker [options]=\"dateOptions\" (selected)=\"changeStartDate($event)\" [(ngModel)]=\"filterStartDate\" class=\"form-control\" placeholder=\"From Date\">\n        </div>\n        <div class=\"col-md-2 col-sm-2 col-xs-12 form-group \">\n          <input type=\"text\" name=\"endDate\" daterangepicker [options]=\"dateOptions\" (selected)=\"changeEndDate($event)\" [(ngModel)]=\"filterEndDate\" class=\"form-control\" placeholder=\"To Date\">\n\n        </div>\n        <div class=\"col-md-2 col-sm-2 col-xs-12 form-group \">\n          <input type=\"text\" name=\"customerName\" [(ngModel)]=\"filterCustomerName\" class=\"form-control\" placeholder=\"Customer Name\">\n        </div>\n        <div class=\"col-md-2 col-sm-2 col-xs-12 form-group \">\n          <select class=\"form-control\" name=\"filterPaymentStatus\" [(ngModel)]=\"filterPaymentStatus\">\n            <option value=\"\">--All--</option>\n            <option value=\"PAID\">Already Payment</option>\n            <option value=\"UNPAID\">Not Yet Payment</option>\n          </select>\n        </div>\n        <div class=\"col-md-4 col-sm-3 col-xs-12 form-group\">\n          <button class=\"btn btn-primary\" type=\"button\" (click)=\"search()\">Go</button>\n          <button class=\"btn btn-default\" type=\"button\" (click)=\"reset()\">Reset</button>\n          <button *ngIf=\"_authenService.hasPermission('ORDER','create')\" class=\"btn btn-success\" routerLink=\"/main/order/add\">Add new</button>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n      <table *ngIf=\"orders && orders.length > 0\" class=\"table table-bordered\">\n        <thead>\n          <tr>\n            <th>Customer Name</th>\n            <th>Phone</th>\n            <td>Created Date</td>\n            <td>Status</td>\n            <th></th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let item of orders\">\n            <td>{{item.CustomerName}}</td>\n            <td>{{item.CustomerMobile}}</td>\n            <td>{{item.CreatedDate | date}}</td>\n            <td *ngIf=\"item.Status==true\">Valid</td>\n             <td *ngIf=\"item.Status==false\">In Valid</td>\n            <td>\n              <a class=\"btn btn-primary\" routerLink=\"/main/order/detail/{{item.ID}}\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Chi tiết hóa đơn\"\n                *ngIf=\"_authenService.hasPermission('ORDER','update')\"> <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i></a>\n              <button class=\"btn btn-danger\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Xóa sản phẩm\" *ngIf=\"_authenService.hasPermission('ORDER','delete')\"\n                (click)=\"delete(item.ID)\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>\n            </td>\n          </tr>\n\n        </tbody>\n      </table>\n      <div *ngIf=\"orders && orders.length == 0\" class=\"col-md-12\">\n        <div class=\"alert alert-info\">\n          <strong>Notification!</strong> No invoices\n        </div>\n\n      </div>\n      <div *ngIf=\"orders && orders.length > 0\" class=\"col-md-12\">\n        <pagination [boundaryLinks]=\"true\" [itemsPerPage]=\"pageSize\" (pageChanged)=\"pageChanged($event)\" [totalItems]=\"totalRow\"\n          [(ngModel)]=\"pageIndex\" class=\"pagination-sm\" previousText=\"&lsaquo;\" nextText=\"&rsaquo;\" firstText=\"&laquo;\" lastText=\"&raquo;\"></pagination>\n      </div>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/order/order.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_upload_service__ = __webpack_require__("../../../../../src/app/core/services/upload.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var OrderComponent = (function () {
    function OrderComponent(_authenService, _dataService, notificationService, utilityService, uploadService) {
        this._authenService = _authenService;
        this._dataService = _dataService;
        this.notificationService = notificationService;
        this.utilityService = utilityService;
        this.uploadService = uploadService;
        this.pageIndex = 1;
        this.pageSize = 20;
        this.pageDisplay = 10;
        this.filterCustomerName = '';
        this.filterStartDate = '';
        this.filterPaymentStatus = '';
        this.filterEndDate = '';
        this.dateOptions = {
            locale: { format: 'DD/MM/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true
        };
    }
    OrderComponent.prototype.ngOnInit = function () {
        this.search();
    };
    OrderComponent.prototype.search = function () {
        var _this = this;
        this._dataService.get('/api/order/getlistpaging?page=' + this.pageIndex
            + '&pageSize=' + this.pageSize + '&startDate=' + this.filterStartDate
            + '&endDate=' + this.filterEndDate + '&customerName=' + this.filterCustomerName
            + '&paymentStatus=' + this.filterPaymentStatus)
            .subscribe(function (response) {
            _this.orders = response.Items;
            _this.pageIndex = response.PageIndex;
        }, function (error) { return _this._dataService.handleError(error); });
    };
    OrderComponent.prototype.reset = function () {
        this.filterCustomerName = '';
        this.filterEndDate = '';
        this.filterStartDate = '';
        this.filterPaymentStatus = '';
        this.search();
    };
    OrderComponent.prototype.delete = function (id) {
        var _this = this;
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () {
            _this._dataService.delete('/api/order/delete', 'id', id).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
                _this.search();
            }, function (error) { return _this._dataService.handleError(error); });
        });
    };
    OrderComponent.prototype.pageChanged = function (event) {
        this.pageIndex = event.page;
        this.search();
    };
    OrderComponent.prototype.changeStartDate = function (value) {
        this.filterStartDate = moment(new Date(value.end._d)).format('DD/MM/YYYY');
    };
    OrderComponent.prototype.changeEndDate = function (value) {
        this.filterEndDate = moment(new Date(value.end._d)).format('DD/MM/YYYY');
    };
    return OrderComponent;
}());
OrderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-order',
        template: __webpack_require__("../../../../../src/app/main/order/order.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/order/order.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_authen_service__["a" /* AuthenService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core_services_data_service__["a" /* DataService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_notification_service__["a" /* NotificationService */]) === "function" && _c || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_utility_service__["a" /* UtilityService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__core_services_upload_service__["a" /* UploadService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__core_services_upload_service__["a" /* UploadService */]) === "function" && _f || Object])
], OrderComponent);

var _a, _b, _c, _e, _f;
//# sourceMappingURL=order.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/order/order.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_component__ = __webpack_require__("../../../../../src/app/main/order/order.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_upload_service__ = __webpack_require__("../../../../../src/app/core/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_daterangepicker__ = __webpack_require__("../../../../ng2-daterangepicker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_daterangepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_daterangepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__order_routes__ = __webpack_require__("../../../../../src/app/main/order/order-routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__order_detail_order_detail_component__ = __webpack_require__("../../../../../src/app/main/order/order-detail/order-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__order_add_order_add_component__ = __webpack_require__("../../../../../src/app/main/order/order-add/order-add.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var OrderModule = (function () {
    function OrderModule() {
    }
    return OrderModule;
}());
OrderModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_9__order_routes__["a" /* OrderRouter */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__["b" /* PaginationModule */],
            __WEBPACK_IMPORTED_MODULE_8_ng2_daterangepicker__["Daterangepicker"],
            __WEBPACK_IMPORTED_MODULE_3_ngx_bootstrap__["a" /* ModalModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__order_component__["a" /* OrderComponent */], __WEBPACK_IMPORTED_MODULE_10__order_detail_order_detail_component__["a" /* OrderDetailComponent */], __WEBPACK_IMPORTED_MODULE_11__order_add_order_add_component__["a" /* OrderAddComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_5__core_services_data_service__["a" /* DataService */], __WEBPACK_IMPORTED_MODULE_6__core_services_utility_service__["a" /* UtilityService */], __WEBPACK_IMPORTED_MODULE_7__core_services_upload_service__["a" /* UploadService */]]
    })
], OrderModule);

//# sourceMappingURL=order.module.js.map

/***/ })

});
//# sourceMappingURL=order.module.chunk.js.map