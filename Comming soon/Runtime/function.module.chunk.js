webpackJsonp(["function.module"],{

/***/ "../../../../../src/app/main/function/function.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/function/function.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-title\">\n  <div class=\"title_left\">\n    <h3>Function List</h3>\n  </div>\n\n  <div class=\"title_right\">\n    <div class=\"col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search\">\n      <div class=\"input-group\">\n        <input type=\"text\" class=\"form-control\" name=\"filter\" (keyup.enter)=\"search()\" [(ngModel)]=\"filter\" placeholder=\"Search...\">\n        <span class=\"input-group-btn\">\n          <button class=\"btn btn-default\" (click)=\"search()\" type=\"button\">Go</button>\n        </span>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"clearfix\"></div>\n<div class=\"row\">\n  <div class=\"col-md-12 col-sm-12 col-xs-12\">\n    <div class=\"x_panel\">\n      <div class=\"x_title\">\n        <ul class=\"nav navbar-right panel_toolbox\">\n          <li>\n            <button class=\"btn btn-success\" (click)=\"showAddModal()\">Add new</button>\n          </li>\n\n        </ul>\n        <div class=\"clearfix\"></div>\n      </div>\n      <div class=\"x_content\">\n        <tree-root #treeFunction [nodes]=\"_functionHierachy\">\n          <ng-template #treeNodeTemplate let-node let-index=\"index\">\n\n            <table class=\"table\">\n\n\n              <tr>\n                <td class=\"col-md-12 col-sm-12 col-xs-12\">\n                  {{ node.data.Name }}\n                </td>\n                <td>\n                  <button class=\"btn btn-xs btn-success\" (click)=\"showPermission(node.data.ID)\">\n                    <i class=\"fa fa-lock\" aria-hidden=\"true\"></i>\n                  </button>\n                </td>\n                <td>\n                  <button class=\"btn btn-xs btn-primary\" (click)=\"showEdit(node.data.ID)\">\n                    <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>\n                  </button>\n                </td>\n                <td>\n                  <button class=\"btn btn-xs btn-danger\" (click)=\"delete(node.data.ID)\">\n                    <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n                  </button>\n                </td>\n\n              </tr>\n\n            </table>\n\n          </ng-template>\n        </tree-root>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Modal add and edit-->\n<div bsModal #addEditModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Add new / Edit</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"addEditModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" novalidate #addEditForm=\"ngForm\" (ngSubmit)=\"saveChanges(addEditForm.valid)\"\n          *ngIf=\"entity\">\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Code</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"hidden\" [(ngModel)]=\"entity.ID\" name=\"id\" />\n              <input type=\"text\" #id=\"ngModel\" [disabled]=\"editFlg\" [(ngModel)]=\"entity.ID\" required name=\"id\" class=\"form-control\" />\n              <small [hidden]=\"id.valid || (id.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have to input code\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Name</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" #name=\"ngModel\" [(ngModel)]=\"entity.Name\" required minlength=\"3\" name=\"name\" class=\"form-control\" />\n              <small [hidden]=\"name.valid || (name.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                Bạn phải nhập tên ít nhất 3 ký tự\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Path</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" #url=\"ngModel\" [(ngModel)]=\"entity.URL\" required name=\"url\" class=\"form-control\" />\n              <small [hidden]=\"url.valid || (url.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input the path\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Order</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"number\" #displayOrder=\"ngModel\" [(ngModel)]=\"entity.DisplayOrder\" required name=\"displayOrder\" class=\"form-control\"\n              />\n              <small [hidden]=\"displayOrder.valid || (displayOrder.pristine && !addEditForm.submitted)\" class=\"text-danger\">\n                You have input order\n              </small>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Parent</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <select class=\"form-control\" name=\"parentId\" [(ngModel)]=\"entity.ParentId\">\n                <option value=\"\">--Chọn chức năng cha--</option>\n                <option *ngFor=\"let x of _functions\" [value]=\"x.ID\">{{x.Name}}</option>\n              </select>\n              <input type=\"hidden\" [(ngModel)]=\"entity.ParentId\" name=\"parentId\" class=\"form-control\" />\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Icon CSS</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <input type=\"text\" #iconCss=\"ngModel\" [(ngModel)]=\"entity.IconCss\" name=\"iconCss\" class=\"form-control\" />\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Status</label>\n            <div class=\"col-md-9 col-sm-9 col-xs-12\">\n              <div class=\"checkbox\">\n                <label>\n                  <input type=\"checkbox\" [(ngModel)]=\"entity.Status\" name=\"status\" #status=\"ngModel\"> Active\n                </label>\n              </div>\n            </div>\n          </div>\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!addEditForm.form.valid\">Update</button>\n              <button type=\"button\" (click)=\"addEditModal.hide()\" class=\"btn btn-primary\">Cancel</button>\n\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Permission popup-->\n\n<div bsModal #permissionModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"\n  aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">Permission</h4>\n        <button type=\"button\" class=\"close pull-right\" (click)=\"permissionModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form class=\"form-horizontal form-label-left input_mask\" novalidate #permissionForm=\"ngForm\" (ngSubmit)=\"savePermission(permissionForm.valid,_permission)\">\n          <table class=\"table\">\n            <thead>\n              <tr>\n                <th>\n                  Name\n                </th>\n                <th>\n                  Permission to do\n                </th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let permission of _permission\">\n                <td>\n                  {{permission.AppRole.Description}}\n                </td>\n                <td>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"permission.CanRead\" [name]=\"permission.id\" [ngModelOptions]=\"{standalone: true}\"> Xem\n                    </label>\n                  </div>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"permission.CanCreate\" [name]=\"permission.id\" name=\"status\" [ngModelOptions]=\"{standalone: true}\"> Thêm\n                    </label>\n                  </div>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"permission.CanUpdate\" [name]=\"permission.id\" name=\"status\" [ngModelOptions]=\"{standalone: true}\"> Sửa\n                    </label>\n                  </div>\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\" [(ngModel)]=\"permission.CanDelete\" [name]=\"permission.id\" name=\"status\" [ngModelOptions]=\"{standalone: true}\"> Xóa\n                    </label>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n          <div class=\"ln_solid\"></div>\n          <div class=\"form-group\">\n            <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!permissionForm.form.valid\">Cập nhật</button>\n              <button type=\"button\" (click)=\"permissionModal.hide()\" class=\"btn btn-primary\">Cancel</button>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/function/function.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FunctionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angular_tree_component__ = __webpack_require__("../../../../angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_data_service__ = __webpack_require__("../../../../../src/app/core/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__ = __webpack_require__("../../../../../src/app/core/services/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_utility_service__ = __webpack_require__("../../../../../src/app/core/services/utility.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__ = __webpack_require__("../../../../../src/app/core/common/message.constants.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_authen_service__ = __webpack_require__("../../../../../src/app/core/services/authen.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FunctionComponent = (function () {
    function FunctionComponent(_dataService, notificationService, utilityService, _authenService) {
        this._dataService = _dataService;
        this.notificationService = notificationService;
        this.utilityService = utilityService;
        this._authenService = _authenService;
        this.filter = '';
    }
    FunctionComponent.prototype.ngOnInit = function () {
        if (this._authenService.checkAccess('USER') == false) {
            this.utilityService.navigateToLogin();
        }
        this.search();
    };
    FunctionComponent.prototype.showPermission = function (id) {
        var _this = this;
        this._dataService.get('/api/appRole/getAllPermission?functionId=' + id).subscribe(function (response) {
            _this.functionId = id;
            _this._permission = response;
            _this.permissionModal.show();
        }, function (error) { return _this._dataService.handleError(error); });
    };
    FunctionComponent.prototype.savePermission = function (valid, _permission) {
        var _this = this;
        if (valid) {
            var data = {
                Permissions: this._permission,
                FunctionId: this.functionId
            };
            this._dataService.post('/api/appRole/savePermission', JSON.stringify(data)).subscribe(function (response) {
                _this.notificationService.printSuccessMessage(response);
                _this.permissionModal.hide();
            }, function (error) { return _this._dataService.handleError(error); });
        }
    };
    //Show add form
    FunctionComponent.prototype.showAddModal = function () {
        this.entity = {};
        this.addEditModal.show();
        this.editFlag = false;
    };
    //Load data
    FunctionComponent.prototype.search = function () {
        var _this = this;
        this._dataService.get('/api/function/getall?filter=' + this.filter)
            .subscribe(function (response) {
            _this._functions = response.filter(function (x) { return x.ParentId == null; });
            _this._functionHierachy = _this.utilityService.Unflatten(response);
        }, function (error) {
            _this._dataService.handleError(error);
        });
    };
    //Save change for modal popup
    FunctionComponent.prototype.saveChanges = function (valid) {
        var _this = this;
        if (valid) {
            if (this.editFlag == false) {
                this._dataService.post('/api/function/add', JSON.stringify(this.entity)).subscribe(function (response) {
                    _this.search();
                    _this.addEditModal.hide();
                    _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CREATED_OK_MSG);
                }, function (error) { return _this._dataService.handleError(error); });
            }
            else {
                this._dataService.put('/api/function/update', JSON.stringify(this.entity)).subscribe(function (response) {
                    _this.search();
                    _this.addEditModal.hide();
                    _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].UPDATED_OK_MSG);
                }, function (error) { return _this._dataService.handleError(error); });
            }
        }
    };
    //Show edit form
    FunctionComponent.prototype.showEdit = function (id) {
        var _this = this;
        this._dataService.get('/api/function/detail/' + id).subscribe(function (response) {
            _this.entity = response;
            _this.editFlag = true;
            _this.addEditModal.show();
        }, function (error) { return _this._dataService.handleError(error); });
    };
    //Action delete
    FunctionComponent.prototype.deleteConfirm = function (id) {
        var _this = this;
        this._dataService.delete('/api/function/delete', 'id', id).subscribe(function (response) {
            _this.notificationService.printSuccessMessage(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].DELETED_OK_MSG);
            _this.search();
        }, function (error) { return _this._dataService.handleError(error); });
    };
    //Click button delete turn on confirm
    FunctionComponent.prototype.delete = function (id) {
        var _this = this;
        this.notificationService.printConfirmationDialog(__WEBPACK_IMPORTED_MODULE_5__core_common_message_constants__["a" /* MessageContstants */].CONFIRM_DELETE_MSG, function () { return _this.deleteConfirm(id); });
    };
    return FunctionComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('addEditModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _a || Object)
], FunctionComponent.prototype, "addEditModal", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('permissionModal'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap_modal__["a" /* ModalDirective */]) === "function" && _b || Object)
], FunctionComponent.prototype, "permissionModal", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_angular_tree_component__["a" /* TreeComponent */]),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_angular_tree_component__["a" /* TreeComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_angular_tree_component__["a" /* TreeComponent */]) === "function" && _c || Object)
], FunctionComponent.prototype, "treeFunction", void 0);
FunctionComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-function',
        template: __webpack_require__("../../../../../src/app/main/function/function.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/function/function.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_services_data_service__["a" /* DataService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_services_notification_service__["a" /* NotificationService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core_services_utility_service__["a" /* UtilityService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_services_utility_service__["a" /* UtilityService */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__core_services_authen_service__["a" /* AuthenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_services_authen_service__["a" /* AuthenService */]) === "function" && _g || Object])
], FunctionComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=function.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/function/function.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FunctionModule", function() { return FunctionModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__function_component__ = __webpack_require__("../../../../../src/app/main/function/function.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular_tree_component__ = __webpack_require__("../../../../angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var functionRoutes = [
    //localhost:4200/main/user
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    //localhost:4200/main/user/index
    { path: 'index', component: __WEBPACK_IMPORTED_MODULE_2__function_component__["a" /* FunctionComponent */] }
];
var FunctionModule = (function () {
    function FunctionModule() {
    }
    return FunctionModule;
}());
FunctionModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["g" /* RouterModule */].forChild(functionRoutes),
            __WEBPACK_IMPORTED_MODULE_4_angular_tree_component__["b" /* TreeModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["a" /* ModalModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_2__function_component__["a" /* FunctionComponent */]]
    })
], FunctionModule);

//# sourceMappingURL=function.module.js.map

/***/ })

});
//# sourceMappingURL=function.module.chunk.js.map